import {HttpClient} from "@angular/common/http";
import {Injectable} from "@angular/core";
import {environment} from "../environment";

@Injectable()
export class FormService {
    constructor(private httpClient: HttpClient) {
    }

    saveForm(FormData:{ formName:string, formJSON: string}){
        return this.httpClient.post(environment.apiUrl + "saveForm", FormData);
   }
    
   displayFormAPI(){
    return this.httpClient.get(environment.apiUrl+ "getFormAPI"); 
   }
    
}