var express = require('express');
var router = express.Router();
var bcrypt = require('bcryptjs');
var jwt = require('jsonwebtoken');
const Joi = require('joi');
var campaignMaster = require('../model/campaign_master');

var myLogger = require('../middleware.js');
var MyLogModel = require('../model/myLog');
var log = new MyLogModel();





const schema = Joi.object().keys({
    
    Added_By: Joi.string().max(20).required().allow(''),
    Create_Date: Joi.string().allow(''),
   
    Channel_Code: Joi.string().max(20).required().allow(''),
    SubChannel_Code: Joi.string().max(20).required().allow(''),
    Campaign_Code: Joi.string().max(20).required().allow(''),
    Campaign_Name: Joi.string().max(100).required().allow(''),
    Campaign_Desc: Joi.string().max(150).required().allow(''),
    Campaign_Remarks: Joi.string().max(300).required().allow(''),
    Campaign_Start_Date: Joi.string().allow(''),
   Campaign_End_Date: Joi.string().allow(''),
    Campaign_Status: Joi.string().max(2).required().allow(''),

    
    
});

const schema1 = Joi.object().keys({
    
  
    Modified_By: Joi.string().max(20).required().allow(''),
   Modified_Date: Joi.string().allow(''),
    Channel_Code: Joi.string().max(20).required().allow(''),
    SubChannel_Code: Joi.string().max(20).required().allow(''),
    Campaign_Code: Joi.string().max(20).required().allow(''),
    Campaign_Name: Joi.string().max(100).required().allow(''),
    Campaign_Desc: Joi.string().max(150).required().allow(''),
    Campaign_Remarks: Joi.string().max(300).required().allow(''),
    Campaign_Start_Date: Joi.string().allow(''),
   Campaign_End_Date: Joi.string().allow(''),
    Campaign_Status: Joi.string().max(2).required().allow(''),
    Txn_ID : Joi.number().required().allow(''),
    
    
});

router.post('/saveCampaign', function (req, res, next) {
    var campaign_master = new campaignMaster();
    var campaignData = {
        Added_By: req.body.Added_By,
        Create_Date: req.body.Create_Date,
      
        Channel_Code: req.body.Channel_Code,
        SubChannel_Code: req.body.SubChannel_Code,
        Campaign_Code: req.body.Campaign_Code,
        Campaign_Name: req.body.Campaign_Name,
        Campaign_Desc: req.body.Campaign_Desc,
        Campaign_Remarks: req.body.Campaign_Remarks,
        Campaign_Start_Date: req.body.Campaign_Start_Date,
        Campaign_End_Date: req.body.Campaign_End_Date,
        Campaign_Status: req.body.Campaign_Status,


   };

    Joi.validate({
        Added_By: req.body.Added_By,
      Create_Date: req.body.Create_Date,
     
        Channel_Code: req.body.Channel_Code,
        SubChannel_Code: req.body.SubChannel_Code,
        Campaign_Code: req.body.Campaign_Code,
        Campaign_Name: req.body.Campaign_Name,
        Campaign_Desc: req.body.Campaign_Desc,
        Campaign_Remarks: req.body.Campaign_Remarks,
        Campaign_Start_Date: req.body.Campaign_Start_Date,
        Campaign_End_Date: req.body.Campaign_End_Date,
        Campaign_Status: req.body.Campaign_Status,
    }, schema, function (err, schemeCampaignData) {
        if (err) {

            buildResponse(res, 200, {
                error: true,
                message: err["message"]
            });
        }
        else {
            campaign_master.saveCampaign(campaignData)
                .then((data) => {
                   console.log(data,"-----------")
                    if (data["data"] == 0) {
                        buildResponse(res, 200, {
                            error: true,
                            message: data["errMsg"]
                        });
                    } else {
                        buildResponse(res, 200, {
                            error: false,
                            message: "Stored Successfully"
                        });

                    }

                }).catch((errLog) => {
                   
                    log.addLog('route :: department.js ' + errLog);
                    buildResponse(res, 500, {
                        error: true,
                        message: "server error"
                    });
                });
        }
    });
});

router.post('/updateCampaign', function (req, res, next) {
    var campaign_master = new campaignMaster();
    var campaignData = {
        
      
        Modified_By: req.body.Modified_By,
        Modified_Date: req.body.Modified_Date,
        Channel_Code: req.body.Channel_Code,
        SubChannel_Code: req.body.SubChannel_Code,
        Campaign_Code: req.body.Campaign_Code,
        Campaign_Name: req.body.Campaign_Name,
        Campaign_Desc: req.body.Campaign_Desc,
        Campaign_Remarks: req.body.Campaign_Remarks,
        Campaign_Start_Date: req.body.Campaign_Start_Date,
        Campaign_End_Date: req.body.Campaign_End_Date,
        Campaign_Status: req.body.Campaign_Status,
        Txn_ID: req.body.Txn_ID,


   };

    Joi.validate({
       
        Modified_By: req.body.Modified_By,
        Modified_Date: req.body.Modified_Date,
        Channel_Code: req.body.Channel_Code,
        SubChannel_Code: req.body.SubChannel_Code,
        Campaign_Code: req.body.Campaign_Code,
        Campaign_Name: req.body.Campaign_Name,
        Campaign_Desc: req.body.Campaign_Desc,
        Campaign_Remarks: req.body.Campaign_Remarks,
        Campaign_Start_Date: req.body.Campaign_Start_Date,
        Campaign_End_Date: req.body.Campaign_End_Date,
        Campaign_Status: req.body.Campaign_Status,
        Txn_ID: req.body.Txn_ID,
     
    }, schema1, function (err, schemeCampaignData) {
        if (err) {

            buildResponse(res, 200, {
                error: true,
                message: err["message"]
            });
        }
        else {
            campaign_master.updateCampaign(campaignData)
                .then((data) => {
                   console.log(data,"-----------")
                    if (data["data"] == 0) {
                        buildResponse(res, 200, {
                            error: true,
                            message: data["errMsg"]
                        });
                    } else {
                        buildResponse(res, 200, {
                            error: false,
                            message: "Stored Successfully"
                        });

                    }

                }).catch((errLog) => {
                   
                    log.addLog('route :: department.js ' + errLog);
                    buildResponse(res, 500, {
                        error: true,
                        message: "server error"
                    });
                });
        }
    });
});

router.post('/getCampaign', function (req, res, next) {
    var campaign_master = new campaignMaster();

    campaign_master.getCampaign().then((campaignData) => {
        
        if (campaignData["data"] == '0') {
            buildResponse(res, 200, {
                error: false,
                message: campaignData["errMsg"],

            });
        } else {
            buildResponse(res, 200, {
                error: false,
                message: "Successfully display!",
                campaignData: campaignData
            });
        }

    });

});

router.post('/getCampaignById', function (req, res, next) {
    var campaign_master = new campaignMaster();
    var Txn_ID=req.body.Txn_ID;
    campaign_master.getCampaignById(Txn_ID).then((campaignData) => {
        
        if (campaignData["data"] == '0') {
            buildResponse(res, 200, {
                error: false,
                message: campaignData["errMsg"],

            });
        } else {
            buildResponse(res, 200, {
                error: false,
                message: "Successfully display!",
                campaignData: campaignData
            });
        }

    });

});



function buildResponse(res, statusCode, responseData) {
    res.status(statusCode).json(responseData);
}

module.exports = router;