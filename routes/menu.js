var express = require('express');
var router = express.Router();
var bcrypt = require('bcryptjs');
var jwt = require('jsonwebtoken');
const Joi = require('joi');
var menuMaster = require('../model/menu');

var myLogger = require('../middleware.js');
var MyLogModel = require('../model/myLog');
var log = new MyLogModel();





const schema = Joi.object().keys({
    Menu_ID : Joi.number().required().allow(''),
    Added_By: Joi.string().max(20).required().allow(''),
    Create_Date: Joi.string().allow(''),
  
   Menu_Name: Joi.string().max(100).required().allow(''),
   Menu_Display_Name: Joi.string().max(100).required().allow(''),
   Parent_Menu_ID: Joi.number().required().allow(''),
   App_Menu_Available: Joi.string().max(1).required().allow(''),
   Web_Menu_Available: Joi.string().max(1).required().allow(''),
   Default_Access_App: Joi.string().max(1).required().allow(''),
   Default_Access_Web: Joi.string().max(1).required().allow(''),
   App_Menu_Status: Joi.string().max(5).required().allow(''),
   Web_Menu_Status: Joi.string().max(5).required().allow(''),
   RedirectURL: Joi.string().max(500).required().allow(''),
   
    
});


const schema1 = Joi.object().keys({
    
    Menu_ID : Joi.number().required().allow(''),
    Modified_By: Joi.string().max(20).required().allow(''),
   Modified_Date: Joi.string().allow(''),
   Menu_Name: Joi.string().max(100).required().allow(''),
   Menu_Display_Name: Joi.string().max(100).required().allow(''),
   Parent_Menu_ID: Joi.number().required().allow(''),
   App_Menu_Available: Joi.string().max(1).required().allow(''),
   Web_Menu_Available: Joi.string().max(1).required().allow(''),
   Default_Access_App: Joi.string().max(1).required().allow(''),
   Default_Access_Web: Joi.string().max(1).required().allow(''),
   App_Menu_Status: Joi.string().max(5).required().allow(''),
   Web_Menu_Status: Joi.string().max(5).required().allow(''),
   RedirectURL: Joi.string().max(500).required().allow(''),
   Txn_ID : Joi.number().required().allow(''),
    
});

router.post('/saveMenu', function (req, res, next) {
    var menu_master = new menuMaster();
    var menuData = {
        Added_By: req.body.Added_By,
        Create_Date: req.body.Create_Date,
        Menu_ID: req.body.Menu_ID,
        Menu_Name: req.body.Menu_Name,
        Menu_Display_Name: req.body.Menu_Display_Name,
        Parent_Menu_ID: req.body.Parent_Menu_ID,
        App_Menu_Available: req.body.App_Menu_Available,
        Web_Menu_Available: req.body.Web_Menu_Available,
        Default_Access_App: req.body.Default_Access_App,
        Default_Access_Web: req.body.Default_Access_Web,
        App_Menu_Status: req.body.App_Menu_Status,
        Web_Menu_Status: req.body.Web_Menu_Status,
        RedirectURL: req.body.RedirectURL,


   };
console.log("req.body.Menu_ID",req.body.Menu_ID)
    Joi.validate({
        Added_By: req.body.Added_By,
        Create_Date: req.body.Create_Date,
        Menu_ID: req.body.Menu_ID,
        Menu_Name: req.body.Menu_Name,
        Menu_Display_Name: req.body.Menu_Display_Name,
        Parent_Menu_ID: req.body.Parent_Menu_ID,
        App_Menu_Available: req.body.App_Menu_Available,
        Web_Menu_Available: req.body.Web_Menu_Available,
        Default_Access_App: req.body.Default_Access_App,
        Default_Access_Web: req.body.Default_Access_Web,
        App_Menu_Status: req.body.App_Menu_Status,
        Web_Menu_Status: req.body.Web_Menu_Status,
        RedirectURL: req.body.RedirectURL,
    }, schema, function (err, schemeMenuData) {
        if (err) {
console.log(err)
            buildResponse(res, 200, {
                error: true,
                message: err["message"]
            });
        }
        else {
            menu_master.saveMenu(menuData)
                .then((data) => {
                   console.log(data,"-----------")
                    if (data["data"] == 0) {
                        buildResponse(res, 200, {
                            error: true,
                            message: data["errMsg"]
                        });
                    } else {
                        buildResponse(res, 200, {
                            error: false,
                            message: "Stored Successfully"
                        });

                    }

                }).catch((errLog) => {
                   
                    log.addLog('route :: menu.js ' + errLog);
                    buildResponse(res, 500, {
                        error: true,
                        message: "server error"
                    });
                });
        }
    });
});

router.post('/updateMenu', function (req, res, next) {
    var menu_master = new menuMaster();
   
    var menuData = {
        Menu_ID: req.body.Menu_ID,
        Modified_By: req.body.Modified_By,
        Modified_Date: req.body.Modified_Date,
        Menu_Name: req.body.Menu_Name,
        Menu_Display_Name: req.body.Menu_Display_Name,
        Parent_Menu_ID: req.body.Parent_Menu_ID,
        App_Menu_Available: req.body.App_Menu_Available,
        Web_Menu_Available: req.body.Web_Menu_Available,
        Default_Access_App: req.body.Default_Access_App,
        Default_Access_Web: req.body.Default_Access_Web,
        App_Menu_Status: req.body.App_Menu_Status,
        Web_Menu_Status: req.body.Web_Menu_Status,
        RedirectURL: req.body.RedirectURL,
        Txn_ID: req.body.Txn_ID,


   };

    Joi.validate({
        Menu_ID: req.body.Menu_ID,
        Modified_By: req.body.Modified_By,
        Modified_Date: req.body.Modified_Date,
        Menu_Name: req.body.Menu_Name,
        Menu_Display_Name: req.body.Menu_Display_Name,
        Parent_Menu_ID: req.body.Parent_Menu_ID,
        App_Menu_Available: req.body.App_Menu_Available,
        Web_Menu_Available: req.body.Web_Menu_Available,
        Default_Access_App: req.body.Default_Access_App,
        Default_Access_Web: req.body.Default_Access_Web,
        App_Menu_Status: req.body.App_Menu_Status,
        Web_Menu_Status: req.body.Web_Menu_Status,
        RedirectURL: req.body.RedirectURL,
        Txn_ID: req.body.Txn_ID,

     
    }, schema1, function (err, schemeMenuData) {
        if (err) {

            buildResponse(res, 200, {
                error: true,
                message: err["message"]
            });
        }
        else {
            menu_master.updateMenu(menuData)
                .then((data) => {
                   console.log(data,"-----------")
                    if (data["data"] == 0) {
                        buildResponse(res, 200, {
                            error: true,
                            message: data["errMsg"]
                        });
                    } else {
                        buildResponse(res, 200, {
                            error: false,
                            message: "Stored Successfully"
                        });

                    }

                }).catch((errLog) => {
                   
                    log.addLog('route :: menu.js ' + errLog);
                    buildResponse(res, 500, {
                        error: true,
                        message: "server error"
                    });
                });
        }
    });
});

router.post('/getMenu', function (req, res, next) {
    var menu_master = new menuMaster();

    menu_master.getMenu().then((menuData) => {
        
        if (menuData["data"] == '0') {
            buildResponse(res, 200, {
                error: false,
                message: menuData["errMsg"],

            });
        } else {
            buildResponse(res, 200, {
                error: false,
                message: "Successfully display!",
                menuData: menuData
            });
        }

    });

});

router.post('/getMenuById', function (req, res, next) {
    var menu_master = new menuMaster();
    var Txn_ID=req.body.Txn_ID;
    menu_master.getMenuById(Txn_ID).then((menuData) => {
        console.log(menuData)
        if (menuData["data"] == '0') {
            buildResponse(res, 200, {
                error: false,
                message: menuData["errMsg"],

            });
        } else {
            buildResponse(res, 200, {
                error: false,
                message: "Successfully display!",
                menuData: menuData
            });
        }

    });

});



function buildResponse(res, statusCode, responseData) {
    res.status(statusCode).json(responseData);
}

module.exports = router;