const Sequelize = require('sequelize');
var con = require('./connection');
const Op = Sequelize.Op;
var MyLogModel = require('../model/myLog');
var log = new MyLogModel();
Sequelize.DATE.prototype._stringify = function _stringify(date, options) {
    return this._applyTimezone(date, options).format('YYYY-MM-DD HH:mm:ss.SSS');
};
module.exports = class ScheudleMeeting {
    constructor() {
        this.ScheudleMeeting = con.define('TXN_Schedule_Meetings', {

            T_INT_Schedule_Meeting_Id: {
                type: Sequelize.INTEGER,
                primaryKey: true,
                autoIncrement: true
            },
            T_INT_Meeting_Id: {
                type: Sequelize.INTEGER,
                allowNull: false,

            },
            T_INT_Meeting_Template_Id: {
                type: Sequelize.INTEGER,
                allowNull: false,

            },
            T_CHR_For_Month_Or_Quoarter: {
                type: Sequelize.STRING(20),
                allowNull: false,

            },
            T_DATE_Meeting_Date: {
                type: Sequelize.DATE,
                allowNull: false,


            },
            T_CHR_Meeting_Start_Time: {
                type: Sequelize.DATE,
                allowNull: false
            },
            T_CHR_Meeting_End_Time: {
                type: Sequelize.DATE,
                allowNull: false
            },
            T_CHR_Location: {
                type: Sequelize.STRING(50),
                allowNull: false
            },
            T_CHR_Invitees: {
                type: Sequelize.STRING(50),
                allowNull: false
            },
            T_CHR_Description: {
                type: Sequelize.STRING(255),
                allowNull: false
            },
            T_CHR_In_Progress: {
                type: Sequelize.STRING(2),
                allowNull: false,
                defaultValue: '0'
            },
            T_CHR_Is_Publish: {
                type: Sequelize.STRING(2),
                allowNull: false,
                defaultValue: '0'
            },
            T_CHR_Publish_By: {
                type: Sequelize.STRING(20),
                allowNull: true,

            },
            T_Date_Published_At: {
                type: Sequelize.DATE,
                allowNull: true,
            },
            T_CHR_Completed_Flag: {
                type: Sequelize.STRING(2),
                allowNull: false,
                defaultValue: '0'
            },
            T_CHR_Delay_Flag: {
                type: Sequelize.STRING(2),
                allowNull: false,
                defaultValue: '0'
            },
            T_CHR_Isdelete: {
                type: Sequelize.STRING(20),
                allowNull: true,
                defaultValue: '0'
            },

            T_CHR_Created_By: {
                type: Sequelize.STRING(20),
                allowNull: true
            },
            T_DATE_Created_Date_Time: {
                type: Sequelize.DATE,
                allowNull: true
            },
            T_CHR_Updated_By: {
                type: Sequelize.STRING(20),
                allowNull: true
            },
            T_DATE_Updated_Date_Time: {
                type: Sequelize.DATE,
                allowNull: true
            },
            T_CHR_deleted_By: {
                type: Sequelize.STRING(20),
                allowNull: true
            },

            T_DATE_Deleted_Date_Time: {
                type: Sequelize.DATE,
                allowNull: true
            },
            database_connection: {
                type: Sequelize.STRING,
                allowNull: true
            },

        },
            {
                timestamps: false,
                createdAt: false,
                UpdatedAt: false,
            });

        this.logNotScheudleMeeting = con.define('Log_Not_Schedule_Meeting', {

            T_INT_Not_Schedule_Meeting_Id: {
                type: Sequelize.INTEGER,
                primaryKey: true,
                autoIncrement: true
            },
            T_INT_Meeting_Id: {
                type: Sequelize.INTEGER,
                allowNull: false,

            },
            T_CHR_Meeting_Type: {
                type: Sequelize.STRING(255),
                allowNull: false,

            },
            T_CHR_Meeting_Short_Name: {
                type: Sequelize.STRING(255),
                allowNull: false,

            },
            T_CHR_Meeting_Full_Name: {
                type: Sequelize.STRING(255),
                allowNull: false,

            },
            T_CHR_Convener_Name: {
                type: Sequelize.STRING(255),
                allowNull: false,

            },
            T_INT_Convener_Id: {
                type: Sequelize.STRING(20),
                allowNull: false,

            },
            T_CHR_Frquency: {
                type: Sequelize.STRING(255),
                allowNull: false,


            },
            T_CHR_To_Check: {
                type: Sequelize.STRING(50),
                allowNull: false,
                unique: true
            },
            T_CHR_Year: {
                type: Sequelize.STRING(50),
                allowNull: false,
            },
            T_CHR_Meeting_Chair: {
                type: Sequelize.STRING(50),
                allowNull: false,
            },
            T_CHR_Meeting_Attendies: {
                type: Sequelize.STRING(50),
                allowNull: false,
            },

            T_CHR_Channel_Deprtment: {
                type: Sequelize.STRING(50),
                allowNull: false,
            },

        },
            {
                timestamps: false,
                createdAt: false,
                UpdatedAt: false,
            });
    }


    getNotSchuledMeetingCount(data) {

        var d = new Date();

        if (data.role == '2') {
            if (data.type == 'meetings') {
                return sequelize.sync().then(() => {

                    return sequelize.query("select mm.T_INT_Meeting_Id,mt.T_CHR_Type_Of_Meeting_Name,mm.T_CHR_Short_Name,mm.T_CHR_Full_Name" +
                        ",mm.T_INT_Frequency,mm.T_DATE_Meeting_From_Date,mm.T_DATE_Meeting_To_Date,mf.T_CHR_Frequency_Name,u.T_CHR_Name  from M_Meeting_Masters as mm" +
                        " inner join M_Type_Of_Meeting_Master as mt on mm.T_INT_Type_Of_Meeting=mt.T_INT_Type_Of_Meeting_Id" +
                        " inner join M_Frequency_Masters as mf on mm.T_INT_Frequency=mf.T_INT_Frequency_Id " +
                        " inner join M_User_Masters as u on u.T_CHR_Employee_Id=mm.T_INT_Convener_Id" +
                        " where mm.T_CHR_Isdelete='0' and mm.T_CHR_Status='Enable' and mm.T_DATE_Meeting_From_Date <= '" + d.toLocaleDateString("en-US") + "' and  mm.T_DATE_Meeting_To_Date  >= '" + d.toLocaleDateString("en-US") + "' and mm.T_INT_Convener_Id='" + data.userId + "' ", { type: sequelize.QueryTypes.SELECT }
                    ).then((data1) => {

                        return data1;

                    }).catch((err) => {
                        log.addLog('route :: Meeting.js ' + err);
                    });

                });
            } else {
                return sequelize.sync().then(() => {

                    return sequelize.query("select mm.T_INT_Meeting_Id,mt.T_CHR_Type_Of_Meeting_Name,mm.T_CHR_Short_Name,mm.T_CHR_Full_Name" +
                        ",mm.T_INT_Frequency,mm.T_DATE_Meeting_From_Date,mm.T_DATE_Meeting_To_Date,mf.T_CHR_Frequency_Name,u.T_CHR_Name,mm.T_CHR_Meeting_Chair,mm.T_CHR_Meeting_Attendies  from M_Meeting_Masters as mm" +
                        " inner join M_Type_Of_Meeting_Master as mt on mm.T_INT_Type_Of_Meeting=mt.T_INT_Type_Of_Meeting_Id" +
                        " inner join M_Frequency_Masters as mf on mm.T_INT_Frequency=mf.T_INT_Frequency_Id " +
                        " inner join M_User_Masters as u on u.T_CHR_Employee_Id=mm.T_INT_Convener_Id" +
                        " where mm.T_CHR_Isdelete='0' and mm.T_CHR_Status='Enable' and mm.T_DATE_Meeting_From_Date <= '" + d.toLocaleDateString("en-US") + "' and  mm.T_DATE_Meeting_To_Date  >= '" + d.toLocaleDateString("en-US") + "'", { type: sequelize.QueryTypes.SELECT }
                    ).then((data1) => {


                        data1.forEach((ele, ind) => {



                            if (ele.T_CHR_Meeting_Chair.search(data.userId) == -1 && ele.T_CHR_Meeting_Attendies.search(data.userId) == -1) {

                                data1.splice(ind, 1);
                            } else {


                            }

                        })

                        return data1;

                    }).catch((err) => {
                        log.addLog('route :: Meeting.js ' + err);
                    });

                });
            }
        } else if (data.role == '3') {
            if (data.type == 'kli') {
                return sequelize.sync().then(() => {

                    return sequelize.query("select mm.T_INT_Meeting_Id,mt.T_CHR_Type_Of_Meeting_Name,mm.T_CHR_Short_Name,mm.T_CHR_Full_Name" +
                        ",mm.T_INT_Frequency,mm.T_DATE_Meeting_From_Date,mm.T_DATE_Meeting_To_Date,mf.T_CHR_Frequency_Name,u.T_CHR_Name  from M_Meeting_Masters as mm" +
                        " inner join M_Type_Of_Meeting_Master as mt on mm.T_INT_Type_Of_Meeting=mt.T_INT_Type_Of_Meeting_Id" +
                        " inner join M_Frequency_Masters as mf on mm.T_INT_Frequency=mf.T_INT_Frequency_Id " +
                        " inner join M_User_Masters as u on u.T_CHR_Employee_Id=mm.T_INT_Convener_Id" +
                        " where mm.T_CHR_Isdelete='0' and mm.T_CHR_Status='Enable' and mm.T_DATE_Meeting_From_Date <= '" + d.toLocaleDateString("en-US") + "' and  mm.T_DATE_Meeting_To_Date  >= '" + d.toLocaleDateString("en-US") + "' ", { type: sequelize.QueryTypes.SELECT }
                    ).then((data1) => {

                        return data1;

                    }).catch((err) => {
                        log.addLog('route :: Meeting.js ' + err);
                    });

                });
            } else {
                return sequelize.sync().then(() => {

                    return sequelize.query("select mm.T_INT_Meeting_Id,mt.T_CHR_Type_Of_Meeting_Name,mm.T_CHR_Short_Name,mm.T_CHR_Full_Name" +
                        ",mm.T_INT_Frequency,mm.T_DATE_Meeting_From_Date,mm.T_DATE_Meeting_To_Date,mf.T_CHR_Frequency_Name,u.T_CHR_Name,mm.T_CHR_Meeting_Chair,mm.T_CHR_Meeting_Attendies  from M_Meeting_Masters as mm" +
                        " inner join M_Type_Of_Meeting_Master as mt on mm.T_INT_Type_Of_Meeting=mt.T_INT_Type_Of_Meeting_Id" +
                        " inner join M_Frequency_Masters as mf on mm.T_INT_Frequency=mf.T_INT_Frequency_Id " +
                        " inner join M_User_Masters as u on u.T_CHR_Employee_Id=mm.T_INT_Convener_Id" +
                        " where mm.T_CHR_Isdelete='0' and mm.T_CHR_Status='Enable' and mm.T_DATE_Meeting_From_Date <= '" + d.toLocaleDateString("en-US") + "' and  mm.T_DATE_Meeting_To_Date  >= '" + d.toLocaleDateString("en-US") + "'", { type: sequelize.QueryTypes.SELECT }
                    ).then((data1) => {


                        data1.forEach((ele, ind) => {



                            if (ele.T_CHR_Meeting_Chair.search(data.userId) == -1 && ele.T_CHR_Meeting_Attendies.search(data.userId) == -1) {

                                data1.splice(ind, 1);
                            } else {


                            }

                        })

                        return data1;

                    }).catch((err) => {
                        log.addLog('route :: Meeting.js ' + err);
                    });

                });
            }



        }

    }
    getNotSchuledMeeting(data, offsetRow, fetchRow) {

        var d = new Date();
        console.log("ddddddddd=", data)
        var condition = "";

        /*  if (data.month) {
            condition = " and MONTH(mm.T_DATE_Created_Date_Time) in (" + data.month + ") ";
            
        } 
         if (data.year) {
            condition = condition+" and YEAR(mm.T_DATE_Created_Date_Time) in (" + data.year + ") ";
            
        }   */

        if (data.meetingName) {
            condition = condition + " and mm.T_INT_Meeting_Id in (" + data.meetingName + ") ";

        }

        if (data.department) {

            condition = condition + "and (mm.T_CHR_Channel_Deprtment='" + data.department + "' " +
                " OR mm.T_CHR_Channel_Deprtment LIKE '" + data.department + "' + ',%' " +
                " OR mm.T_CHR_Channel_Deprtment LIKE '%,' + '" + data.department + "' + ',%' " +
                "  OR mm.T_CHR_Channel_Deprtment LIKE '%,' + '" + data.department + "') ";

        }
        if (data.searchText) {
            condition = condition + " and  (mm.T_CHR_Short_Name LIKE '%" + data.searchText + "%' or mm.T_CHR_FUll_Name LIKE '%" + data.searchText + "%' or u.T_CHR_Name LIKE '%" + data.searchText + "%' )";
        }

        if (data.role == '2') {
            if (data.type == 'meetings') {
                return sequelize.sync().then(() => {

                    return sequelize.query("select mm.T_INT_Meeting_Id,mt.T_CHR_Type_Of_Meeting_Name,mm.T_CHR_Short_Name,mm.T_CHR_Full_Name" +
                        ",mm.T_INT_Frequency,mm.T_DATE_Meeting_From_Date,mm.T_DATE_Meeting_To_Date,mf.T_CHR_Frequency_Name,u.T_CHR_Name  from M_Meeting_Masters as mm" +
                        " inner join M_Type_Of_Meeting_Master as mt on mm.T_INT_Type_Of_Meeting=mt.T_INT_Type_Of_Meeting_Id" +
                        " inner join M_Frequency_Masters as mf on mm.T_INT_Frequency=mf.T_INT_Frequency_Id " +
                        " inner join M_User_Masters as u on u.T_CHR_Employee_Id=mm.T_INT_Convener_Id" +
                        " where mm.T_CHR_Isdelete='0' and mm.T_CHR_Status='Enable' and mm.T_DATE_Meeting_From_Date <= '" + d.toLocaleDateString("en-US") + "' and  mm.T_DATE_Meeting_To_Date  >= '" + d.toLocaleDateString("en-US") + "' and mm.T_INT_Convener_Id='" + data.userId + "'"
                        + condition + " ORDER BY " +
                        "mm.T_INT_Meeting_Id " +

                        " OFFSET " + offsetRow + " ROWS " +
                        " FETCH NEXT " + fetchRow + " ROWS ONLY ", { type: sequelize.QueryTypes.SELECT }
                    ).then((data1) => {

                        return data1;

                    }).catch((err) => {
                        log.addLog('route :: Meeting.js ' + err);
                    });

                });
            } else {
                return sequelize.sync().then(() => {

                    return sequelize.query("select mm.T_INT_Meeting_Id,mt.T_CHR_Type_Of_Meeting_Name,mm.T_CHR_Short_Name,mm.T_CHR_Full_Name" +
                        ",mm.T_INT_Frequency,mm.T_DATE_Meeting_From_Date,mm.T_DATE_Meeting_To_Date,mf.T_CHR_Frequency_Name,u.T_CHR_Name,mm.T_CHR_Meeting_Chair,mm.T_CHR_Meeting_Attendies  from M_Meeting_Masters as mm" +
                        " inner join M_Type_Of_Meeting_Master as mt on mm.T_INT_Type_Of_Meeting=mt.T_INT_Type_Of_Meeting_Id" +
                        " inner join M_Frequency_Masters as mf on mm.T_INT_Frequency=mf.T_INT_Frequency_Id " +
                        " inner join M_User_Masters as u on u.T_CHR_Employee_Id=mm.T_INT_Convener_Id" +
                        " where mm.T_CHR_Isdelete='0' and mm.T_CHR_Status='Enable' and mm.T_DATE_Meeting_From_Date <= '" + d.toLocaleDateString("en-US") + "' and  mm.T_DATE_Meeting_To_Date  >= '" + d.toLocaleDateString("en-US") + "' " + condition + " and ((mm.T_CHR_Meeting_Chair='" + data.userId + "'"

                        + " OR mm.T_CHR_Meeting_Chair LIKE '" + data.userId + "' + ',%' " +
                        " OR mm.T_CHR_Meeting_Chair LIKE '%,' + '" + data.userId + "' + ',%' " +
                        "  OR mm.T_CHR_Meeting_Chair LIKE '%,' + '" + data.userId + "') or (mm.T_CHR_Meeting_Attendies='" + data.userId + "' " +

                        "  OR mm.T_CHR_Meeting_Attendies LIKE '" + data.userId + "' + ',%' " +
                        " OR mm.T_CHR_Meeting_Attendies LIKE '%,' + '" + data.userId + "' + ',%' " +
                        " OR mm.T_CHR_Meeting_Attendies LIKE '%,' + '" + data.userId + "')) " +
                        "ORDER BY " +
                        "mm.T_DATE_Created_Date_Time desc" +

                        " OFFSET " + offsetRow + " ROWS " +
                        " FETCH NEXT " + fetchRow + " ROWS ONLY ", { type: sequelize.QueryTypes.SELECT }
                    ).then((data1) => {


                        data1.forEach((ele, ind) => {



                            if (ele.T_CHR_Meeting_Chair.search(data.userId) == -1 && ele.T_CHR_Meeting_Attendies.search(data.userId) == -1) {

                                data1.splice(ind, 1);
                            } else {


                            }

                        })

                        return data1;

                    }).catch((err) => {
                        log.addLog('route :: Meeting.js ' + err);
                    });

                });
            }
        } else if (data.role == '3') {
            if (data.type == 'kli') {
                return sequelize.sync().then(() => {


                    console.log("999999999999999=", condition)
                    return sequelize.query("select mm.T_INT_Meeting_Id,mt.T_CHR_Type_Of_Meeting_Name,mm.T_CHR_Short_Name,mm.T_CHR_Full_Name" +
                        ",mm.T_INT_Frequency,mm.T_DATE_Meeting_From_Date,mm.T_DATE_Meeting_To_Date,mf.T_CHR_Frequency_Name,u.T_CHR_Name  from M_Meeting_Masters as mm" +
                        " inner join M_Type_Of_Meeting_Master as mt on mm.T_INT_Type_Of_Meeting=mt.T_INT_Type_Of_Meeting_Id" +
                        " inner join M_Frequency_Masters as mf on mm.T_INT_Frequency=mf.T_INT_Frequency_Id " +
                        " inner join M_User_Masters as u on u.T_CHR_Employee_Id=mm.T_INT_Convener_Id" +
                        " where mm.T_CHR_Isdelete='0' and mm.T_CHR_Status='Enable' and mm.T_DATE_Meeting_From_Date <= '" + d.toLocaleDateString("en-US") + "' and  mm.T_DATE_Meeting_To_Date  >= '" + d.toLocaleDateString("en-US") + "'  "
                        + condition + " ORDER BY " +
                        "mm.T_INT_Meeting_Id " +

                        " OFFSET " + offsetRow + " ROWS " +
                        " FETCH NEXT " + fetchRow + " ROWS ONLY ", { type: sequelize.QueryTypes.SELECT }
                    ).then((data1) => {

                        return data1;

                    }).catch((err) => {
                        console.log(err)
                        log.addLog('route :: Meeting.js ' + err);
                    });

                });
            } else {
                return sequelize.sync().then(() => {

                    return sequelize.query("select mm.T_INT_Meeting_Id,mt.T_CHR_Type_Of_Meeting_Name,mm.T_CHR_Short_Name,mm.T_CHR_Full_Name" +
                        ",mm.T_INT_Frequency,mm.T_DATE_Meeting_From_Date,mm.T_DATE_Meeting_To_Date,mf.T_CHR_Frequency_Name,u.T_CHR_Name,mm.T_CHR_Meeting_Chair,mm.T_CHR_Meeting_Attendies  from M_Meeting_Masters as mm" +
                        " inner join M_Type_Of_Meeting_Master as mt on mm.T_INT_Type_Of_Meeting=mt.T_INT_Type_Of_Meeting_Id" +
                        " inner join M_Frequency_Masters as mf on mm.T_INT_Frequency=mf.T_INT_Frequency_Id " +
                        " inner join M_User_Masters as u on u.T_CHR_Employee_Id=mm.T_INT_Convener_Id" +
                        " where mm.T_CHR_Isdelete='0' and mm.T_CHR_Status='Enable' and mm.T_DATE_Meeting_From_Date <= '" + d.toLocaleDateString("en-US") + "' and  mm.T_DATE_Meeting_To_Date  >= '" + d.toLocaleDateString("en-US") + "' " + condition + " and ((mm.T_CHR_Meeting_Chair='" + data.userId + "'"

                        + " OR mm.T_CHR_Meeting_Chair LIKE '" + data.userId + "' + ',%' " +
                        " OR mm.T_CHR_Meeting_Chair LIKE '%,' + '" + data.userId + "' + ',%' " +
                        "  OR mm.T_CHR_Meeting_Chair LIKE '%,' + '" + data.userId + "') or (mm.T_CHR_Meeting_Attendies='" + data.userId + "' " +

                        "  OR mm.T_CHR_Meeting_Attendies LIKE '" + data.userId + "' + ',%' " +
                        " OR mm.T_CHR_Meeting_Attendies LIKE '%,' + '" + data.userId + "' + ',%' " +
                        " OR mm.T_CHR_Meeting_Attendies LIKE '%,' + '" + data.userId + "')) " +
                        "ORDER BY " +
                        "mm.T_DATE_Created_Date_Time desc" +

                        " OFFSET " + offsetRow + " ROWS " +
                        " FETCH NEXT " + fetchRow + " ROWS ONLY ", { type: sequelize.QueryTypes.SELECT }
                    ).then((data1) => {


                        data1.forEach((ele, ind) => {



                            if (ele.T_CHR_Meeting_Chair.search(data.userId) == -1 && ele.T_CHR_Meeting_Attendies.search(data.userId) == -1) {

                                data1.splice(ind, 1);
                            } else {


                            }

                        })

                        return data1;

                    }).catch((err) => {
                        log.addLog('route :: Meeting.js ' + err);
                    });

                });
            }


        }

    }

    getScheduledMeeting(data, offsetRow, fetchRow) {

        var d = new Date();
        var condition = "";

        if (data.month) {
            condition = " and MONTH(tm.T_DATE_Meeting_Date) in (" + data.month + ") ";

        }
        if (data.year) {
            condition = condition + " and YEAR(tm.T_DATE_Meeting_Date) in (" + data.year + ") ";

        }
        if (data.meetingName) {
            condition = condition + " and mm.T_INT_Meeting_Id in (" + data.meetingName + ") ";

        }

        if (data.department) {

            condition = condition + "and (mm.T_CHR_Channel_Deprtment='" + data.department + "' " +
                " OR mm.T_CHR_Channel_Deprtment LIKE '" + data.department + "' + ',%' " +
                " OR mm.T_CHR_Channel_Deprtment LIKE '%,' + '" + data.department + "' + ',%' " +
                "  OR mm.T_CHR_Channel_Deprtment LIKE '%,' + '" + data.department + "') ";

        }

        if (data.searchText) {
            condition = condition + " and  (mm.T_CHR_Short_Name LIKE '%" + data.searchText + "%' or mm.T_CHR_FUll_Name LIKE '%" + data.searchText + "%' or u.T_CHR_Name LIKE '%" + data.searchText + "%' )";
        }

        if (data.role == '2') {
            if (data.type == 'meetings') {
                return sequelize.sync().then(() => {

                    return sequelize.sync().then(() => {

                        return sequelize.query("select tm.T_CHR_In_Progress,tm.T_INT_Schedule_Meeting_Id,tm.T_INT_Meeting_Template_Id, mm.T_INT_Meeting_Id,mt.T_CHR_Type_Of_Meeting_Name,mm.T_CHR_Short_Name,mm.T_CHR_Full_Name " +
                            " ,mm.T_INT_Frequency,mm.T_DATE_Meeting_From_Date,mm.T_DATE_Meeting_To_Date,mf.T_CHR_Frequency_Name, " +
                            " u.T_CHR_Name  from M_Meeting_Masters as mm " +
                            " inner join M_Type_Of_Meeting_Master as mt on mm.T_INT_Type_Of_Meeting=mt.T_INT_Type_Of_Meeting_Id " +
                            " inner join M_Frequency_Masters as mf on mm.T_INT_Frequency=mf.T_INT_Frequency_Id  " +
                            " inner join TXN_Schedule_Meetings as tm on tm.T_INT_Meeting_Id=mm.T_INT_Meeting_Id " +
                            " inner join M_User_Masters as u on u.T_CHR_Employee_Id=tm.T_CHR_Created_By " +
                            " where mm.T_CHR_Isdelete='0' and tm.T_CHR_Delay_Flag='0' and tm.T_CHR_Completed_Flag='0' and mm.T_INT_Convener_Id='" + data.userId + "' " + condition + " " +
                            "ORDER BY " +
                            "tm.T_DATE_Created_Date_Time desc" +

                            " OFFSET " + offsetRow + " ROWS " +
                            " FETCH NEXT " + fetchRow + " ROWS ONLY ", { type: sequelize.QueryTypes.SELECT }
                        ).then((data1) => {
                            return data1;

                        }).catch((err) => {
                            log.addLog('route :: Meeting.js ' + err);
                        });

                    });

                });
            } else {
                return sequelize.sync().then(() => {

                    return sequelize.query("select tm.T_CHR_In_Progress,tm.T_INT_Schedule_Meeting_Id,tm.T_INT_Meeting_Template_Id,mm.T_INT_Meeting_Id,mt.T_CHR_Type_Of_Meeting_Name,mm.T_CHR_Short_Name,mm.T_CHR_Full_Name " +
                        " ,mm.T_INT_Frequency,mm.T_DATE_Meeting_From_Date,mm.T_DATE_Meeting_To_Date,mf.T_CHR_Frequency_Name, " +
                        " u.T_CHR_Name  from M_Meeting_Masters as mm " +
                        " inner join M_Type_Of_Meeting_Master as mt on mm.T_INT_Type_Of_Meeting=mt.T_INT_Type_Of_Meeting_Id " +
                        " inner join M_Frequency_Masters as mf on mm.T_INT_Frequency=mf.T_INT_Frequency_Id  " +
                        " inner join TXN_Schedule_Meetings as tm on tm.T_INT_Meeting_Id=mm.T_INT_Meeting_Id " +
                        " inner join M_User_Masters as u on u.T_CHR_Employee_Id=tm.T_CHR_Created_By " +
                        " where mm.T_CHR_Isdelete='0' and tm.T_CHR_Delay_Flag='0' and tm.T_CHR_Completed_Flag='0'   " + condition + " and ((mm.T_CHR_Meeting_Chair='" + data.userId + "'" +

                        " OR mm.T_CHR_Meeting_Chair LIKE '" + data.userId + "' + ',%' " +
                        " OR mm.T_CHR_Meeting_Chair LIKE '%,' + '" + data.userId + "' + ',%' " +
                        "  OR mm.T_CHR_Meeting_Chair LIKE '%,' + '" + data.userId + "') or (mm.T_CHR_Meeting_Attendies='" + data.userId + "' " +

                        "  OR mm.T_CHR_Meeting_Attendies LIKE '" + data.userId + "' + ',%' " +
                        " OR mm.T_CHR_Meeting_Attendies LIKE '%,' + '" + data.userId + "' + ',%' " +
                        " OR mm.T_CHR_Meeting_Attendies LIKE '%,' + '" + data.userId + "')) " +
                        "ORDER BY " +
                        "tm.T_DATE_Created_Date_Time desc" +

                        " OFFSET " + offsetRow + " ROWS " +
                        " FETCH NEXT " + fetchRow + " ROWS ONLY ", { type: sequelize.QueryTypes.SELECT }
                    ).then((data1) => {
                        return data1;

                    }).catch((err) => {
                        log.addLog('route :: Meeting.js ' + err);
                    });

                });
            }
        } else if (data.role == '3') {
            if (data.type == 'kli') {
                return sequelize.sync().then(() => {

                    return sequelize.sync().then(() => {

                        return sequelize.query("select tm.T_CHR_In_Progress,tm.T_INT_Schedule_Meeting_Id,tm.T_INT_Meeting_Template_Id, mm.T_INT_Meeting_Id,mt.T_CHR_Type_Of_Meeting_Name,mm.T_CHR_Short_Name,mm.T_CHR_Full_Name " +
                            " ,mm.T_INT_Frequency,mm.T_DATE_Meeting_From_Date,mm.T_DATE_Meeting_To_Date,mf.T_CHR_Frequency_Name, " +
                            " u.T_CHR_Name  from M_Meeting_Masters as mm " +
                            " inner join M_Type_Of_Meeting_Master as mt on mm.T_INT_Type_Of_Meeting=mt.T_INT_Type_Of_Meeting_Id " +
                            " inner join M_Frequency_Masters as mf on mm.T_INT_Frequency=mf.T_INT_Frequency_Id  " +
                            " inner join TXN_Schedule_Meetings as tm on tm.T_INT_Meeting_Id=mm.T_INT_Meeting_Id " +
                            " inner join M_User_Masters as u on u.T_CHR_Employee_Id=tm.T_CHR_Created_By " +
                            " where mm.T_CHR_Isdelete='0' and tm.T_CHR_Delay_Flag='0' and tm.T_CHR_Completed_Flag='0'  " + condition + " " +
                            "ORDER BY " +
                            "tm.T_DATE_Created_Date_Time desc" +

                            " OFFSET " + offsetRow + " ROWS " +
                            " FETCH NEXT " + fetchRow + " ROWS ONLY ", { type: sequelize.QueryTypes.SELECT }
                        ).then((data1) => {
                            return data1;

                        }).catch((err) => {
                            log.addLog('route :: Meeting.js ' + err);
                        });

                    });

                });
            }
            else {
                return sequelize.sync().then(() => {

                    return sequelize.query("select tm.T_CHR_In_Progress,tm.T_INT_Schedule_Meeting_Id,tm.T_INT_Meeting_Template_Id,mm.T_INT_Meeting_Id,mt.T_CHR_Type_Of_Meeting_Name,mm.T_CHR_Short_Name,mm.T_CHR_Full_Name " +
                        " ,mm.T_INT_Frequency,mm.T_DATE_Meeting_From_Date,mm.T_DATE_Meeting_To_Date,mf.T_CHR_Frequency_Name, " +
                        " u.T_CHR_Name  from M_Meeting_Masters as mm " +
                        " inner join M_Type_Of_Meeting_Master as mt on mm.T_INT_Type_Of_Meeting=mt.T_INT_Type_Of_Meeting_Id " +
                        " inner join M_Frequency_Masters as mf on mm.T_INT_Frequency=mf.T_INT_Frequency_Id  " +
                        " inner join TXN_Schedule_Meetings as tm on tm.T_INT_Meeting_Id=mm.T_INT_Meeting_Id " +
                        " inner join M_User_Masters as u on u.T_CHR_Employee_Id=tm.T_CHR_Created_By " +
                        " where mm.T_CHR_Isdelete='0' and tm.T_CHR_Delay_Flag='0' and tm.T_CHR_Completed_Flag='0'   " + condition + " and ((mm.T_CHR_Meeting_Chair='" + data.userId + "'" +

                        " OR mm.T_CHR_Meeting_Chair LIKE '" + data.userId + "' + ',%' " +
                        " OR mm.T_CHR_Meeting_Chair LIKE '%,' + '" + data.userId + "' + ',%' " +
                        "  OR mm.T_CHR_Meeting_Chair LIKE '%,' + '" + data.userId + "') or (mm.T_CHR_Meeting_Attendies='" + data.userId + "' " +

                        "  OR mm.T_CHR_Meeting_Attendies LIKE '" + data.userId + "' + ',%' " +
                        " OR mm.T_CHR_Meeting_Attendies LIKE '%,' + '" + data.userId + "' + ',%' " +
                        " OR mm.T_CHR_Meeting_Attendies LIKE '%,' + '" + data.userId + "')) " +
                        "ORDER BY " +
                        "tm.T_DATE_Created_Date_Time desc" +

                        " OFFSET " + offsetRow + " ROWS " +
                        " FETCH NEXT " + fetchRow + " ROWS ONLY ", { type: sequelize.QueryTypes.SELECT }
                    ).then((data1) => {
                        return data1;

                    }).catch((err) => {
                        log.addLog('route :: Meeting.js ' + err);
                    });

                });
            }
        }

    }

    getScheduledMeetingExportToExcel(data, offsetRow, fetchRow) {

        var d = new Date();
        var condition = "";

        if (data.month) {
            condition = " and MONTH(tm.T_DATE_Meeting_Date) in (" + data.month + ") ";

        }
        if (data.year) {
            condition = condition + " and YEAR(tm.T_DATE_Meeting_Date) in (" + data.year + ") ";

        }
        if (data.meetingName) {
            condition = condition + " and mm.T_INT_Meeting_Id in (" + data.meetingName + ") ";

        }

        if (data.department) {

            condition = condition + "and (mm.T_CHR_Channel_Deprtment='" + data.department + "' " +
                " OR mm.T_CHR_Channel_Deprtment LIKE '" + data.department + "' + ',%' " +
                " OR mm.T_CHR_Channel_Deprtment LIKE '%,' + '" + data.department + "' + ',%' " +
                "  OR mm.T_CHR_Channel_Deprtment LIKE '%,' + '" + data.department + "') ";

        }

        if (data.searchText) {
            condition = condition + " and  (mm.T_CHR_Short_Name LIKE '%" + data.searchText + "%' or mm.T_CHR_FUll_Name LIKE '%" + data.searchText + "%' or u.T_CHR_Name LIKE '%" + data.searchText + "%' )";
        }

        if (data.role == '2') {
            if (data.type == 'meetings') {
                return sequelize.sync().then(() => {

                    return sequelize.sync().then(() => {

                        return sequelize.query("select tm.T_CHR_In_Progress,tm.T_INT_Schedule_Meeting_Id,tm.T_INT_Meeting_Template_Id, mm.T_INT_Meeting_Id,mt.T_CHR_Type_Of_Meeting_Name,mm.T_CHR_Short_Name,mm.T_CHR_Full_Name " +
                            " ,mm.T_INT_Frequency,mm.T_DATE_Meeting_From_Date,mm.T_DATE_Meeting_To_Date,mf.T_CHR_Frequency_Name, " +
                            " u.T_CHR_Name  from M_Meeting_Masters as mm " +
                            " inner join M_Type_Of_Meeting_Master as mt on mm.T_INT_Type_Of_Meeting=mt.T_INT_Type_Of_Meeting_Id " +
                            " inner join M_Frequency_Masters as mf on mm.T_INT_Frequency=mf.T_INT_Frequency_Id  " +
                            " inner join TXN_Schedule_Meetings as tm on tm.T_INT_Meeting_Id=mm.T_INT_Meeting_Id " +
                            " inner join M_User_Masters as u on u.T_CHR_Employee_Id=tm.T_CHR_Created_By " +
                            " where mm.T_CHR_Isdelete='0' and tm.T_CHR_Delay_Flag='0' and tm.T_CHR_Completed_Flag='0' and mm.T_INT_Convener_Id='" + data.userId + "' " + condition + " ", { type: sequelize.QueryTypes.SELECT }
                        ).then((data1) => {
                            return data1;

                        }).catch((err) => {
                            log.addLog('route :: Meeting.js ' + err);
                        });

                    });

                });
            } else {
                return sequelize.sync().then(() => {

                    return sequelize.query("select tm.T_CHR_In_Progress,tm.T_INT_Schedule_Meeting_Id,tm.T_INT_Meeting_Template_Id,mm.T_INT_Meeting_Id,mt.T_CHR_Type_Of_Meeting_Name,mm.T_CHR_Short_Name,mm.T_CHR_Full_Name " +
                        " ,mm.T_INT_Frequency,mm.T_DATE_Meeting_From_Date,mm.T_DATE_Meeting_To_Date,mf.T_CHR_Frequency_Name, " +
                        " u.T_CHR_Name  from M_Meeting_Masters as mm " +
                        " inner join M_Type_Of_Meeting_Master as mt on mm.T_INT_Type_Of_Meeting=mt.T_INT_Type_Of_Meeting_Id " +
                        " inner join M_Frequency_Masters as mf on mm.T_INT_Frequency=mf.T_INT_Frequency_Id  " +
                        " inner join TXN_Schedule_Meetings as tm on tm.T_INT_Meeting_Id=mm.T_INT_Meeting_Id " +
                        " inner join M_User_Masters as u on u.T_CHR_Employee_Id=tm.T_CHR_Created_By " +
                        " where mm.T_CHR_Isdelete='0' and tm.T_CHR_Delay_Flag='0' and tm.T_CHR_Completed_Flag='0'   " + condition + " and ((mm.T_CHR_Meeting_Chair='" + data.userId + "'" +

                        " OR mm.T_CHR_Meeting_Chair LIKE '" + data.userId + "' + ',%' " +
                        " OR mm.T_CHR_Meeting_Chair LIKE '%,' + '" + data.userId + "' + ',%' " +
                        "  OR mm.T_CHR_Meeting_Chair LIKE '%,' + '" + data.userId + "') or (mm.T_CHR_Meeting_Attendies='" + data.userId + "' " +

                        "  OR mm.T_CHR_Meeting_Attendies LIKE '" + data.userId + "' + ',%' " +
                        " OR mm.T_CHR_Meeting_Attendies LIKE '%,' + '" + data.userId + "' + ',%' " +
                        " OR mm.T_CHR_Meeting_Attendies LIKE '%,' + '" + data.userId + "'))", { type: sequelize.QueryTypes.SELECT }
                    ).then((data1) => {
                        return data1;

                    }).catch((err) => {
                        log.addLog('route :: Meeting.js ' + err);
                    });

                });
            }
        } else if (data.role == '3') {
            if (data.type == 'kli') {
                return sequelize.sync().then(() => {

                    return sequelize.sync().then(() => {

                        return sequelize.query("select tm.T_CHR_In_Progress,tm.T_INT_Schedule_Meeting_Id,tm.T_INT_Meeting_Template_Id, mm.T_INT_Meeting_Id,mt.T_CHR_Type_Of_Meeting_Name,mm.T_CHR_Short_Name,mm.T_CHR_Full_Name " +
                            " ,mm.T_INT_Frequency,mm.T_DATE_Meeting_From_Date,mm.T_DATE_Meeting_To_Date,mf.T_CHR_Frequency_Name, " +
                            " u.T_CHR_Name  from M_Meeting_Masters as mm " +
                            " inner join M_Type_Of_Meeting_Master as mt on mm.T_INT_Type_Of_Meeting=mt.T_INT_Type_Of_Meeting_Id " +
                            " inner join M_Frequency_Masters as mf on mm.T_INT_Frequency=mf.T_INT_Frequency_Id  " +
                            " inner join TXN_Schedule_Meetings as tm on tm.T_INT_Meeting_Id=mm.T_INT_Meeting_Id " +
                            " inner join M_User_Masters as u on u.T_CHR_Employee_Id=tm.T_CHR_Created_By " +
                            " where mm.T_CHR_Isdelete='0' and tm.T_CHR_Delay_Flag='0' and tm.T_CHR_Completed_Flag='0' " + condition + " ", { type: sequelize.QueryTypes.SELECT }
                        ).then((data1) => {
                            return data1;

                        }).catch((err) => {
                            log.addLog('route :: Meeting.js ' + err);
                        });

                    });

                });
            }
            else {
                return sequelize.sync().then(() => {

                    return sequelize.query("select tm.T_CHR_In_Progress,tm.T_INT_Schedule_Meeting_Id,tm.T_INT_Meeting_Template_Id,mm.T_INT_Meeting_Id,mt.T_CHR_Type_Of_Meeting_Name,mm.T_CHR_Short_Name,mm.T_CHR_Full_Name " +
                        " ,mm.T_INT_Frequency,mm.T_DATE_Meeting_From_Date,mm.T_DATE_Meeting_To_Date,mf.T_CHR_Frequency_Name, " +
                        " u.T_CHR_Name  from M_Meeting_Masters as mm " +
                        " inner join M_Type_Of_Meeting_Master as mt on mm.T_INT_Type_Of_Meeting=mt.T_INT_Type_Of_Meeting_Id " +
                        " inner join M_Frequency_Masters as mf on mm.T_INT_Frequency=mf.T_INT_Frequency_Id  " +
                        " inner join TXN_Schedule_Meetings as tm on tm.T_INT_Meeting_Id=mm.T_INT_Meeting_Id " +
                        " inner join M_User_Masters as u on u.T_CHR_Employee_Id=tm.T_CHR_Created_By " +
                        " where mm.T_CHR_Isdelete='0' and tm.T_CHR_Delay_Flag='0' and tm.T_CHR_Completed_Flag='0'   " + condition + " and ((mm.T_CHR_Meeting_Chair='" + data.userId + "'" +

                        " OR mm.T_CHR_Meeting_Chair LIKE '" + data.userId + "' + ',%' " +
                        " OR mm.T_CHR_Meeting_Chair LIKE '%,' + '" + data.userId + "' + ',%' " +
                        "  OR mm.T_CHR_Meeting_Chair LIKE '%,' + '" + data.userId + "') or (mm.T_CHR_Meeting_Attendies='" + data.userId + "' " +

                        "  OR mm.T_CHR_Meeting_Attendies LIKE '" + data.userId + "' + ',%' " +
                        " OR mm.T_CHR_Meeting_Attendies LIKE '%,' + '" + data.userId + "' + ',%' " +
                        " OR mm.T_CHR_Meeting_Attendies LIKE '%,' + '" + data.userId + "'))", { type: sequelize.QueryTypes.SELECT }
                    ).then((data1) => {
                        return data1;

                    }).catch((err) => {
                        log.addLog('route :: Meeting.js ' + err);
                    });

                });
            }
        }

    }


    getCompletedMeeting(data, offsetRow, fetchRow) {

        var d = new Date();

        var condition = "";

        if (data.month) {
            condition = " and MONTH(tm.T_DATE_Meeting_Date) in (" + data.month + ") ";

        }
        if (data.year) {
            condition = condition + " and YEAR(tm.T_DATE_Meeting_Date) in (" + data.year + ") ";

        }

        if (data.meetingName) {
            condition = condition + " and mm.T_INT_Meeting_Id in (" + data.meetingName + ") ";

        }
        if (data.department) {

            condition = condition + "and (mm.T_CHR_Channel_Deprtment='" + data.department + "' " +
                " OR mm.T_CHR_Channel_Deprtment LIKE '" + data.department + "' + ',%' " +
                " OR mm.T_CHR_Channel_Deprtment LIKE '%,' + '" + data.department + "' + ',%' " +
                "  OR mm.T_CHR_Channel_Deprtment LIKE '%,' + '" + data.department + "') ";

        }

        if (data.searchText) {
            condition = condition + " and  (mm.T_CHR_Short_Name LIKE '%" + data.searchText + "%' or mm.T_CHR_FUll_Name LIKE '%" + data.searchText + "%' or u.T_CHR_Name LIKE '%" + data.searchText + "%' )";
        }

        if (data.role == '2') {
            if (data.type == 'meetings') {
                return sequelize.sync().then(() => {

                    return sequelize.sync().then(() => {

                        return sequelize.query("select tm.T_INT_Schedule_Meeting_Id,tm.T_INT_Meeting_Template_Id,mm.T_INT_Meeting_Id,mt.T_CHR_Type_Of_Meeting_Name,mm.T_CHR_Short_Name,mm.T_CHR_Full_Name " +
                            " ,mm.T_INT_Frequency,mm.T_DATE_Meeting_From_Date,mm.T_DATE_Meeting_To_Date,mf.T_CHR_Frequency_Name, " +
                            " u.T_CHR_Name  from M_Meeting_Masters as mm " +
                            " inner join M_Type_Of_Meeting_Master as mt on mm.T_INT_Type_Of_Meeting=mt.T_INT_Type_Of_Meeting_Id " +
                            " inner join M_Frequency_Masters as mf on mm.T_INT_Frequency=mf.T_INT_Frequency_Id  " +
                            " inner join TXN_Schedule_Meetings as tm on tm.T_INT_Meeting_Id=mm.T_INT_Meeting_Id " +
                            " inner join M_User_Masters as u on u.T_CHR_Employee_Id=tm.T_CHR_Created_By " +
                            " where mm.T_CHR_Isdelete='0'  and tm.T_CHR_Completed_Flag='1' and mm.T_INT_Convener_Id='" + data.userId + "' " + condition + " " +
                            "ORDER BY " +
                            "tm.T_DATE_Created_Date_Time desc" +

                            " OFFSET " + offsetRow + " ROWS " +
                            " FETCH NEXT " + fetchRow + " ROWS ONLY ", { type: sequelize.QueryTypes.SELECT }
                        ).then((data1) => {
                            return data1;

                        }).catch((err) => {
                            log.addLog('route :: Meeting.js ' + err);
                        });

                    });

                });
            } else {
                return sequelize.sync().then(() => {

                    return sequelize.query("select tm.T_INT_Schedule_Meeting_Id,tm.T_INT_Meeting_Template_Id,mm.T_INT_Meeting_Id,mt.T_CHR_Type_Of_Meeting_Name,mm.T_CHR_Short_Name,mm.T_CHR_Full_Name " +
                        " ,mm.T_INT_Frequency,mm.T_DATE_Meeting_From_Date,mm.T_DATE_Meeting_To_Date,mf.T_CHR_Frequency_Name, " +
                        " u.T_CHR_Name  from M_Meeting_Masters as mm " +
                        " inner join M_Type_Of_Meeting_Master as mt on mm.T_INT_Type_Of_Meeting=mt.T_INT_Type_Of_Meeting_Id " +
                        " inner join M_Frequency_Masters as mf on mm.T_INT_Frequency=mf.T_INT_Frequency_Id  " +
                        " inner join TXN_Schedule_Meetings as tm on tm.T_INT_Meeting_Id=mm.T_INT_Meeting_Id " +
                        " inner join M_User_Masters as u on u.T_CHR_Employee_Id=tm.T_CHR_Created_By " +
                        " where mm.T_CHR_Isdelete='0' and tm.T_CHR_Completed_Flag='1' " + condition + " and ((mm.T_CHR_Meeting_Chair='" + data.userId + "'" +

                        " OR mm.T_CHR_Meeting_Chair LIKE '" + data.userId + "' + ',%' " +
                        " OR mm.T_CHR_Meeting_Chair LIKE '%,' + '" + data.userId + "' + ',%' " +
                        "  OR mm.T_CHR_Meeting_Chair LIKE '%,' + '" + data.userId + "') or (mm.T_CHR_Meeting_Attendies='" + data.userId + "' " +

                        "  OR mm.T_CHR_Meeting_Attendies LIKE '" + data.userId + "' + ',%' " +
                        " OR mm.T_CHR_Meeting_Attendies LIKE '%,' + '" + data.userId + "' + ',%' " +
                        " OR mm.T_CHR_Meeting_Attendies LIKE '%,' + '" + data.userId + "')) " +
                        "ORDER BY " +
                        "tm.T_DATE_Created_Date_Time desc" +

                        " OFFSET " + offsetRow + " ROWS " +
                        " FETCH NEXT " + fetchRow + " ROWS ONLY ", { type: sequelize.QueryTypes.SELECT }
                    ).then((data1) => {
                        return data1;

                    }).catch((err) => {
                        log.addLog('route :: Meeting.js ' + err);
                    });

                });
            }
        } else if (data.role == '3') {
            if (data.type == 'kli') {
                return sequelize.sync().then(() => {

                    return sequelize.sync().then(() => {

                        return sequelize.query("select tm.T_INT_Schedule_Meeting_Id,tm.T_INT_Meeting_Template_Id,mm.T_INT_Meeting_Id,mt.T_CHR_Type_Of_Meeting_Name,mm.T_CHR_Short_Name,mm.T_CHR_Full_Name " +
                            " ,mm.T_INT_Frequency,mm.T_DATE_Meeting_From_Date,mm.T_DATE_Meeting_To_Date,mf.T_CHR_Frequency_Name, " +
                            " u.T_CHR_Name  from M_Meeting_Masters as mm " +
                            " inner join M_Type_Of_Meeting_Master as mt on mm.T_INT_Type_Of_Meeting=mt.T_INT_Type_Of_Meeting_Id " +
                            " inner join M_Frequency_Masters as mf on mm.T_INT_Frequency=mf.T_INT_Frequency_Id  " +
                            " inner join TXN_Schedule_Meetings as tm on tm.T_INT_Meeting_Id=mm.T_INT_Meeting_Id " +
                            " inner join M_User_Masters as u on u.T_CHR_Employee_Id=tm.T_CHR_Created_By " +
                            " where mm.T_CHR_Isdelete='0'  and tm.T_CHR_Completed_Flag='1'   " + condition + " " +
                            "ORDER BY " +
                            "tm.T_DATE_Created_Date_Time desc" +

                            " OFFSET " + offsetRow + " ROWS " +
                            " FETCH NEXT " + fetchRow + " ROWS ONLY ", { type: sequelize.QueryTypes.SELECT }
                        ).then((data1) => {
                            return data1;

                        }).catch((err) => {
                            log.addLog('route :: Meeting.js ' + err);
                        });

                    });

                });
            }
            else {
                return sequelize.sync().then(() => {

                    return sequelize.query("select tm.T_INT_Schedule_Meeting_Id,tm.T_INT_Meeting_Template_Id,mm.T_INT_Meeting_Id,mt.T_CHR_Type_Of_Meeting_Name,mm.T_CHR_Short_Name,mm.T_CHR_Full_Name " +
                        " ,mm.T_INT_Frequency,mm.T_DATE_Meeting_From_Date,mm.T_DATE_Meeting_To_Date,mf.T_CHR_Frequency_Name, " +
                        " u.T_CHR_Name  from M_Meeting_Masters as mm " +
                        " inner join M_Type_Of_Meeting_Master as mt on mm.T_INT_Type_Of_Meeting=mt.T_INT_Type_Of_Meeting_Id " +
                        " inner join M_Frequency_Masters as mf on mm.T_INT_Frequency=mf.T_INT_Frequency_Id  " +
                        " inner join TXN_Schedule_Meetings as tm on tm.T_INT_Meeting_Id=mm.T_INT_Meeting_Id " +
                        " inner join M_User_Masters as u on u.T_CHR_Employee_Id=tm.T_CHR_Created_By " +
                        " where mm.T_CHR_Isdelete='0' and tm.T_CHR_Completed_Flag='1' " + condition + " and ((mm.T_CHR_Meeting_Chair='" + data.userId + "'" +

                        " OR mm.T_CHR_Meeting_Chair LIKE '" + data.userId + "' + ',%' " +
                        " OR mm.T_CHR_Meeting_Chair LIKE '%,' + '" + data.userId + "' + ',%' " +
                        "  OR mm.T_CHR_Meeting_Chair LIKE '%,' + '" + data.userId + "') or (mm.T_CHR_Meeting_Attendies='" + data.userId + "' " +

                        "  OR mm.T_CHR_Meeting_Attendies LIKE '" + data.userId + "' + ',%' " +
                        " OR mm.T_CHR_Meeting_Attendies LIKE '%,' + '" + data.userId + "' + ',%' " +
                        " OR mm.T_CHR_Meeting_Attendies LIKE '%,' + '" + data.userId + "')) " +
                        "ORDER BY " +
                        "tm.T_DATE_Created_Date_Time desc" +

                        " OFFSET " + offsetRow + " ROWS " +
                        " FETCH NEXT " + fetchRow + " ROWS ONLY ", { type: sequelize.QueryTypes.SELECT }
                    ).then((data1) => {
                        return data1;

                    }).catch((err) => {
                        log.addLog('route :: Meeting.js ' + err);
                    });

                });
            }
        }

    }

    getCompletedMeetingExportToExcel(data, offsetRow, fetchRow) {

        var d = new Date();

        var condition = "";

        if (data.month) {
            condition = " and MONTH(tm.T_DATE_Meeting_Date) in (" + data.month + ") ";

        }
        if (data.year) {
            condition = condition + " and YEAR(tm.T_DATE_Meeting_Date) in (" + data.year + ") ";

        }

        if (data.meetingName) {
            condition = condition + " and mm.T_INT_Meeting_Id in (" + data.meetingName + ") ";

        }
        if (data.department) {

            condition = condition + "and (mm.T_CHR_Channel_Deprtment='" + data.department + "' " +
                " OR mm.T_CHR_Channel_Deprtment LIKE '" + data.department + "' + ',%' " +
                " OR mm.T_CHR_Channel_Deprtment LIKE '%,' + '" + data.department + "' + ',%' " +
                "  OR mm.T_CHR_Channel_Deprtment LIKE '%,' + '" + data.department + "') ";

        }

        if (data.searchText) {
            condition = condition + " and  (mm.T_CHR_Short_Name LIKE '%" + data.searchText + "%' or mm.T_CHR_FUll_Name LIKE '%" + data.searchText + "%' or u.T_CHR_Name LIKE '%" + data.searchText + "%' )";
        }

        if (data.role == '2') {
            if (data.type == 'meetings') {
                return sequelize.sync().then(() => {

                    return sequelize.sync().then(() => {

                        return sequelize.query("select tm.T_INT_Schedule_Meeting_Id,tm.T_INT_Meeting_Template_Id,mm.T_INT_Meeting_Id,mt.T_CHR_Type_Of_Meeting_Name,mm.T_CHR_Short_Name,mm.T_CHR_Full_Name " +
                            " ,mm.T_INT_Frequency,mm.T_DATE_Meeting_From_Date,mm.T_DATE_Meeting_To_Date,mf.T_CHR_Frequency_Name, " +
                            " u.T_CHR_Name  from M_Meeting_Masters as mm " +
                            " inner join M_Type_Of_Meeting_Master as mt on mm.T_INT_Type_Of_Meeting=mt.T_INT_Type_Of_Meeting_Id " +
                            " inner join M_Frequency_Masters as mf on mm.T_INT_Frequency=mf.T_INT_Frequency_Id  " +
                            " inner join TXN_Schedule_Meetings as tm on tm.T_INT_Meeting_Id=mm.T_INT_Meeting_Id " +
                            " inner join M_User_Masters as u on u.T_CHR_Employee_Id=tm.T_CHR_Created_By " +
                            " where mm.T_CHR_Isdelete='0'  and tm.T_CHR_Completed_Flag='1' and mm.T_INT_Convener_Id='" + data.userId + "' " + condition + "  ", { type: sequelize.QueryTypes.SELECT }
                        ).then((data1) => {
                            return data1;

                        }).catch((err) => {
                            log.addLog('route :: Meeting.js ' + err);
                        });

                    });

                });
            } else {
                return sequelize.sync().then(() => {

                    return sequelize.query("select tm.T_INT_Schedule_Meeting_Id,tm.T_INT_Meeting_Template_Id,mm.T_INT_Meeting_Id,mt.T_CHR_Type_Of_Meeting_Name,mm.T_CHR_Short_Name,mm.T_CHR_Full_Name " +
                        " ,mm.T_INT_Frequency,mm.T_DATE_Meeting_From_Date,mm.T_DATE_Meeting_To_Date,mf.T_CHR_Frequency_Name, " +
                        " u.T_CHR_Name  from M_Meeting_Masters as mm " +
                        " inner join M_Type_Of_Meeting_Master as mt on mm.T_INT_Type_Of_Meeting=mt.T_INT_Type_Of_Meeting_Id " +
                        " inner join M_Frequency_Masters as mf on mm.T_INT_Frequency=mf.T_INT_Frequency_Id  " +
                        " inner join TXN_Schedule_Meetings as tm on tm.T_INT_Meeting_Id=mm.T_INT_Meeting_Id " +
                        " inner join M_User_Masters as u on u.T_CHR_Employee_Id=tm.T_CHR_Created_By " +
                        " where mm.T_CHR_Isdelete='0'  and tm.T_CHR_Completed_Flag='1' " + condition + " and ((mm.T_CHR_Meeting_Chair='" + data.userId + "'" +

                        " OR mm.T_CHR_Meeting_Chair LIKE '" + data.userId + "' + ',%' " +
                        " OR mm.T_CHR_Meeting_Chair LIKE '%,' + '" + data.userId + "' + ',%' " +
                        "  OR mm.T_CHR_Meeting_Chair LIKE '%,' + '" + data.userId + "') or (mm.T_CHR_Meeting_Attendies='" + data.userId + "' " +

                        "  OR mm.T_CHR_Meeting_Attendies LIKE '" + data.userId + "' + ',%' " +
                        " OR mm.T_CHR_Meeting_Attendies LIKE '%,' + '" + data.userId + "' + ',%' " +
                        " OR mm.T_CHR_Meeting_Attendies LIKE '%,' + '" + data.userId + "')) ", { type: sequelize.QueryTypes.SELECT }
                    ).then((data1) => {
                        return data1;

                    }).catch((err) => {
                        log.addLog('route :: Meeting.js ' + err);
                    });

                });
            }
        } else if (data.role == '3') {
            if (data.type == 'kli') {
                return sequelize.sync().then(() => {

                    return sequelize.sync().then(() => {

                        return sequelize.query("select tm.T_INT_Schedule_Meeting_Id,tm.T_INT_Meeting_Template_Id,mm.T_INT_Meeting_Id,mt.T_CHR_Type_Of_Meeting_Name,mm.T_CHR_Short_Name,mm.T_CHR_Full_Name " +
                            " ,mm.T_INT_Frequency,mm.T_DATE_Meeting_From_Date,mm.T_DATE_Meeting_To_Date,mf.T_CHR_Frequency_Name, " +
                            " u.T_CHR_Name  from M_Meeting_Masters as mm " +
                            " inner join M_Type_Of_Meeting_Master as mt on mm.T_INT_Type_Of_Meeting=mt.T_INT_Type_Of_Meeting_Id " +
                            " inner join M_Frequency_Masters as mf on mm.T_INT_Frequency=mf.T_INT_Frequency_Id  " +
                            " inner join TXN_Schedule_Meetings as tm on tm.T_INT_Meeting_Id=mm.T_INT_Meeting_Id " +
                            " inner join M_User_Masters as u on u.T_CHR_Employee_Id=tm.T_CHR_Created_By " +
                            " where mm.T_CHR_Isdelete='0'  and tm.T_CHR_Completed_Flag='1'  " + condition + "  ", { type: sequelize.QueryTypes.SELECT }
                        ).then((data1) => {
                            return data1;

                        }).catch((err) => {
                            log.addLog('route :: Meeting.js ' + err);
                        });

                    });

                });
            }
            else {
                return sequelize.sync().then(() => {

                    return sequelize.query("select tm.T_INT_Schedule_Meeting_Id,tm.T_INT_Meeting_Template_Id,mm.T_INT_Meeting_Id,mt.T_CHR_Type_Of_Meeting_Name,mm.T_CHR_Short_Name,mm.T_CHR_Full_Name " +
                        " ,mm.T_INT_Frequency,mm.T_DATE_Meeting_From_Date,mm.T_DATE_Meeting_To_Date,mf.T_CHR_Frequency_Name, " +
                        " u.T_CHR_Name  from M_Meeting_Masters as mm " +
                        " inner join M_Type_Of_Meeting_Master as mt on mm.T_INT_Type_Of_Meeting=mt.T_INT_Type_Of_Meeting_Id " +
                        " inner join M_Frequency_Masters as mf on mm.T_INT_Frequency=mf.T_INT_Frequency_Id  " +
                        " inner join TXN_Schedule_Meetings as tm on tm.T_INT_Meeting_Id=mm.T_INT_Meeting_Id " +
                        " inner join M_User_Masters as u on u.T_CHR_Employee_Id=tm.T_CHR_Created_By " +
                        " where mm.T_CHR_Isdelete='0'  and tm.T_CHR_Completed_Flag='1' " + condition + " and ((mm.T_CHR_Meeting_Chair='" + data.userId + "'" +

                        " OR mm.T_CHR_Meeting_Chair LIKE '" + data.userId + "' + ',%' " +
                        " OR mm.T_CHR_Meeting_Chair LIKE '%,' + '" + data.userId + "' + ',%' " +
                        "  OR mm.T_CHR_Meeting_Chair LIKE '%,' + '" + data.userId + "') or (mm.T_CHR_Meeting_Attendies='" + data.userId + "' " +

                        "  OR mm.T_CHR_Meeting_Attendies LIKE '" + data.userId + "' + ',%' " +
                        " OR mm.T_CHR_Meeting_Attendies LIKE '%,' + '" + data.userId + "' + ',%' " +
                        " OR mm.T_CHR_Meeting_Attendies LIKE '%,' + '" + data.userId + "')) ", { type: sequelize.QueryTypes.SELECT }
                    ).then((data1) => {
                        return data1;

                    }).catch((err) => {
                        log.addLog('route :: Meeting.js ' + err);
                    });

                });
            }
        }

    }

    getOverdueMeeting(data, offsetRow, fetchRow) {

        var d = new Date();
        var condition = "";

        if (data.month) {
            condition = " and MONTH(tm.T_DATE_Meeting_Date) in (" + data.month + ") ";

        }
        if (data.year) {
            condition = condition + " and YEAR(tm.T_DATE_Meeting_Date) in (" + data.year + ") ";

        }
        if (data.meetingName) {
            condition = condition + " and mm.T_INT_Meeting_Id in (" + data.meetingName + ") ";

        }
        if (data.department) {

            condition = condition + "and (mm.T_CHR_Channel_Deprtment='" + data.department + "' " +
                " OR mm.T_CHR_Channel_Deprtment LIKE '" + data.department + "' + ',%' " +
                " OR mm.T_CHR_Channel_Deprtment LIKE '%,' + '" + data.department + "' + ',%' " +
                "  OR mm.T_CHR_Channel_Deprtment LIKE '%,' + '" + data.department + "') ";

        }

        if (data.searchText) {
            condition = condition + " and  (mm.T_CHR_Short_Name LIKE '%" + data.searchText + "%' or mm.T_CHR_FUll_Name LIKE '%" + data.searchText + "%' or u.T_CHR_Name LIKE '%" + data.searchText + "%' )";
        }

        if (data.role == '2') {
            if (data.type == 'meetings') {
                return sequelize.sync().then(() => {

                    return sequelize.sync().then(() => {

                        return sequelize.query("select tm.T_CHR_In_Progress,tm.T_INT_Schedule_Meeting_Id,tm.T_INT_Meeting_Template_Id,mm.T_INT_Meeting_Id,mt.T_CHR_Type_Of_Meeting_Name,mm.T_CHR_Short_Name,mm.T_CHR_Full_Name " +
                            " ,mm.T_INT_Frequency,mm.T_DATE_Meeting_From_Date,mm.T_DATE_Meeting_To_Date,mf.T_CHR_Frequency_Name, " +
                            " u.T_CHR_Name  from M_Meeting_Masters as mm " +
                            " inner join M_Type_Of_Meeting_Master as mt on mm.T_INT_Type_Of_Meeting=mt.T_INT_Type_Of_Meeting_Id " +
                            " inner join M_Frequency_Masters as mf on mm.T_INT_Frequency=mf.T_INT_Frequency_Id  " +
                            " inner join TXN_Schedule_Meetings as tm on tm.T_INT_Meeting_Id=mm.T_INT_Meeting_Id " +
                            " inner join M_User_Masters as u on u.T_CHR_Employee_Id=tm.T_CHR_Created_By " +
                            " where mm.T_CHR_Isdelete='0' and tm.T_CHR_Delay_Flag='1' and tm.T_CHR_Completed_Flag='0' and mm.T_INT_Convener_Id='" + data.userId + "' " + condition + " " +
                            "ORDER BY " +
                            "tm.T_DATE_Created_Date_Time desc" +

                            " OFFSET " + offsetRow + " ROWS " +
                            " FETCH NEXT " + fetchRow + " ROWS ONLY ", { type: sequelize.QueryTypes.SELECT }
                        ).then((data1) => {
                            return data1;

                        }).catch((err) => {
                            log.addLog('route :: Meeting.js ' + err);
                        });

                    });

                });
            } else {
                return sequelize.sync().then(() => {

                    return sequelize.query("select tm.T_CHR_In_Progress,tm.T_INT_Schedule_Meeting_Id,tm.T_INT_Meeting_Template_Id,mm.T_INT_Meeting_Id,mt.T_CHR_Type_Of_Meeting_Name,mm.T_CHR_Short_Name,mm.T_CHR_Full_Name " +
                        " ,mm.T_INT_Frequency,mm.T_DATE_Meeting_From_Date,mm.T_DATE_Meeting_To_Date,mf.T_CHR_Frequency_Name, " +
                        " u.T_CHR_Name  from M_Meeting_Masters as mm " +
                        " inner join M_Type_Of_Meeting_Master as mt on mm.T_INT_Type_Of_Meeting=mt.T_INT_Type_Of_Meeting_Id " +
                        " inner join M_Frequency_Masters as mf on mm.T_INT_Frequency=mf.T_INT_Frequency_Id  " +
                        " inner join TXN_Schedule_Meetings as tm on tm.T_INT_Meeting_Id=mm.T_INT_Meeting_Id " +
                        " inner join M_User_Masters as u on u.T_CHR_Employee_Id=tm.T_CHR_Created_By " +
                        " where mm.T_CHR_Isdelete='0' and tm.T_CHR_Delay_Flag='1' and tm.T_CHR_Completed_Flag='0' " + condition + "  and ((mm.T_CHR_Meeting_Chair='" + data.userId + "'" +

                        " OR mm.T_CHR_Meeting_Chair LIKE '" + data.userId + "' + ',%' " +
                        " OR mm.T_CHR_Meeting_Chair LIKE '%,' + '" + data.userId + "' + ',%' " +
                        "  OR mm.T_CHR_Meeting_Chair LIKE '%,' + '" + data.userId + "') or (mm.T_CHR_Meeting_Attendies='" + data.userId + "' " +

                        "  OR mm.T_CHR_Meeting_Attendies LIKE '" + data.userId + "' + ',%' " +
                        " OR mm.T_CHR_Meeting_Attendies LIKE '%,' + '" + data.userId + "' + ',%' " +
                        " OR mm.T_CHR_Meeting_Attendies LIKE '%,' + '" + data.userId + "')) " +
                        "ORDER BY " +
                        "tm.T_DATE_Created_Date_Time desc" +

                        " OFFSET " + offsetRow + " ROWS " +
                        " FETCH NEXT " + fetchRow + " ROWS ONLY ", { type: sequelize.QueryTypes.SELECT }
                    ).then((data1) => {
                        return data1;

                    }).catch((err) => {
                        log.addLog('route :: Meeting.js ' + err);
                    });

                });
            }
        } else if (data.role == '3') {
            if (data.type == 'kli') {
                return sequelize.sync().then(() => {

                    return sequelize.sync().then(() => {

                        return sequelize.query("select tm.T_CHR_In_Progress,tm.T_INT_Schedule_Meeting_Id,tm.T_INT_Meeting_Template_Id,mm.T_INT_Meeting_Id,mt.T_CHR_Type_Of_Meeting_Name,mm.T_CHR_Short_Name,mm.T_CHR_Full_Name " +
                            " ,mm.T_INT_Frequency,mm.T_DATE_Meeting_From_Date,mm.T_DATE_Meeting_To_Date,mf.T_CHR_Frequency_Name, " +
                            " u.T_CHR_Name  from M_Meeting_Masters as mm " +
                            " inner join M_Type_Of_Meeting_Master as mt on mm.T_INT_Type_Of_Meeting=mt.T_INT_Type_Of_Meeting_Id " +
                            " inner join M_Frequency_Masters as mf on mm.T_INT_Frequency=mf.T_INT_Frequency_Id  " +
                            " inner join TXN_Schedule_Meetings as tm on tm.T_INT_Meeting_Id=mm.T_INT_Meeting_Id " +
                            " inner join M_User_Masters as u on u.T_CHR_Employee_Id=tm.T_CHR_Created_By " +
                            " where mm.T_CHR_Isdelete='0' and tm.T_CHR_Delay_Flag='1' and tm.T_CHR_Completed_Flag='0'  " + condition + " " +
                            "ORDER BY " +
                            "tm.T_DATE_Created_Date_Time desc" +

                            " OFFSET " + offsetRow + " ROWS " +
                            " FETCH NEXT " + fetchRow + " ROWS ONLY ", { type: sequelize.QueryTypes.SELECT }
                        ).then((data1) => {
                            return data1;

                        }).catch((err) => {
                            log.addLog('route :: Meeting.js ' + err);
                        });

                    });

                });
            }
            else {
                return sequelize.sync().then(() => {

                    return sequelize.query("select tm.T_CHR_In_Progress,tm.T_INT_Schedule_Meeting_Id,tm.T_INT_Meeting_Template_Id,mm.T_INT_Meeting_Id,mt.T_CHR_Type_Of_Meeting_Name,mm.T_CHR_Short_Name,mm.T_CHR_Full_Name " +
                        " ,mm.T_INT_Frequency,mm.T_DATE_Meeting_From_Date,mm.T_DATE_Meeting_To_Date,mf.T_CHR_Frequency_Name, " +
                        " u.T_CHR_Name  from M_Meeting_Masters as mm " +
                        " inner join M_Type_Of_Meeting_Master as mt on mm.T_INT_Type_Of_Meeting=mt.T_INT_Type_Of_Meeting_Id " +
                        " inner join M_Frequency_Masters as mf on mm.T_INT_Frequency=mf.T_INT_Frequency_Id  " +
                        " inner join TXN_Schedule_Meetings as tm on tm.T_INT_Meeting_Id=mm.T_INT_Meeting_Id " +
                        " inner join M_User_Masters as u on u.T_CHR_Employee_Id=tm.T_CHR_Created_By " +
                        " where mm.T_CHR_Isdelete='0' and tm.T_CHR_Delay_Flag='1' and tm.T_CHR_Completed_Flag='0' " + condition + "  and ((mm.T_CHR_Meeting_Chair='" + data.userId + "'" +

                        " OR mm.T_CHR_Meeting_Chair LIKE '" + data.userId + "' + ',%' " +
                        " OR mm.T_CHR_Meeting_Chair LIKE '%,' + '" + data.userId + "' + ',%' " +
                        "  OR mm.T_CHR_Meeting_Chair LIKE '%,' + '" + data.userId + "') or (mm.T_CHR_Meeting_Attendies='" + data.userId + "' " +

                        "  OR mm.T_CHR_Meeting_Attendies LIKE '" + data.userId + "' + ',%' " +
                        " OR mm.T_CHR_Meeting_Attendies LIKE '%,' + '" + data.userId + "' + ',%' " +
                        " OR mm.T_CHR_Meeting_Attendies LIKE '%,' + '" + data.userId + "')) " +
                        "ORDER BY " +
                        "tm.T_DATE_Created_Date_Time desc" +

                        " OFFSET " + offsetRow + " ROWS " +
                        " FETCH NEXT " + fetchRow + " ROWS ONLY ", { type: sequelize.QueryTypes.SELECT }
                    ).then((data1) => {
                        return data1;

                    }).catch((err) => {
                        log.addLog('route :: Meeting.js ' + err);
                    });

                });
            }
        }

    }
    getOverdueMeetingExportToExcel(data, offsetRow, fetchRow) {

        var d = new Date();
        var condition = "";

        if (data.month) {
            condition = " and MONTH(tm.T_DATE_Meeting_Date) in (" + data.month + ") ";

        }
        if (data.year) {
            condition = condition + " and YEAR(tm.T_DATE_Meeting_Date) in (" + data.year + ") ";

        }
        if (data.meetingName) {
            condition = condition + " and mm.T_INT_Meeting_Id in (" + data.meetingName + ") ";

        }
        if (data.department) {

            condition = condition + "and (mm.T_CHR_Channel_Deprtment='" + data.department + "' " +
                " OR mm.T_CHR_Channel_Deprtment LIKE '" + data.department + "' + ',%' " +
                " OR mm.T_CHR_Channel_Deprtment LIKE '%,' + '" + data.department + "' + ',%' " +
                "  OR mm.T_CHR_Channel_Deprtment LIKE '%,' + '" + data.department + "') ";

        }

        if (data.searchText) {
            condition = condition + " and  (mm.T_CHR_Short_Name LIKE '%" + data.searchText + "%' or mm.T_CHR_FUll_Name LIKE '%" + data.searchText + "%' or u.T_CHR_Name LIKE '%" + data.searchText + "%' )";
        }

        if (data.role == '2') {
            if (data.type == 'meetings') {
                return sequelize.sync().then(() => {

                    return sequelize.sync().then(() => {

                        return sequelize.query("select tm.T_CHR_In_Progress,tm.T_INT_Schedule_Meeting_Id,tm.T_INT_Meeting_Template_Id,mm.T_INT_Meeting_Id,mt.T_CHR_Type_Of_Meeting_Name,mm.T_CHR_Short_Name,mm.T_CHR_Full_Name " +
                            " ,mm.T_INT_Frequency,mm.T_DATE_Meeting_From_Date,mm.T_DATE_Meeting_To_Date,mf.T_CHR_Frequency_Name, " +
                            " u.T_CHR_Name  from M_Meeting_Masters as mm " +
                            " inner join M_Type_Of_Meeting_Master as mt on mm.T_INT_Type_Of_Meeting=mt.T_INT_Type_Of_Meeting_Id " +
                            " inner join M_Frequency_Masters as mf on mm.T_INT_Frequency=mf.T_INT_Frequency_Id  " +
                            " inner join TXN_Schedule_Meetings as tm on tm.T_INT_Meeting_Id=mm.T_INT_Meeting_Id " +
                            " inner join M_User_Masters as u on u.T_CHR_Employee_Id=tm.T_CHR_Created_By " +
                            " where mm.T_CHR_Isdelete='0' and tm.T_CHR_Delay_Flag='1' and tm.T_CHR_Completed_Flag='0' and mm.T_INT_Convener_Id='" + data.userId + "' " + condition + " ", { type: sequelize.QueryTypes.SELECT }
                        ).then((data1) => {
                            return data1;

                        }).catch((err) => {
                            log.addLog('route :: Meeting.js ' + err);
                        });

                    });

                });
            } else {
                return sequelize.sync().then(() => {

                    return sequelize.query("select tm.T_CHR_In_Progress,tm.T_INT_Schedule_Meeting_Id,tm.T_INT_Meeting_Template_Id,mm.T_INT_Meeting_Id,mt.T_CHR_Type_Of_Meeting_Name,mm.T_CHR_Short_Name,mm.T_CHR_Full_Name " +
                        " ,mm.T_INT_Frequency,mm.T_DATE_Meeting_From_Date,mm.T_DATE_Meeting_To_Date,mf.T_CHR_Frequency_Name, " +
                        " u.T_CHR_Name  from M_Meeting_Masters as mm " +
                        " inner join M_Type_Of_Meeting_Master as mt on mm.T_INT_Type_Of_Meeting=mt.T_INT_Type_Of_Meeting_Id " +
                        " inner join M_Frequency_Masters as mf on mm.T_INT_Frequency=mf.T_INT_Frequency_Id  " +
                        " inner join TXN_Schedule_Meetings as tm on tm.T_INT_Meeting_Id=mm.T_INT_Meeting_Id " +
                        " inner join M_User_Masters as u on u.T_CHR_Employee_Id=tm.T_CHR_Created_By " +
                        " where mm.T_CHR_Isdelete='0' and tm.T_CHR_Delay_Flag='1' and tm.T_CHR_Completed_Flag='0' " + condition + "  and ((mm.T_CHR_Meeting_Chair='" + data.userId + "'" +

                        " OR mm.T_CHR_Meeting_Chair LIKE '" + data.userId + "' + ',%' " +
                        " OR mm.T_CHR_Meeting_Chair LIKE '%,' + '" + data.userId + "' + ',%' " +
                        "  OR mm.T_CHR_Meeting_Chair LIKE '%,' + '" + data.userId + "') or (mm.T_CHR_Meeting_Attendies='" + data.userId + "' " +

                        "  OR mm.T_CHR_Meeting_Attendies LIKE '" + data.userId + "' + ',%' " +
                        " OR mm.T_CHR_Meeting_Attendies LIKE '%,' + '" + data.userId + "' + ',%' " +
                        " OR mm.T_CHR_Meeting_Attendies LIKE '%,' + '" + data.userId + "')) ", { type: sequelize.QueryTypes.SELECT }
                    ).then((data1) => {
                        return data1;

                    }).catch((err) => {
                        log.addLog('route :: Meeting.js ' + err);
                    });

                });
            }
        } else if (data.role == '3') {
            if (data.type == 'kli') {
                return sequelize.sync().then(() => {

                    return sequelize.sync().then(() => {

                        return sequelize.query("select tm.T_CHR_In_Progress,tm.T_INT_Schedule_Meeting_Id,tm.T_INT_Meeting_Template_Id,mm.T_INT_Meeting_Id,mt.T_CHR_Type_Of_Meeting_Name,mm.T_CHR_Short_Name,mm.T_CHR_Full_Name " +
                            " ,mm.T_INT_Frequency,mm.T_DATE_Meeting_From_Date,mm.T_DATE_Meeting_To_Date,mf.T_CHR_Frequency_Name, " +
                            " u.T_CHR_Name  from M_Meeting_Masters as mm " +
                            " inner join M_Type_Of_Meeting_Master as mt on mm.T_INT_Type_Of_Meeting=mt.T_INT_Type_Of_Meeting_Id " +
                            " inner join M_Frequency_Masters as mf on mm.T_INT_Frequency=mf.T_INT_Frequency_Id  " +
                            " inner join TXN_Schedule_Meetings as tm on tm.T_INT_Meeting_Id=mm.T_INT_Meeting_Id " +
                            " inner join M_User_Masters as u on u.T_CHR_Employee_Id=tm.T_CHR_Created_By " +
                            " where mm.T_CHR_Isdelete='0' and tm.T_CHR_Delay_Flag='1' and tm.T_CHR_Completed_Flag='0'  " + condition + " ", { type: sequelize.QueryTypes.SELECT }
                        ).then((data1) => {
                            return data1;

                        }).catch((err) => {
                            log.addLog('route :: Meeting.js ' + err);
                        });

                    });

                });
            }
            else {
                return sequelize.sync().then(() => {

                    return sequelize.query("select tm.T_CHR_In_Progress,tm.T_INT_Schedule_Meeting_Id,tm.T_INT_Meeting_Template_Id,mm.T_INT_Meeting_Id,mt.T_CHR_Type_Of_Meeting_Name,mm.T_CHR_Short_Name,mm.T_CHR_Full_Name " +
                        " ,mm.T_INT_Frequency,mm.T_DATE_Meeting_From_Date,mm.T_DATE_Meeting_To_Date,mf.T_CHR_Frequency_Name, " +
                        " u.T_CHR_Name  from M_Meeting_Masters as mm " +
                        " inner join M_Type_Of_Meeting_Master as mt on mm.T_INT_Type_Of_Meeting=mt.T_INT_Type_Of_Meeting_Id " +
                        " inner join M_Frequency_Masters as mf on mm.T_INT_Frequency=mf.T_INT_Frequency_Id  " +
                        " inner join TXN_Schedule_Meetings as tm on tm.T_INT_Meeting_Id=mm.T_INT_Meeting_Id " +
                        " inner join M_User_Masters as u on u.T_CHR_Employee_Id=tm.T_CHR_Created_By " +
                        " where mm.T_CHR_Isdelete='0' and tm.T_CHR_Delay_Flag='1' and tm.T_CHR_Completed_Flag='0' " + condition + "  and ((mm.T_CHR_Meeting_Chair='" + data.userId + "'" +

                        " OR mm.T_CHR_Meeting_Chair LIKE '" + data.userId + "' + ',%' " +
                        " OR mm.T_CHR_Meeting_Chair LIKE '%,' + '" + data.userId + "' + ',%' " +
                        "  OR mm.T_CHR_Meeting_Chair LIKE '%,' + '" + data.userId + "') or (mm.T_CHR_Meeting_Attendies='" + data.userId + "' " +

                        "  OR mm.T_CHR_Meeting_Attendies LIKE '" + data.userId + "' + ',%' " +
                        " OR mm.T_CHR_Meeting_Attendies LIKE '%,' + '" + data.userId + "' + ',%' " +
                        " OR mm.T_CHR_Meeting_Attendies LIKE '%,' + '" + data.userId + "')) ", { type: sequelize.QueryTypes.SELECT }
                    ).then((data1) => {
                        return data1;

                    }).catch((err) => {
                        log.addLog('route :: Meeting.js ' + err);
                    });

                });
            }
        }

    }

    getNotSchuledMeetingNew(element1, selectedMonth, selectedYear) {
        var dataArr = [];
        return sequelize.query("select T_INT_Schedule_Meeting_Id,T_INT_Meeting_Id,T_DATE_Meeting_Date from TXN_Schedule_Meetings as mm  where mm.T_CHR_Isdelete='0' and mm.T_INT_Meeting_Id='" + element1.T_INT_Meeting_Id + "'", { type: sequelize.QueryTypes.SELECT }
        ).then((data2) => {
            if (data2.length > 0) {
                data2.forEach(element2 => {
                    var d1 = new Date();
                    var d2 = new Date(element2.T_DATE_Meeting_Date);

                    if (element1.T_INT_Frequency == '1') {

                        var dd = String(d1.getDate()).padStart(2, '0');
                        var mm = String(d1.getMonth() + 1).padStart(2, '0'); //January is 0!
                        var yyyy = d1.getFullYear();

                        var date1 = mm + '-' + dd + '-' + yyyy;

                        var dd = String(d2.getDate()).padStart(2, '0');
                        var mm = String(d2.getMonth() + 1).padStart(2, '0'); //January is 0!
                        var yyyy = d2.getFullYear();

                        var date2 = mm + '-' + dd + '-' + yyyy;

                        if (selectedMonth.length > 0 && selectedYear.length == 0) {

                        } else if (selectedMonth.length == 0 && selectedYear.length > 0) {

                        } else if (selectedMonth.length > 0 && selectedYear.length > 0) {


                        } else {

                            if (element1.T_INT_Meeting_Id == element2.T_INT_Meeting_Id && date1 == date2) {

                            } else {

                                console.log("element1===", element1)
                                dataArr.push({
                                    "meeting_id": element1.T_INT_Meeting_Id,
                                    "meeting_type": element1.T_CHR_Type_Of_Meeting_Name,
                                    "meeting_frequency": element1.T_CHR_Frequency_Name,
                                    "meeting_short_name": element1.T_CHR_Short_Name,
                                    "meeting_full_name": element1.T_CHR_Full_Name,
                                    "convener_name": element1.T_CHR_Name
                                });
                            }
                        }



                    } else if (element1.T_INT_Frequency == '2') {

                        Date.prototype.getWeek1 = function () {
                            var onejan = new Date(this.getFullYear(), 0, 1);
                            return Math.ceil((((this - onejan) / 86400000) + onejan.getDay() + 1) / 7);
                        }

                        Date.prototype.getWeek2 = function () {
                            var onejan = new Date(this.getFullYear(), 0, 1);
                            return Math.ceil((((this - onejan) / 86400000) + onejan.getDay() + 1) / 7);
                        }

                        var today = new Date();
                        var currentWeekNumber = today.getWeek1();

                        var meetingDate = new Date(element2.T_DATE_Meeting_Date);
                        var meetingWeekNumber = meetingDate.getWeek2();


                        if (selectedMonth.length > 0 && selectedYear.length == 0) {

                        } else if (selectedMonth.length == 0 && selectedYear.length > 0) {

                        } else if (selectedMonth.length > 0 && selectedYear.length > 0) {


                        } else {
                            if (element1.T_INT_Meeting_Id == element2.T_INT_Meeting_Id && currentWeekNumber == meetingWeekNumber && today.getFullYear() == meetingDate.getFullYear()) {

                            } else {
                                dataArr.push({
                                    "meeting_id": element1.T_INT_Meeting_Id,
                                    "meeting_type": element1.T_CHR_Type_Of_Meeting_Name,
                                    "meeting_frequency": element1.T_CHR_Frequency_Name,
                                    "meeting_short_name": element1.T_CHR_Short_Name,
                                    "meeting_full_name": element1.T_CHR_Full_Name,
                                    "convener_name": element1.T_CHR_Name
                                });
                            }
                        }
                    } else if (element1.T_INT_Frequency == '3') {

                        var mm = String(d1.getMonth() + 1).padStart(2, '0'); //January is 0!
                        var yyyy1 = d1.getFullYear();

                        var date1 = mm;


                        var mm = String(d2.getMonth() + 1).padStart(2, '0'); //January is 0!
                        var yyyy2 = d2.getFullYear();

                        var date2 = mm;


                        if (element1.T_INT_Meeting_Id == element2.T_INT_Meeting_Id && date1 == date2 && yyyy1 == yyyy2) {

                        } else {
                            dataArr.push({
                                "meeting_id": element1.T_INT_Meeting_Id,
                                "meeting_type": element1.T_CHR_Type_Of_Meeting_Name,
                                "meeting_frequency": element1.T_CHR_Frequency_Name,
                                "meeting_short_name": element1.T_CHR_Short_Name,
                                "meeting_full_name": element1.T_CHR_Full_Name,
                                "convener_name": element1.T_CHR_Name
                            });
                        }
                    } else if (element1.T_INT_Frequency == '4') {

                        var mm = String(d1.getMonth() + 1).padStart(2, '0'); //January is 0!
                        var yyyy1 = d1.getFullYear();
                        var date1;

                        if (mm == '01' || mm == '02' || mm == '03') {
                            date1 = '4';
                        } else if (mm == '04' || mm == '05' || mm == '06') {
                            date1 = '1';
                        } else if (mm == '07' || mm == '08' || mm == '09') {
                            date1 = '2';
                        } else if (mm == '10' || mm == '11' || mm == '12') {
                            date1 = '3';
                        }
                        var mm = String(d2.getMonth() + 1).padStart(2, '0'); //January is 0!
                        var yyyy2 = d2.getFullYear();
                        var date2;

                        if (mm == '01' || mm == '02' || mm == '03') {
                            date2 = '4';
                        } else if (mm == '04' || mm == '05' || mm == '06') {
                            date2 = '1';
                        } else if (mm == '07' || mm == '08' || mm == '09') {
                            date2 = '2';
                        } else if (mm == '10' || mm == '11' || mm == '12') {
                            date2 = '3';
                        }


                        if (element1.T_INT_Meeting_Id == element2.T_INT_Meeting_Id && date1 == date2 && yyyy1 == yyyy2) {

                        } else {
                            dataArr.push({
                                "meeting_id": element1.T_INT_Meeting_Id,
                                "meeting_type": element1.T_CHR_Type_Of_Meeting_Name,
                                "meeting_frequency": element1.T_CHR_Frequency_Name,
                                "meeting_short_name": element1.T_CHR_Short_Name,
                                "meeting_full_name": element1.T_CHR_Full_Name,
                                "convener_name": element1.T_CHR_Name
                            });
                        }
                    } else if (element1.T_INT_Frequency == '5') {

                        var mm = String(d1.getMonth() + 1).padStart(2, '0'); //January is 0!
                        var yyyy1 = d1.getFullYear();
                        var date1;

                        if (mm == '01' || mm == '02' || mm == '03' || mm == '04') {
                            date1 = '1';
                        } else if (mm == '05' || mm == '06' || mm == '07' || mm == '08') {
                            date1 = '2';
                        } else if (mm == '08' || mm == '09' || mm == '10' || mm == '11') {
                            date1 = '3';
                        }
                        var mm = String(d2.getMonth() + 1).padStart(2, '0'); //January is 0!
                        var yyyy2 = d2.getFullYear();
                        var date2;

                        if (mm == '01' || mm == '02' || mm == '03' || mm == '04') {
                            date2 = '1';
                        } else if (mm == '05' || mm == '06' || mm == '07' || mm == '08') {
                            date2 = '2';
                        } else if (mm == '08' || mm == '09' || mm == '10' || mm == '11') {
                            date2 = '3';
                        }


                        if (element1.T_INT_Meeting_Id == element2.T_INT_Meeting_Id && date1 == date2 && yyyy1 == yyyy2) {

                        } else {
                            dataArr.push({
                                "meeting_id": element1.T_INT_Meeting_Id,
                                "meeting_type": element1.T_CHR_Type_Of_Meeting_Name,
                                "meeting_frequency": element1.T_CHR_Frequency_Name,
                                "meeting_short_name": element1.T_CHR_Short_Name,
                                "meeting_full_name": element1.T_CHR_Full_Name,
                                "convener_name": element1.T_CHR_Name
                            });
                        }
                    } else if (element1.T_INT_Frequency == '6') {

                        var mm = String(d1.getMonth() + 1).padStart(2, '0'); //January is 0!
                        var yyyy1 = d1.getFullYear();


                        var mm = String(d2.getMonth() + 1).padStart(2, '0'); //January is 0!
                        var yyyy2 = d2.getFullYear();

                        if (element1.T_INT_Meeting_Id == element2.T_INT_Meeting_Id && yyyy1 == yyyy2) {

                        } else {
                            dataArr.push({
                                "meeting_id": element1.T_INT_Meeting_Id,
                                "meeting_type": element1.T_CHR_Type_Of_Meeting_Name,
                                "meeting_frequency": element1.T_CHR_Frequency_Name,
                                "meeting_short_name": element1.T_CHR_Short_Name,
                                "meeting_full_name": element1.T_CHR_Full_Name,
                                "convener_name": element1.T_CHR_Name
                            });
                        }
                    }

                });
            } else {

                dataArr.push({
                    "meeting_id": element1.T_INT_Meeting_Id,
                    "meeting_type": element1.T_CHR_Type_Of_Meeting_Name,
                    "meeting_frequency": element1.T_CHR_Frequency_Name,
                    "meeting_short_name": element1.T_CHR_Short_Name,
                    "meeting_full_name": element1.T_CHR_Full_Name,
                    "convener_name": element1.T_CHR_Name
                });
            }


            return dataArr;

        }).catch(err => {
            log.addLog('route :: Meeting.js ' + err);
        });
    }
    getNotSchuledMeetingForLog(data, offsetRow, fetchRow) {


        var condition = "";
        console.log("ccccccccccccc=", data)
        if (data.month) {
            condition = " and MONTH(T_DATE_Created_Date_Time) in (" + data.month + ") ";

        }
        if (data.year) {
            condition = condition + " and YEAR(T_DATE_Created_Date_Time) in (" + data.year + ") ";

        }
        if (data.meetingName) {
            condition = condition + " and T_INT_Meeting_Id in (" + data.meetingName + ") ";

        }

        if (data.department) {

            condition = condition + "and (T_CHR_Channel_Deprtment='" + data.department + "' " +
                " OR T_CHR_Channel_Deprtment LIKE '" + data.department + "' + ',%' " +
                " OR T_CHR_Channel_Deprtment LIKE '%,' + '" + data.department + "' + ',%' " +
                "  OR T_CHR_Channel_Deprtment LIKE '%,' + '" + data.department + "') ";

        }

        if (data.searchText) {
            condition = condition + " and  (T_CHR_Meeting_Short_Name LIKE '%" + data.searchText + "%' or T_CHR_Meeting_Full_Name LIKE '%" + data.searchText + "%' or T_CHR_Convener_Name LIKE '%" + data.searchText + "%' )";
        }


        if (data.role == '2') {
            if (data.type == 'meetings') {
                return sequelize.query("select T_INT_Meeting_Id,T_CHR_Meeting_Type,T_CHR_Meeting_Short_Name,T_CHR_Meeting_Full_Name,T_CHR_Convener_Name,T_CHR_Frquency " +
                    " from Log_Not_Schedule_Meetings where T_INT_Convener_Id='" + data.userId + "' " + condition
                    + "ORDER BY " +
                    "T_INT_Meeting_Id " +

                    " OFFSET " + offsetRow + " ROWS " +
                    " FETCH NEXT " + fetchRow + " ROWS ONLY ", { type: sequelize.QueryTypes.SELECT }
                ).then((data2) => {
                    return data2;
                }).catch((err) => {
                    log.addLog('route :: Meeting.js ' + err);
                });
            } else {
                return sequelize.query("select T_INT_Meeting_Id,T_CHR_Meeting_Type,T_CHR_Meeting_Short_Name,T_CHR_Meeting_Full_Name,T_CHR_Convener_Name,T_CHR_Frquency " +
                    " from Log_Not_Schedule_Meetings where T_INT_Convener_Id='" + data.userId + "'" +
                    condition + "  and ((T_CHR_Meeting_Chair='" + data.userId + "'" +

                    " OR T_CHR_Meeting_Chair LIKE '" + data.userId + "' + ',%' " +
                    " OR T_CHR_Meeting_Chair LIKE '%,' + '" + data.userId + "' + ',%' " +
                    "  OR T_CHR_Meeting_Chair LIKE '%,' + '" + data.userId + "') or (T_CHR_Meeting_Attendies='" + data.userId + "' " +

                    "  OR T_CHR_Meeting_Attendies LIKE '" + data.userId + "' + ',%' " +
                    " OR T_CHR_Meeting_Attendies LIKE '%,' + '" + data.userId + "' + ',%' " +
                    " OR T_CHR_Meeting_Attendies LIKE '%,' + '" + data.userId + "')) " +
                    "ORDER BY " +
                    "T_DATE_Created_Date_Time desc" +

                    " OFFSET " + offsetRow + " ROWS " +
                    " FETCH NEXT " + fetchRow + " ROWS ONLY", { type: sequelize.QueryTypes.SELECT }
                ).then((data2) => {
                    return data2;
                }).catch((err) => {
                    log.addLog('route :: Meeting.js ' + err);
                });
            }
        } else if (data.role == '3') {
           
            if (data.type == 'kli') {
                if (condition) {
                    condition = "where " + condition.substr(4, condition.length);
                }
                return sequelize.query("select T_INT_Meeting_Id,T_CHR_Meeting_Type,T_CHR_Meeting_Short_Name,T_CHR_Meeting_Full_Name,T_CHR_Convener_Name,T_CHR_Frquency " +
                    " from Log_Not_Schedule_Meetings  " + condition
                    + "ORDER BY " +
                    "T_INT_Meeting_Id " +

                    " OFFSET " + offsetRow + " ROWS " +
                    " FETCH NEXT " + fetchRow + " ROWS ONLY ", { type: sequelize.QueryTypes.SELECT }
                ).then((data2) => {
                    return data2;
                }).catch((err) => {
                    log.addLog('route :: Meeting.js ' + err);
                });
            }
            else {
                return sequelize.query("select T_INT_Meeting_Id,T_CHR_Meeting_Type,T_CHR_Meeting_Short_Name,T_CHR_Meeting_Full_Name,T_CHR_Convener_Name,T_CHR_Frquency " +
                " from Log_Not_Schedule_Meetings where T_INT_Convener_Id='" + data.userId + "'" +
                condition + "  and ((T_CHR_Meeting_Chair='" + data.userId + "'" +

                " OR T_CHR_Meeting_Chair LIKE '" + data.userId + "' + ',%' " +
                " OR T_CHR_Meeting_Chair LIKE '%,' + '" + data.userId + "' + ',%' " +
                "  OR T_CHR_Meeting_Chair LIKE '%,' + '" + data.userId + "') or (T_CHR_Meeting_Attendies='" + data.userId + "' " +

                "  OR T_CHR_Meeting_Attendies LIKE '" + data.userId + "' + ',%' " +
                " OR T_CHR_Meeting_Attendies LIKE '%,' + '" + data.userId + "' + ',%' " +
                " OR T_CHR_Meeting_Attendies LIKE '%,' + '" + data.userId + "')) " +
                "ORDER BY " +
                "T_DATE_Created_Date_Time desc" +

                " OFFSET " + offsetRow + " ROWS " +
                " FETCH NEXT " + fetchRow + " ROWS ONLY", { type: sequelize.QueryTypes.SELECT }
            ).then((data2) => {
                return data2;
            }).catch((err) => {
                log.addLog('route :: Meeting.js ' + err);
            });
            } console.log("ooooooooooooooooooo===", condition)



        }


    }

    getNotSchuledMeetingForLogCount(data) {
        var condition = "";
        if (data.month) {
            condition = " and MONTH(T_DATE_Created_Date_Time) in (" + data.month + ") ";

        }
        if (data.year) {
            condition = condition + " and YEAR(T_DATE_Created_Date_Time) in (" + data.year + ") ";

        }
        if (data.meetingName) {
            condition = condition + " and T_INT_Meeting_Id in (" + data.meetingName + ") ";

        }

        if (data.department) {

            condition = condition + "and (T_CHR_Channel_Deprtment='" + data.department + "' " +
                " OR T_CHR_Channel_Deprtment LIKE '" + data.department + "' + ',%' " +
                " OR T_CHR_Channel_Deprtment LIKE '%,' + '" + data.department + "' + ',%' " +
                "  OR T_CHR_Channel_Deprtment LIKE '%,' + '" + data.department + "') ";

        }


        if (data.role == '2') {
            if (data.type == 'meetings') {
                return sequelize.query("select T_INT_Meeting_Id,T_CHR_Meeting_Type,T_CHR_Meeting_Short_Name,T_CHR_Meeting_Full_Name,T_CHR_Convener_Name,T_CHR_Frquency " +
                    " from Log_Not_Schedule_Meetings where T_INT_Convener_Id='" + data.userId + "' " + condition, { type: sequelize.QueryTypes.SELECT }
                ).then((data2) => {
                    return data2;
                }).catch((err) => {
                    log.addLog('route :: Meeting.js ' + err);
                });
            } else {

                return sequelize.query("select T_INT_Meeting_Id,T_CHR_Meeting_Type,T_CHR_Meeting_Short_Name,T_CHR_Meeting_Full_Name,T_CHR_Convener_Name,T_CHR_Frquency " +
                    " from Log_Not_Schedule_Meetings where    ((T_CHR_Meeting_Chair='" + data.userId + "'" +

                    " OR T_CHR_Meeting_Chair LIKE '" + data.userId + "' + ',%' " +
                    " OR T_CHR_Meeting_Chair LIKE '%,' + '" + data.userId + "' + ',%' " +
                    "  OR T_CHR_Meeting_Chair LIKE '%,' + '" + data.userId + "') or (T_CHR_Meeting_Attendies='" + data.userId + "' " +

                    "  OR T_CHR_Meeting_Attendies LIKE '" + data.userId + "' + ',%' " +
                    " OR T_CHR_Meeting_Attendies LIKE '%,' + '" + data.userId + "' + ',%' " +
                    " OR T_CHR_Meeting_Attendies LIKE '%,' + '" + data.userId + "')) " + condition, { type: sequelize.QueryTypes.SELECT }
                ).then((data2) => {
                    return data2;
                }).catch((err) => {
                    log.addLog('route :: Meeting.js ' + err);
                });
            }
        } else if (data.role == '3') {
           
            if (data.type == 'kli') {
                if (condition) {
                    condition = "where " + condition.substr(4, condition.length);
                }
                return sequelize.query("select T_INT_Meeting_Id,T_CHR_Meeting_Type,T_CHR_Meeting_Short_Name,T_CHR_Meeting_Full_Name,T_CHR_Convener_Name,T_CHR_Frquency " +
                    " from Log_Not_Schedule_Meetings " + condition, { type: sequelize.QueryTypes.SELECT }
                ).then((data2) => {
                    return data2;
                }).catch((err) => {
                    log.addLog('route :: Meeting.js ' + err);
                });
            }
            else {
                return sequelize.query("select T_INT_Meeting_Id,T_CHR_Meeting_Type,T_CHR_Meeting_Short_Name,T_CHR_Meeting_Full_Name,T_CHR_Convener_Name,T_CHR_Frquency " +
                " from Log_Not_Schedule_Meetings where    ((T_CHR_Meeting_Chair='" + data.userId + "'" +

                " OR T_CHR_Meeting_Chair LIKE '" + data.userId + "' + ',%' " +
                " OR T_CHR_Meeting_Chair LIKE '%,' + '" + data.userId + "' + ',%' " +
                "  OR T_CHR_Meeting_Chair LIKE '%,' + '" + data.userId + "') or (T_CHR_Meeting_Attendies='" + data.userId + "' " +

                "  OR T_CHR_Meeting_Attendies LIKE '" + data.userId + "' + ',%' " +
                " OR T_CHR_Meeting_Attendies LIKE '%,' + '" + data.userId + "' + ',%' " +
                " OR T_CHR_Meeting_Attendies LIKE '%,' + '" + data.userId + "'))" + condition, { type: sequelize.QueryTypes.SELECT }
            ).then((data2) => {
                return data2;
            }).catch((err) => {
                log.addLog('route :: Meeting.js ' + err);
            });
            }
        }

    }
    saveScheduleMeeting(meetingData) {
        return sequelize.sync().then(() => {
            return this.ScheudleMeeting.create({
                T_INT_Meeting_Id: meetingData.Meeting_Id,
                T_INT_Meeting_Template_Id: meetingData.Meeting_Template_Id,
                T_CHR_For_Month_Or_Quoarter: meetingData.For_Month_Or_Quoarter,
                T_DATE_Meeting_Date: meetingData.Meeting_Date,
                T_CHR_Meeting_Start_Time: meetingData.Meeting_Start_Time,
                T_CHR_Meeting_End_Time: meetingData.Meeting_End_Time,
                T_CHR_Location: meetingData.Location,

                T_CHR_Invitees: meetingData.Invitees.toString(),
                T_CHR_Description: meetingData.Description,
                T_CHR_Created_By: meetingData.created_by,
                T_DATE_Created_Date_Time: new Date()

            }).catch(function (errLog) {

                log.addLog('model :: meeting.js ' + errLog);
                var err;
                if (errLog == "SequelizeUniqueConstraintError: Validation error") {


                    err = {
                        data: 0,
                        errMsg: "Short name and full name should be unique"
                    }
                } else if (errLog != "SequelizeUniqueConstraintError: Validation error") {


                    err = {
                        data: 0,
                        errMsg: errLog["errors"][0]["message"]
                    }

                }
                return err;
            });
        }).catch(function (errLog) {


            log.addLog('model :: meeting.js ' + errLog);
        });
    }
    isPublish(meetingData) {
        return sequelize.sync().then(() => {
            return this.ScheudleMeeting.update({

                T_CHR_Completed_Flag: '1',
                T_CHR_Is_Publish: '1',
                T_CHR_Publish_By: meetingData.created_by,
                T_Date_Published_At: new Date()

            }, {
                    where: {
                        T_INT_Schedule_Meeting_Id: meetingData.scheduleId
                    }
                }).catch(function (errLog) {

                    log.addLog('model :: meeting.js ' + errLog);
                    var err;
                    if (errLog == "SequelizeUniqueConstraintError: Validation error") {


                        err = {
                            data: 0,
                            errMsg: "Short name and full name should be unique"
                        }
                    } else if (errLog != "SequelizeUniqueConstraintError: Validation error") {


                        err = {
                            data: 0,
                            errMsg: errLog["errors"][0]["message"]
                        }

                    }
                    return err;
                });
        }).catch(function (errLog) {


            log.addLog('model :: meeting.js ' + errLog);
        });
    }
    getMeetingCount(data) {

        var condition = "";
        console.log("ccccccccccccc=", data)
        if (data.month) {
            condition = " and MONTH(tm.T_DATE_Meeting_Date) in (" + data.month + ") ";

        }
        if (data.year) {
            condition = condition + " and YEAR(tm.T_DATE_Meeting_Date) in (" + data.year + ") ";

        }
        if (data.meetingName) {
            condition = condition + " and mm.T_INT_Meeting_Id in (" + data.meetingName + ") ";

        }
        if (data.department) {
            condition = condition + "and (mm.T_CHR_Channel_Deprtment='" + data.department + "' " +
                " OR mm.T_CHR_Channel_Deprtment LIKE '" + data.department + "' + ',%' " +
                " OR mm.T_CHR_Channel_Deprtment LIKE '%,' + '" + data.department + "' + ',%' " +
                "  OR mm.T_CHR_Channel_Deprtment LIKE '%,' + '" + data.department + "') ";
        }

        if (data.role == '2') {
            if (data.type == 'meetings') {
                return sequelize.sync().then(() => {
                    return sequelize.query("select top 1 (select COUNT(tm.T_INT_Schedule_Meeting_Id)   " +
                        " from TXN_Schedule_Meetings as tm  inner join M_Meeting_Masters as mm " +
                        " on tm.T_INT_Meeting_Id=mm.T_INT_Meeting_Id " +
                        " where tm.T_CHR_Completed_Flag='0' and tm.T_CHR_Delay_Flag='0' and mm.T_INT_Convener_Id='" + data.userId + "'  " + condition + ") as scheduled_count," +

                        "  (select COUNT(tm.T_INT_Schedule_Meeting_Id)   " +
                        " from TXN_Schedule_Meetings as tm  inner join M_Meeting_Masters as mm " +
                        " on tm.T_INT_Meeting_Id=mm.T_INT_Meeting_Id " +
                        " where tm.T_CHR_Completed_Flag='1' and mm.T_INT_Convener_Id='" + data.userId + "'  " + condition + ") as completed_count," +

                        " (select COUNT(tm.T_INT_Schedule_Meeting_Id)   " +
                        " from TXN_Schedule_Meetings as tm  inner join M_Meeting_Masters as mm " +
                        " on tm.T_INT_Meeting_Id=mm.T_INT_Meeting_Id " +
                        " where tm.T_CHR_Delay_Flag='1' and tm.T_CHR_Completed_Flag='0' and mm.T_INT_Convener_Id='" + data.userId + "'  " + condition + ") as overdue_count from TXN_Schedule_Meetings as tm " +
                        " inner join M_Meeting_Masters as mm on tm.T_INT_Meeting_Id=mm.T_INT_Meeting_Id ",
                        { type: sequelize.QueryTypes.SELECT }).then((countData) => {
                            console.log("cccccccount datat==", countData)
                            return countData;
                        }).catch((err) => {
                            log.addLog('route :: Meeting.js ' + err);
                        });
                }).catch((err) => {
                    log.addLog('route :: Meeting.js ' + err);
                });
            } else {
                return sequelize.sync().then(() => {
                    return sequelize.query("select top 1 (select COUNT(tm.T_INT_Schedule_Meeting_Id)   " +
                        " from TXN_Schedule_Meetings as tm  inner join M_Meeting_Masters as mm " +
                        " on tm.T_INT_Meeting_Id=mm.T_INT_Meeting_Id " +
                        " where tm.T_CHR_Completed_Flag='0' and tm.T_CHR_Delay_Flag='0' and ((mm.T_CHR_Meeting_Chair='" + data.userId + "'" +

                        " OR mm.T_CHR_Meeting_Chair LIKE '" + data.userId + "' + ',%' " +
                        " OR mm.T_CHR_Meeting_Chair LIKE '%,' + '" + data.userId + "' + ',%' " +
                        "  OR mm.T_CHR_Meeting_Chair LIKE '%,' + '" + data.userId + "') or (mm.T_CHR_Meeting_Attendies='" + data.userId + "' " +

                        "  OR mm.T_CHR_Meeting_Attendies LIKE '" + data.userId + "' + ',%' " +
                        " OR mm.T_CHR_Meeting_Attendies LIKE '%,' + '" + data.userId + "' + ',%' " +
                        " OR mm.T_CHR_Meeting_Attendies LIKE '%,' + '" + data.userId + "'))  " + condition + ") as scheduled_count," +

                        "  (select COUNT(tm.T_INT_Schedule_Meeting_Id)   " +
                        " from TXN_Schedule_Meetings as tm  inner join M_Meeting_Masters as mm " +
                        " on tm.T_INT_Meeting_Id=mm.T_INT_Meeting_Id " +
                        " where tm.T_CHR_Completed_Flag='1' and ((mm.T_CHR_Meeting_Chair='" + data.userId + "'" +

                        " OR mm.T_CHR_Meeting_Chair LIKE '" + data.userId + "' + ',%' " +
                        " OR mm.T_CHR_Meeting_Chair LIKE '%,' + '" + data.userId + "' + ',%' " +
                        "  OR mm.T_CHR_Meeting_Chair LIKE '%,' + '" + data.userId + "') or (mm.T_CHR_Meeting_Attendies='" + data.userId + "' " +

                        "  OR mm.T_CHR_Meeting_Attendies LIKE '" + data.userId + "' + ',%' " +
                        " OR mm.T_CHR_Meeting_Attendies LIKE '%,' + '" + data.userId + "' + ',%' " +
                        " OR mm.T_CHR_Meeting_Attendies LIKE '%,' + '" + data.userId + "'))  " + condition + ") as completed_count," +

                        " (select COUNT(tm.T_INT_Schedule_Meeting_Id)   " +
                        " from TXN_Schedule_Meetings as tm  inner join M_Meeting_Masters as mm " +
                        " on tm.T_INT_Meeting_Id=mm.T_INT_Meeting_Id " +
                        " where tm.T_CHR_Delay_Flag='1' and tm.T_CHR_Completed_Flag='0' and ((mm.T_CHR_Meeting_Chair='" + data.userId + "'" +

                        " OR mm.T_CHR_Meeting_Chair LIKE '" + data.userId + "' + ',%' " +
                        " OR mm.T_CHR_Meeting_Chair LIKE '%,' + '" + data.userId + "' + ',%' " +
                        "  OR mm.T_CHR_Meeting_Chair LIKE '%,' + '" + data.userId + "') or (mm.T_CHR_Meeting_Attendies='" + data.userId + "' " +

                        "  OR mm.T_CHR_Meeting_Attendies LIKE '" + data.userId + "' + ',%' " +
                        " OR mm.T_CHR_Meeting_Attendies LIKE '%,' + '" + data.userId + "' + ',%' " +
                        " OR mm.T_CHR_Meeting_Attendies LIKE '%,' + '" + data.userId + "'))  " + condition + ") as overdue_count from TXN_Schedule_Meetings as tm " +
                        " inner join M_Meeting_Masters as mm on tm.T_INT_Meeting_Id=mm.T_INT_Meeting_Id ",
                        { type: sequelize.QueryTypes.SELECT }).then((countData) => {
                            return countData;
                        }).catch((err) => {
                            log.addLog('route :: Meeting.js ' + err);
                        });
                }).catch((err) => {
                    log.addLog('route :: Meeting.js ' + err);
                });
            }
        } else if (data.role == '3') {
            if (data.type == 'kli') {
                return sequelize.sync().then(() => {
                    return sequelize.query("select top 1 (select COUNT(tm.T_INT_Schedule_Meeting_Id)   " +
                        " from TXN_Schedule_Meetings as tm  inner join M_Meeting_Masters as mm " +
                        " on tm.T_INT_Meeting_Id=mm.T_INT_Meeting_Id " +
                        " where tm.T_CHR_Completed_Flag='0' and tm.T_CHR_Delay_Flag='0'   " + condition + ") as scheduled_count," +

                        "  (select COUNT(tm.T_INT_Schedule_Meeting_Id)   " +
                        " from TXN_Schedule_Meetings as tm  inner join M_Meeting_Masters as mm " +
                        " on tm.T_INT_Meeting_Id=mm.T_INT_Meeting_Id " +
                        " where tm.T_CHR_Completed_Flag='1'  " + condition + ") as completed_count," +

                        " (select COUNT(tm.T_INT_Schedule_Meeting_Id)   " +
                        " from TXN_Schedule_Meetings as tm  inner join M_Meeting_Masters as mm " +
                        " on tm.T_INT_Meeting_Id=mm.T_INT_Meeting_Id " +
                        " where tm.T_CHR_Delay_Flag='1' and tm.T_CHR_Completed_Flag='0'  " + condition + ") as overdue_count from TXN_Schedule_Meetings as tm " +
                        " inner join M_Meeting_Masters as mm on tm.T_INT_Meeting_Id=mm.T_INT_Meeting_Id ",
                        { type: sequelize.QueryTypes.SELECT }).then((countData) => {
                            console.log("cccccccount datat==", countData)
                            return countData;
                        }).catch((err) => {
                            console.log("11111111111111111====", err)
                            log.addLog('route :: Meeting.js ' + err);
                        });
                }).catch((err) => {
                    console.log("11111111111111111====", err)
                    log.addLog('route :: Meeting.js ' + err);
                });
            }
            else {
                return sequelize.sync().then(() => {
                    return sequelize.query("select top 1 (select COUNT(tm.T_INT_Schedule_Meeting_Id)   " +
                        " from TXN_Schedule_Meetings as tm  inner join M_Meeting_Masters as mm " +
                        " on tm.T_INT_Meeting_Id=mm.T_INT_Meeting_Id " +
                        " where tm.T_CHR_Completed_Flag='0' and tm.T_CHR_Delay_Flag='0' and ((mm.T_CHR_Meeting_Chair='" + data.userId + "'" +

                        " OR mm.T_CHR_Meeting_Chair LIKE '" + data.userId + "' + ',%' " +
                        " OR mm.T_CHR_Meeting_Chair LIKE '%,' + '" + data.userId + "' + ',%' " +
                        "  OR mm.T_CHR_Meeting_Chair LIKE '%,' + '" + data.userId + "') or (mm.T_CHR_Meeting_Attendies='" + data.userId + "' " +

                        "  OR mm.T_CHR_Meeting_Attendies LIKE '" + data.userId + "' + ',%' " +
                        " OR mm.T_CHR_Meeting_Attendies LIKE '%,' + '" + data.userId + "' + ',%' " +
                        " OR mm.T_CHR_Meeting_Attendies LIKE '%,' + '" + data.userId + "'))  " + condition + ") as scheduled_count," +

                        "  (select COUNT(tm.T_INT_Schedule_Meeting_Id)   " +
                        " from TXN_Schedule_Meetings as tm  inner join M_Meeting_Masters as mm " +
                        " on tm.T_INT_Meeting_Id=mm.T_INT_Meeting_Id " +
                        " where tm.T_CHR_Completed_Flag='1' and ((mm.T_CHR_Meeting_Chair='" + data.userId + "'" +

                        " OR mm.T_CHR_Meeting_Chair LIKE '" + data.userId + "' + ',%' " +
                        " OR mm.T_CHR_Meeting_Chair LIKE '%,' + '" + data.userId + "' + ',%' " +
                        "  OR mm.T_CHR_Meeting_Chair LIKE '%,' + '" + data.userId + "') or (mm.T_CHR_Meeting_Attendies='" + data.userId + "' " +

                        "  OR mm.T_CHR_Meeting_Attendies LIKE '" + data.userId + "' + ',%' " +
                        " OR mm.T_CHR_Meeting_Attendies LIKE '%,' + '" + data.userId + "' + ',%' " +
                        " OR mm.T_CHR_Meeting_Attendies LIKE '%,' + '" + data.userId + "'))  " + condition + ") as completed_count," +

                        " (select COUNT(tm.T_INT_Schedule_Meeting_Id)   " +
                        " from TXN_Schedule_Meetings as tm  inner join M_Meeting_Masters as mm " +
                        " on tm.T_INT_Meeting_Id=mm.T_INT_Meeting_Id " +
                        " where tm.T_CHR_Delay_Flag='1' and tm.T_CHR_Completed_Flag='0' and ((mm.T_CHR_Meeting_Chair='" + data.userId + "'" +

                        " OR mm.T_CHR_Meeting_Chair LIKE '" + data.userId + "' + ',%' " +
                        " OR mm.T_CHR_Meeting_Chair LIKE '%,' + '" + data.userId + "' + ',%' " +
                        "  OR mm.T_CHR_Meeting_Chair LIKE '%,' + '" + data.userId + "') or (mm.T_CHR_Meeting_Attendies='" + data.userId + "' " +

                        "  OR mm.T_CHR_Meeting_Attendies LIKE '" + data.userId + "' + ',%' " +
                        " OR mm.T_CHR_Meeting_Attendies LIKE '%,' + '" + data.userId + "' + ',%' " +
                        " OR mm.T_CHR_Meeting_Attendies LIKE '%,' + '" + data.userId + "'))  " + condition + ") as overdue_count from TXN_Schedule_Meetings as tm " +
                        " inner join M_Meeting_Masters as mm on tm.T_INT_Meeting_Id=mm.T_INT_Meeting_Id ",
                        { type: sequelize.QueryTypes.SELECT }).then((countData) => {
                            return countData;
                        }).catch((err) => {
                            log.addLog('route :: Meeting.js ' + err);
                        });
                }).catch((err) => {
                    log.addLog('route :: Meeting.js ' + err);
                });
            }

        }
    }
    getMeetingTemplate(data) {
        return sequelize.sync().then(() => {
            return sequelize.query("select mt.form_id,mt.T_CHR_Template_Name,mm.T_CHR_Meeting_Attendies from Meeting_Templates as mt inner join M_Meeting_Masters as mm on mm.T_INT_Meeting_Id=mt.T_INT_Meeting_Id where mt.T_INT_Meeting_Id='" + data.meetingId + "'  ;", { type: sequelize.QueryTypes.SELECT });
        });
    }

    getMeetingScheduleDetails(data) {
        return sequelize.sync().then(() => {
            return sequelize.query("select T_INT_Meeting_Id,T_INT_Meeting_Template_Id,T_CHR_For_Month_Or_Quoarter,T_DATE_Meeting_Date from TXN_Schedule_Meetings where T_INT_Meeting_Id='" + data.meetingId + "'  ;", { type: sequelize.QueryTypes.SELECT });
        });
    }

    getNotSchuledMeetingScheduler(data) {

        var d = new Date();
        return sequelize.sync().then(() => {

            return sequelize.query("select mm.T_INT_Meeting_Id,mt.T_CHR_Type_Of_Meeting_Name,mm.T_CHR_Short_Name,mm.T_CHR_Full_Name" +
                ",mm.T_INT_Frequency,mm.T_CHR_Meeting_Chair,mm.T_CHR_Meeting_Attendies,mm.T_CHR_Channel_Deprtment,mm.T_DATE_Meeting_From_Date,mm.T_DATE_Meeting_To_Date,mf.T_CHR_Frequency_Name,u.T_CHR_Name,mm.T_INT_Convener_Id  from M_Meeting_Masters as mm" +
                " inner join M_Type_Of_Meeting_Master as mt on mm.T_INT_Type_Of_Meeting=mt.T_INT_Type_Of_Meeting_Id" +
                " inner join M_Frequency_Masters as mf on mm.T_INT_Frequency=mf.T_INT_Frequency_Id " +
                " inner join M_User_Masters as u on u.T_CHR_Employee_Id=mm.T_INT_Convener_Id" +
                " where mm.T_CHR_Isdelete='0' and mm.T_CHR_Status='Enable' and mm.T_DATE_Meeting_From_Date <= '" + d.toLocaleDateString("en-US") + "' and  mm.T_DATE_Meeting_To_Date  >= '" + d.toLocaleDateString("en-US") + "' ", { type: sequelize.QueryTypes.SELECT }
            ).then((data1) => {


                data1.forEach(element1 => {
                    return sequelize.query("select T_INT_Schedule_Meeting_Id,T_INT_Meeting_Id,T_DATE_Meeting_Date from TXN_Schedule_Meetings as mm  where mm.T_CHR_Isdelete='0' and mm.T_INT_Meeting_Id='" + element1.T_INT_Meeting_Id + "'", { type: sequelize.QueryTypes.SELECT }
                    ).then((data2) => {
                        if (data2.length > 0) {
                            data2.forEach(element2 => {
                                var d1 = new Date();
                                var d2 = new Date(element2.T_DATE_Meeting_Date);

                                if (element1.T_INT_Frequency == '1') {

                                    var dd = String(d1.getDate()).padStart(2, '0');
                                    var mm = String(d1.getMonth() + 1).padStart(2, '0'); //January is 0!
                                    var yyyy = d1.getFullYear();

                                    var date1 = mm + '-' + dd + '-' + yyyy;

                                    var dd = String(d2.getDate()).padStart(2, '0');
                                    var mm = String(d2.getMonth() + 1).padStart(2, '0'); //January is 0!
                                    var yyyy = d2.getFullYear();

                                    var date2 = mm + '-' + dd + '-' + yyyy;


                                    if (element1.T_INT_Meeting_Id == element2.T_INT_Meeting_Id && date1 == date2) {

                                    } else {


                                        return sequelize.query("select T_INT_Meeting_Id from Log_Not_Schedule_Meetings as mm  where mm.T_INT_Meeting_Id='" + element1.T_INT_Meeting_Id + "' and mm.T_CHR_Frquency='" + element1.T_CHR_Frequency_Name + "' and mm.T_CHR_To_Check='" + date1 + "' ", { type: sequelize.QueryTypes.SELECT }
                                        ).then((data3) => {

                                            if (data3.length > 0) {
                                                /*      return sequelize.query(" from Log_Not_Schedule_Meetings as mm  where mm.T_INT_Meeting_Id='" + element1.T_INT_Meeting_Id + "' and mm.T_CHR_Frquency='" + element1.T_CHR_Frequency_Name + "' and mm.T_CHR_To_Check='" + date1 + "' ", { type: sequelize.QueryTypes.SELECT }
                                             ).then((d) => {
                                             })  */
                                            } else {



                                                this.logNotScheudleMeeting.create({
                                                    T_INT_Meeting_Id: element1.T_INT_Meeting_Id,
                                                    T_CHR_Meeting_Type: element1.T_CHR_Type_Of_Meeting_Name,
                                                    T_CHR_Meeting_Short_Name: element1.T_CHR_Short_Name,
                                                    T_CHR_Meeting_Full_Name: element1.T_CHR_Full_Name,
                                                    T_CHR_Frquency: element1.T_CHR_Frequency_Name,
                                                    T_CHR_Convener_Name: element1.T_CHR_Name,
                                                    T_CHR_To_Check: date1,
                                                    T_CHR_Year: '',
                                                    T_INT_Convener_Id: element1.T_INT_Convener_Id,
                                                    T_CHR_Meeting_Chair: element1.T_CHR_Meeting_Chair,
                                                    T_CHR_Meeting_Attendies: element1.T_CHR_Meeting_Attendies,
                                                    T_CHR_Channel_Deprtment: element1.T_CHR_Channel_Deprtment


                                                }).catch(function (errLog) {

                                                });

                                            }
                                        });
                                    }

                                } else if (element1.T_INT_Frequency == '2') {

                                    Date.prototype.getWeek1 = function () {
                                        var onejan = new Date(this.getFullYear(), 0, 1);
                                        return Math.ceil((((this - onejan) / 86400000) + onejan.getDay() + 1) / 7);
                                    }

                                    Date.prototype.getWeek2 = function () {
                                        var onejan = new Date(this.getFullYear(), 0, 1);
                                        return Math.ceil((((this - onejan) / 86400000) + onejan.getDay() + 1) / 7);
                                    }

                                    var today = new Date();
                                    var currentWeekNumber = today.getWeek1();

                                    var meetingDate = new Date(element2.T_DATE_Meeting_Date);
                                    var meetingWeekNumber = meetingDate.getWeek2();

                                    if (element1.T_INT_Meeting_Id == element2.T_INT_Meeting_Id && currentWeekNumber == meetingWeekNumber && today.getFullYear() == meetingDate.getFullYear()) {

                                    } else {
                                        return sequelize.query("select T_INT_Meeting_Id from Log_Not_Schedule_Meetings as mm  where mm.T_INT_Meeting_Id='" + element1.T_INT_Meeting_Id + "' and mm.T_CHR_Frquency='" + element1.T_CHR_Frequency_Name + "' and mm.T_CHR_To_Check='" + currentWeekNumber + "' and mm.T_CHR_Year='" + today.getFullYear() + "' ", { type: sequelize.QueryTypes.SELECT }
                                        ).then((data3) => {

                                            if (data3.length > 0) {

                                            } else {

                                                this.logNotScheudleMeeting.create({
                                                    T_INT_Meeting_Id: element1.T_INT_Meeting_Id,
                                                    T_CHR_Meeting_Type: element1.T_CHR_Type_Of_Meeting_Name,
                                                    T_CHR_Meeting_Short_Name: element1.T_CHR_Short_Name,
                                                    T_CHR_Meeting_Full_Name: element1.T_CHR_Full_Name,
                                                    T_CHR_Frquency: element1.T_CHR_Frequency_Name,
                                                    T_CHR_Convener_Name: element1.T_CHR_Name,
                                                    T_CHR_To_Check: currentWeekNumber,
                                                    T_CHR_Year: today.getFullYear(),
                                                    T_INT_Convener_Id: element1.T_INT_Convener_Id,
                                                    T_CHR_Meeting_Chair: element1.T_CHR_Meeting_Chair,
                                                    T_CHR_Meeting_Attendies: element1.T_CHR_Meeting_Attendies,
                                                    T_CHR_Channel_Deprtment: element1.T_CHR_Channel_Deprtment


                                                }).catch(function (errLog) {

                                                });
                                            }
                                        });
                                    }
                                } else if (element1.T_INT_Frequency == '3') {

                                    var mm = String(d1.getMonth() + 1).padStart(2, '0'); //January is 0!
                                    var yyyy1 = d1.getFullYear();

                                    var date1 = mm;


                                    var mm = String(d2.getMonth() + 1).padStart(2, '0'); //January is 0!
                                    var yyyy2 = d2.getFullYear();

                                    var date2 = mm;


                                    if (element1.T_INT_Meeting_Id == element2.T_INT_Meeting_Id && date1 == date2 && yyyy1 == yyyy2) {

                                    } else {
                                        return sequelize.query("select T_INT_Meeting_Id from Log_Not_Schedule_Meetings as mm  where mm.T_INT_Meeting_Id='" + element1.T_INT_Meeting_Id + "' and mm.T_CHR_Frquency='" + element1.T_CHR_Frequency_Name + "' and mm.T_CHR_To_Check='" + date1 + "' and mm.T_CHR_Year='" + yyyy1 + "' ", { type: sequelize.QueryTypes.SELECT }
                                        ).then((data3) => {

                                            if (data3.length > 0) {

                                            } else {
                                                this.logNotScheudleMeeting.create({
                                                    T_INT_Meeting_Id: element1.T_INT_Meeting_Id,
                                                    T_CHR_Meeting_Type: element1.T_CHR_Type_Of_Meeting_Name,
                                                    T_CHR_Meeting_Short_Name: element1.T_CHR_Short_Name,
                                                    T_CHR_Meeting_Full_Name: element1.T_CHR_Full_Name,
                                                    T_CHR_Frquency: element1.T_CHR_Frequency_Name,
                                                    T_CHR_Convener_Name: element1.T_CHR_Name,
                                                    T_CHR_To_Check: date1,
                                                    T_CHR_Year: yyyy1,
                                                    T_INT_Convener_Id: element1.T_INT_Convener_Id,
                                                    T_CHR_Meeting_Chair: element1.T_CHR_Meeting_Chair,
                                                    T_CHR_Meeting_Attendies: element1.T_CHR_Meeting_Attendies,
                                                    T_CHR_Channel_Deprtment: element1.T_CHR_Channel_Deprtment


                                                }).catch(function (errLog) {

                                                });

                                            }
                                        });
                                    }
                                } else if (element1.T_INT_Frequency == '4') {

                                    var mm = String(d1.getMonth() + 1).padStart(2, '0'); //January is 0!
                                    var yyyy1 = d1.getFullYear();
                                    var date1;

                                    if (mm == '01' || mm == '02' || mm == '03') {
                                        date1 = '4';
                                    } else if (mm == '04' || mm == '05' || mm == '06') {
                                        date1 = '1';
                                    } else if (mm == '07' || mm == '08' || mm == '09') {
                                        date1 = '2';
                                    } else if (mm == '10' || mm == '11' || mm == '12') {
                                        date1 = '3';
                                    }
                                    var mm = String(d2.getMonth() + 1).padStart(2, '0'); //January is 0!
                                    var yyyy2 = d2.getFullYear();
                                    var date2;

                                    if (mm == '01' || mm == '02' || mm == '03') {
                                        date2 = '4';
                                    } else if (mm == '04' || mm == '05' || mm == '06') {
                                        date2 = '1';
                                    } else if (mm == '07' || mm == '08' || mm == '09') {
                                        date2 = '2';
                                    } else if (mm == '10' || mm == '11' || mm == '12') {
                                        date2 = '3';
                                    }


                                    if (element1.T_INT_Meeting_Id == element2.T_INT_Meeting_Id && date1 == date2 && yyyy1 == yyyy2) {

                                    } else {
                                        return sequelize.query("select T_INT_Meeting_Id from Log_Not_Schedule_Meetings as mm  where mm.T_INT_Meeting_Id='" + element1.T_INT_Meeting_Id + "' and mm.T_CHR_Frquency='" + element1.T_CHR_Frequency_Name + "' and mm.T_CHR_To_Check='" + date1 + "' and mm.T_CHR_Year='" + yyyy1 + "' ", { type: sequelize.QueryTypes.SELECT }
                                        ).then((data3) => {

                                            if (data3.length > 0) {

                                            } else {
                                                this.logNotScheudleMeeting.create({
                                                    T_INT_Meeting_Id: element1.T_INT_Meeting_Id,
                                                    T_CHR_Meeting_Type: element1.T_CHR_Type_Of_Meeting_Name,
                                                    T_CHR_Meeting_Short_Name: element1.T_CHR_Short_Name,
                                                    T_CHR_Meeting_Full_Name: element1.T_CHR_Full_Name,
                                                    T_CHR_Frquency: element1.T_CHR_Frequency_Name,
                                                    T_CHR_Convener_Name: element1.T_CHR_Name,
                                                    T_CHR_To_Check: date1,
                                                    T_CHR_Year: yyyy1,
                                                    T_INT_Convener_Id: element1.T_INT_Convener_Id,
                                                    T_CHR_Meeting_Chair: element1.T_CHR_Meeting_Chair,
                                                    T_CHR_Meeting_Attendies: element1.T_CHR_Meeting_Attendies,
                                                    T_CHR_Channel_Deprtment: element1.T_CHR_Channel_Deprtment


                                                }).catch(function (errLog) {

                                                });

                                            }
                                        });
                                    }
                                } else if (element1.T_INT_Frequency == '5') {

                                    var mm = String(d1.getMonth() + 1).padStart(2, '0'); //January is 0!
                                    var yyyy1 = d1.getFullYear();
                                    var date1;

                                    if (mm == '01' || mm == '02' || mm == '03' || mm == '04') {
                                        date1 = '1';
                                    } else if (mm == '05' || mm == '06' || mm == '07' || mm == '08') {
                                        date1 = '2';
                                    } else if (mm == '08' || mm == '09' || mm == '10' || mm == '11') {
                                        date1 = '3';
                                    }
                                    var mm = String(d2.getMonth() + 1).padStart(2, '0'); //January is 0!
                                    var yyyy2 = d2.getFullYear();
                                    var date2;

                                    if (mm == '01' || mm == '02' || mm == '03' || mm == '04') {
                                        date2 = '1';
                                    } else if (mm == '05' || mm == '06' || mm == '07' || mm == '08') {
                                        date2 = '2';
                                    } else if (mm == '08' || mm == '09' || mm == '10' || mm == '11') {
                                        date2 = '3';
                                    }


                                    if (element1.T_INT_Meeting_Id == element2.T_INT_Meeting_Id && date1 == date2 && yyyy1 == yyyy2) {

                                    } else {
                                        return sequelize.query("select T_INT_Meeting_Id from Log_Not_Schedule_Meetings as mm  where mm.T_INT_Meeting_Id='" + element1.T_INT_Meeting_Id + "' and mm.T_CHR_Frquency='" + element1.T_CHR_Frequency_Name + "' and mm.T_CHR_To_Check='" + date1 + "' and mm.T_CHR_Year='" + yyyy1 + "' ", { type: sequelize.QueryTypes.SELECT }
                                        ).then((data3) => {

                                            if (data3.length > 0) {

                                            } else {

                                                this.logNotScheudleMeeting.create({
                                                    T_INT_Meeting_Id: element1.T_INT_Meeting_Id,
                                                    T_CHR_Meeting_Type: element1.T_CHR_Type_Of_Meeting_Name,
                                                    T_CHR_Meeting_Short_Name: element1.T_CHR_Short_Name,
                                                    T_CHR_Meeting_Full_Name: element1.T_CHR_Full_Name,
                                                    T_CHR_Frquency: element1.T_CHR_Frequency_Name,
                                                    T_CHR_Convener_Name: element1.T_CHR_Name,
                                                    T_CHR_To_Check: date1,
                                                    T_CHR_Year: yyyy1,
                                                    T_INT_Convener_Id: element1.T_INT_Convener_Id,
                                                    T_CHR_Meeting_Chair: element1.T_CHR_Meeting_Chair,
                                                    T_CHR_Meeting_Attendies: element1.T_CHR_Meeting_Attendies,
                                                    T_CHR_Channel_Deprtment: element1.T_CHR_Channel_Deprtment


                                                }).catch(function (errLog) {

                                                });

                                            }
                                        });
                                    }
                                } else if (element1.T_INT_Frequency == '6') {

                                    var mm = String(d1.getMonth() + 1).padStart(2, '0'); //January is 0!
                                    var yyyy1 = d1.getFullYear();


                                    var mm = String(d2.getMonth() + 1).padStart(2, '0'); //January is 0!
                                    var yyyy2 = d2.getFullYear();

                                    if (element1.T_INT_Meeting_Id == element2.T_INT_Meeting_Id && yyyy1 == yyyy2) {

                                    } else {
                                        return sequelize.query("select T_INT_Meeting_Id from Log_Not_Schedule_Meetings as mm  where mm.T_INT_Meeting_Id='" + element1.T_INT_Meeting_Id + "' and mm.T_CHR_Frquency='" + element1.T_CHR_Frequency_Name + "' and mm.T_CHR_To_Check='" + yyyy1 + "' and mm.T_CHR_Year='" + yyyy1 + "' ", { type: sequelize.QueryTypes.SELECT }
                                        ).then((data3) => {

                                            if (data3.length > 0) {

                                            } else {

                                                this.logNotScheudleMeeting.create({
                                                    T_INT_Meeting_Id: element1.T_INT_Meeting_Id,
                                                    T_CHR_Meeting_Type: element1.T_CHR_Type_Of_Meeting_Name,
                                                    T_CHR_Meeting_Short_Name: element1.T_CHR_Short_Name,
                                                    T_CHR_Meeting_Full_Name: element1.T_CHR_Full_Name,
                                                    T_CHR_Frquency: element1.T_CHR_Frequency_Name,
                                                    T_CHR_Convener_Name: element1.T_CHR_Name,
                                                    T_CHR_To_Check: yyyy1,
                                                    T_CHR_Year: yyyy1,
                                                    T_INT_Convener_Id: element1.T_INT_Convener_Id,
                                                    T_CHR_Meeting_Chair: element1.T_CHR_Meeting_Chair,
                                                    T_CHR_Meeting_Attendies: element1.T_CHR_Meeting_Attendies,
                                                    T_CHR_Channel_Deprtment: element1.T_CHR_Channel_Deprtment


                                                }).catch(function (errLog) {

                                                });

                                            }
                                        });
                                    }
                                }

                            });
                        } else {
                            var d1 = new Date();

                            if (element1.T_INT_Frequency == '1') {

                                var dd = String(d1.getDate()).padStart(2, '0');
                                var mm = String(d1.getMonth() + 1).padStart(2, '0'); //January is 0!
                                var yyyy = d1.getFullYear();

                                var date1 = mm + '-' + dd + '-' + yyyy;
                                this.logNotScheudleMeeting.create({
                                    T_INT_Meeting_Id: element1.T_INT_Meeting_Id,
                                    T_CHR_Meeting_Type: element1.T_CHR_Type_Of_Meeting_Name,
                                    T_CHR_Meeting_Short_Name: element1.T_CHR_Short_Name,
                                    T_CHR_Meeting_Full_Name: element1.T_CHR_Full_Name,
                                    T_CHR_Frquency: element1.T_CHR_Frequency_Name,
                                    T_CHR_Convener_Name: element1.T_CHR_Name,
                                    T_CHR_To_Check: date1,
                                    T_CHR_Year: '',
                                    T_INT_Convener_Id: element1.T_INT_Convener_Id,
                                    T_CHR_Meeting_Chair: element1.T_CHR_Meeting_Chair,
                                    T_CHR_Meeting_Attendies: element1.T_CHR_Meeting_Attendies,
                                    T_CHR_Channel_Deprtment: element1.T_CHR_Channel_Deprtment


                                }).catch(function (errLog) {

                                });
                            } else if (element1.T_INT_Frequency == '2') {

                                Date.prototype.getWeek1 = function () {
                                    var onejan = new Date(this.getFullYear(), 0, 1);
                                    return Math.ceil((((this - onejan) / 86400000) + onejan.getDay() + 1) / 7);
                                }

                                var today = new Date();
                                var currentWeekNumber = today.getWeek1();
                                this.logNotScheudleMeeting.create({
                                    T_INT_Meeting_Id: element1.T_INT_Meeting_Id,
                                    T_CHR_Meeting_Type: element1.T_CHR_Type_Of_Meeting_Name,
                                    T_CHR_Meeting_Short_Name: element1.T_CHR_Short_Name,
                                    T_CHR_Meeting_Full_Name: element1.T_CHR_Full_Name,
                                    T_CHR_Frquency: element1.T_CHR_Frequency_Name,
                                    T_CHR_Convener_Name: element1.T_CHR_Name,
                                    T_CHR_To_Check: currentWeekNumber,
                                    T_CHR_Year: today.getFullYear(),
                                    T_INT_Convener_Id: element1.T_INT_Convener_Id,
                                    T_CHR_Meeting_Chair: element1.T_CHR_Meeting_Chair,
                                    T_CHR_Meeting_Attendies: element1.T_CHR_Meeting_Attendies,
                                    T_CHR_Channel_Deprtment: element1.T_CHR_Channel_Deprtment


                                }).catch(function (errLog) {

                                });
                            } else if (element1.T_INT_Frequency == '3') {

                                var mm = String(d1.getMonth() + 1).padStart(2, '0'); //January is 0!
                                var yyyy1 = d1.getFullYear();

                                var date1 = mm;

                                this.logNotScheudleMeeting.create({
                                    T_INT_Meeting_Id: element1.T_INT_Meeting_Id,
                                    T_CHR_Meeting_Type: element1.T_CHR_Type_Of_Meeting_Name,
                                    T_CHR_Meeting_Short_Name: element1.T_CHR_Short_Name,
                                    T_CHR_Meeting_Full_Name: element1.T_CHR_Full_Name,
                                    T_CHR_Frquency: element1.T_CHR_Frequency_Name,
                                    T_CHR_Convener_Name: element1.T_CHR_Name,
                                    T_CHR_To_Check: date1,
                                    T_CHR_Year: yyyy1,
                                    T_INT_Convener_Id: element1.T_INT_Convener_Id,
                                    T_CHR_Meeting_Chair: element1.T_CHR_Meeting_Chair,
                                    T_CHR_Meeting_Attendies: element1.T_CHR_Meeting_Attendies,
                                    T_CHR_Channel_Deprtment: element1.T_CHR_Channel_Deprtment


                                }).catch(function (errLog) {

                                });
                            } else if (element1.T_INT_Frequency == '4') {

                                var mm = String(d1.getMonth() + 1).padStart(2, '0'); //January is 0!
                                var yyyy1 = d1.getFullYear();
                                var date1;

                                if (mm == '01' || mm == '02' || mm == '03') {
                                    date1 = '4';
                                } else if (mm == '04' || mm == '05' || mm == '06') {
                                    date1 = '1';
                                } else if (mm == '07' || mm == '08' || mm == '09') {
                                    date1 = '2';
                                } else if (mm == '10' || mm == '11' || mm == '12') {
                                    date1 = '3';
                                }

                                this.logNotScheudleMeeting.create({
                                    T_INT_Meeting_Id: element1.T_INT_Meeting_Id,
                                    T_CHR_Meeting_Type: element1.T_CHR_Type_Of_Meeting_Name,
                                    T_CHR_Meeting_Short_Name: element1.T_CHR_Short_Name,
                                    T_CHR_Meeting_Full_Name: element1.T_CHR_Full_Name,
                                    T_CHR_Frquency: element1.T_CHR_Frequency_Name,
                                    T_CHR_Convener_Name: element1.T_CHR_Name,
                                    T_CHR_To_Check: date1,
                                    T_CHR_Year: yyyy1,
                                    T_INT_Convener_Id: element1.T_INT_Convener_Id,
                                    T_CHR_Meeting_Chair: element1.T_CHR_Meeting_Chair,
                                    T_CHR_Meeting_Attendies: element1.T_CHR_Meeting_Attendies,
                                    T_CHR_Channel_Deprtment: element1.T_CHR_Channel_Deprtment


                                }).catch(function (errLog) {

                                });
                            } else if (element1.T_INT_Frequency == '5') {

                                var mm = String(d1.getMonth() + 1).padStart(2, '0'); //January is 0!
                                var yyyy1 = d1.getFullYear();
                                var date1;

                                if (mm == '01' || mm == '02' || mm == '03' || mm == '04') {
                                    date1 = '1';
                                } else if (mm == '05' || mm == '06' || mm == '07' || mm == '08') {
                                    date1 = '2';
                                } else if (mm == '08' || mm == '09' || mm == '10' || mm == '11') {
                                    date1 = '3';
                                }

                                this.logNotScheudleMeeting.create({
                                    T_INT_Meeting_Id: element1.T_INT_Meeting_Id,
                                    T_CHR_Meeting_Type: element1.T_CHR_Type_Of_Meeting_Name,
                                    T_CHR_Meeting_Short_Name: element1.T_CHR_Short_Name,
                                    T_CHR_Meeting_Full_Name: element1.T_CHR_Full_Name,
                                    T_CHR_Frquency: element1.T_CHR_Frequency_Name,
                                    T_CHR_Convener_Name: element1.T_CHR_Name,
                                    T_CHR_To_Check: date1,
                                    T_CHR_Year: yyyy1,
                                    T_INT_Convener_Id: element1.T_INT_Convener_Id,
                                    T_CHR_Meeting_Chair: element1.T_CHR_Meeting_Chair,
                                    T_CHR_Meeting_Attendies: element1.T_CHR_Meeting_Attendies,
                                    T_CHR_Channel_Deprtment: element1.T_CHR_Channel_Deprtment


                                }).catch(function (errLog) {

                                });
                            } else if (element1.T_INT_Frequency == '6') {

                                var mm = String(d1.getMonth() + 1).padStart(2, '0'); //January is 0!
                                var yyyy1 = d1.getFullYear();

                                this.logNotScheudleMeeting.create({
                                    T_INT_Meeting_Id: element1.T_INT_Meeting_Id,
                                    T_CHR_Meeting_Type: element1.T_CHR_Type_Of_Meeting_Name,
                                    T_CHR_Meeting_Short_Name: element1.T_CHR_Short_Name,
                                    T_CHR_Meeting_Full_Name: element1.T_CHR_Full_Name,
                                    T_CHR_Frquency: element1.T_CHR_Frequency_Name,
                                    T_CHR_Convener_Name: element1.T_CHR_Name,
                                    T_CHR_To_Check: yyyy1,
                                    T_CHR_Year: yyyy1,
                                    T_INT_Convener_Id: element1.T_INT_Convener_Id,
                                    T_CHR_Meeting_Chair: element1.T_CHR_Meeting_Chair,
                                    T_CHR_Meeting_Attendies: element1.T_CHR_Meeting_Attendies,
                                    T_CHR_Channel_Deprtment: element1.T_CHR_Channel_Deprtment


                                }).catch(function (errLog) {

                                });
                            }


                        }
                    });



                });
                /*
                } */





            }).catch((err) => {

            });

        });
    }

    deleteLogScheduleMeeting(meetingId, Schedule_Meeting_Id) {
        return sequelize.sync().then(() => {

            return sequelize.query("select mm.T_INT_Meeting_Id,mt.T_CHR_Type_Of_Meeting_Name,mm.T_CHR_Short_Name,mm.T_CHR_Full_Name" +
                ",mm.T_INT_Frequency,mm.T_DATE_Meeting_From_Date,mm.T_DATE_Meeting_To_Date,mf.T_CHR_Frequency_Name,u.T_CHR_Name,mm.T_INT_Convener_Id  from M_Meeting_Masters as mm" +
                " inner join M_Type_Of_Meeting_Master as mt on mm.T_INT_Type_Of_Meeting=mt.T_INT_Type_Of_Meeting_Id" +
                " inner join M_Frequency_Masters as mf on mm.T_INT_Frequency=mf.T_INT_Frequency_Id " +
                " inner join M_User_Masters as u on u.T_CHR_Employee_Id=mm.T_INT_Convener_Id" +
                " where mm.T_CHR_Isdelete='0' and mm.T_CHR_Status='Enable' and mm.T_INT_Meeting_Id='" + meetingId + "' ", { type: sequelize.QueryTypes.SELECT }
            ).then((data1) => {

                data1.forEach(element1 => {
                    return sequelize.query("select T_INT_Schedule_Meeting_Id,T_INT_Meeting_Id,T_DATE_Meeting_Date from TXN_Schedule_Meetings as mm  where mm.T_CHR_Isdelete='0' and mm.T_INT_Meeting_Id='" + element1.T_INT_Meeting_Id + "'", { type: sequelize.QueryTypes.SELECT }
                    ).then((data2) => {
                        if (data2.length > 0) {
                            data2.forEach(element2 => {

                                var d2 = new Date(element2.T_DATE_Meeting_Date);

                                if (element1.T_INT_Frequency == '1') {

                                    var dd = String(d2.getDate()).padStart(2, '0');
                                    var mm = String(d2.getMonth() + 1).padStart(2, '0'); //January is 0!
                                    var yyyy = d2.getFullYear();

                                    var date2 = mm + '-' + dd + '-' + yyyy;

                                    return sequelize.query("select T_INT_Meeting_Id from Log_Not_Schedule_Meetings as mm  where mm.T_INT_Meeting_Id='" + element1.T_INT_Meeting_Id + "' and mm.T_CHR_Frquency='" + element1.T_CHR_Frequency_Name + "' and mm.T_CHR_To_Check='" + date2 + "' ", { type: sequelize.QueryTypes.SELECT }
                                    ).then((data3) => {

                                        if (data3.length > 0) {
                                            return sequelize.query("delete from Log_Not_Schedule_Meetings  where T_INT_Meeting_Id='" + element1.T_INT_Meeting_Id + "' and T_CHR_Frquency='" + element1.T_CHR_Frequency_Name + "' and T_CHR_To_Check='" + date2 + "' ", { type: sequelize.QueryTypes.SELECT }
                                            ).then((d) => {
                                                return sequelize.query("update TXN_Schedule_Meetings set T_CHR_Delay_Flag='1'  where T_INT_Meeting_Id='" + element1.T_INT_Meeting_Id + "' and T_INT_Schedule_Meeting_Id='" + Schedule_Meeting_Id + "' ", { type: sequelize.QueryTypes.SELECT }
                                                ).then((d) => {
                                                });
                                            })
                                        }
                                    });


                                } else if (element1.T_INT_Frequency == '2') {



                                    Date.prototype.getWeek2 = function () {
                                        var onejan = new Date(this.getFullYear(), 0, 1);
                                        return Math.ceil((((this - onejan) / 86400000) + onejan.getDay() + 1) / 7);
                                    }


                                    var meetingDate = new Date(element2.T_DATE_Meeting_Date);
                                    var meetingWeekNumber = meetingDate.getWeek2();

                                    return sequelize.query("select T_INT_Meeting_Id from Log_Not_Schedule_Meetings as mm  where mm.T_INT_Meeting_Id='" + element1.T_INT_Meeting_Id + "' and mm.T_CHR_Frquency='" + element1.T_CHR_Frequency_Name + "' and mm.T_CHR_To_Check='" + meetingWeekNumber + "' ", { type: sequelize.QueryTypes.SELECT }
                                    ).then((data3) => {

                                        if (data3.length > 0) {
                                            return sequelize.query("delete from Log_Not_Schedule_Meetings where T_INT_Meeting_Id='" + element1.T_INT_Meeting_Id + "' and T_CHR_Frquency='" + element1.T_CHR_Frequency_Name + "' and T_CHR_To_Check='" + meetingWeekNumber + "' ", { type: sequelize.QueryTypes.SELECT }
                                            ).then((d) => {
                                                return sequelize.query("update TXN_Schedule_Meetings set T_CHR_Delay_Flag='1'  where T_INT_Meeting_Id='" + element1.T_INT_Meeting_Id + "' and T_INT_Schedule_Meeting_Id='" + Schedule_Meeting_Id + "' ", { type: sequelize.QueryTypes.SELECT }
                                                ).then((d) => {
                                                });
                                            })
                                        }
                                    });

                                } else if (element1.T_INT_Frequency == '3') {


                                    var mm = String(d2.getMonth() + 1).padStart(2, '0'); //January is 0!
                                    var yyyy2 = d2.getFullYear();

                                    var date2 = mm;
                                    return sequelize.query("select T_INT_Meeting_Id from Log_Not_Schedule_Meetings as mm  where mm.T_INT_Meeting_Id='" + element1.T_INT_Meeting_Id + "' and mm.T_CHR_Frquency='" + element1.T_CHR_Frequency_Name + "' and mm.T_CHR_To_Check='" + date2 + "' ", { type: sequelize.QueryTypes.SELECT }
                                    ).then((data3) => {

                                        if (data3.length > 0) {
                                            return sequelize.query("delete from Log_Not_Schedule_Meetings  where T_INT_Meeting_Id='" + element1.T_INT_Meeting_Id + "' and T_CHR_Frquency='" + element1.T_CHR_Frequency_Name + "' and T_CHR_To_Check='" + date2 + "' ", { type: sequelize.QueryTypes.SELECT }
                                            ).then((d) => {
                                                return sequelize.query("update TXN_Schedule_Meetings set T_CHR_Delay_Flag='1'  where T_INT_Meeting_Id='" + element1.T_INT_Meeting_Id + "' and T_INT_Schedule_Meeting_Id='" + Schedule_Meeting_Id + "' ", { type: sequelize.QueryTypes.SELECT }
                                                ).then((d) => {
                                                });
                                            })
                                        }
                                    });

                                } else if (element1.T_INT_Frequency == '4') {


                                    var mm = String(d2.getMonth() + 1).padStart(2, '0'); //January is 0!
                                    var yyyy2 = d2.getFullYear();
                                    var date2;

                                    if (mm == '01' || mm == '02' || mm == '03') {
                                        date2 = '4';
                                    } else if (mm == '04' || mm == '05' || mm == '06') {
                                        date2 = '1';
                                    } else if (mm == '07' || mm == '08' || mm == '09') {
                                        date2 = '2';
                                    } else if (mm == '10' || mm == '11' || mm == '12') {
                                        date2 = '3';
                                    }

                                    return sequelize.query("select T_INT_Meeting_Id from Log_Not_Schedule_Meetings as mm  where mm.T_INT_Meeting_Id='" + element1.T_INT_Meeting_Id + "' and mm.T_CHR_Frquency='" + element1.T_CHR_Frequency_Name + "' and mm.T_CHR_To_Check='" + date2 + "' ", { type: sequelize.QueryTypes.SELECT }
                                    ).then((data3) => {

                                        if (data3.length > 0) {
                                            return sequelize.query("delete from Log_Not_Schedule_Meetings  where T_INT_Meeting_Id='" + element1.T_INT_Meeting_Id + "' and T_CHR_Frquency='" + element1.T_CHR_Frequency_Name + "' and T_CHR_To_Check='" + date2 + "' ", { type: sequelize.QueryTypes.SELECT }
                                            ).then((d) => {
                                                return sequelize.query("update TXN_Schedule_Meetings set T_CHR_Delay_Flag='1'  where T_INT_Meeting_Id='" + element1.T_INT_Meeting_Id + "' and T_INT_Schedule_Meeting_Id='" + Schedule_Meeting_Id + "' ", { type: sequelize.QueryTypes.SELECT }
                                                ).then((d) => {
                                                });
                                            })
                                        }
                                    });

                                } else if (element1.T_INT_Frequency == '5') {


                                    var mm = String(d2.getMonth() + 1).padStart(2, '0'); //January is 0!
                                    var yyyy2 = d2.getFullYear();
                                    var date2;

                                    if (mm == '01' || mm == '02' || mm == '03' || mm == '04') {
                                        date2 = '1';
                                    } else if (mm == '05' || mm == '06' || mm == '07' || mm == '08') {
                                        date2 = '2';
                                    } else if (mm == '08' || mm == '09' || mm == '10' || mm == '11') {
                                        date2 = '3';
                                    }

                                    return sequelize.query("select T_INT_Meeting_Id from Log_Not_Schedule_Meetings as mm  where mm.T_INT_Meeting_Id='" + element1.T_INT_Meeting_Id + "' and mm.T_CHR_Frquency='" + element1.T_CHR_Frequency_Name + "' and mm.T_CHR_To_Check='" + date2 + "' ", { type: sequelize.QueryTypes.SELECT }
                                    ).then((data3) => {

                                        if (data3.length > 0) {
                                            return sequelize.query("delete from Log_Not_Schedule_Meetings where T_INT_Meeting_Id='" + element1.T_INT_Meeting_Id + "' and T_CHR_Frquency='" + element1.T_CHR_Frequency_Name + "' and T_CHR_To_Check='" + date2 + "' ", { type: sequelize.QueryTypes.SELECT }
                                            ).then((d) => {
                                                return sequelize.query("update TXN_Schedule_Meetings set T_CHR_Delay_Flag='1'  where T_INT_Meeting_Id='" + element1.T_INT_Meeting_Id + "' and T_INT_Schedule_Meeting_Id='" + Schedule_Meeting_Id + "' ", { type: sequelize.QueryTypes.SELECT }
                                                ).then((d) => {
                                                });
                                            })
                                        }
                                    });

                                } else if (element1.T_INT_Frequency == '6') {



                                    var mm = String(d2.getMonth() + 1).padStart(2, '0'); //January is 0!
                                    var yyyy2 = d2.getFullYear();

                                    return sequelize.query("select T_INT_Meeting_Id from Log_Not_Schedule_Meetings as mm  where mm.T_INT_Meeting_Id='" + element1.T_INT_Meeting_Id + "' and mm.T_CHR_Frquency='" + element1.T_CHR_Frequency_Name + "' and mm.T_CHR_To_Check='" + date2 + "' ", { type: sequelize.QueryTypes.SELECT }
                                    ).then((data3) => {

                                        if (data3.length > 0) {
                                            return sequelize.query("delete from Log_Not_Schedule_Meetings where T_INT_Meeting_Id='" + element1.T_INT_Meeting_Id + "' and T_CHR_Frquency='" + element1.T_CHR_Frequency_Name + "' and T_CHR_To_Check='" + date2 + "' ", { type: sequelize.QueryTypes.SELECT }
                                            ).then((d) => {
                                                return sequelize.query("update TXN_Schedule_Meetings set T_CHR_Delay_Flag='1'  where T_INT_Meeting_Id='" + element1.T_INT_Meeting_Id + "' and T_INT_Schedule_Meeting_Id='" + Schedule_Meeting_Id + "' ", { type: sequelize.QueryTypes.SELECT }
                                                ).then((d) => {
                                                });
                                            })
                                        }
                                    });

                                }

                            });
                        } else {



                        }
                    });



                });
                /*
                } */





            }).catch((err) => {

            });

        });
    }
};