var express = require('express');
var router = express.Router();
var bcrypt = require('bcryptjs');
var jwt = require('jsonwebtoken');
const Joi = require('joi');
var Form = require('../model/forms');
// var userAccess = require('../model/user');
// var Applications = require('../model/application');
var multer = require('multer');
// var DIR = './uploads/';
var path = require('path');
var myLogger = require('../middleware.js');
var MyLogModel = require('../model/myLog');
var log = new MyLogModel();
var errMsg="Server error! please try again or contact to administrator";
// const schema = Joi.object().keys({
//     requestId: Joi.required().not(''),
//     requestName: Joi.required().not(''),
//     requestDescription: Joi.required().not(''),
//     requestRate: Joi.required().not(''),
//     requestQuantity: Joi.required().not(''),

// });

const schema = Joi.object().keys({
    Name: Joi.string().max(50).not(''),
    Title: Joi.string().max(50).not(''),

    version: Joi.string().max(20).not(''),
    app_id: Joi.required().not(''),

    Table_Name: Joi.string().max(50).not(''),
    formJson_string: Joi.allow(''),
});
const schema1 = Joi.object().keys({
    Name: Joi.string().max(50).not(''),
    Title: Joi.string().max(50).not(''),

    version: Joi.string().max(20).not(''),

    Table_Name: Joi.string().max(50).not(''),
    formJson_string: Joi.not(''),

});

router.post('/saveForm', myLogger, function (req, res, next) {

    var request = new Form();

    var Data = {
        Name: req.body.name,
        Title: req.body.title,

        version: req.body.version,
        app_id: req.body.app_id,

        Table_Name: req.body.proposedTableName,

        formJson_string: req.body.json_string,
        userId: req.body.userId
    };

    Joi.validate({
        Name: req.body.name,
        Title: req.body.title,

        version: req.body.version,
        app_id: req.body.app_id,

        Table_Name: req.body.proposedTableName,
        formJson_string: req.body.json_string,

    }, schema, function (err, requestData) {
        if (err) {
            buildResponse(res, 200, {
                error: true,
                message: err['details'][0]["message"]
            });
        }
        else {
            request.saveForm(Data)
                .then((data) => {
                    if (data["data"] == 0) {
                        buildResponse(res, 200, {
                            error: true,
                            message: data["errMsg"]
                        });
                    } else {
                        buildResponse(res, 200, {
                            error: false,
                            message: "Stored Successfully"
                        });

                    }


                }).catch((errLog) => {
                    log.addLog('route :: form.js ' + errLog);
                    buildResponse(res, 500, {
                        error: true,
                        message: "server error"
                    });
                });
        }
    });
});

router.get('/getAPI/:id', myLogger, function (req, res, next) {
    var formId = req.params.id;
    var request = new Form();

    request.getAPIS(formId).then((requestData) => {
        buildResponse(res, 200, {
            error: false,
            message: "Successfully display!",
            RequestDataAPI: requestData
        });
    });
});

router.get('/getForms/:id', myLogger, function (req, res, next) {
    var appId = req.params.id;
    var request = new Form();
    request.getForms(appId).then((requestData) => {

        buildResponse(res, 200, {
            error: false,
            message: "Successfully display!",
            RequestData: requestData
        });
    });
});

router.post('/getForms', myLogger, function (req, res, next) {
    var request = new Form();
    var reqData = {
        formId: req.body.formId,

    }
    request.getFormById(reqData)
        .then((requestData) => {
            buildResponse(res, 200, {
                error: false,
                message: 'Forms Fetched successfully',
                RequestData: requestData
            });
        }).catch((err) => {
            buildResponse(res, 404, {
                error: false,
                message: 'Failed to Fetch',

            });
        });
});

router.post('/getFormsData', myLogger, function (req, res, next) {
    var request = new Form();
    var reqData = {
        formId: req.body.formId,
        appId: req.body.appId,
    }
    request.getFormById(reqData)
        .then((requestData) => {
            buildResponse(res, 200, {
                error: false,
                message: 'Forms Fetched successfully',
                RequestData: requestData
            });
        }).catch((err) => {
            buildResponse(res, 404, {
                error: false,
                message: 'Failed to Fetch',

            });
        });
});

router.post('/updateForm/:id', myLogger, function (req, res, next) {
    var formId = req.params.id;

    var request = new Form();

    var Data = {
        Name: req.body.name,
        Title: req.body.title,
        version: req.body.version,
        Table_Name: req.body.proposedTableName,
        formJson_string: req.body.json_string,
        status: req.body.status,
        updtaedBy: req.body.updtaedBy
    };
    Joi.validate({
        Name: req.body.name,
        Title: req.body.title,

        version: req.body.version,

        Table_Name: req.body.proposedTableName,

        formJson_string: req.body.json_string,
    }, schema1, function (err, requestData) {
        if (err) {
            buildResponse(res, 200, {
                error: true,
                message: err['details'][0]["message"]
            });
        }
        else {
            request.updateForm(Data, formId)
                .then((data) => {
                    if (data["data"] == 0) {
                        buildResponse(res, 200, {
                            error: true,
                            message: data["errMsg"]
                        });
                    } else {
                        buildResponse(res, 200, {
                            error: false,
                            message: "Updated Successfully"
                        });

                    }
                }).catch((errLog) => {
                    log.addLog('route :: form.js ' + errLog);
                    buildResponse(res, 500, {
                        error: true,
                        message: "server error"
                    });
                });
        }
    });
});

router.get('/deleteForms/:id', myLogger, function (req, res, next) {
    var request = new Form();
    var reqData = req.params.id;
    request.deleteForm(reqData)
        .then((requestData) => {
            buildResponse(res, 200, {
                error: false,
                message: 'Forms Fetched successfully',
                RequestData: requestData
            });
        }).catch((err) => {
            buildResponse(res, 404, {
                error: false,
                message: 'Failed to Fetch',

            });
        });
});

router.get('/getApplicationForm/:id', myLogger, function (req, res, next) {
    var request = new Form();
    var reqData = req.params.id;

    request.getApplicationForm(reqData)
        .then((requestData) => {
            buildResponse(res, 200, {
                error: false,
                message: 'Forms Fetched successfully',
                RequestData: requestData
            });
        }).catch((err) => {
            buildResponse(res, 404, {
                error: false,
                message: 'Failed to Fetch',

            });
        });
});

router.get('/tableDetailstable/:column/:table/:whereSELECT/:connectionId', myLogger, function (req, res, next) {
    console.log("hiiiiii");
    var request = new Form();
    var table = req.params.table;
    var columns = req.params.column;
    var whereSELECT = req.params.whereSELECT;
    var connectionId = "1";


    request.getDatabaseById(connectionId).then((data) => {
        datad = {
            error: false,
            message: 'Successfully display!',
            responseData: data
        }
        console.log("in fortable", datad);
        request.tableDetailstable(table, columns, whereSELECT, datad).then((data) => {
            buildResponse(res, 200, {
                error: false,
                message: "Form Updated Successfully",
                vdata: data
            });
        }).catch((err) => {
            buildResponse(res, 200, {
                error: true,
                message: "Fail to Update",
            });
        });
    });
});

router.get('/DynamictableDetails/:column/:table/:scheduledId', myLogger, function (req, res, next) {
    console.log("hiiiiii");
    var request = new Form();
    var table = req.params.table;
    var columns = req.params.column;
   var scheduledId=req.params.scheduledId;



        request.DynamictableDetails(table, columns,scheduledId ).then((data) => {
            buildResponse(res, 200, {
                error: false,
                message: "Form Updated Successfully",
                vdata: data
            });
        }).catch((err) => {
            buildResponse(res, 200, {
                error: true,
                message: "Fail to Update",
            });
        });
    
});

router.get('/getupdateOnchangeData/:where/:fromtable/:columnnameto/:coldefaultval/:columnvalto/:whereString', myLogger, function (req, res, next) {
    console.log("hiiiiii");
    var request = new Form();
    var where = req.params.where;

    //var fields=where.split("or");
    //var col1=fields[0];
    //var col2=fields[1];

    var fromtable = req.params.fromtable;
    var columnnameto = req.params.columnnameto;
    var coldefaultval = req.params.coldefaultval;
    var columnvalto = req.params.columnvalto;
    var wherestring = req.params.whereString + "";




    request.tableonchangeDetails(where, fromtable, columnnameto, coldefaultval, columnvalto, wherestring).then((data) => {
        buildResponse(res, 200, {
            error: false,
            message: "Form Updated Successfully",
            vdata: data
        });
    }).catch((err) => {
        buildResponse(res, 200, {
            error: true,
            message: "Fail to Update",
        });
    });
});





router.get('/getFormForAccess', myLogger, function (req, res, next) {


    var request = new Form();
    request.getFormForAccess().then((requestData) => {
        buildResponse(res, 200, {
            error: false,
            message: "Successfully display!",
            RequestData: requestData
        });
    });
});


router.get('/dynamicAPIAlongWithJSONString/:id', myLogger, function (req, res, next) {
    var formId = req.params.id;
    var request = new Form();
    request.dynamicAPIAlongWithJSONString(formId).then((requestData) => {
        buildResponse(res, 200, {
            error: false,
            message: "Successfully display!",
            RequestData: requestData
        });
        // res.send(RequestData);
    });
});

router.post('/formRenderDataSave/:formId', myLogger, function (req, res, next) {
    var request = new Form();
    var ip = req.headers['x-forwarded-for'] || req.connection.remoteAddress;

    var formId = req.params.formId;


    request.formRenderDataSave(formId, req.body, ip).then((data) => {

       
        if (data["data"] == 0) {
            request.formRenderDataDelete(formId, req.body, ip);
            buildResponse(res, 200, {
                error: true,
                message: errMsg
            });
        } else {
            if (req.body.dynamicTableSaveArr.length > 0) {

                var cnt = 0;
                var len = 0;
                req.body.dynamicTableSaveArr.forEach(element => {
                    len = len + element.columnValue.length;
                    element.columnValue.forEach(ele => {
                        
                        request.formRenderSaveDynamicTable(element.actionTableName, element.columName, ele,req.body.scheduledId).then((data) => {

                            console.log("ddddddddddddddd=", data)

                            if (data["data"] == 0) {


                                req.body.dynamicTableSaveArr.forEach(element => {
                                    len = len + element.columnValue.length;
                                    element.columnValue.forEach(ele => {
                                        
                                        request.formRenderDeleteDynamicTable(element.actionTableName, element.columName,req.body.scheduledId);
                                    });
                                });
                                request.formRenderDataDelete(formId, req.body, ip);
                                buildResponse(res, 200, {
                                    error: true,
                                    message: errMsg
                                });
                            } else {
                                cnt = cnt + 1;


                            }

                            if (cnt == len) {
                                buildResponse(res, 200, {
                                    error: false,
                                    message: "Stored Successfully",
                                    data: data
                                });
                            }
                        }).catch((errLog) => {

                        });
                    });

                });
            } else {
                console.log("render data=", data[0]);
                if (data[0] !== undefined) {
                    buildResponse(res, 200, {
                        error: true,
                        message: data['parent']['code'] + ' - Failed To Store.',

                    });
                } else {
                    buildResponse(res, 200, {
                        error: false,
                        message: "Stored Successfully",
                        data: data
                    });
                }
            }
        }

        console.log("333333333333333=", req.body.dynamicTableSaveArr)



    }).catch((errLog) => {

    });
});

// if file uploading using with this routing...

// router.post('/formRenderDataSave/:formId',myLogger, function (req, res, next) {
//     var request = new Form();
//     var formId = req.params.formId;

//     // request.formRenderDataSave(formId, req.body).then(()=>{
//     //     console.log("=======================","data");
//     //     return "";
//     // })

//    // request.formRenderDataSave(formId, req.body);


//     request.formRenderDataSave(formId, req.body).then((data) => {
//         console.log("==============data===========",data);
//                 buildResponse(res, 200, {
//                     error: false,
//                      message: "Stored Successfully"
//                   });
//               }).catch((err) => {
//               buildResponse(res, 500, {
//                   error: true,
//                  message: "Fail to Store"
//                 });
//             });    var upload = multer({
//                 storage: storage
//             }).array('DocumentUploadX')
//             upload(req, res, function (err) {

//                 if (err) {
//                   // An error occurred when uploading
//                   console.log(err);
//                   buildResponse(res, 422, {
//                     error: true,
//                     message: 'An Error occured in uploading',
//                 });
//                   //return res.status(422).send("an Error occured")
//                 }
//                // No error occured.
//         //       path= req.file.originalname;
//                // fileName = req.file.originalname;
//               //  console.log("fileName34 " + fileName);
//                 buildResponse(res, 200, {
//                     error: false,
//                     message: 'Uploading Completed ... ',
//                     RequestData: path
//                 });
//                // return res.send("Upload Completed for "+ path);
//           });
// });



router.post('/formRenderDataUpdate/:formId', myLogger, function (req, res, next) {
    var request = new Form();
    var formId = req.params.formId;
    var scheduledId = req.body.scheduledId;

    var ip = req.headers['x-forwarded-for'] || req.connection.remoteAddress;
    // console.log("ipss", ip)
    request.formRenderDataUpdate(formId, scheduledId, req.body, ip).then((data) => {

        console.log("render data=", data);

        if (data["data"] == 0) {
           
            buildResponse(res, 200, {
                error: true,
                message: data["errMsg"]
            });
        }else{

            if (req.body.dynamicTableSaveArr.length > 0) {

                var cnt = 0;
                var len = 0;
                req.body.dynamicTableSaveArr.forEach(element => {
                    len = len + element.columnValue.length;
                    request.formRenderDeleteDynamicTable(element.actionTableName, element.columName,req.body.scheduledId).then((d)=>{
                    element.columnValue.forEach(ele => {
                        
                        
                            request.formRenderSaveDynamicTable(element.actionTableName, element.columName, ele,req.body.scheduledId).then((data) => {

                              
                                    cnt = cnt + 1;
    
                                if (cnt == len) {
                                    
                                    buildResponse(res, 200, {
                                        error: false,
                                        message: "updated Successfully",
                                        data: data
                                    });
                                }
                            }).catch((errLog) => {
                   
                                log.addLog('route :: forms.js ' + errLog);
                                buildResponse(res, 500, {
                                    error: true,
                                    message: errMsg
                                });
                            });
                        })
                        
                    }).catch((errLog) => {
                   
                        log.addLog('route :: forms.js ' + errLog);
                        buildResponse(res, 500, {
                            error: true,
                            message: errMsg
                        });
                    });

                });
            } else {
               
                    buildResponse(res, 200, {
                        error: false,
                        message: "updated Successfully",
                        data: data
                    });
                
            }

        }

       /*  if (data !== undefined) {
            buildResponse(res, 200, {
                error: true,
                message: data['parent']['code'] + ' - Failed To Store.',

            });
        } else {
            buildResponse(res, 200, {
                error: false,
                message: "Stored Successfully",

            });
        } */
    })
});

// router.post('/formRenderDataUpdate/:formId',myLogger, function (req, res, next) {
//     var request = new Form();
//     var formId = req.params.formId;
//     request.formRenderDataUpdate(formId, req.body).then((data) => {
//                 buildResponse(res, 200, {
//                     error: false,
//                      message: "Stored Successfully"
//                   });
//               }).catch((err) => {
//               buildResponse(res, 500, {
//                   error: true,
//                  message: "Fail to Store"
//                 });
//             });
// });

// router.post('/',
//     function (req, res, next) {
//         var reqId = req.params.groupId;
//         var group = new Group();

//             group.renderFormDataSave(Data);
//         });

//         buildResponse(res, 200, {
//             error: false,
//             message: "Successfully store!",
//         });

router.post('/getSelectedAppForm', myLogger, function (req, res, next) {
    var AppId = req.body;
    var request = new Form();
    request.getSelectedAppForm(AppId).then((appData) => {
        buildResponse(res, 200, {
            error: false,
            message: "Successfully display!",
            AppData: appData
        });
    });
});




//Dont take
router.post('/getRenderFormData', myLogger, function (req, res, next) {
    var request = new Form();
    var reqData = {
        formId: req.body.formId,
        appId: req.body.appId,
        proposedTableName: req.body.proposedTableName,
    };
    console.log("getRenderFormData------->" + JSON.stringify(reqData));
    request.getRenderFormData(reqData).then((requestData) => {
        buildResponse(res, 200, {
            error: false,
            message: 'Forms Fetched successfully',
            RequestData: requestData
        });
    }).catch((err) => {
        buildResponse(res, 404, {
            error: false,
            message: 'Failed to Fetch',

        });
    });
});


router.get('/deleteRenderFormDataById/:dataid/:tableActionColumn/:proposedTableName', myLogger, function (req, res, next) {
    var request = new Form();
    var dataid = req.params.dataid;
    var proposedTableName = req.params.proposedTableName;
    var tableActionColumn = req.params.tableActionColumn;
    request.deleteRenderFormDataById(dataid, tableActionColumn, proposedTableName)
        .then((requestData) => {
            buildResponse(res, 200, {
                error: false,
                message: 'deleted successfully',
                RequestData: requestData
            });
        }).catch((err) => {
            buildResponse(res, 404, {
                error: false,
                message: 'delete unsuccessful',

            });
        });
});



router.post('/getRenderFormDataById/:dataid', myLogger, function (req, res, next) {
    var request = new Form();
    var dataid = req.params.dataid;
    var reqData = {
        formId: req.body.formId,
        proposedTableName: req.body.praposed_table_name,
        action_column: req.body.action_column

    };

    console.log(";;;", dataid, reqData.proposedTableName, reqData.action_column);

    request.getRenderFormDataById(dataid, reqData.proposedTableName, reqData.action_column).then((requestData) => {
        buildResponse(res, 200, {
            error: false,
            message: 'Forms Fetched successfully',
            RequestData: requestData
        });
    }).catch((err) => {
        buildResponse(res, 404, {
            error: false,
            message: 'Failed to Fetch',

        });
    });
});

router.get('/getDashboardTotalCount/:tableName', myLogger, function (req, res, next) {
    var request = new Form();
    var tablenameval = req.params.tableName;
    console.log("in route", tablenameval);

    var totalcount = 0;
    //    for (var i=0;i< req.body.length;i++)
    //    {
    request.getDashboardTotalCount(tablenameval).then((countData) => {

        //      console.log("countdata=",countData)
        buildResponse(res, 200, {
            error: false,
            message: 'Forms Fetched successfully',
            listData: countData,
            countData: countData.length

        });
    });

    //}

});


router.get('/getDashboardPendingCount/:tableName', myLogger, function (req, res, next) {
    var request = new Form();
    var tablenameval = req.params.tableName;

    //console.log("tablenameval",tablenameval);
    var totalcount = 0;
    request.getDashboardPendingCount(tablenameval).then((countData) => {

        buildResponse(res, 200, {
            error: false,
            message: 'Forms Fetched successfully',
            listData: countData,
            countData: countData.length

        });
    });



});


router.get('/getDashboardCompletedCount/:tableName', myLogger, function (req, res, next) {
    var request = new Form();
    var tablenameval = req.params.tableName;


    var totalcount = 0;
    request.getDashboardCompletedCount(tablenameval).then((countData) => {

        buildResponse(res, 200, {
            error: false,
            message: 'Forms Fetched successfully',
            listData: countData,
            countData: countData.length


        });
    });



});

router.get('/getDashboardRejectedCount/:tableName', myLogger, function (req, res, next) {
    var request = new Form();
    var tablenameval = req.params.tableName;


    var totalcount = 0;
    request.getDashboardRejectedCount(tablenameval).then((countData) => {

        buildResponse(res, 200, {
            error: false,
            message: 'Forms Fetched successfully',
            listData: countData,
            countData: countData.length

        });
    });



});

// file uploading......
var storage = multer.diskStorage({
    destination: function (req, file, callback) {
        callback(null, './uploads')
    },
    filename: function (req, file, callback) {
        console.log(file)
        callback(null, file.originalname + '-' + Date.now() + path.extname(file.originalname))
    }
})

router.post('/fileUpload', myLogger, function (req, res, next) {


    var upload = multer({
        storage: storage
    }).single('demo')
    upload(req, res, function (err) {

        if (err) {
            // An error occurred when uploading
            console.log(err);
            return res.status(422).send("an Error occured")
        }
        // No error occured.
        // path= req.file.originalname;
        // fileName = req.file.originalname;
        return res.send("Upload Completed for " + path);
    });

});

router.get('/getTableName', myLogger, function (req, res, next) {
    var request = new Form();
    var data = req.body;
    //custCon.connectDatabase(hoste, databasee, usernamee, passworde, porte);
    request.getTableName().then((getTableNamesData) => {
        buildResponse(res, 200, {
            error: false,
            message: "Successfully display!",
            getTableNamesData: getTableNamesData
        });
    });
});

router.post('/getTableNames', myLogger, function (req, res, next) {
    var request = new Form();
    var data = req.body;
    console.log("datadata", data);
    //custCon.connectDatabase(hoste, databasee, usernamee, passworde, porte);
    request.getTableNames(data).then((getTableNamesData) => {
        buildResponse(res, 200, {
            error: false,
            message: "Successfully display!",
            getTableNamesData: getTableNamesData
        });
    });
});

router.post('/getTableColumn/:tableName', myLogger, function (req, res, next) {
    var request = new Form();
    var data = req.body;
    var tableName = req.params.tableName;
    request.getTableColumn(tableName, data).then((getTableColumnsData) => {
        buildResponse(res, 200, {
            error: false,
            message: "Successfully display!",
            getTableColumnsData: getTableColumnsData


        });
    });
});

router.get('/getActionTableColumns/:tableName', myLogger, function (req, res, next) {
    var request = new Form();

    var tableName = req.params.tableName;
    request.getActionTableColumns(tableName).then((data) => {
        buildResponse(res, 200, {
            error: false,
            message: "Successfully display!",
            data: data


        });
    });
});

router.get('/getViewNames', myLogger, function (req, res, next) {
    var request = new Form();

    request.getViewNames().then((getViewNamesData) => {
        buildResponse(res, 200, {
            error: false,
            message: "Successfully display!",
            getViewNamesData: getViewNamesData
        });
    });
});
router.post('/getViewName', myLogger, function (req, res, next) {
    var request = new Form();
    var data = req.body;
    console.log("data---->>>>", data);
    request.getViewName(data).then((getViewNamesData) => {
        buildResponse(res, 200, {
            error: false,
            message: "Success!",
            getViewNamesData: getViewNamesData
        });
    });
});


router.get('/getTableColumns/:tableName', myLogger, function (req, res, next) {
    var request = new Form();
    var tableName = req.params.tableName;
    request.getTableColumns(tableName).then((getTableColumnsData) => {
        buildResponse(res, 200, {
            error: false,
            message: "Successfully display!",
            getTableColumnsData: getTableColumnsData
        });
    });
});

router.get('/getTableColumnsLabelData/:tableName/:columnNameLabel/:columnNameValue/:ind/:connectionId', myLogger, function (req, res, next) {
    var request = new Form();
    var tableName = req.params.tableName;
    var columnNameLabel = req.params.columnNameLabel;
    var columnNameValue = req.params.columnNameValue;
    var connectionId = "1";


    request.getDatabaseById(connectionId).then((data) => {
        datad = {
            error: false,
            message: 'Successfully display!',
            responseData: data
        }
        console.log("gagag", datad);
        request.getTableColumnsLabelData(tableName, columnNameLabel, columnNameValue, datad).then((LabelData) => {
            buildResponse(res, 200, {
                error: false,
                message: "Successfully display!",
                LabelData: LabelData,
                IndFromTable: req.params.ind
            });
        });
    });

});

router.get('/getTableColumnsLabelDataDynamiCTable/:tableName/:columnNameLabel/:columnNameValue/:where/:searchText', myLogger, function (req, res, next) {
    var request = new Form();
    var tableName = req.params.tableName;
    var columnNameLabel = req.params.columnNameLabel;
    var columnNameValue = req.params.columnNameValue;
    var searchText=req.params.searchText;
    var where = req.params.where;
    


        request.getTableColumnsLabelDataDynamiCTable(tableName, columnNameLabel, columnNameValue, where,searchText).then((LabelData) => {
            buildResponse(res, 200, {
                error: false,
                message: "Successfully display!",
                LabelData: LabelData,
                IndFromTable: req.params.ind
            });
        });
   

});

router.get('/getTableColumnsLabelDataPrevious/:tableName/:columnNameLabel/:columnNameValue/:ind/:connectionId/:arr', myLogger, function (req, res, next) {
    var request = new Form();
    var tableName = req.params.tableName;
    var columnNameLabel = req.params.columnNameLabel;
    var columnNameValue = req.params.columnNameValue;
    var connectionId = "1";
    var arr = req.params.arr;

    console.log("arr=", arr);

    request.getDatabaseById(connectionId).then((data) => {
        datad = {
            error: false,
            message: 'Successfully display!',
            responseData: data
        }
        console.log("gagag", datad);
        request.getTableColumnsLabelDataPrevious(tableName, columnNameLabel, columnNameValue, datad, arr).then((LabelDataPrevious) => {

            buildResponse(res, 200, {
                error: false,
                message: "Successfully display!",
                LabelData: LabelDataPrevious,
                IndFromTable: req.params.ind
            });
            console.log("label", LabelDataPrevious);
        });
    });

});




router.get('/getTableColumnsLabelDataFilter/:tableName/:columnNameLabel/:columnNameValue/:value/:connectionId', myLogger, function (req, res, next) {
    var request = new Form();
    var tableName = req.params.tableName;
    var columnNameLabel = req.params.columnNameLabel;
    var columnNameValue = req.params.columnNameValue;
    var connectionId = "1";
    var value = req.params.value;
    console.log("hiiiiiiiiiiiii")
    request.getDatabaseById(connectionId).then((data) => {
        datad = {
            error: false,
            message: 'Successfully display!',
            responseData: data
        }
        console.log("gagag", datad);
        request.getTableColumnsLabelDataFilter(tableName, columnNameLabel, columnNameValue, datad, value).then((LabelData) => {
            buildResponse(res, 200, {
                error: false,
                message: "Successfully display!",
                LabelData: LabelData,
                IndFromTable: req.params.ind
            });
        });
    });

});

router.get('/getTableColumnsLabelDatawhere/:tableName/:columnNameLabel/:columnNameValue/:ind/:whereString', myLogger, function (req, res, next) {
    var request = new Form();
    var tableName = req.params.tableName;
    var columnNameLabel = req.params.columnNameLabel;
    var columnNameValue = req.params.columnNameValue;
    var whereString = req.params.whereString;

    console.log("whereString===", whereString);
    request.getTableColumnsLabelDatawhere(tableName, columnNameLabel, columnNameValue, whereString).then((LabelData) => {
        buildResponse(res, 200, {
            error: false,
            message: "Successfully display!",
            LabelData: LabelData,
            IndFromTable: req.params.ind
        });
    });
});

router.get('/getTableColumnsLabelDatawherePrevious/:tableName/:columnNameLabel/:columnNameValue/:ind/:whereString/:selectarr', myLogger, function (req, res, next) {
    var request = new Form();
    var tableName = req.params.tableName;
    var columnNameLabel = req.params.columnNameLabel;
    var columnNameValue = req.params.columnNameValue;
    var whereString = req.params.whereString;
    var selectarr = req.params.selectarr;

    console.log("whereString===", whereString);
    request.getTableColumnsLabelDatawherePrevious(tableName, columnNameLabel, columnNameValue, whereString, selectarr).then((LabelDataPrevious) => {
        buildResponse(res, 200, {
            error: false,
            message: "Successfully display!",
            LabelData: LabelDataPrevious,
            IndFromTable: req.params.ind
        });
        console.log("LabelDataPrevious===", LabelDataPrevious);
    });

});
router.get('/getTableColumnsLabelDatawhereFilter/:tableName/:columnNameLabel/:columnNameValue/:value/:whereString', myLogger, function (req, res, next) {
    var request = new Form();
    var tableName = req.params.tableName;
    var columnNameLabel = req.params.columnNameLabel;
    var columnNameValue = req.params.columnNameValue;
    var whereString = req.params.whereString;
    var value = req.params.value;

    console.log("whereString===", whereString);
    request.getTableColumnsLabelDatawhereFilter(tableName, columnNameLabel, columnNameValue, whereString, value).then((LabelData) => {
        buildResponse(res, 200, {
            error: false,
            message: "Successfully display!",
            LabelData: LabelData,
            IndFromTable: req.params.ind
        });
    });
});




router.get('/getTableColumnsValueData/:tableName/:columnName', myLogger, function (req, res, next) {
    var request = new Form();
    var tableName = req.params.tableName;
    var columnName = req.params.columnName;
    request.getTableColumnsValueData(tableName, columnName).then((ValueData) => {
        buildResponse(res, 200, {
            error: false,
            message: "Successfully display!",
            ValueData: ValueData
        });
    });
});

router.post('/insertTableValue', myLogger, function (req, res, next) {
    console.log("in route")
    var request = new Form();
    var tableName = req.body.table;
    var columnName = req.body.key;
    var valueName = req.body.colValues;
    // console.log("jjjjjjj",req.body.colValues);
    request.insertTableValues(tableName, columnName, valueName).then((ValueData) => {
        buildResponse(res, 200, {
            error: false,
            message: "Successfully inserted!",
            ValueData: ValueData
        });
    });
});

router.post('/tableDetails', myLogger, function (req, res, next) {
    //console.log("hiiiiii");
    var request = new Form();
    var requestData = req.body

    for (var x = 0; x < requestData.length; x++) {
        if (x == 0) {
            var columnArray = [] = [];
            requestData[0].forEach(element => {
                columnArray.push(element.column);

            });
        }
        if (x == 1) {
            var table = requestData[1];
        }
    }

    columns = columnArray.toString()
    request.tableDetails(table, columns).then((data) => {
        buildResponse(res, 200, {
            error: false,
            message: "Form Updated Successfully",
            vdata: data
        });
    }).catch((err) => {
        buildResponse(res, 200, {
            error: true,
            message: "Fail to Update",
        });
    });
});

router.get('/getTableData/:tableName/:columnName/:whereString/:connectionId', myLogger, function (req, res, next) {

    console.log("in route");
    var request = new Form();
    var table = req.params.tableName;
    var columns = req.params.columnName;
    var wherestring = req.params.whereString;
    var connectionId = "1";
    request.getDatabaseById(connectionId).then((data) => {
        console.log("datad", data);
        datad = {
            error: false,
            message: 'Successfully display!',
            responseData: data
        }
        console.log("table, columns, wherestring,datad", table, columns, wherestring, datad);
        request.getTableData(table, columns, wherestring, datad).then((data) => {
            buildResponse(res, 200, {
                error: false,
                message: "Data Fetched Successfully",
                vdata: data
            });
        }).catch((err) => {
            buildResponse(res, 500, {
                error: true,
                message: "Fail to Fetch",
            });
        });
    });

});

router.get('/getMaxCount/:tableName/:columnName/:connectionId', myLogger, function (req, res, next) {
    var request = new Form();
    var tableName = req.params.tableName;
    var columnName = req.params.columnName;
    var connectionId = "1";
    request.getDatabaseById(connectionId).then((data) => {

        datad = {
            error: false,
            message: 'Successfully display!',
            responseData: data
        }

        request.getMaxCount(tableName, columnName, datad).then((ValueData) => {

            buildResponse(res, 200, {
                error: false,
                message: "Successfully display!",
                responseData: ValueData
            });
        });
    });
});


router.get('/getMaxCountAdvanced/:tableName/:columnName/:whrcon', myLogger, function (req, res, next) {
    var request = new Form();
    var tableName = req.params.tableName;
    var columnName = req.params.columnName;
    var whrcon = req.params.whrcon;
    request.getMaxCountAdvanced(tableName, columnName, whrcon).then((ValueData) => {
        buildResponse(res, 200, {
            error: false,
            message: "Successfully display!",
            responseData: ValueData
        });
    });
});
router.get('/getDatabaseNames', myLogger, function (req, res, next) {
    var request = new Form();

    request.getDatabaseNames().then((database) => {
        buildResponse(res, 200, {
            error: false,
            message: "Successfully display!",
            responseData: database
        });
    });
});

router.get('/getDatabaseById/:id', myLogger, function (req, res, next) {
    var request = new Form();
    //var custCon = new customCon();
    var appId = req.params.id;
    request.getDatabaseById(appId).then((database) => {
        console.log("datatse", database[0]);

        var hoste = database[0]['host'];
        var databasee = database[0]['database'];
        var usernamee = database[0]['username'];
        var passworde = database[0]['password'];
        var porte = database[0]['port'];

        //    console.log("host",hoste);
        //    console.log("database",databasee);
        //    console.log("username",usernamee);
        //    console.log("password",passworde);
        //    console.log("port",porte);
        //    console.log("host",hoste);



        //console.log("ffff",custCon.connectDatabase(host, database, username, password, port));
        buildResponse(res, 200, {
            error: false,
            message: "Successfully display!",
            responseData: database
        });
    });
});
router.get('/foundOrNot', myLogger, function (req, res, next) {
    var request = new Form();

    request.foundOrNot().then((database) => {
        buildResponse(res, 200, {
            error: false,
            message: "Tables are found!",
            responseData: database
        });
    });
});




router.get('/getPMSTaskMaster/:userId', myLogger, function (req, res, next) {
    var request = new Form();
    var userId = req.params.userId;





    request.getPMSTaskMasters(userId).then((data) => {

        // console.log("userId=",userId);
        // console.log("requestData=",data);
        buildResponse(res, 200, {
            error: false,
            message: 'Forms Fetched successfully',
            formData: data
        });
    }).catch((err) => {
        buildResponse(res, 404, {
            error: false,
            message: 'Failed to Fetch',

        });
    });
});

router.get('/getPMSProjectMaster/:userId', myLogger, function (req, res, next) {
    var request = new Form();
    var userId = req.params.userId;





    request.getPMSProjectMaster(userId).then((requestData) => {
        buildResponse(res, 200, {
            error: false,
            message: 'Forms Fetched successfully',
            formData: requestData
        });
    }).catch((err) => {
        buildResponse(res, 404, {
            error: false,
            message: 'Failed to Fetch',

        });
    });
});

router.get('/getParentTableColumns/:tableName', myLogger, function (req, res, next) {
    var request = new Form();
    var tableName = req.params.tableName;





    request.getParentTableColumns(tableName).then((requestData) => {
        buildResponse(res, 200, {
            error: false,
            message: 'Forms Fetched successfully',
            getTableColumnsData: requestData
        });
    }).catch((err) => {
        buildResponse(res, 404, {
            error: false,
            message: 'Failed to Fetch',

        });
    });
});


router.get('/getMaxVerion/:app_id_param', myLogger, function (req, res, next) {
    var request = new Form();
    var app_id_param = req.params.app_id_param;

    request.getMaxVerion(app_id_param).then((requestData) => {
        buildResponse(res, 200, {
            error: false,

            data: requestData
        });
    }).catch((err) => {
        buildResponse(res, 404, {
            error: false,
            message: 'Failed to Fetch',

        });
    });
});

router.post('/getMeetingTemplate', myLogger, function (req, res, next) {
    var request = new Form();

    request.getMeetingTemplate(req.body).then((data) => {
        buildResponse(res, 200, {
            error: false,
            message: "Successfully display!",
            data: data
        });
    });
});

function buildResponse(res, statusCode, responseData) {
    res.status(statusCode).json(responseData);
}

module.exports = router;
