var express = require('express');
var router = express.Router();
var bcrypt = require('bcryptjs');
var jwt = require('jsonwebtoken');
const Joi = require('joi');
var GMDashboard = require('../../model/GMDashboard')
var path = require('path')
var myLogger = require('../../middleware.js');
   
router.get('/getDashboardUserDept/:userid',myLogger, function (req, res, next) {
    var request = new GMDashboard();
    var userid = req.params.userid;
   

console.log("userid",userid);
  var totalcount=0;
    request.getDashboardUserDept(userid).then((userData) => {

        buildResponse(res, 200, {
            error: false,
            message: 'Forms Fetched successfully',
          responseData:userData
           
        });
    });



});
router.get('/getDashboardTotalCount/:tableName/:userid',myLogger, function (req, res, next) {
    var request = new GMDashboard();
var tablenameval = req.params.tableName;
var userid= req.params.userid;
// var group=req.params.group;
// var dept=req.params.dept;
// console.log("in route total",group,dept);
 
  var totalcount=0;
//    for (var i=0;i< req.body.length;i++)
//    {
    request.getDashboardTotalCount(tablenameval,userid).then((countData) => {
    request.getDashboardTotalCount
        //console.log("countdata=",countData)
        buildResponse(res, 200, {
            error: false,
            message: 'Forms Fetched successfully',
            listData:countData,
            countData: countData.length
           
        });
    });

  //}

});


router.get('/getDashboardPendingCount/:tableName/:columnname/:userid',myLogger, function (req, res, next) {
    var request = new GMDashboard();
    var tablenameval = req.params.tableName;
    var colomname=req.params.columnname;
     var userid=req.params.userid;
// var userid= req.params.userid;
// var group=req.params.group;
// var dept=req.params.dept;

console.log("tablenameval",tablenameval);
  var totalcount=0;
    request.getDashboardPendingCount(tablenameval,colomname,userid).then((countData) => {

        buildResponse(res, 200, {
            error: false,
            message: 'Forms Fetched successfully',
            listData:countData,
            countData: countData.length
           
        });
    });



});


router.get('/getDashboardCompletedCount/:tableName/:columnname/:userid',myLogger, function (req, res, next) {
    var request = new GMDashboard();
    var tablenameval = req.params.tableName;
    var colomname=req.params.columnname;
     var userid=req.params.userid;
// var userid= req.params.userid;
// var group=req.params.group;
// var dept=req.params.dept;

  var totalcount=0;
    request.getDashboardCompletedCount(tablenameval,colomname,userid).then((countData) => {

        buildResponse(res, 200, {
            error: false,
            message: 'Forms Fetched successfully',
            listData:countData,
            countData: countData.length
            

        });
    });



});

router.get('/getDashboardHoldCount/:tableName/:columnname/:userid',myLogger, function (req, res, next) {
    var request = new GMDashboard();
    var tablenameval = req.params.tableName;
    var colomname=req.params.columnname;
     var userid=req.params.userid;
// var userid= req.params.userid;
// var group=req.params.group;
// var dept=req.params.dept;

  var totalcount=0;
    request.getDashboardHoldCount(tablenameval,colomname).then((countData) => {

        buildResponse(res, 200, {
            error: false,
            message: 'Forms Fetched successfully',
            listData:countData,
            countData: countData.length
           
        });
    });

 

});

router.get('/getDashboardIssueCount/:tableName/:columnname',myLogger, function (req, res, next) {
    var request = new GMDashboard();
    var tablenameval = req.params.tableName;
    var colomname=req.params.columnname;
    
// var userid= req.params.userid;
// var group=req.params.group;
// var dept=req.params.dept;

  var totalcount=0;
    request.getDashboardIssueCount(tablenameval,colomname).then((countData) => {

        buildResponse(res, 200, {
            error: false,
            message: 'Forms Fetched successfully',
            listData:countData,
            countData: countData.length
           
        });
    });

 

});

router.get('/getDashboardYTSCount/:tableName/:columnname',myLogger, function (req, res, next) {
    var request = new GMDashboard();
    var tablenameval = req.params.tableName;
    var colomname=req.params.columnname;

    // var userid= req.params.userid;
    // var group=req.params.group;
    // var dept=req.params.dept;
  var totalcount=0;
    request.getDashboardYTSCount(tablenameval,colomname).then((countData) => {

        buildResponse(res, 200, {
            error: false,
            message: 'Forms Fetched successfully',
            listData:countData,
            countData: countData.length
           
        });
    });

 

});



router.get('/getDashboardRejectedCount/:tableName/:columnname/:userid',myLogger, function (req, res, next) {
    var request = new GMDashboard();
    var tablenameval = req.params.tableName;
    var colomname=req.params.columnname;

     var userid= req.params.userid;
    // var group=req.params.group;
    // var dept=req.params.dept;

    var totalcount = 0;
    request.getDashboardRejectedCount(tablenameval,colomname,userid).then((countData) => {

        buildResponse(res, 200, {
            error: false,
            message: 'Forms Fetched successfully',
            listData: countData,
            countData: countData.length

        });
    });



});



router.post('/saveRequest',myLogger, function (req, res, next) {
    var request = new GMDashboard();
    var requestData = {
        issueTitle: req.body.issueTitle,
        issueDomain: req.body.domain,
        issueDescription: req.body.issueDescription,
        requestDate: req.body.requestDate,
        expectedCompletionDate: req.body.expectedCompletionDate,
        issueAttachement: req.body.issueAttachement,
        Created_By:req.body.Created_By,
    
    };
    request.saveRequest(requestData)
        .then((data) => {
            
            buildResponse(res, 200, {
                error: false,
                 message: "Stored Successfully"
              });

          }).catch((err) => {
          buildResponse(res, 200, {
              error: true,
             message: "Fail to Store"
            });
        });
   
});

router.post('/UpdateRequest/:id',myLogger, function (req, res, next) {
    var reqId=req.params.id;
    var request = new GMDashboard();
    var requestData = {
        issueTitle: req.body.issueTitle,
        issueDomain: req.body.domain,
        issueDescription: req.body.issueDescription,
        requestDate: req.body.requestDate,
        expectedCompletionDate: req.body.expectedCompletionDate,
        issueAttachement: req.body.issueAttachement,
        Created_By:req.body.Created_By,
    };
    request.UpdateRequest(requestData,reqId)
        .then((data) => {
            buildResponse(res, 200, {
                error: false,
                 message: "Stored Successfully"
              });

          }).catch((err) => {
          buildResponse(res, 200, {
              error: true,
             message: "Fail to Store"
            });
        });
   
});

router.post('/saveRequestUpdates',myLogger, function (req, res, next) {
    var request = new GMDashboard();
    var requestData = {
        tantativeDate: req.body.tantativeDate,
        attachement: req.body.attachement,
        comment: req.body.comment,
        status: req.body.status,
       requestId:req.body.requestId,
       Created_By:req.body.Created_By,
    };
    request.saveRequestUpdates(requestData)
        .then((data) => {
            buildResponse(res, 200, {
                error: false,
                 message: "Stored Successfully"
              });
          }).catch((err) => {
          buildResponse(res, 200, {
              error: true,
             message: "Fail to Store"
            });
        });
   
});

router.get('/deleteRequest/:id',myLogger, function (req, res, next) {
    var request = new GMDashboard();
    var reqData = req.params.id;
    request.deleteRequest(reqData)
        .then((userData) => {
            buildResponse(res, 200, {
                error: false,
                message: 'User Deleted successfully',
                UserData: userData
            });
        }).catch((err) => {
            buildResponse(res, 500, {
                error: false,
                message: 'Please Try Again',
            });
        });
});


router.get('/getRequest/:id',myLogger, function (req, res, next) {

    var request = new GMDashboard();
    var reqData = req.params.id;
    request.getRequest(reqData)
        .then((requestData) => {
            buildResponse(res, 200, {
                error: false,
                message: 'Request Fetched successfully',
                requestData: requestData
            });
        }).catch((err) => {
            buildResponse(res, 404, {
                error: false,
                message: 'Failed to Fetch',
            });
        });
});

router.get('/getUpdates/:id',myLogger, function (req, res, next) {

    var request = new GMDashboard();
    var reqData = req.params.id;
    request.getUpdates(reqData)
        .then((requestData) => {
            buildResponse(res, 200, {
                error: false,
                message: 'Updates for request fetch successfully',
                requestData: requestData
            });
        }).catch((err) => {
            buildResponse(res, 404, {
                error: false,
                message: 'Failed to Fetch',
            });
        });
});
router.get('/getRequestUpdatesById/:id',myLogger, function (req, res, next) {

    var request = new GMDashboard();
    var reqData = req.params.id;
    request.getRequestUpdatesById(reqData)
        .then((requestData) => {
            buildResponse(res, 200, {
                error: false,
                message: 'Updates for request fetch successfully',
                requestData: requestData
            });
        }).catch((err) => {
            buildResponse(res, 404, {
                error: false,
                message: 'Failed to Fetch',
            });
        });
});

router.get('/getDomains',myLogger, function (req, res, next) {
    console.log("hhhhhhh");
    var request = new GMDashboard();
    request.getDomains().then((domains) => {
        buildResponse(res, 200, {
            error: false,
            message: 'Domains Fetched successfully',
            responseData: domains,
        });
    }).catch((err) => {
        buildResponse(res, 200, {
            error: true,
            message: err,
        });
    });
});

router.post('/sendRequestorEmail',myLogger, function (req, res, next) {
    
   var request = new GMDashboard();
	console.log("body=",req.body);
    request.sendRequestorEmail(req.body);
	 buildResponse(res, 200, {
            error: false,
            message: 'Email Intimation Notification Send Successfully'
        });
});


router.post('/sendApproverEmail',myLogger, function (req, res, next) {
    
   var request = new GMDashboard();
	console.log("body=app",req.body);
    request.sendApproverEmail(req.body);
	 buildResponse(res, 200, {
            error: false,
            message: 'Email Intimation Notification Send Successfully'
        });
});

router.post('/sendEditRequestorEmail',myLogger, function (req, res, next) {
    
    var request = new GMDashboard();
     console.log("body=",req.body);
     request.sendEditRequestorEmail(req.body);
      buildResponse(res, 200, {
             error: false,
             message: 'Email Intimation Notification Send Successfully'
         });
 });


router.get('/getUsersEmail/:userId',myLogger, function (req, res, next) {
    var request = new GMDashboard();
    var reqData = req.params.userId;
    request.getUsersEmail(reqData)
        .then((userData) => {
            buildResponse(res, 200, {
                error: false,
                message: 'User Email found successfully',
                UserData: userData
            });
        }).catch((err) => {
            buildResponse(res, 404, {
                error: false,
                message: 'Please Try Again',
            });
        });
});

function buildResponse(res, statusCode, responseData) {
    res.status(statusCode).json(responseData);
}

module.exports = router;
