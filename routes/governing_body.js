var express = require('express');
var router = express.Router();
var bcrypt = require('bcryptjs');
var jwt = require('jsonwebtoken');
const Joi = require('joi');
var governing_body = require('../model/governing_body');
var governing_body_master = new governing_body();
var myLogger = require('../middleware.js');
var MyLogModel = require('../model/myLog');
var log = new MyLogModel();

const schema = Joi.object().keys({
    governing_body_name: Joi.string().regex(/^[a-zA-Z ]*$/, '. Special symbols and numbers are not allow').max(20).required().not(''),
    governing_body_status: Joi.string().max(10).required().not(''),
    created_by: Joi.string().max(50).required().not(''),
     
});

const schemaUpdate = Joi.object().keys({
    governing_body_name: Joi.string().max(50).required().not(''),
    governing_body_status: Joi.string().max(10).required().not(''),
    updated_by: Joi.string().allow(''),
    governing_body_id:Joi.number().not('')
     
});

router.post('/saveGoverningBody', myLogger, function (req, res, next) {
    
    var governingBodyData = {
        governing_body_name: req.body.governing_body_name,
        governing_body_status: req.body.governing_body_status,
        created_by: req.body.created_by,
   };
  

   console.log("governingBodyData-",governingBodyData);
    Joi.validate({
        governing_body_name: req.body.governing_body_name,
        governing_body_status: req.body.governing_body_status,
        created_by: req.body.created_by,
     
     
    }, schema, function (err, schgoverningBodyData) {
        if (err) {

            console.log("error found",err);
            buildResponse(res, 200, {
                error: true,
                message: err['details'][0]["message"]
            });
        }
        else {
            console.log("dddddddddddddd",governingBodyData);
            governing_body_master.saveGoverningBody(governingBodyData)
                .then((data) => {
                   console.log(data,"-----------")
                    if (data["data"] == 0) {
                        buildResponse(res, 200, {
                            error: true,
                            message: data["errMsg"]
                        });
                    } else {
                        buildResponse(res, 200, {
                            error: false,
                            message: "Stored Successfully"
                        });

                    }

                }).catch((errLog) => {
                   
                    log.addLog('route :: governing_body.js ' + errLog);
                    buildResponse(res, 500, {
                        error: true,
                        message: "server error"
                    });
                });
        }
    });
});

router.post('/updateGoverningBody', myLogger, function (req, res, next) {
    
    var governingBodyData = {
        governing_body_name: req.body.governing_body_name,
        governing_body_status: req.body.governing_body_status,
        updated_by: req.body.updated_by,
        governing_body_id:req.body.governing_body_id
   };
   
   console.log("govvvvvrrrrrrr",governingBodyData)
    Joi.validate({
        governing_body_name: req.body.governing_body_name,
        governing_body_status: req.body.governing_body_status,
        updated_by: req.body.updated_by,
        governing_body_id:req.body.governing_body_id
     
     
    }, schemaUpdate, function (err, governingBodyData) {
        if (err) {

            buildResponse(res, 200, {
                error: true,
                message: err['details'][0]["message"]
            });
        }
        else {
            console.log("govvvvv",governingBodyData)
            governing_body_master.udpateGoverningBody(governingBodyData)
                .then((data) => {
                   console.log(data,"-----------")
                    if (data["data"] == 0) {
                        buildResponse(res, 200, {
                            error: true,
                            message: data["errMsg"]
                        });
                    } else {
                        buildResponse(res, 200, {
                            error: false,
                            message: "Updated Successfully"
                        });

                    }

                }).catch((errLog) => {
                   
                    log.addLog('route :: governing_body.js ' + errLog);
                    buildResponse(res, 500, {
                        error: true,
                        message: "server error"
                    });
                });
        }
    });
});

router.get('/getGoverningBody', myLogger, function (req, res, next) {
    governing_body_master.getGoverningBody().then((Data) => {
        if (Data["data"] == '0') {
            buildResponse(res, 200, {
                error: false,
                message: Data["errMsg"],
            });
        } else {
            buildResponse(res, 200, {
                error: false,
                message: "Successfully display!",
                data: Data
            });
        }
    });
});

router.post('/deleteGoverningBody', myLogger, function (req, res, next) {
    //var department = new Department();
    var governingBodyData={
        governing_body_id : req.body.governing_body_id,
        deleted_by:req.body.deleted_by
    };
    //var governing_body_id = req.body.governing_body_id;
    console.log(governingBodyData);
    governing_body_master.deleteGoverningBody(governingBodyData)
        .then((data) => {
            buildResponse(res, 200, {
                error: false,
                message: 'Governing body Deleted successfully',
                data: data
            });
        }).catch((errLog) => {
            log.addLog('route :: governing_body.js ' + errLog);
            buildResponse(res, 404, {
                error: false,
                message: 'Please Try Again',
            });
        });
});

function buildResponse(res, statusCode, responseData) {
    res.status(statusCode).json(responseData);
}

module.exports = router;