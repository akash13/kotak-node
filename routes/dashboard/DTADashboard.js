var express = require('express');
var router = express.Router();
var bcrypt = require('bcryptjs');
var jwt = require('jsonwebtoken');
const Joi = require('joi');
var DTADashboardModel = require('../../model/DTADashboardModel');
var path = require('path')
var myLogger = require('../../middleware.js');

router.get('/getemployee/:userid', myLogger,function (req, res, next) {
    var request = new DTADashboardModel();
    var userid = req.params.userid;
    request.getemployee(userid).then((data) => {
        buildResponse(res, 200, {
            error: false,
            message: "Successfully display!",
            responseData: data
        });
    }).catch((err) => {
        buildResponse(res, 200, {
            error: false,
            message: 'Error in Connecting',
        });
    });


});

router.get('/getUserDetailsForPirForm/:id',myLogger, function (req, res, next) {
    var request = new DTADashboardModel();
    var id = req.params.id;
    request.getUserDetailsForPirForm(id).then((data) => {
        buildResponse(res, 200, {
            error: false,
            message: "Successfully display!",
            responseData: data
        });
    });
});

router.get('/getDataOfCurrentRecord/:id',myLogger, function (req, res, next) {
    var request = new DTADashboardModel();
    var reqData = req.params.id;
    request.getDataOfCurrentRecord(reqData)
        .then((userData) => {
            buildResponse(res, 200, {
                error: false,
                message: 'Data Fetch Successfully',
                UserData: userData
            });
        }).catch((err) => {
            buildResponse(res, 500, {
                error: false,
                message: 'Please Try Again',
            });
        });
});
router.get('/getCurrentRecord/:id',myLogger, function (req, res, next) {
    var request = new DTADashboardModel();
    console.log('getCurrentRecord')
    var reqData = req.params.id;
    request.getCurrentRecord(reqData)
        .then((currentdatarecord) => {

            console.log(currentdatarecord);
            buildResponse(res, 200, {
                error: false,
                message: 'Data Fetch Successfully for Udateing Status',
                UserData: currentdatarecord
            });
        }).catch((err) => {
            buildResponse(res, 500, {
                error: false,
                message: 'Please Try Again',
            });
        });
});

router.post('/UpdateRequestStatus',myLogger, function (req, res, next) {

    var request = new DTADashboardModel();
    var data = req.body;
    request.UpdateRequestStatus(data)
        .then((data) => {
            buildResponse(res, 200, {
                error: false,
                message: "Record Updated Successfully"
            });

        }).catch((err) => {
            buildResponse(res, 200, {
                error: true,
                message: "Fail to Store"
            });
        });

});
router.post('/UpdateRequest',myLogger, function (req, res, next) {
    var request = new DTADashboardModel();
    var data = req.body;
    console.log('data',data);
    request.UpdateRequest(data)
        .then((data) => {
            buildResponse(res, 200, {
                error: false,
                message: "Record Updated Successfully"
            });

        }).catch((err) => {
            buildResponse(res, 200, {
                error: true,
                message: "Fail to Store"
            });
        });
});

router.post('/getDashboardTotalCount',myLogger, function (req, res, next) {
    var request = new DTADashboardModel();
    var data = req.body;
    request.getDashboardTotalCount(data).then((countData) => {
        buildResponse(res, 200, {
            error: false,
            message: 'Records Fetched successfully',
            listData:countData,
            countData: countData.length

        });
    });
});


router.post('/getDashboardPendingCount',myLogger, function (req, res, next) {
    
    var data = req.body;
    var request = new DTADashboardModel();
    request.getDashboardPendingCount(data).then((countData) => {
        buildResponse(res, 200, {
            error: false,
            message: 'Forms Fetched successfully',
            listData:countData,
            countData: countData.length
        });
    });
});


router.post('/getDashboardCompletedCount',myLogger, function (req, res, next) {
    var data = req.body;
    var request = new DTADashboardModel();
    request.getDashboardCompletedCount(data).then((countData) => {
        buildResponse(res, 200, {
            error: false,
            message: 'Forms Fetched successfully',
            listData:countData,
            countData: countData.length


        });
    });



});

router.post('/getDashboardVeifiedCount',myLogger, function (req, res, next) {
    var request = new DTADashboardModel();
    var data = req.body;
    request.getDashboardVeifiedCount(data).then((countData) => {
        buildResponse(res, 200, {
            error: false,
            message: 'Forms Fetched successfully',
            listData:countData,
            countData: countData.length


        });
    });



});

router.post('/getDashboardIssueCount',myLogger, function (req, res, next) {
    var request = new DTADashboardModel();
    var data = req.body;

    request.getDashboardIssueCount(data).then((countData) => {
        buildResponse(res, 200, {
            error: false,
            message: 'Forms Fetched successfully',
            listData:countData,
            countData: countData.length

        });
    });



});
router.post('/getLineCetegoryAccessControl',myLogger, function (req, res, next) {
    var request = new DTADashboardModel();
    var data = req.body;

    request.getLineCetegoryAccessControl(data).then((countData) => {
        if(countData.length>0){
            buildResponse(res, 200, {
                error: false,
                message: 'Forms Fetched successfully',
                listData:countData,
            });
        }else{
            buildResponse(res, 200, {
                error: true,
                message: "Sorry, You are not allowed to manipulate any Data in Why Why analysis!!!"
            });
        }
        
    })
});


router.get('/accessWiseData/:id',myLogger, function (req, res, next) {
    var reqData = req.params.id;
    var request = new DTADashboardModel();
    request.accessWiseData(reqData).then((countData) => {
        buildResponse(res, 200, {
            error: false,
            message: 'Forms Fetched successfully',
            listData:countData,
           
        });
    });
});

router.get('/getDistinctShops',myLogger, function (req, res, next) {
    var request = new DTADashboardModel();
    request.getDistinctShops().then((countData) => {
        buildResponse(res, 200, {
                error: false,
                message: 'Shops Fetched successfully',
                listData:countData,
            });
    });
});
router.get('/getDistinctLines/:lines',myLogger, function (req, res, next) {
    var data = req.params.lines;
    console.log('data---->',data)
    var request = new DTADashboardModel();
    request.getDistinctLines(data).then((countData) => {
        buildResponse(res, 200, {
            error: false,
            message: 'Lines Fetched successfully',
            listData:countData,
        });
    });
});

router.get('/getDistinctCategory/:lines',myLogger, function (req, res, next) {
    var data = req.params.lines;
    var request = new DTADashboardModel();
    request.getDistinctCategory(data).then((countData) => {
        buildResponse(res, 200, {
            error: false,
            message: 'Category Fetched successfully',
            listData:countData,
        });
    });
});

router.post('/getReportDashboardDTA',myLogger, function (req, res, next) {
    var user = new DTADashboardModel();
    var data = req.body;
    // console.log("data",data);
    user.getReportDashboardDTA(data).then((data) => {
        buildResponse(res, 200, {
            error: false,
            message: "Successfully display!",
            TableData: data


        });
    });
});



router.get('/checkForPreviousHDR/:id',myLogger, function (req, res, next) {
    var reqData = req.params.id;
    var request = new DTADashboardModel();
    request.checkForPreviousHDR(reqData).then((countData) => {
        buildResponse(res, 200, {
            error: false,
            message: 'Forms Fetched successfully',
            listData:countData,
           
        });
    });
});


router.get('/checkForPreviousHDRMain/:id',myLogger, function (req, res, next) {
    var reqData = req.params.id;
    var request = new DTADashboardModel();
    request.checkForPreviousHDRMain(reqData).then((countData) => {
        buildResponse(res, 200, {
            error: false,
            message: 'Forms Fetched successfully',
            listData:countData,
           
        });
    });
});


router.get('/kaisanNoCheck',myLogger, function (req, res, next) {
    // var reqData = req.params.id;
    var request = new DTADashboardModel();
    request.kaisanNoCheck().then((countData) => {
        buildResponse(res, 200, {
            error: false,
            message: 'Forms Fetched successfully',
            listData:countData,
           
        });
    });
});
router.get('/deleteRequest/:id',myLogger, function (req, res, next) {
    var reqData = req.params.id;
    var request = new DTADashboardModel();
    request.deleteRequest(reqData).then((countData) => {
        buildResponse(res, 200, {
            error: false,
            message: 'Forms Fetched successfully',
        });
    }).catch(()=>{
        buildResponse(res, 200, {
            error: false,
            message: 'Unable To delete',
        });
    })
});









function buildResponse(res, statusCode, responseData) {
    res.status(statusCode).json(responseData);
}

module.exports = router;
