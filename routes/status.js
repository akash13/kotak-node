var express = require('express');
var router = express.Router();
var bcrypt = require('bcryptjs');
var jwt = require('jsonwebtoken');
const Joi = require('joi');
var Status = require('../model/status');

var myLogger = require('../middleware.js');
var MyLogModel = require('../model/myLog');
var log = new MyLogModel();

const schema = Joi.object().keys({
    status_name: Joi.string().regex(/^[a-zA-Z ]*$/, '. Special symbols and numbers are not allow').max(20).required().not(''),
    status_status: Joi.string().max(10).required().not(''),
    created_by: Joi.string().allow(''),
    updated_by: Joi.string().allow(''),
});

router.post('/status', myLogger, function (req, res, next) {
    var status = new Status();
    var statusData = {
        status_name: req.body.status_name,
        status_status: req.body.status_status,
        created_by: req.body.created_by,
   };

    Joi.validate({
        status_name: req.body.status_name,
        status_status: req.body.status_status,
        created_by: req.body.created_by,
     
     
    }, schema, function (err, statusData) {
        if (err) {

            buildResponse(res, 200, {
                error: true,
                message: err['details'][0]["message"]
            });
        }
        else {
            status.saveStatus(statusData)
                .then((data) => {
                   console.log(data,"-----------")
                    if (data["data"] == 0) {
                        buildResponse(res, 200, {
                            error: true,
                            message: data["errMsg"]
                        });
                    } else {
                        buildResponse(res, 200, {
                            error: false,
                            message: "Stored Successfully"
                        });

                    }

                }).catch((errLog) => {
                   
                    log.addLog('route :: user.js ' + errLog);
                    buildResponse(res, 500, {
                        error: true,
                        message: "server error"
                    });
                });
        }
    });
});

router.post('/updateStatus', myLogger, function (req, res, next) {
    var status = new Status();
    var statusData = {
        status_id:req.body.status_id,
         status_name: req.body.status_name,
         status_status: req.body.status_status,
         updated_by: req.body.updated_by,
   };
console.log(statusData,"---------")
    Joi.validate({
        status_name: req.body.status_name,
        status_status: req.body.status_status,
        updated_by: req.body.updated_by,
     
     
    }, schema, function (err, schemastatusData) {
        if (err) {

            buildResponse(res, 200, {
                error: true,
                message: err['details'][0]["message"]
            });
        }
        else {
            status.updateStatus(statusData)
                .then((data) => {
                   console.log(data,"-----------")
                    if (data["data"] == 0) {
                        buildResponse(res, 200, {
                            error: true,
                            message: data["errMsg"]
                        });
                    } else {
                        buildResponse(res, 200, {
                            error: false,
                            message: "Updated Successfully"
                        });

                    }

                }).catch((errLog) => {
                   
                    log.addLog('route :: user.js ' + errLog);
                    buildResponse(res, 500, {
                        error: true,
                        message: "server error"
                    });
                });
        }
    });
});

router.get('/getStatus', myLogger, function (req, res, next) {
    var status = new Status();

    status.getStatus().then((data) => {
        
        if (data["data"] == '0') {
            buildResponse(res, 200, {
                error: false,
                message: data["errMsg"],

            });
        } else {
            buildResponse(res, 200, {
                error: false,
                message: "Successfully display!",
                data: data
            });
        }

    });

});

router.post('/deleteStatus', myLogger, function (req, res, next) {
    var status = new Status();
    var statusData={
        status_id:req.body.status_id,
        deleted_by:req.body.deleted_by
    };
    //var reqData = req.body.status_id;
    status.deleteStatus(statusData)
        .then((data) => {
            buildResponse(res, 200, {
                error: false,
                message: 'Status Deleted successfully',
                data: data
            });
        }).catch((errLog) => {
            log.addLog('route :: status.js ' + errLog);
            buildResponse(res, 404, {
                error: false,
                message: 'Please Try Again',
            });
        });
});

function buildResponse(res, statusCode, responseData) {
    res.status(statusCode).json(responseData);
}

module.exports = router;