const Sequelize = require('sequelize');
var con = require('./connection');
var randomstring = require("randomstring");
var bcrypt = require('bcryptjs');
var MyLogModel = require('../model/myLog');
var log = new MyLogModel();
const Op = Sequelize.Op;
var errMsg = "Server issue please try after sometime";

Sequelize.DATE.prototype._stringify = function _stringify(date, options) {
    return this._applyTimezone(date, options).format('YYYY-MM-DD HH:mm:ss.SSS');
  };
  
module.exports = class User {

    constructor() {

        this.UsrLgnToken = sequelize.define('Log_User_Login_Details', {
            T_INT_ID: { type: Sequelize.INTEGER, primaryKey: true, autoIncrement: true },
            T_CHR_Employee_Id: Sequelize.STRING,
            T_CHR_Token: Sequelize.STRING,
            T_DATE_createdAt: Sequelize.DATE,
            T_CHR_Isactive: {
                type: Sequelize.STRING(2),
                defaultValue: '0'
            },
        },
            {
                timestamps: false,
                createdAt: false,
                UpdatedAt: false,
            },
        );
        this.USER = con.define('M_User_Master', {
            T_INT_ID: {
                type: Sequelize.INTEGER,
                primaryKey: true,
                autoIncrement: true
            },
            T_CHR_Employee_Id: {
                type: Sequelize.STRING(20),
                allowNull: false,
                unique: true
            },
            T_CHR_Name: {
                type: Sequelize.STRING(50),
                allowNull: false
            },
            T_CHR_Email: {
                type: Sequelize.STRING(50),
                allowNull: true,
               
            },
            T_INT_mobile: {
                type: Sequelize.STRING(12),
                allowNull: true
            },
            T_CHR_telephone: {
                type: Sequelize.STRING(20),
                allowNull: true
            },
            T_INT_Department: {
                type: Sequelize.INTEGER,
                allowNull: true
            },
            T_CHR_Role: {
                type: Sequelize.STRING(10),
                allowNull: false
            },
            T_CHR_Password: {
                type: Sequelize.STRING,
                allowNull: true
            },
            T_CHR_Status: {
                type: Sequelize.STRING(10),
                allowNull: false
            },
            T_CHR_emp_authentication_type: {
                type: Sequelize.STRING(20),
                allowNull: false
            },

            T_CHR_Designation: {
                type: Sequelize.STRING(20),
                allowNull: true
            },
            T_CHR_Created_By: {
                type: Sequelize.STRING(20),
                allowNull: true
            },
            T_DATE_Created_Date_Time: {
                type: Sequelize.DATE,
                allowNull: true
            },
            T_CHR_Updated_By: {
                type: Sequelize.STRING(20),
                allowNull: true
            },
            T_DATE_Updated_Date_Time: {
                type: Sequelize.DATE,
                allowNull: true
            },
            T_CHR_Deleted_By: {
                type: Sequelize.STRING(20),
                allowNull: true
            },
            T_DATE_Deleted_Date_Time: {
                type: Sequelize.DATE,
                allowNull: true
            },
            T_CHR_Isdelete: {
                type: Sequelize.INTEGER,
                allowNull: true,
                defaultValue: 0
            },
        },
            {
                timestamps: false,
                createdAt: false,
                UpdatedAt: false,
            },


        );

        this.ROLE = con.define('M_Role_Master', {
            T_INT_Role_Id: {
                type: Sequelize.INTEGER,
                primaryKey: true,
                autoIncrement: true
            },

            T_CHR_Role_Name: {
                type: Sequelize.STRING(20),
                allowNull: false
            },


            T_CHR_Role_Status: {
                type: Sequelize.STRING(10),
                allowNull: false
            },



            T_CHR_Isdelete: {
                type: Sequelize.STRING(2),
                allowNull: false,
                defaultValue: '0'

            },
            T_CHR_Created_By: {
                type: Sequelize.STRING(20),
                allowNull: true
            },
            T_DATE_Created_Date_Time: {
                type: Sequelize.DATE,
                allowNull: true
            },
            T_CHR_Updated_By: {
                type: Sequelize.STRING(20),
                allowNull: true
            },
            T_DATE_Updated_Date_Time: {
                type: Sequelize.DATE,
                allowNull: true
            },
            T_CHR_Deleted_By: {
                type: Sequelize.STRING(20),
                allowNull: true
            },
            T_DATE_Deleted_Date_Time: {
                type: Sequelize.DATE,
                allowNull: true
            }
        },
            {
                timestamps: false,
                createdAt: false,
                UpdatedAt: false,
            },





        );
    }

    saveUser(userData) {
        console.log("this save user data mode", userData);
        return sequelize.sync().then(() => {
            return this.USER.create({
                T_CHR_Employee_Id: userData.empId,
                T_CHR_Name: userData.empName,
                T_CHR_Email: userData.empEmail,
                T_INT_mobile: userData.empContact,
                T_CHR_telephone: userData.empTelephone,
                T_CHR_Password: userData.empPassword,
                T_CHR_emp_authentication_type: userData.emp_authentication_type,
                T_CHR_Status: userData.empStatus,
                T_CHR_Role: userData.empRole,
                T_CHR_Designation: userData.empDesignation,
                T_INT_Department: userData.empDepartment,
                T_CHR_Created_By: userData.createdBy,
                T_DATE_Created_Date_Time:new Date()

            }).catch(function (errLog) {

                log.addLog('model :: user.js 130' + errLog);
                var err;
                if (errLog == "SequelizeUniqueConstraintError: Validation error") {
                    err = {
                        data: 0,
                        errMsg: "Employee Id and email should be unique"
                    }
                } else if (errLog != "SequelizeUniqueConstraintError: Validation error") {
                    err = {
                        data: 0,
                        errMsg: errLog["errors"][0]["message"]
                    }

                }
                return err;
            });
        }).catch(function (errLog) {
            log.addLog('model :: user.js 147' + errLog);
        });
    }

    getUsers() {
        return sequelize.sync().then(() => {
            return this.USER.all({ limit: 50 }, {
                where: {
                    T_CHR_Isdelete: '0',
                    T_CHR_Status : 'enable'
                }
            }).then((data) => {

                return data
            }).catch((errLog) => {
                var err = {
                    data: 0,
                    errMsg: errMsg
                }
                log.addLog('model :: user.js 160' + errLog);
                return err;

            });
        }).catch((errLog) => {
            var err = {
                data: 0,
                errMsg: errMsg
            }
            log.addLog('model :: user.js 169 ' + errLog);
            return err;
        });
    }
    getInviteesUsers() {
        return sequelize.sync().then(() => {
            return this.USER.all({ limit: 50 }, {
                where: {
                    T_CHR_Isdelete: '0'
                }
            }).then((data) => {

                return data
            }).catch((errLog) => {
                var err = {
                    data: 0,
                    errMsg: errMsg
                }
                log.addLog('model :: user.js 160' + errLog);
                return err;

            });
        }).catch((errLog) => {
            var err = {
                data: 0,
                errMsg: errMsg
            }
            log.addLog('model :: user.js 169 ' + errLog);
            return err;
        });
    }


    getPermanentUsers(data) {
        console.log("ddddddddd===", data)
        return sequelize.sync().then(() => {
            return this.USER.all({
                where: {
                    T_CHR_Isdelete: '0',
                    T_CHR_Employee_Id: {
                        [Sequelize.Op.in]: data.userId.split(',')
                    }
                }
            }).then((data) => {
             
                return data
            }).catch((errLog) => {
                var err = {
                    data: 0,
                    errMsg: errMsg
                }
                console.log(errLog);
                log.addLog('model :: user.js 160' + errLog);
                return err;

            });
        }).catch((errLog) => {
            console.log(errLog);
            var err = {
                data: 0,
                errMsg: errMsg
            }
            log.addLog('model :: user.js 169 ' + errLog);
            return err;
        });
    }

    getSelectedInviteesUsers(data) {
        console.log("ddddddddd===", data)
        return sequelize.sync().then(() => {
            return this.USER.all({
                where: {
                    T_CHR_Isdelete: '0',
                    T_CHR_Employee_Id: {
                        [Sequelize.Op.in]: data.userId.split(',')
                    }
                }
            }).then((data) => {
             
                return data
            }).catch((errLog) => {
                var err = {
                    data: 0,
                    errMsg: errMsg
                }
                console.log(errLog);
                log.addLog('model :: user.js ' + errLog);
                return err;

            });
        }).catch((errLog) => {
            console.log(errLog);
            var err = {
                data: 0,
                errMsg: errMsg
            }
            log.addLog('model :: user.js  ' + errLog);
            return err;
        });
    }
    getSelectedChairUsers(data) {
        console.log("ddddddddd===", data)
        return sequelize.sync().then(() => {
            return this.USER.all({
                where: {
                    T_CHR_Isdelete: '0',
                    T_CHR_Employee_Id: {
                        [Sequelize.Op.in]: data.userId.split(',')
                    }
                }
            }).then((data) => {
             
                return data
            }).catch((errLog) => {
                var err = {
                    data: 0,
                    errMsg: errMsg
                }
                console.log(errLog);
                log.addLog('model :: user.js ' + errLog);
                return err;

            });
        }).catch((errLog) => {
            console.log(errLog);
            var err = {
                data: 0,
                errMsg: errMsg
            }
            log.addLog('model :: user.js  ' + errLog);
            return err;
        });
    }
    
    getFilterInviteesUsers(data) {
        var l = 50;
        var where = "a=10";
        return sequelize.sync().then(() => {
            return this.USER.findAll({
                limit: 50,
                where: {
                    T_CHR_Isdelete: '0',
                    T_CHR_Name: {
                        [Op.like]: '%' + data.value + '%'
                    }
                }
            }).then((data) => {

                return data
            }).catch((errLog) => {
                var err = {
                    data: 0,
                    errMsg: errMsg
                }
                log.addLog('model :: user.js 160' + errLog);
                return err;

            });
        }).catch((errLog) => {
            var err = {
                data: 0,
                errMsg: errMsg
            }
            log.addLog('model :: user.js 169 ' + errLog);
            return err;
        });
    }

    getUserById(Id) {
        return sequelize.sync().then(() => {
            return this.USER.findOne({
                where: {
                    T_INT_ID: Id
                },
            }).then((data) => {
                return data;
            }).catch((errLog) => {

                var err = {
                    data: 0,
                    errMsg: errMsg
                }
                log.addLog('model :: user.js 188' + errLog);

                return err;

            });
        }).catch((errLog) => {

            var err = {
                data: 0,
                errMsg: errMsg
            }
            log.addLog('model :: user.js 199' + errLog);

            return err;
        });
    }



    updateUser(userData, Id) {
        return sequelize.sync().then(() => {
            return this.USER.update({
                T_CHR_Employee_Id: userData.empId,
                T_CHR_Name: userData.empName,
                T_CHR_Email: userData.empEmail,
                T_INT_mobile: userData.empContact,
                T_CHR_telephone: userData.empTelephone,
                T_CHR_emp_authentication_type: userData.emp_authentication_type,
                T_CHR_Status: userData.empStatus,
                T_CHR_Role: userData.empRole,
                T_CHR_Designation: userData.empDesignation,
                T_INT_Department: userData.empDepartment,
                T_CHR_Updated_By: userData.updatedBy,
            }, {
                    where: {
                        T_INT_ID: Id
                    }
                }).catch(function (errLog) {
                    log.addLog('model :: user.js 232' + errLog);
                    var err;
                    if (errLog == "SequelizeUniqueConstraintError: Validation error") {
                        err = {
                            data: 0,
                            errMsg: "Employee Id and email should be unique"
                        }
                    } else if (errLog != "SequelizeUniqueConstraintError: Validation error") {
                        err = {
                            data: 0,
                            errMsg: errLog["errors"][0]["message"]
                        }

                    }
                    return err;
                });
        }).catch(function (errLog) {
            log.addLog('model :: user.js 249' + errLog);
            return errLog;
        });
    }


    newPassword(Id, pass) {
        return sequelize.sync().then(() => {
            return this.USER.update({
                T_CHR_Password: pass,
            }, {
                    where: {
                        T_CHR_Employee_Id: Id
                    }
                });
        });
    }


    insertForADLogin(tokenData) {
        return sequelize.sync().then(() => {
            return this.UsrLgnToken.create({
                T_CHR_Employee_Id: tokenData.emp_id,
                T_CHR_Token: tokenData.token,
                T_DATE_createdAt: new Date,
                T_CHR_Isactive: '0',
            })
        });
    }

    deleteUser(Id) {
        return sequelize.sync().then(() => {
            return this.USER.update({
                T_CHR_Isdelete: '1'
            }, {
                    where: {
                        T_INT_ID: Id
                    }
                });
        });
    }

    getUserByEmpId(Id) {
        console.log("iiiiiiiiiiii", Id)
        return sequelize.sync().then(() => {

            return this.USER.findOne({
                where: {
                    T_CHR_Employee_Id: Id
                },
            }

            ).catch((e) => {
                console.log("eeeeeeeeeeeeeeee", e)
            });
        }).catch((e) => {
            console.log("eeeeeeeeeeeeeeee", e)
        });
    }



    getUserByEmpIdForChangePass(userData, pass) {
        console.log("iiiiiiiiiiii", userData);
        console.log("pass-----", pass);
        return sequelize.sync().then(() => {
            return this.USER.findOne({
                where: {
                    T_CHR_Employee_Id: userData.emp_id
                },
            }).then((data) => {

                if (data != null) {
                    console.log(data['dataValues']['T_CHR_Password'], "==========", pass)
                    if (this.isValidPassword(data['dataValues']['T_CHR_Password'], userData.currentPassword)) {
                        return data;
                    }
                } else {
                    return null;
                }
            }).catch((err) => {
                console.log("111111111", err)
                buildResponse(res, 200, {
                    error: true,
                    message: "An error occurred!"
                });
            });
        });
    }




    isValidPassword(userpass, password) {
        console.log("pass comapare", userpass, password);
        return bcrypt.compareSync(password, userpass);
    }


    signIn(userData) {
        return sequelize.sync().then(() => {

            return sequelize.query("select u.T_INT_ID,u.T_CHR_Employee_Id,u.T_CHR_Name,T_CHR_Email,T_CHR_Designation," +
                " u.T_CHR_Role,r.T_CHR_Role_Name,u.T_CHR_Password from M_User_Masters as u inner join M_Role_Masters as r on u.T_CHR_Role=r.T_INT_Role_Id " +
                " where T_CHR_Employee_Id = '" + userData.username + "'", { bind: ['active'], type: sequelize.QueryTypes.SELECT }
            ).then(data => {
                console.log("11111111111", data)
                if (data != null) {

                    if (this.isValidPassword(data[0]['T_CHR_Password'], userData.password)) {
                        var userToken = randomstring.generate({
                            length: 24,
                            charset: 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789'
                        });
                        var tokenData = { T_CHR_Employee_Id: data[0]['T_CHR_Employee_Id'], token: userToken, data: data };
                        return sequelize.query("update Log_User_Login_Details set T_CHR_Isactive ='1' where  T_CHR_Employee_Id ='" + tokenData.T_CHR_Employee_Id + "'", {


                            type: sequelize.QueryTypes.INSERT
                        }).then(() => {
                            return sequelize.sync().then(() => {
                                return this.UsrLgnToken.create({
                                    T_CHR_Employee_Id: tokenData.T_CHR_Employee_Id,
                                    T_CHR_Token: tokenData.token,
                                    T_DATE_createdAt: new Date,
                                    T_CHR_Isactive: '0',


                                })
                            }).then(() => {
                                return tokenData;
                            });

                        })







                        // this.isuserTokenExist(tokenData);
                        // return tokenData;
                    }
                }
                else {
                    console.log("22222222")
                    return null;
                }
            }).catch((err) => {
                console.log(err)
            });
        });
    }


    signinForVIntranet(userData) {
        return sequelize.sync().then(() => {

            return sequelize.query("SELECT 'Vintranet' as Vintranet,COMPANY_NAME,DATE_OF_BIRTH,DEPARTMENT,DIVISION,EDUCATION,EMP_CODE,EMP_NO,ENCRYPTEDPASS,EXTENSION_NO,EmpName,FIRST_NAME,FULL_NAME,JOB_TITLE,LAST_NAME,LOCATION,MANAGER,MOBILE_NO,RESIDENTIAL_NO,SAP_ID,SUB_DEPARTMENT,USER_NAME,WORK_EMAIL from V_INTRANET_PORTAL where USER_NAME ='" + userData.username + "';", {
                type: sequelize.QueryTypes.SELECT
            }).then(data => {
                // 
                if (data[0] === 'undefined' || data[0] === undefined) {
                    return null;
                }
                else {

                    let ddate = new Date(data[0]['DATE_OF_BIRTH']);
                    var dd = ddate.getDate();
                    var mm = ddate.getMonth() + 1; //January is 0!
                    var yyyy = ddate.getFullYear();
                    if (dd < 10) {
                        this.dd = '0' + dd + ''
                    } else {
                        this.dd = dd + '';
                    }
                    if (mm < 10) {
                        this.mm = '0' + mm + ''
                    } else {
                        this.mm = mm + '';
                    }
                    var password = this.dd + this.mm + yyyy;
                    //var password = JSON.stringify(data[0]['DATE_OF_BIRTH']).slice(1, 11);
                    // 
                    if (data != null) {
                        if (password === userData.password) {
                            // 
                            var userToken = randomstring.generate({
                                length: 24,
                                charset: 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789'
                            });
                            var tokenData = { T_CHR_Employee_Id: data[0]['USER_NAME'], token: userToken, data: data[0] };

                            sequelize.sync().then(() => {
                                this.UsrLgnToken.create({
                                    T_CHR_Employee_Id: tokenData.emp_id,
                                    T_CHR_Token: tokenData.token
                                })
                            });
                            return tokenData;
                            // var tokenlength =this.isuserTokenExist(tokenData);
                            // //
                            // 
                            // if(tokenlength>0){
                            //     return tokenData;
                            // }

                        }

                    }
                    else {
                        // 
                        return null;
                    }
                }
            });
        });
    }

    isuserTokenExist(EmpData) {
        sequelize.sync().then((data) => {
            this.UsrLgnToken.find({
                where:
                {
                    T_CHR_Employee_Id: EmpData.emp_id
                }

            }).then(data => {
                if (data != null) {
                    sequelize.sync().then(() => {
                        this.UsrLgnToken.update({
                            T_CHR_Token: EmpData.token,
                        }, {
                                where: {
                                    T_CHR_Employee_Id: EmpData.emp_id
                                }
                            }).then(data => {
                                return data;
                            });
                    });
                } else {
                    sequelize.sync().then(() => {
                        this.UsrLgnToken.create({
                            T_CHR_Employee_Id: EmpData.emp_id,
                            T_CHR_Token: EmpData.token
                        }).then(data => {
                            return data;
                        })
                    });
                }


            });
        });

    }



    getUserToken(EmpData) {
        return sequelize.query("select * from dbo.Log_User_Login_Details where T_CHR_Employee_Id = '" + EmpData.emp_id + "' AND T_CHR_Token='" + EmpData.token + "'", { bind: ['active'], type: sequelize.QueryTypes.SELECT }
        ).then(TokenData => {
            return TokenData;
        })
    }

    setlogouttokentonull(id) {
        return sequelize.sync().then(() => {

            return sequelize.query("update Log_User_Login_Details set T_CHR_Isactive ='1' where  T_CHR_Employee_Id ='" + id + "'", {


                type: sequelize.QueryTypes.INSERT
            });

            //     }
        })
        //   
    }

    deleteADUser() {
        return sequelize.sync().then(() => {
            return this.USER.destroy({
                where: {
                    T_CHR_emp_authentication_type: 'Windows AD'
                }
            });
        });
    }

    inserSSOFailedToDB(userName) {
        return sequelize.sync().then(() => {
            return sequelize.query("insert into FailedSSOUsersData (ticket_no) values('" + userName + "'); ", {
                type: sequelize.QueryTypes.INSERT
            })
        })

    }

    getEmpRole() {
        return sequelize.sync().then(() => {
            return this.ROLE.all().then((data) => {
                return data
            }).catch((errLog) => {
                var err = {
                    data: 0,
                    errMsg: errMsg
                }
                log.addLog('model :: user.js 519' + errLog);
                return err;

            });
        }).catch((errLog) => {
            var err = {
                data: 0,
                errMsg: errMsg
            }
            log.addLog('model :: user.js 529 ' + errLog);
            return err;
        });
    }




};