var express = require('express');
var router = express.Router();
var PatentinfoModel = require('../../model/PatentModel');
var soap = require('strong-soap').soap;
var myLogger = require('../../middleware.js');
var options = {
    wsdl_headers: {
        'Authorization': 'Basic ' + new Buffer('piwebusr' + ':' + 'piproxy123').toString('base64'),
    }
};





router.get('/getassignmentTeamMailIDs/', myLogger, function (req, res, next) {
    var request = new PatentinfoModel();
  //  var userId = req.params.userId;
    request.getassignmentTeamMailIDs().then((countData) => {

        //  console.log('in route :',countData);
        buildResponse(res, 200, {
            error: false,
            message: 'Forms Fetched successfully',
            listData: countData,
            countData: countData.length

        });
    });

    //}

});
router.post('/sendMailFromIndentor', myLogger, function (req, res, next) {
    var request = new PatentinfoModel();
    var data = req.body;
    //console.log('in route data', data);
    request.sendMailFromIndentor(data)
});


router.get('/getGrantedPatentCount/:userId', myLogger, function (req, res, next) {
    var request = new PatentinfoModel();
    var userId = req.params.userId;
    request.getGrantedPatentCount(userId).then((countData) => {

        //  console.log('in route :',countData);
        buildResponse(res, 200, {
            error: false,
            message: 'Forms Fetched successfully',
            listData: countData,
            countData: countData.length

        });
    });

    //}

});

router.get('/getInIPOProcessCount/:userId', myLogger, function (req, res, next) {
    var request = new PatentinfoModel();
    var userId = req.params.userId;
    request.getInIPOProcessCount(userId).then((countData) => {

        //  console.log('in route :',countData);
        buildResponse(res, 200, {
            error: false,
            message: 'Forms Fetched successfully',
            listData: countData,
            countData: countData.length

        });
    });

    //}

});

router.get('/getInProcessCount/:userId', myLogger, function (req, res, next) {
    var request = new PatentinfoModel();
    var userId = req.params.userId;
    request.getInProcessCount(userId).then((countData) => {

        //  console.log('in route :',countData);
        buildResponse(res, 200, {
            error: false,
            message: 'Forms Fetched successfully',
            listData: countData,
           

        });
    });

    //}

});
router.post('/SearchReportAtIPOStatus', myLogger, function (req, res, next) {
    var request = new PatentinfoModel();
    var data = req.body;
   // console.log('data==', data);
    request.SearchReportAtIPOStatus(data)
        .then((data) => {
            buildResponse(res, 200, {
                error: false,
                message: "Record Inserted Successfully",
                responseData: data
            });
        }).catch((err) => {
            buildResponse(res, 400, {
                error: true,
                message: err
            });
        });
});
router.post('/ResponseReportAtIPOStatus', myLogger, function (req, res, next) {
    var request = new PatentinfoModel();
    var data = req.body;
   // console.log('data==', data);
    request.ResponseReportAtIPOStatus(data)
        .then((data) => {
            buildResponse(res, 200, {
                error: false,
                message: "Record Inserted Successfully",
                responseData: data
            });
        }).catch((err) => {
            buildResponse(res, 400, {
                error: true,
                message: err
            });
        });
});
router.post('/GrantPatentAtIPOStatus', myLogger, function (req, res, next) {
    var request = new PatentinfoModel();
    var data = req.body;
   // console.log('data==', data);
    request.GrantPatentAtIPOStatus(data)
        .then((data) => {
            buildResponse(res, 200, {
                error: false,
                message: "Record Inserted Successfully",
                responseData: data
            });
        }).catch((err) => {
            buildResponse(res, 400, {
                error: true,
                message: err
            });
        });
});
router.post('/UpdateExaminationAtIPOStatus', myLogger, function (req, res, next) {
    var request = new PatentinfoModel();
    var data = req.body;
   // console.log('data==', data);
    request.UpdateExaminationAtIPOStatus(data)
        .then((data) => {
            buildResponse(res, 200, {
                error: false,
                message: "Record Inserted Successfully",
                responseData: data
            });
        }).catch((err) => {
            buildResponse(res, 400, {
                error: true,
                message: err
            });
        });
});

router.post('/UpdatePSSubmission', myLogger, function (req, res, next) {
    var request = new PatentinfoModel();
    var data = req.body;
   // console.log('data==', data);
    request.UpdatePSSubmission(data)
        .then((data) => {
            buildResponse(res, 200, {
                error: false,
                message: "Record Inserted Successfully",
                responseData: data
            });
        }).catch((err) => {
            buildResponse(res, 400, {
                error: true,
                message: err
            });
        });
});

router.post('/UpdateCSSubmission', myLogger, function (req, res, next) {
    var request = new PatentinfoModel();
    var data = req.body;
   // console.log('data==', data);
    request.UpdateCSSubmission(data)
        .then((data) => {
            buildResponse(res, 200, {
                error: false,
                message: "Record Inserted Successfully",
                responseData: data
            });
        }).catch((err) => {
            buildResponse(res, 400, {
                error: true,
                message: err
            });
        });
});


router.post('/UpdateReviewStage2', myLogger, function (req, res, next) {
    var request = new PatentinfoModel();
    var data = req.body;
   // console.log('data==', data);
    request.UpdateReviewStage2(data)
        .then((data) => {
            buildResponse(res, 200, {
                error: false,
                message: "Record Inserted Successfully",
                responseData: data
            });
        }).catch((err) => {
            buildResponse(res, 400, {
                error: true,
                message: err
            });
        });
});
router.post('/UpdateReviewStage1', myLogger, function (req, res, next) {
    var request = new PatentinfoModel();
    var data = req.body;
   // console.log('data==', data);
    request.UpdateReviewStage1(data)
        .then((data) => {
            buildResponse(res, 200, {
                error: false,
                message: "Record Inserted Successfully",
                responseData: data
            });
        }).catch((err) => {
            buildResponse(res, 400, {
                error: true,
                message: err
            });
        });
});


router.post('/ResponseFromAssignee', myLogger, function (req, res, next) {
    var request = new PatentinfoModel();
    var data = req.body;
   // console.log('data==', data);
    request.ResponseFromAssignee(data)
        .then((data) => {
            buildResponse(res, 200, {
                error: false,
                message: "Record Inserted Successfully",
                responseData: data
            });
        }).catch((err) => {
            buildResponse(res, 400, {
                error: true,
                message: err
            });
        });
});


router.post('/AssignPatent', myLogger, function (req, res, next) {
    var request = new PatentinfoModel();
    var data = req.body;
   // console.log('data==', data);
    request.AssignPatent(data)
        .then((data) => {
            buildResponse(res, 200, {
                error: false,
                message: "Record Inserted Successfully",
                responseData: data
            });
        }).catch((err) => {
            buildResponse(res, 400, {
                error: true,
                message: err
            });
        });
});
router.get('/getUserDetailsForPirForm/:id', myLogger, function (req, res, next) {
    console.log("in mroute", req.params.id)
    var request = new PatentinfoModel();
    var id = req.params.id;

    request.getUserDetailsForPirForm(id).then((data) => {
        buildResponse(res, 200, {
            error: false,
            message: "Successfully display!",
            responseData: data
        });
    });
});


router.post('/UpdatePatentData', myLogger, function (req, res, next) {
    var request = new PatentinfoModel();
    var data = req.body;
   // console.log('data==', data);
    request.UpdatePatentData(data)
        .then((data) => {
            buildResponse(res, 200, {
                error: false,
                message: "Record Inserted Successfully",
                responseData: data
            });
        }).catch((err) => {
            buildResponse(res, 400, {
                error: true,
                message: err
            });
        });
});

router.post('/submitData', myLogger, function (req, res, next) {
    var request = new PatentinfoModel();
    var data = req.body;
  
    request.submitData(data)
        .then((data) => {
            buildResponse(res, 200, {
                error: false,
                message: "Record Inserted Successfully",
                responseData: data
            });
        }).catch((err) => {
            buildResponse(res, 400, {
                error: true,
                message: err
            });
        });
});
router.get('/getEmployeeList', myLogger, function (req, res, next) {
  
    var request = new PatentinfoModel();
    // var id = req.params.id;
    request.getEmployeeList().then((data) => {
        buildResponse(res, 200, {
            error: false,
            message: "Successfully display!",
            responseData: data
        });
    });
});



router.get('/getSpecializationList', myLogger, function (req, res, next) {
   
    var request = new PatentinfoModel();
    // var id = req.params.id;
    request.getSpecializationList().then((data) => {
        buildResponse(res, 200, {
            error: false,
            message: "Successfully display!",
            responseData: data
        });
    });
});
router.get('/getPlantLocations', myLogger, function (req, res, next) {

    var request = new PatentinfoModel();
    // var id = req.params.id;
    request.getPlantLocations().then((data) => {
        buildResponse(res, 200, {
            error: false,
            message: "Successfully display!",
            responseData: data
        });
    });
});


router.get('/getPurchaseOrg', myLogger, function (req, res, next) {
    console.log("in mroute", req.params.id)
    var request = new PatentinfoModel();
    // var id = req.params.id;
    request.getPurchaseOrg().then((data) => {
        buildResponse(res, 200, {
            error: false,
            message: "Successfully display!",
            responseData: data
        });
    });
});

router.get('/getDocumentType', myLogger, function (req, res, next) {
    console.log("in mroute", req.params.id)
    var request = new PatentinfoModel();
    // var id = req.params.id;
    request.getDocumentType().then((data) => {
        buildResponse(res, 200, {
            error: false,
            message: "Successfully display!",
            responseData: data
        });
    });
});

router.get('/getBuyerCode', myLogger, function (req, res, next) {
    console.log("in mroute", req.params.id)
    var request = new PatentinfoModel();
    // var id = req.params.id;
    request.getBuyerCode().then((data) => {
        buildResponse(res, 200, {
            error: false,
            message: "Buyer Code Successfully display!",
            responseData: data
        });
    });
});





router.get('/getVendorDetails/:vcode', myLogger, function (req, res, next) {
    console.log("in mroute", req.params.id)
    var request = new PatentinfoModel();
    var id = req.params.vcode;
    //console.log("idididididid",id);
    var url = "http://bfpis.kalyanicorp.com:50000/dir/wsdl?p=ic/b39b7f9e36423b43809ded8db3c355a3";
    var RequestData = {
        MT_RMGP_READ_VENDOR_REQ: { LIFNR: id }
    }
    soap.createClient(url, options, function (err, client) {
        if (err) {
            callback(err);
            return;
        }
        console.log('Client is ready');
        client.setSecurity(new soap.BasicAuthSecurity('piwebusr', 'piproxy123'));
        client.SI_RMGP_READ_VENDORService.HTTP_Port.SI_RMGP_READ_VENDOR(RequestData, function (err, response) {
            if (err) {
                console.log("err", err);
            } else {
                console.log("response", response);
                buildResponse(res, 200, {
                    error: false,
                    message: "Successfully display!",
                    responseData: response
                });
            }
        });

    });
});


router.get('/getBuyerCodeDetailsOnload', myLogger, function (req, res, next) {
    console.log("in mroute", req.params.id)
    var request = new PatentinfoModel();
    var id = req.params.vcode;
    //console.log("idididididid",id);
    var url = "http://bfpis.kalyanicorp.com:50000/dir/wsdl?p=ic/e062ba8204be38a39ce63a4033b5fff9";
    var RequestData = {
        MT_PURCHASE_BUYER_MASTER_REQ: { BUYER_TAB: { BUYER_DATA: { BUYER_CODE: '06', BUYER_NAME: 'M S DURUGKAR' } } }

    }
    soap.createClient(url, options, function (err, client) {
        if (err) {
            callback(err);
            return;
        }
        console.log('Client is ready');
        client.setSecurity(new soap.BasicAuthSecurity('piwebusr', 'piproxy123'));
        client.SI_PURCHASE_BUYER_MASTERService.HTTP_Port.SI_PURCHASE_BUYER_MASTER(RequestData, function (err, response) {
            if (err) {
                console.log("err", err);
            } else {
                console.log("response", response);
                request.truncateTableBuyerode().then((data) => {



                    response.BUYER_TAB.BUYER_DATA.forEach(element => {
                        console.log("buyer", element);
                        request.insertInBuyerode(element).then((data) => {

                        });

                    });

                    buildResponse(res, 200, {
                        error: false,
                        message: "Successfully display!",
                        responseData: data
                    });

                });

            }
        });


    });
});


router.get('/ListBuyerCodeDetails', myLogger, function (req, res, next) {
    console.log("in mroute", req.params.id)
    var request = new PatentinfoModel();
    // var id = req.params.id;
    request.ListBuyerCodeDetails().then((data) => {
        buildResponse(res, 200, {
            error: false,
            message: "Buyer Code Successfully display!",
            responseData: data
        });
    });
});




router.post('/getMatfield', myLogger, function (req, res, next) {
    var matno = 'X09307001376,X09307001370';
    var responseArray = [];
    var data = req.body;
    var url = "http://bfpis.kalyanicorp.com:50000/dir/wsdl?p=ic/ebf0bb58a4283866b643c0a934a538cb";
    console.log('ididididid', data);
    var RequestData = {
        MT_PURCHASE_MATERIAL_DATA_REQ:
        {
            MATERIAL_TAB: {
                MATERIAL_DATA: {
                    VENDOR_CODE: data.Vendor_Code,
                    PURCHASING_ORG: data.Purchase_Org,
                    BUYER_CODE: data.Buyer_Code,
                    MATERIAL_NO: data.Material_Code,
                    UNIT_PRICE: "0.0",
                    QUANTITY: "0.0",
                    VENDOR_CURRENCY: '',
                    VENDOR_LOCATION: "",
                    PR_NO: "",
                    PR_RELEASE_DATE: "",
                    MATERIAL_DESC: "",
                    UNIT_OF_MEASURE: "",
                    INFO_CATEGORY: "",
                    PLANTS: ""
                }
            }
        }
    }
    soap.createClient(url, options, function (err, client) {
        if (err) {
            callback(err);
            return;
        }
        client.setSecurity(new soap.BasicAuthSecurity('piwebusr', 'piproxy123'));
        client.SI_PURCHASE_MATERIAL_DATAService.HTTP_Port.SI_PURCHASE_MATERIAL_DATA(RequestData, function (err, response, envelope) {
            if (err) {
                console.log("err", err);

                buildResponse(res, 200, {
                    error: true,
                    message: "errr"
                });
            } else {
                console.log("response", response);
                buildResponse(res, 200, {
                    error: false,
                    message: "record fetch succcessfull",
                    responseData: response
                });
            }
        });
    });
});

router.post('/getMatfieldByPRNO', myLogger, function (req, res, next) {
    //var matno = 'X09307001376,X09307001370';
    var responseArray = [];
    var data = req.body;
    var url = "http://bfpis.kalyanicorp.com:50000/dir/wsdl?p=ic/ebf0bb58a4283866b643c0a934a538cb";
    console.log('getbypr', data);
    var RequestData = {
        MT_PURCHASE_MATERIAL_DATA_REQ:
        {
            MATERIAL_TAB: {
                MATERIAL_DATA: {
                    VENDOR_CODE: data.Vendor_Code,
                    PURCHASING_ORG: data.Purchase_Org,
                    BUYER_CODE: data.Buyer_Code,
                    MATERIAL_NO: '',
                    UNIT_PRICE: "0.0",
                    QUANTITY: "0.0",
                    VENDOR_CURRENCY: '',
                    VENDOR_LOCATION: "",
                    PR_NO: data.PR_NO,
                    PR_RELEASE_DATE: "",
                    MATERIAL_DESC: "",
                    UNIT_OF_MEASURE: "",
                    INFO_CATEGORY: "",
                    PLANTS: ""
                }
            }
        }
    }
    soap.createClient(url, options, function (err, client) {
        if (err) {
            callback(err);
            return;
        }
        client.setSecurity(new soap.BasicAuthSecurity('piwebusr', 'piproxy123'));
        client.SI_PURCHASE_MATERIAL_DATAService.HTTP_Port.SI_PURCHASE_MATERIAL_DATA(RequestData, function (err, response, envelope) {
            if (err) {
                console.log("err", err);

                buildResponse(res, 200, {
                    error: true,
                    message: "errr"
                });
            } else {
                console.log("response", response);
                buildResponse(res, 200, {
                    error: false,
                    message: "record fetch succcessfull",
                    responseData: response
                });
            }
        });
    });
});


router.post('/submitData', myLogger, function (req, res, next) {

    var request = new PatentinfoModel();
    var data = req.body;
    console.log("dataaaaa", data);

    request.submitData(data)
        .then((data) => {
            buildResponse(res, 200, {
                error: false,
                message: "Record Inserted Successfully",
                responseData: data
            });

        }).catch((err) => {
            buildResponse(res, 200, {
                error: true,
                message: "Fail to Store"
            });
        });

})



router.get('/getpatentList/:userId', myLogger, function (req, res, next) {
    var request = new PatentinfoModel();
    var userId = req.params.userId;
    request.getpatentList(userId).then((countData) => {

        //  console.log('in route :',countData);
        buildResponse(res, 200, {
            error: false,
            message: 'Forms Fetched successfully',
            listData: countData,
            countData: countData.length

        });
    });

    //}

});

router.get('/deletePIR/:id', myLogger, function (req, res, next) {
    var request = new PatentinfoModel();
    var reqData = req.params.id;
    request.deletePIR(reqData)
        .then((userData) => {
            buildResponse(res, 200, {
                error: false,
                message: ' Deleted successfully',
                UserData: userData
            });
        }).catch((err) => {
            buildResponse(res, 500, {
                error: false,
                message: 'Please Try Again',
            });
        });
});

router.get('/getDataOnUpdateForm/:id', myLogger, function (req, res, next) {
    var request = new PatentinfoModel();
    var reqData = req.params.id;
    request.getDataOnUpdateForm(reqData)
        .then((userData) => {
            buildResponse(res, 200, {
                error: false,
                message: 'Data Fetch Successfully',
                UserData: userData
            });
        }).catch((err) => {
            buildResponse(res, 500, {
                error: false,
                message: 'Please Try Again',
            });
        });
});


router.post('/UpdateData', myLogger, function (req, res, next) {

    var request = new PatentinfoModel();
    var data = req.body;
    console.log("dataaaaa", data);

    request.UpdateData(data)
        .then((data) => {
            buildResponse(res, 200, {
                error: false,
                message: "Record Updated Successfully"
            });

        }).catch((err) => {
            buildResponse(res, 200, {
                error: true,
                message: "Fail to Store"
            });
        });

});


router.get('/getApprovalMatrixByBuyerCode/:id', myLogger, function (req, res, next) {
    var request = new PatentinfoModel();
    var buyCode = req.params.id;
    request.getApprovalMatrixByBuyerCode(buyCode)
        .then((data) => {
            buildResponse(res, 200, {
                error: false,
                message: 'Data of Release Fetch Successfully',
                releaseData: data
            });
        }).catch((err) => {
            buildResponse(res, 500, {
                error: false,
                message: 'Please Try Again',
            });
        });
});

router.post('/ApprovalData', myLogger, function (req, res, next) {

    var request = new PatentinfoModel();
    var data = req.body;
    console.log("dataaaaa", data);

    request.ApprovalData(data)
        .then((data) => {
            buildResponse(res, 200, {
                error: false,
                message: "Record Updated Successfully"
            });

        }).catch((err) => {
            buildResponse(res, 200, {
                error: true,
                message: "Fail to Store"
            });
        });

});


router.get('/getCurrentUserBuyerCode/:currentUser', myLogger, function (req, res, next) {
    // console.log("in mroute", req.params.currentUser)
    var request = new PatentinfoModel();
    var currentUser = req.params.currentUser;
    request.getCurrentUserBuyerCode(currentUser).then((data) => {
        buildResponse(res, 200, {
            error: false,
            message: "Successfully got the buyer display!",
            responseData: data
        });
    });
});

router.get('/finalApproveAndPushToSAP/:reqNo/:status/:process', myLogger, function (req, res, next) {
    console.log("in mroute push")
    var request = new PatentinfoModel();
    var reqNo = req.params.reqNo;
    // var reqNo ='3';
    var status = req.params.status;
    var process = req.params.process;
    console.log("aaja", reqNo, status, process);

    request.getDataFORSAPSEND(reqNo).then((data) => {
        console.log('aaja222', data)
        request.finalApproveAndPushToSAP(reqNo, status, process, data).then((record) => {
            buildResponse(res, 200, {
                error: false,
                message: "Successfully Saved into SAP!",
                responseData: record
            });
        });
    });


});

router.post('/getEmailId', myLogger, function (req, res, next) {

    var request = new PatentinfoModel();
    var data = req.body;
    //console.log("dataaaaa", data);

    request.getEmailId(data.release_1st)
        .then((data1) => {
            console.log('data1', data1[0]['FULL_NAME'], data1[0]['WORK_EMAIL'])
            var arrayformail = [];
            arrayformail.push(arrayformail['cc'] = data1[0]);
            if (data.release_2nd != 'NO') {
                request.getEmailId(data.release_2nd)
                    .then((data2) => {
                        console.log('data2', data2)
                        arrayformail.push(arrayformail['to'] = data2[0]);
                        buildResponse(res, 200, {
                            error: false,
                            message: "Email fetch Successfully",
                            EmailData: arrayformail
                        });
                    })
            } else if (data.release_3rd != 'NO') {
                request.getEmailId(data.release_3rd)
                    .then((data3) => {
                        console.log('data3', data3)
                        arrayformail.push(arrayformail['to'] = data3[0]);
                        buildResponse(res, 200, {
                            error: false,
                            message: "Email fetch Successfully",
                            EmailData: arrayformail
                        });
                    })
            }
            else if (data.release_4th != 'NO') {
                request.getEmailId(data.release_4th)
                    .then((data4) => {
                        console.log('data4', data4);
                        arrayformail.push(arrayformail['to'] = data4[0]);
                        buildResponse(res, 200, {
                            error: false,
                            message: "Email fetch Successfully",
                            EmailData: arrayformail
                        });
                    })
            }
            else if (data.release_excise != 'NO') {
                console.log('in excise');
                request.getEmailId(data.release_excise)
                    .then((datae) => {
                        console.log('datae', datae)
                        var toarray = [];
                        var objexcise = {}

                        datae.forEach((element, index) => {
                            toarray.push(element.WORK_EMAIL);
                        });
                        objexcise['WORK_EMAIL'] = toarray.toString();

                        arrayformail.push(arrayformail['to'] = objexcise);


                        buildResponse(res, 200, {
                            error: false,
                            message: "Email fetch Successfully",
                            EmailData: arrayformail
                        });
                    })
            }
            else if (data.release_5th != 'NO') {
                request.getEmailId(data.release_5th)
                    .then((data5) => {
                        console.log('data5', data5)
                        arrayformail.push(arrayformail['to'] = data5[0]);
                        buildResponse(res, 200, {
                            error: false,
                            message: "Email fetch Successfully",
                            EmailData: arrayformail
                        });
                    })
            }




        }).catch((err) => {
            buildResponse(res, 200, {
                error: true,
                message: "Fail to Store"
            });
        });

});

router.post('/sendSubmitMail', myLogger, function (req, res, next) {

    var request = new PatentinfoModel();
    console.log("body=", req.body);
    request.sendSubmitMail(req.body);

    buildResponse(res, 200, {
        error: false,
        message: 'Email Intimation Notification Send Successfully'
    });
});

router.post('/getEmailUser', myLogger, function (req, res, next) {

    var request = new PatentinfoModel();
    var data = req.body;
    console.log("userMail", data);

    request.getEmailUser(data)
        .then((data1) => {
            console.log('data1', data1);
            buildResponse(res, 200, {
                error: false,
                message: 'Email Intimation Notification Send Successfully',
                responseData: data1
            });
        });
});


router.get('/getDelegationIPRList/:userId', myLogger, function (req, res, next) {
    var request = new PatentinfoModel();
    var userId = req.params.userId;
    var listDataTotal = [];
    var count = 0;
    var datacount = 0;
    request.getDelegationIPRList(userId).then((countData) => {
        console.log(countData.length);
        datacount = countData.length;

        if (countData.length) {
            countData.forEach((element) => {


                request.getUserWiseDelegation(element.DELEGATION_FROM).then((delegationdata) => {
                    count++;

                    delegationdata['listData'].forEach((userdelegation) => {
                        listDataTotal.push(userdelegation);
                    })
                    //listDataTotal.push(delegationdata['listData']);
                    console.log(count, datacount);
                    if (count == datacount) {
                        buildResponse(res, 200, {
                            error: false,
                            message: 'Forms Fetched successfully',
                            listData: listDataTotal,
                            countData: listDataTotal.length

                        });
                    }

                });


            })




        }


    }).catch((err) => {
        console.log('err', err);
        buildResponse(res, 200, {
            error: true,
            message: 'Error in fetching record',


        });
    });

    //}

});


router.get('/getDelegationofUser/:userId', myLogger, function (req, res, next) {
    var request = new PatentinfoModel();
    var userId = req.params.userId;
    request.getDelegationofUser(userId).then((countData) => {
        buildResponse(res, 200, {
            error: false,
            message: 'Forms Fetched successfully',
            listData: countData,
            countData: countData.length

        });
    });

    //}

});

// router.get('/getDelegationIPRList/:userId',myLogger, function (req, res, next) {
//     var request = new PatentinfoModel();
//     var userId= req.params.userId;
//     request.getDelegationIPRList(userId).then((countData) => {
//         buildResponse(res, 200, {
//             error: false,
//             message: 'Forms Fetched successfully',
//             listData: countData,
//             countData: countData.length

//         });
//     });

//     //}

// });


// router.get('/getDelegationofUser/:userId',myLogger, function (req, res, next) {
//     var request = new PatentinfoModel();
//     var userId= req.params.userId;
//     request.getDelegationofUser(userId).then((countData) => {
//         buildResponse(res, 200, {
//             error: false,
//             message: 'Forms Fetched successfully',
//             listData: countData,
//             countData: countData.length

//         });
//     });

//     //}

// });



router.get('/getVendorCode', myLogger, function (req, res, next) {
    var request = new PatentinfoModel();
    request.getVendorCode()
        .then((userData) => {
            buildResponse(res, 200, {
                error: false,
                message: 'Data Fetch Successfully',
                responseData: userData
            });
        }).catch((err) => {
            buildResponse(res, 500, {
                error: false,
                message: 'Please Try Again',
            });
        });
});

router.post('/onSubmitReportData/', myLogger, function (req, res, next) {
    var request = new PatentinfoModel();
    var data = req.body;
    request.onSubmitReportData(data).then((data) => {
        buildResponse(res, 200, {
            error: false,
            message: "Successfully display!",
            responseData: data
        });
    });
});


router.post('/onSubmitReportDataForExcel/', myLogger, function (req, res, next) {
    var request = new PatentinfoModel();
    var data = req.body;
    request.onSubmitReportDataForExcel(data).then((data) => {
        buildResponse(res, 200, {
            error: false,
            message: "Successfully display!",
            responseData: data
        });
    });
});

router.get('/getIPRListForSelf/:userId', myLogger, function (req, res, next) {
    var request = new PatentinfoModel();
    var userId = req.params.userId;
    request.getIPRListForSelf(userId).then((countData) => {
        buildResponse(res, 200, {
            error: false,
            message: 'Forms Fetched successfully',
            listData: countData,
            countData: countData.length

        });
    });

    //}

});






function buildResponse(res, statusCode, responseData) {
    res.status(statusCode).json(responseData);
}

module.exports = router;