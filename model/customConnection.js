var Sequelize = require('Sequelize');

module.exports = class connectionCustom {
    constructor(){
    }
     connectDatabase(_host, _database, _username, _password, _port) {
        // console.log("ddddddd",_username);
        
        // console.log("password",_password);
        // console.log("port",_port); 
        var Sequelize1 = new Sequelize({
            dialect: 'mssql',
            database: _database,
            username: _username,
            host: _host,
            port: _port,
            password: _password,
            logging: true,
            options: {
                encrypt: false // Use this if you're on Windows Azure
                , instanceName: 'SQLEXPRESS'
            }
            });
            

            Sequelize1.authenticate().then(() => {
                    console.log('Connection Done.');
                })
                .catch(err => {
                    console.error('Failed to Connect:', err);
                });
//console.log("Sequelize1",Sequelize1);
            return Sequelize1;
    }
}


