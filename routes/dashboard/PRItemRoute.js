var express = require('express');
var router = express.Router();
var NewStockModel = require('../../model/PRItemModel.js');
var myLogger = require('../../middleware.js');




router.get('/getSAPCodeAutomatedForX/:matgrpid', myLogger, function (req, res, next) {
    var request = new NewStockModel();
     var matgrpid= req.params.matgrpid;
    request.getSAPCodeAutomatedForX(matgrpid).then((countData) => {
        buildResponse(res, 200, {
            error: false,
            message: 'Data Fetched successfully',
            listData: countData,
            countData: countData.length
        });
    });
});

router.get('/getSAPCodeAutomatedForZ/:matgrpid', myLogger, function (req, res, next) {
    var request = new NewStockModel();
     var matgrpid= req.params.matgrpid;
    request.getSAPCodeAutomatedForZ(matgrpid).then((countData) => {
        buildResponse(res, 200, {
            error: false,
            message: 'Data Fetched successfully',
            listData: countData,
            countData: countData.length
        });
    });
});

router.get('/getCPPending', myLogger, function (req, res, next) {
    var request = new NewStockModel();
    request.getCPPending().then((data) => {
        buildResponse(res, 200, {
            error: false,
            message: 'UOM Fetched successfully',
            responseData: data,
        });
    }).catch((err) => {
        buildResponse(res, 400, {
            error: true,
            message: 'Failed to Fetch',
        });
    });
});
router.get('/getBuyerPending', myLogger, function (req, res, next) {
    var request = new NewStockModel();
    request.getBuyerPending().then((data) => {
        buildResponse(res, 200, {
            error: false,
            message: 'UOM Fetched successfully',
            responseData: data,
        });
    }).catch((err) => {
        buildResponse(res, 400, {
            error: true,
            message: 'Failed to Fetch',
        });
    });
});


router.get('/getExcisePending', myLogger, function (req, res, next) {
    var request = new NewStockModel();
    request.getExcisePending().then((data) => {
        buildResponse(res, 200, {
            error: false,
            message: 'UOM Fetched successfully',
            responseData: data,
        });
    }).catch((err) => {
        buildResponse(res, 400, {
            error: true,
            message: 'Failed to Fetch',
        });
    });
});



router.get('/getCostingPending', myLogger, function (req, res, next) {
    var request = new NewStockModel();
    request.getCostingPending().then((data) => {
        buildResponse(res, 200, {
            error: false,
            message: 'UOM Fetched successfully',
            responseData: data,
        });
    }).catch((err) => {
        buildResponse(res, 400, {
            error: true,
            message: 'Failed to Fetch',
        });
    });
});
router.get('/getStorePending', myLogger, function (req, res, next) {
    var request = new NewStockModel();
    request.getStorePending().then((data) => {
        buildResponse(res, 200, {
            error: false,
            message: 'UOM Fetched successfully',
            responseData: data,
        });
    }).catch((err) => {
        buildResponse(res, 400, {
            error: true,
            message: 'Failed to Fetch',
        });
    });
});

router.get('/getMaterialHeadPending', myLogger, function (req, res, next) {
    var request = new NewStockModel();
    request.getMaterialHeadPending().then((data) => {
        buildResponse(res, 200, {
            error: false,
            message: 'UOM Fetched successfully',
            responseData: data,
        });
    }).catch((err) => {
        buildResponse(res, 400, {
            error: true,
            message: 'Failed to Fetch',
        });
    });
});
router.get('/getCodificationPending', myLogger, function (req, res, next) {
    var request = new NewStockModel();
    request.getCodificationPending().then((data) => {
        buildResponse(res, 200, {
            error: false,
            message: 'UOM Fetched successfully',
            responseData: data,
        });
    }).catch((err) => {
        buildResponse(res, 400, {
            error: true,
            message: 'Failed to Fetch',
        });
    });
});

router.get('/getGSTPending', myLogger, function (req, res, next) {
    var request = new NewStockModel();
    request.getGSTPending().then((data) => {
        buildResponse(res, 200, {
            error: false,
            message: 'UOM Fetched successfully',
            responseData: data,
        });
    }).catch((err) => {
        buildResponse(res, 400, {
            error: true,
            message: 'Failed to Fetch',
        });
    });
});

router.get('/getrejectedcount', myLogger, function (req, res, next) {
    var request = new NewStockModel();
    request.getrejectedcount().then((data) => {
        buildResponse(res, 200, {
            error: false,
            message: 'UOM Fetched successfully',
            responseData: data,
        });
    }).catch((err) => {
        buildResponse(res, 400, {
            error: true,
            message: 'Failed to Fetch',
        });
    });
});




router.get('/getHODPending', myLogger, function (req, res, next) {
    var request = new NewStockModel();
    request.getHODPending().then((data) => {
        buildResponse(res, 200, {
            error: false,
            message: 'UOM Fetched successfully',
            responseData: data,
        });
    }).catch((err) => {
        buildResponse(res, 400, {
            error: true,
            message: 'Failed to Fetch',
        });
    });
});
router.get('/getDashboardTotalCount', myLogger, function (req, res, next) {
    var request = new NewStockModel();
    request.getDashboardTotalCount().then((data) => {
        buildResponse(res, 200, {
            error: false,
            message: 'UOM Fetched successfully',
            responseData: data,
        });
    }).catch((err) => {
        buildResponse(res, 400, {
            error: true,
            message: 'Failed to Fetch',
        });
    });
});


router.get('/getUOM', myLogger, function (req, res, next) {
    var request = new NewStockModel();
    request.getUOM().then((data) => {
        buildResponse(res, 200, {
            error: false,
            message: 'UOM Fetched successfully',
            responseData: data,
        });
    }).catch((err) => {
        buildResponse(res, 400, {
            error: true,
            message: 'Failed to Fetch',
        });
    });
});

router.get('/getGSTUsersMail', myLogger, function (req, res, next) {
    var request = new NewStockModel();
    request.getGSTUsersMail().then((data) => {
        buildResponse(res, 200, {
            error: false,
            message: 'UOM Fetched successfully',
            responseData: data,
        });
    }).catch((err) => {
        buildResponse(res, 400, {
            error: true,
            message: 'Failed to Fetch',
        });
    });
});



router.get('/getlatestdownloaded', myLogger, function (req, res, next) {
    var request = new NewStockModel();
    request.getlatestdownloaded().then((data) => {
        buildResponse(res, 200, {
            error: false,
            message: 'UOM Fetched successfully',
            responseData: data,
        });
    }).catch((err) => {
        buildResponse(res, 400, {
            error: true,
            message: 'Failed to Fetch',
        });
    });
});


router.get('/getLabStockOffice', myLogger, function (req, res, next) {
    var request = new NewStockModel();
    request.getLabStockOffice().then((data) => {
        buildResponse(res, 200, {
            error: false,
            message: 'list Fetched successfully',
            responseData: data,
        });
    }).catch((err) => {
        buildResponse(res, 400, {
            error: true,
            message: 'Failed to Fetch',
        });
    });
});


router.get('/getPlant', myLogger, function (req, res, next) {
    var request = new NewStockModel();
    request.getPlant().then((data) => {
        buildResponse(res, 200, {
            error: false,
            message: 'Plant Fetched successfully',
            responseData: data,
        });
    }).catch((err) => {
        buildResponse(res, 400, {
            error: true,
            message: 'Failed to Fetch',
        });
    });
});

router.get('/getStoresByPlant/:plant', myLogger, function (req, res, next) {
    var request = new NewStockModel();
    var plant = req.params.plant;
    request.getStoresByPlant(plant).then((data) => {
        buildResponse(res, 200, {
            error: false,
            message: 'Stores Fetched successfully',
            responseData: data,
        });
    }).catch((err) => {
        buildResponse(res, 400, {
            error: true,
            message: 'Failed to Fetch',
        });
    });
});

router.get('/getMRPCNTRLByPlant/:plant', myLogger, function (req, res, next) {
    var request = new NewStockModel();
    var plant = req.params.plant;
    request.getMRPCNTRLByPlant(plant).then((data) => {
        buildResponse(res, 200, {
            error: false,
            message: 'CNTRL Fetched successfully',
            responseData: data,
        });
    }).catch((err) => {
        buildResponse(res, 400, {
            error: true,
            message: 'Failed to Fetch',
        });
    });
});

router.get('/getCategory', myLogger, function (req, res, next) {
    console.log('in cat');
    var request = new NewStockModel();
    request.getCategory().then((data) => {
        buildResponse(res, 200, {
            error: false,
            message: 'Category Fetched successfully',
            responseData: data,
        });
    }).catch((err) => {
        buildResponse(res, 400, {
            error: true,
            message: 'Failed to Fetch',
        });
    });
});
router.get('/valuationClassList/:id', myLogger, function (req, res, next) {
    var request = new NewStockModel();
    var reqcat=req.params.id;
    request.valuationClassList(reqcat).then((data) => {
        buildResponse(res, 200, {
            error: false,
            message: 'CategoryType Fetched successfully',
            responseData: data,
        });
    }).catch((err) => {
        buildResponse(res, 400, {
            error: true,
            message: 'Failed to Fetch',
        });
    });
});


router.get('/getbuyerdetails/:id', myLogger, function (req, res, next) {
    var request = new NewStockModel();
    var buyid=req.params.id;
    request.getbuyerdetails(buyid).then((data) => {
        buildResponse(res, 200, {
            error: false,
            message: 'CategoryType Fetched successfully',
            responseData: data,
        });
    }).catch((err) => {
        buildResponse(res, 400, {
            error: true,
            message: 'Failed to Fetch',
        });
    });
});
router.get('/getMaterialGroup', myLogger, function (req, res, next) {
    var request = new NewStockModel();
    request.getMaterialGroup().then((data) => {
        buildResponse(res, 200, {
            error: false,
            message: 'MaterialGroup Fetched successfully',
            responseData: data,
        });
    }).catch((err) => {
        buildResponse(res, 400, {
            error: true,
            message: 'Failed to Fetch',
        });
    });
});

router.get('/getMaterialType', myLogger, function (req, res, next) {
    var request = new NewStockModel();
    request.getMaterialType().then((data) => {
        buildResponse(res, 200, {
            error: false,
            message: 'MaterialType Fetched successfully',
            responseData: data,
        });
    }).catch((err) => {
        buildResponse(res, 400, {
            error: true,
            message: 'Failed to Fetch',
        });
    });
});
router.get('/getValuationClass/:MaterialType', myLogger, function (req, res, next) {
    var request = new NewStockModel();
    var MaterialType = req.params.MaterialType;

    request.getValuationClass(MaterialType).then((data) => {
        buildResponse(res, 200, {
            error: false,
            message: 'MaterialType Fetched successfully',
            responseData: data,
        });
    }).catch((err) => {
        buildResponse(res, 400, {
            error: true,
            message: 'Failed to Fetch',
        });
    });
});

router.post('/onSubmitSAPCodeReportData/', myLogger, function (req, res, next) {
    var request = new NewStockModel();
    var data = req.body;
    request.onSubmitSAPCodeReportData(data).then((data) => {
        buildResponse(res, 200, {
            error: false,
            message: "Successfully display!",
            responseData: data
        });
    });
});


router.post('/onSubmitReportDataForExcel/', myLogger, function (req, res, next) {
    var request = new NewStockModel();
    var data = req.body;
    request.onSubmitReportDataForExcel(data).then((data) => {
        buildResponse(res, 200, {
            error: false,
            message: "Successfully display!",
            responseData: data
        });
    });
});
router.get('/getBuyerCode/:id', myLogger, function (req, res, next) {
    var request = new NewStockModel();
    var MaterialType = req.params.id;
    console.log('MaterialType==',MaterialType);
    request.getBuyerCode(MaterialType).then((data) => {
        buildResponse(res, 200, {
            error: false,
            message: 'BuyerCode Fetched successfully',
            responseData: data,
        });
    }).catch((err) => {
        buildResponse(res, 400, {
            error: true,
            message: 'Failed to Fetch',
        });
    });
});

router.get('/getprofitCenters', myLogger, function (req, res, next) {
    var request = new NewStockModel();
    request.getprofitCenters().then((data) => {
        buildResponse(res, 200, {
            error: false,
            message: 'Fetched successfully',
            responseData: data,
        });
    }).catch((err) => {
        buildResponse(res, 400, {
            error: true,
            message: 'Failed to Fetch',
        });
    });
});

router.get('/getCPPersonDetails/:plant', myLogger, function (req, res, next) {
    var request = new NewStockModel();
    var plant = req.params.plant;
    request.getCPPersonDetails(plant).then((data) => {
        buildResponse(res, 200, {
            error: false,
            message: 'CNTRL Fetched successfully',
            responseData: data,
        });
    }).catch((err) => {
        buildResponse(res, 400, {
            error: true,
            message: 'Failed to Fetch',
        });
    });
});
router.get('/getCPMasters', myLogger, function (req, res, next) {
    var request = new NewStockModel();
    request.getCPMasters().then((data) => {
        buildResponse(res, 200, {
            error: false,
            message: 'CPMasters Fetched successfully',
            responseData: data,
        });
    }).catch((err) => {
        buildResponse(res, 400, {
            error: true,
            message: 'Failed to Fetch',
        });
    });
});
router.get('/getStorePerson/:plant/:store', myLogger, function (req, res, next) {
    var request = new NewStockModel();
    var plant = req.params.plant;
    var store = req.params.store;
    request.getStorePerson(plant, store).then((data) => {
        buildResponse(res, 200, {
            error: false,
            message: 'Store person Fetched successfully',
            responseData: data,
        });
    }).catch((err) => {
        buildResponse(res, 400, {
            error: true,
            message: 'Failed to Fetch',
        });
    });
});


router.post('/submitData', myLogger, function (req, res, next) {
    var request = new NewStockModel();
    var data = req.body;
    request.submitData(data)
        .then((data) => {
            buildResponse(res, 200, {
                error: false,
                message: "Record Inserted Successfully",
                responseData: data
            });
        }).catch((err) => {
            buildResponse(res, 400, {
                error: true,
                message: err
            });
        });
});


router.get('/getPRItemList', myLogger, function (req, res, next) {
    var request = new NewStockModel();
    // var userid= req.params.userid;
    request.getPRItemList().then((countData) => {
        buildResponse(res, 200, {
            error: false,
            message: 'Data Fetched successfully',
            listData: countData,
            countData: countData.length
        });
    });
});





router.get('/getMatCodificationMaster', myLogger, function (req, res, next) {
    var request = new NewStockModel();
    // var userid= req.params.userid;
    request.getMatCodificationMaster().then((countData) => {
        buildResponse(res, 200, {
            error: false,
            message: 'Codification Master Fetched successfully',
            listData: countData,
            countData: countData.length
        });
    });
});

router.get('/getCurrentRecordData/:ItemStockID', myLogger, function (req, res, next) {
    var request = new NewStockModel();
    var ItemStockID = req.params.ItemStockID;
    request.getCurrentRecordData(ItemStockID).then((data) => {
        buildResponse(res, 200, {
            error: false,
            message: 'DATA Fetched successfully',
            responseData: data,
        });
    }).catch((err) => {
        buildResponse(res, 400, {
            error: true,
            message: 'Failed to Fetch',
        });
    });
});

router.post('/UpdateResubmitDatabyOtherBuyer', myLogger, function (req, res, next) {
    var request = new NewStockModel();
    var data = req.body;
    request.UpdateResubmitDatabyOtherBuyer(data)
        .then((data) => {
            buildResponse(res, 200, {
                error: false,
                message: "Record Updated Successfully",
                responseData: data
            });
        }).catch((err) => {
            buildResponse(res, 400, {
                error: true,
                message: "Fail to Update"
            });
        });
});


router.post('/UpdateResubmitData', myLogger, function (req, res, next) {
    var request = new NewStockModel();
    var data = req.body;
    request.UpdateResubmitData(data)
        .then((data) => {
            buildResponse(res, 200, {
                error: false,
                message: "Record Updated Successfully",
                responseData: data
            });
        }).catch((err) => {
            buildResponse(res, 400, {
                error: true,
                message: "Fail to Update"
            });
        });
});

router.post('/UpdateData', myLogger, function (req, res, next) {
    var request = new NewStockModel();
    var data = req.body;
    request.UpdateData(data)
        .then((data) => {
            console.log('data=',data);
            
                buildResponse(res, 200, {
                    error: false,
                    message: "Record Updated Successfully",
                    responseData: data
                });
          
                buildResponse(res, 200, {
                    error: true,
                    message: "Error while updating. Please try again!!!!",
                    responseData: data
                });
         
        }).catch((err) => {
            buildResponse(res, 400, {
                error: true,
                message: "Fail to Update",
                responseData:err,
            });
        });
});
router.post('/tranfertootherbuyerUpdateData', myLogger, function (req, res, next) {
    var request = new NewStockModel();
    var data = req.body;
    request.tranfertootherbuyerUpdateData(data)
        .then((data) => {
            console.log('data=',data);
            
                buildResponse(res, 200, {
                    error: false,
                    message: "Record Updated Successfully",
                    responseData: data
                });
          
                buildResponse(res, 200, {
                    error: true,
                    message: "Error while updating. Please try again!!!!",
                    responseData: data
                });
         
        }).catch((err) => {
            buildResponse(res, 400, {
                error: true,
                message: "Fail to Update",
                responseData:err,
            });
        });
});




router.post('/UpdateDownloadedData', myLogger, function (req, res, next) {

  
    var request = new NewStockModel();
    var data = req.body;
    console.log('data',data)
    request.UpdateDownloadedData(data)

        .then((data) => {
            buildResponse(res, 200, {
                error: false,
                message: "Record Updated Successfully",
                responseData: data
            });
        }).catch((err) => {
            buildResponse(res, 400, {
                error: true,
                message: "Fail to Update"
            });
        });
});



router.post('/tranfertootherbuyerData', myLogger, function (req, res, next) {
    var request = new NewStockModel();
    var data = req.body;
    request.tranfertootherbuyerData(data)
        .then((data) => {
            buildResponse(res, 200, {
                error: false,
                message: "Record Updated Successfully",
                responseData: data
            });
        }).catch((err) => {
            buildResponse(res, 400, {
                error: true,
                message: "Fail to Update"
            });
        });
});




router.post('/UpdatedByCreator', myLogger, function (req, res, next) {
    var request = new NewStockModel();
    var data = req.body;
    request.UpdatedByCreator(data)
        .then((data) => {
            buildResponse(res, 200, {
                error: false,
                message: "Record Updated Successfully",
                responseData: data
            });
        }).catch((err) => {
            buildResponse(res, 400, {
                error: true,
                message: "Fail to Update"
            });
        });
});



router.get('/getUserEmail/:id', myLogger, function (req, res, next) {
    var request = new NewStockModel();
    var id = req.params.id;
    request.getUserEmail(id).then((data) => {
        if (data['0']['emp_email'] != null) {
            buildResponse(res, 200, {
                error: false,
                message: "Successfully display!",
                responseData: data
            });
        } else {
            request.getUserVintraTicket(id).then((data) => {
                buildResponse(res, 200, {
                    error: false,
                    message: "Successfully display!",
                    responseData: data
                });
            })
        }
    });
});

router.get('/getGSTTeam/:id', myLogger, function (req, res, next) {
    var request = new NewStockModel();
    var id = req.params.id;
    request.getGSTTeam(id).then((data) => {

        buildResponse(res, 200, {
            error: false,
            message: "Successfully display!",
            responseData: data
        });

    });
});
router.get('/getCostingTeam/:id', myLogger, function (req, res, next) {
    var request = new NewStockModel();
    var id = req.params.id;
    request.getCostingTeam(id).then((data) => {

        buildResponse(res, 200, {
            error: false,
            message: "Successfully display!",
            responseData: data
        });

    });
});
router.get('/getMaterialHead/:id', myLogger, function (req, res, next) {
    var request = new NewStockModel();
    var id = req.params.id;
    request.getMaterialHead(id).then((data) => {

        buildResponse(res, 200, {
            error: false,
            message: "Successfully display!",
            responseData: data
        });

    });
});







router.get('/getUserTicket/:id', myLogger, function (req, res, next) {
    var request = new NewStockModel();
    var id = req.params.id;
    request.getUserTicket(id).then((data) => {
        buildResponse(res, 200, {
            error: false,
            message: "Successfully display!",
            responseData: data
        });
    });
});

router.post('/RejectedByBuyer', myLogger, function (req, res, next) {
    var request = new NewStockModel();
    var data = req.body;
    request.RejectedByBuyer(data)
        .then((data) => {
            buildResponse(res, 200, {
                error: false,
                message: "Record Updated Successfully",
                responseData: data
            });
        }).catch((err) => {
            buildResponse(res, 400, {
                error: true,
                message: "Fail to Update"
            });
        });
});

router.post('/RejectedByOtherBuyer', myLogger, function (req, res, next) {
    var request = new NewStockModel();
    var data = req.body;
    request.RejectedByOtherBuyer(data)
        .then((data) => {
            buildResponse(res, 200, {
                error: false,
                message: "Record Updated Successfully",
                responseData: data
            });
        }).catch((err) => {
            buildResponse(res, 400, {
                error: true,
                message: "Fail to Update"
            });
        });
});


router.post('/ApprovedByCosting', myLogger, function (req, res, next) {
    var request = new NewStockModel();
    var data = req.body;
    request.ApprovedByCosting(data)
        .then((data) => {
            buildResponse(res, 200, {
                error: false,
                message: "Record Updated Successfully",
                responseData: data
            });
        }).catch((err) => {
            buildResponse(res, 400, {
                error: true,
                message: "Fail to Update"
            });
        });
});




// *********************



// *********************

router.post('/ApprovedByCodification', myLogger, function (req, res, next) {
    var request = new NewStockModel();
    var data = req.body;
    console.log('in route data', data);
    request.ApprovedByCodification(data)
        .then((data) => {
            buildResponse(res, 200, {
                error: false,
                message: "Record Updated By Store Successfully",
                responseData: data
            });
        }).catch((err) => {
            buildResponse(res, 400, {
                error: true,
                message: "Fail to Update"
            });
        });
});



router.post('/UpdateDataForGST', myLogger, function (req, res, next) {
    var request = new NewStockModel();
    var data = req.body;
    //console.log('in route data', data);
    request.UpdateDataForGST(data)
        .then((data) => {
            buildResponse(res, 200, {
                error: false,
                message: "Record Updated By Store Successfully",
                responseData: data
            });
        }).catch((err) => {
            buildResponse(res, 400, {
                error: true,
                message: "Fail to Update"
            });
        });
});
router.post('/sendMailFromIndentor', myLogger, function (req, res, next) {
    var request = new NewStockModel();
    var data = req.body;
    console.log('in route data', data);
    request.sendMailFromIndentor(data)
});


router.post('/sendRejectionMail', myLogger, function (req, res, next) {
    var request = new NewStockModel();
    var data = req.body;
    console.log('in route data', data);
    request.sendRejectionMail(data)
});




function buildResponse(res, statusCode, responseData) {
    res.status(statusCode).json(responseData);
}

module.exports = router;