var express = require('express');
var router = express.Router();
var Master = require('../../model/HFD');

var imageData = [];



router.get('/getEmployeename/:lic', function (req, res, next) {
    var request = new Master();
    //console.log("in route");
    var lic = req.params.lic;
    request.getEmployeename(lic).then((data) => {
        buildResponse(res, 200, {
            error: false,
            message: "Successfully display!",
            responseData: data
        });
        // console.log("awardData=",responseData);
    });


});
router.get('/getEmployeenameDesignerLIC/:designerlic', function (req, res, next) {
    var request = new Master();
    //console.log("in route");
    var designerlic = req.params.designerlic;
    request.getEmployeenameDesignerLIC(designerlic).then((data) => {
        buildResponse(res, 200, {
            error: false,
            message: "Successfully display!",
            responseData: data
        });
        // console.log("awardData=",responseData);
    });


});
router.get('/getEmployeenameDesigner/:designer', function (req, res, next) {
    var request = new Master();
    //console.log("in route");
    var designer = req.params.designer;
    request.getEmployeenameDesigner(designer).then((data) => {
        buildResponse(res, 200, {
            error: false,
            message: "Successfully display!",
            responseData: data
        });
        // console.log("awardData=",responseData);
    });


});



router.get('/getcadmodelstatus', function (req, res, next) {
    var request = new Master();
    //console.log("in route");
    request.getcadmodelstatus().then((data) => {
        buildResponse(res, 200, {
            error: false,
            message: "Successfully display!",
            responseData: data
        });
        // console.log("awardData=",responseData);
    });


});

router.get('/getemployee', function (req, res, next) {
    var request = new Master();
    //console.log("in route");
    request.getemployee().then((data) => {
        buildResponse(res, 200, {
            error: false,
            message: "Successfully display!",
            responseData: data
        });
        // console.log("awardData=",responseData);
    });


});

router.get('/getemployeeforUpdatePage', function (req, res, next) {
    var request = new Master();
    //console.log("in route");
    request.getemployeeforUpdatePage().then((data) => {
        buildResponse(res, 200, {
            error: false,
            message: "Successfully display!",
            responseData: data
        });
        // console.log("awardData=",responseData);
    });


});

router.get('/getemployeeOnInput/:userid', function (req, res, next) {
    var request = new Master();
    var userid = req.params.userid;
    //console.log("in route");
    request.employeeOnInput(userid).then((data) => {
        buildResponse(res, 200, {
            error: false,
            message: "Successfully display!",
            responseData: data
        });
        // console.log("awardData=",responseData);
    });


});

router.get('/ShowHFDDAta/:id', function (req, res, next) {
    var request = new Master();
    var id = req.params.id;
    //console.log("in route");
    request.ShowHFDDAta(id).then((data) => {
        buildResponse(res, 200, {
            error: false,
            message: "Successfully display!",
            responseData: data
        });
        // console.log("awardData=",responseData);
    });


});




router.get('/getcadmodellerstatus', function (req, res, next) {
    var request = new Master();
    //console.log("in route");
    request.getcadmodellerstatus().then((data) => {
        buildResponse(res, 200, {
            error: false,
            message: "Successfully display!",
            responseData: data
        });
        // console.log("awardData=",responseData);
    });


});

router.get('/getcaddecision', function (req, res, next) {
    var request = new Master();
    //console.log("in route");
    request.getcaddecision().then((data) => {
        buildResponse(res, 200, {
            error: false,
            message: "Successfully display!",
            responseData: data
        });
        // console.log("awardData=",responseData);
    });


});

router.get('/cadmodeltype', function (req, res, next) {
    var request = new Master();
    //console.log("in route");
    request.cadmodeltype().then((data) => {
        buildResponse(res, 200, {
            error: false,
            message: "Successfully display!",
            responseData: data
        });
        // console.log("awardData=",responseData);
    });


});


router.post('/saveCADData', function (req, res, next) {


    //console.log("inside cad route");
    var request = new Master();
    var AWARD_ID = 0;
    var CADData = {


        dienumber: req.body.dienumber,
        dieelement: req.body.dieelement,
        refredrawno: req.body.refredrawno,
        custdrawno: req.body.custdrawno,
        custmodelno: req.body.custmodelno,
        modelloc: req.body.modelloc,
        modelstatus: req.body.modelstatus,
        srev: req.body.srev,
        l1rev: req.body.l1rev,
        l2rev: req.body.l2rev,
        l3rev: req.body.l3rev,
        cadmodeller: req.body.cadmodeller,
        cadlic: req.body.cadlic,
        designer: req.body.designer,
        designlic: req.body.designlic,
        modellercomm: req.body.modellercomm,
        modellerstatus: req.body.modellerstatus,
        cadtime: req.body.cadtime,
        modeltype: req.body.modeltype,
        cadliccomm: req.body.cadliccomm,
        cadlicdecision: req.body.cadlicdecision,

        designercomm: req.body.designercomm,
        designerdecision: req.body.designerdecision,
        createdByname: req.body.createdByname,
        desiglicomm: req.body.desiglicomm,
        pressline: req.body.pressline,
        designerlic: req.body.designerlic,
        filedata: req.body.filedata,



        today: req.body.date,



    };
    console.log("CADData", CADData);

    request.saveData(CADData).then((data) => {
        buildResponse(res, 200, {
            error: false,
            message: "Saved Successfully!",
            responseData: data
        });

    })
        .catch((err) => {
            buildResponse(res, 200, {
                error: true,
                message: "Fail to Store"
            });
        });
});


router.post('/UpdateCADData', function (req, res, next) {


    console.log("inside cad route");
    var request = new Master();
    var AWARD_ID = 0;
    var CADData = {


        dienumber: req.body.dienumber,
        dieelement: req.body.dieelement,
        refredrawno: req.body.refredrawno,
        custdrawno: req.body.custdrawno,
        custmodelno: req.body.custmodelno,
        modelloc: req.body.modelloc,
        modelstatus: req.body.modelstatus,
        srev: req.body.srev,
        l1rev: req.body.l1rev,
        l2rev: req.body.l2rev,
        l3rev: req.body.l3rev,
        cadmodeller: req.body.cadmodeller,
        cadlic: req.body.cadlic,
        designer: req.body.designer,
        designlic: req.body.designlic,
        modellercomm: req.body.modellercomm,
        modellerstatus: req.body.modellerstatus,
        cadtime: req.body.cadtime,
        modeltype: req.body.modeltype,
        cadliccomm: req.body.cadliccomm,
        cadlicdecision: req.body.cadlicdecision,

        designercomm: req.body.designercomm,
        designerdecision: req.body.designerdecision,
        createdByname: req.body.createdByname,
        desiglicomm: req.body.desiglicomm,

        designerlic: req.body.designerlic,
        filedata: req.body.filedata,

        pressline: req.body.pressline,

        id: req.body.id,

        updatedat: req.body.updatedat,
        NoOfDefect: req.body.NoOfDefect


    };
     console.log("awardData1",CADData.NoOfDefect);

    request.UpdateCADData(CADData).then((data) => {
        buildResponse(res, 200, {
            error: false,
            message: "Saved Successfully",
            responseData: data
        });

    })
        .catch((err) => {
            buildResponse(res, 200, {
                error: true,
                message: "Fail to Store"
            });
        });
});



router.get('/getDashboardTotalCount/:userid', function (req, res, next) {
    var request = new Master();
    var userid = req.params.userid;
    // var group=req.params.group;
    // var dept=req.params.dept;
    console.log("in route total",userid);

    var totalcount = 0;
    //    for (var i=0;i< req.body.length;i++)
    //    {
    request.getDashboardTotalCount(userid).then((countData) => {
        request.getDashboardTotalCount
        //log("countdata=",countData)
        buildResponse(res, 200, {
            error: false,
            message: 'Forms Fetched successfully',
            listData: countData,
            countData: countData.length

        });
    });

    //}

});

router.get('/getDashboardHFD/:userid', function (req, res, next) {
    var request = new Master();
    var userid = req.params.userid;
    // var group=req.params.group;
    // var dept=req.params.dept;
    console.log("in route total",userid);

    var totalcount = 0;
    //    for (var i=0;i< req.body.length;i++)
    //    {
    request.getDashboardHFD(userid).then((countData) => {
        request.getDashboardTotalCount
        //log("countdata=",countData)
        buildResponse(res, 200, {
            error: false,
            message: 'Forms Fetched successfully',
            listData: countData,
            countData: countData.length

        });
    });

    //}

});



router.get('/getDashboardPendingCount/:userid', function (req, res, next) {
    var request = new Master();
    var userid = req.params.userid;
    // var group=req.params.group;
    // var dept=req.params.dept;
    // log("in route total",group,dept);

    var totalcount = 0;
    //    for (var i=0;i< req.body.length;i++)
    //    {
    request.getDashboardPendingCount(userid).then((countData) => {
        request.getDashboardPendingCount
        //log("countdata=",countData)
        buildResponse(res, 200, {
            error: false,
            message: 'Forms Fetched successfully',
            listData: countData,
            countData: countData.length

        });
    });

    //}

});

router.get('/deleteRequest/:id', function (req, res, next) {
    var request = new Master();
    var reqData = req.params.id;
    request.deleteRequest(reqData)
        .then((userData) => {
            buildResponse(res, 200, {
                error: false,
                message: 'User Deleted successfully',
                UserData: userData
            });
        }).catch((err) => {
            buildResponse(res, 500, {
                error: false,
                message: 'Please Try Again',
            });
        });
});
router.get('/getDashboardRejectedCount/:userid', function (req, res, next) {
    var request = new Master();
    var userid = req.params.userid;
    // var group=req.params.group;
    // var dept=req.params.dept;
    // log("in route total",group,dept);

    var totalcount = 0;
    //    for (var i=0;i< req.body.length;i++)
    //    {
    request.getDashboardRejectedCount(userid).then((countData) => {
        request.getDashboardRejectedCount
        //log("countdata=",countData)
        buildResponse(res, 200, {
            error: false,
            message: 'Forms Fetched successfully',
            listData: countData,
            countData: countData.length

        });
    });

    //}

});

router.get('/getDashboardCompletedCount/:userid', function (req, res, next) {
    var request = new Master();
    var userid = req.params.userid;
    // var group=req.params.group;
    // var dept=req.params.dept;
    // log("in route total",group,dept);

    var totalcount = 0;
    //    for (var i=0;i< req.body.length;i++)
    //    {
    request.getDashboardCompletedCount(userid).then((countData) => {
        request.getDashboardCompletedCount
        // console.log("countdata=",countData)
        buildResponse(res, 200, {
            error: false,
            message: 'Forms Fetched successfully',
            listData: countData,
            countData: countData.length

        });
    });

    //}

});


router.get('/getUsersEmail/:id', function (req, res, next) {
    var request = new Master();
    var id = req.params.id;
    //console.log("in route");
    request.getUsersEmail(id).then((data) => {
        buildResponse(res, 200, {
            error: false,
            message: "Email Sent Successfully",
            responseData: data
        });
        // console.log("awardData=",responseData);
    });


});




router.post('/sendRequestorEmail', function (req, res, next) {

    var request = new Master();
    // console.log("body=",req.body);
    request.sendRequestorEmail(req.body);
    buildResponse(res, 200, {
        error: false,
        message: 'Email Intimation Notification Send Successfully'
    });
});



router.post('/emailRequestNotificationfinal', function (req, res, next) {

    var request = new Master();
    //console.log("body=",req.body);
    request.emailRequestNotificationfinal(req.body);

    buildResponse(res, 200, {
        error: false,
        message: 'Email Intimation Notification Send Successfully'
    });
});

router.get('/checkBeforeMail/:id', function (req, res, next) {
    var request = new Master();
    var id = req.params.id;
    //console.log("in route");
    request.checkBeforeMail(id).then((data) => {
        buildResponse(res, 200, {
            error: false,
            message: "Email Sent Successfully",
            responseData: data
        });
        // console.log("awardData=",responseData);
    });

});


var schedule = require('node-schedule');
var rule = new schedule.RecurrenceRule();
rule.dayOfWeek = [0, new schedule.Range(0, 6)];
rule.hour = 11;
rule.minute = 19;

// var j = schedule.scheduleJob(rule, function(){
// },getdata);

// function getdata(req,res,next){
router.get('/getDataForEmailJobScheduling', function (req, res, next) {
    var request = new Master();
    var dateNow = new Date();
    var dateSingleDigit = dateNow.getDate();
    dd = dateSingleDigit < 10 ? '0' + dateSingleDigit : dateSingleDigit;
    var monthSingleDigit = dateNow.getMonth() + 1;
    mm = monthSingleDigit < 10 ? '0' + monthSingleDigit : monthSingleDigit;
    var yy = dateNow.getFullYear().toString();
    var currentDate = dd + '-' + mm + '-' + yy;

    request.getDataForEmailJobScheduling().then((data) => {
        data.forEach(element => {
            
            console.log("element",element);
            if (element.cad_lic_decision != 3) {
                if(currentDate > element.Created_At) {
                    console.log("1");
                    var emailData={
                        To:element.cadlicemail,
                        Cc:element.modelleremail,
                        'Die Number':element.die_number,
                        'Die Element':element.die_element,
                        'Model Location':element.model_location,
                        Subject:"Reminder Mail:"+element.die_number +" # "+ element.die_element +"  Submitted for approval",
                        Regards:'IT Team',
                    }
                    request.sendRequestorEmail(emailData);
                    buildResponse(res, 200, {
                           error: false,
                           message: 'Email Intimation Notification Send Successfully'
                       });
                }
            }else if(element.designer_decision!=3){
                if(currentDate > element.Updated_At) {
                    console.log("2");
                    var emailData={
                        To:element.designeremail,
                        Cc:element.modelleremail+","+element.cadlicemail,
                        'Die Number':element.die_number,
                        'Die Element':element.die_element,
                        'Model Location':element.model_location,
                        Subject:"Reminder Mail:"+element.die_number +" # "+ element.die_element +"  Submitted for approval",
                        Regards:'IT Team',
                    }
                    request.sendRequestorEmail(emailData);
                    buildResponse(res, 200, {
                           error: false,
                           message: 'Email Intimation Notification Send Successfully'
                       });
                } 
            }else if(element.designer_lic_decision!=3){
                if(currentDate > element.Updated_At) {
                    console.log("3");
                    var emailData={
                        To:element.designerlicemail,
                        Cc:element.modelleremail+","+element.cadlicemail+','+element.designeremail,
                        'Die Number':element.die_number,
                        'Die Element':element.die_element,
                        'Model Location':element.model_location,
                        Subject:"Reminder Mail: "+element.die_number +" # "+ element.die_element +"  Submitted for approval",
                        Regards:'IT Team',
                    }
                    request.sendRequestorEmail(emailData);
                    buildResponse(res, 200, {
                           error: false,
                           message: 'Email Intimation Notification Send Successfully'
                       });
                } 
            }
        });
        // console.log("datadata=",data);
    });

})
//  }


router.post('/CADGetDataForExport', function (req, res, next) {
    var request = new Master();
    var data = req.body;
    var dataArray = [];
    request.CADGetDataForExport(data).then((datas) => {
        buildResponse(res, 200, {
            error: false,
            message: "Successfully display!",
            TableData: datas
        });
    });

});
router.post('/CADGetReportByFromTO', function (req, res, next) {


    var request = new Master();
    var data = req.body;
    //console.log("data",data);
    request.CADGetReportByFromTO(data).then((data) => {
        buildResponse(res, 200, {
            error: false,
            message: "Successfully IPT Display!",
            TableData: data


        });
    });
});

router.post('/CADGetReportWithAll', function (req, res, next) {
    var request = new Master();
    var data = req.body;
    //console.log("data",data);
    request.CADGetReportWithAll(data).then((data) => {
        buildResponse(res, 200, {
            error: false,
            message: "Successfully display!",
            TableData: data


        });
    })
});


router.get('/selectDivision/:id', function (req, res, next) {
    var request = new Master();
    var id = req.params.id;
    //console.log("in route");
    request.selectDivision(id).then((data) => {
        buildResponse(res, 200, {
            error: false,
            message: "Email Sent Successfully",
            responseData: data
        });
        // console.log("awardData=",responseData);
    });

});

router.post('/saveHFDData', function (req, res, next) {
    var request = new Master();
    var data = req.body;
    console.log("in route",data);
    request.saveHFDData(data).then((data) => {
        buildResponse(res, 200, {
            error: false,
            message: "Saved Successfully",
        });
        // console.log("awardData=",responseData);
    });

});

router.post('/updateHFD', function (req, res, next) {
    console.log("bodyqueryj=",req.body.query);
    var request = new Master();
    
    request.updateHFD(req.body.query).then((data) => {
        buildResponse(res, 200, {
            error: false,
            message: "Updated Successfully",
        });
        // console.log("awardData=",responseData);
    }).catch((err) => {
        buildResponse(res, 200, {
            error: true,
            message: "Fail to Store"
        });
    });
});

function buildResponse(res, statusCode, responseData) {
    res.status(statusCode).json(responseData);
}

module.exports = router;