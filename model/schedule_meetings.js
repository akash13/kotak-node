const Sequelize = require('sequelize');
var con = require('./connection');
var randomstring = require("randomstring");
var bcrypt = require('bcryptjs');
var MyLogModel = require('../model/myLog');
var log = new MyLogModel();
const Op = Sequelize.Op;
var errMsg = "Server issue please try after sometime";

Sequelize.DATE.prototype._stringify = function _stringify(date, options) {
    return this._applyTimezone(date, options).format('YYYY-MM-DD HH:mm:ss.SSS');
  };


module.exports = class Department {

    constructor() {

      
        this.SCHEDULEMEETINGS = con.define('schedule_meetings_master', {
            schedule_meeting_id: {
                type: Sequelize.INTEGER,
                primaryKey: true,
                autoIncrement: true
            },
           
            meeting_id: {
                type: Sequelize.INTEGER,
                allowNull: false,
                //unique: true
            },
            meeting_template_id: {
                type: Sequelize.INTEGER,
                allowNull: false,
               
            },
            frequency: {
                type: Sequelize.INTEGER,
                allowNull: false,
                //defaultValue:'0'
               
            },
            meeting_date: {
                type: Sequelize.DATE,
                allowNull: true
            },
            meeting_start_time: {
                type: Sequelize.TIME,
                allowNull: true
            },
            meeting_end_time: {
                type: Sequelize.TIME,
                allowNull: true
            },
            location: {
                type: Sequelize.STRING(50),
                allowNull: true
            },
            attendies: {
                type: Sequelize.INTEGER,
                allowNull: false
            },
            description: {
                type: Sequelize.STRING(255),
                allowNull: true
            },
            is_publish: {
                type: Sequelize.STRING(2),
                allowNull: true,
                defaultValue:'0'
            },
            completed_flag: {
                type: Sequelize.STRING(2),
                allowNull: true,
                defaultValue:'0'
            },
            created_by: {
                type: Sequelize.INTEGER,
                allowNull: false
            },
            created_date_time: {
                type: Sequelize.DATE,
                allowNull: true
            },
            updated_by: {
                type: Sequelize.INTEGER,
                allowNull: true
            },
            updated_date_time: {
                type: Sequelize.DATE,
                allowNull: true
            },
            is_delete: {
                type: Sequelize.STRING(2),
                allowNull: true,
                defaultValue:'0'
            },
            deleted_by: {
                type: Sequelize.INTEGER,
                allowNull: true
            },
            deleted_date_time: {
                type: Sequelize.DATE,
                allowNull: true
            },
            
        },
        {
            timestamps:false,
           createdAt: false,
           UpdatedAt: false,
       });
    }

    saveScheduleMeetings(ScheduleMeetingsData) {
        //console.log("++++++++++++++++++++++++++++",ScheduleMeetingsData)
        return sequelize.sync().then(() => {
            return this.SCHEDULEMEETINGS.create({
              
                meeting_id: ScheduleMeetingsData.meeting_id,
                meeting_template_id:ScheduleMeetingsData.meeting_template_id,
                frequency:ScheduleMeetingsData.frequency,
                meeting_date:ScheduleMeetingsData.meeting_date,
                meeting_start_time:ScheduleMeetingsData.meeting_start_time,
                meeting_end_time:ScheduleMeetingsData.meeting_end_time,
                location:ScheduleMeetingsData.location,
                attendies:ScheduleMeetingsData.attendies,
                description:ScheduleMeetingsData.description,
                created_by:ScheduleMeetingsData.created_by,
                created_date_time:new Date()
                
              
            }).catch(function (errLog) {
                console.log("errMsg:", errLog)
                log.addLog('model :: schedule_meetings.js ' + errLog);
                var err;
                if (errLog == "SequelizeUniqueConstraintError: Validation error") {
                    err = {
                        data: 0,
                        errMsg: "ScheduleMeetingsData Body name should be unique"
                    }
                } else if (errLog != "SequelizeUniqueConstraintError: Validation error") {
                    err = {
                        data: 0,
                        errMsg: errLog["errors"][0]["message"]
                    }

                }
                return err;
            });
        }).catch(function (errLog) {
            log.addLog('model :: schedule_meetings.js ' + errLog);
        });
    }

    

    updateScheduleMeetings(ScheduleMeetingsData) {
        //console.log("sheddddu------------",ScheduleMeetingsData);
        return sequelize.sync().then(() => {
            return this.SCHEDULEMEETINGS.update({
              
            meeting_id: ScheduleMeetingsData.meeting_id,
            meeting_template_id:ScheduleMeetingsData.meeting_template_id,
            frequency:ScheduleMeetingsData.frequency,
            meeting_date:ScheduleMeetingsData.meeting_date,
            meeting_start_time:ScheduleMeetingsData.meeting_start_time,
            meeting_end_time:ScheduleMeetingsData.meeting_end_time,
            location:ScheduleMeetingsData.location,
            attendies:ScheduleMeetingsData.attendies,
            description:ScheduleMeetingsData.description,
          
            updated_by:ScheduleMeetingsData.updated_by,
            updated_date_time:new Date()
              
            },{
                where:{
                    schedule_meeting_id:ScheduleMeetingsData.schedule_meeting_id
                }
            }).catch(function (errLog) {
                console.log("errMsg:", errLog)
                log.addLog('model :: schedule_meetings.js ' + errLog);
                var err;
                if (errLog == "SequelizeUniqueConstraintError: Validation error") {
                    err = {
                        data: 0,
                        errMsg: "Governing Body name should be unique"
                    }
                } else if (errLog != "SequelizeUniqueConstraintError: Validation error") {
                    err = {
                        data: 0,
                        errMsg: errLog["errors"][0]["message"]
                    }

                }
                return err;
            });
        }).catch(function (errLog) {
            log.addLog('model :: schedule_meetings.js ' + errLog);
        });
    }

    getScheduleMeetings(){
        var dd=new Date();
        console.log("dd---------------",dd);
        //console.log("in meetings body");
        return sequelize.sync().then(() => {
            return this.SCHEDULEMEETINGS.all().then((data) => {
                return data
            }).catch((errLog) => {
                var err = {
                    data: 0,
                    errMsg: errMsg
                }
                log.addLog('model :: schedule_meetings.js 215' + errLog);
                return err;
                
            });
        }).catch((errLog) => {
            var err = {
                data: 0,
                errMsg: errMsg
            }
            log.addLog('model :: schedule_meetings.js 224 ' + errLog);
            return err;
        });
    }
   
    deleteScheduleMeetings(schedule_meeting_data){
        return sequelize.sync().then(() => {
            return this.SCHEDULEMEETINGS.update({
                is_delete:'1',
                deleted_by:schedule_meeting_data.deleted_by,
                deleted_date_time:new Date()
            },{
                where: {
                    schedule_meeting_id:schedule_meeting_data.schedule_meeting_id
                }
            }).catch((errLog) => {
                var err = {
                    data: 0,
                    errMsg: errMsg
                }
                log.addLog('model :: schedule_meetings.js 244 ' + errLog);
                return err;
            });
        });
    }

};