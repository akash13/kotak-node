const Sequelize = require('sequelize');
var con = require('./connection');
//var Application = require('./application');
const Op = Sequelize.Op;
var result="";
var nodemailer=require('nodemailer');
var transporter = nodemailer.createTransport({
 service: 'mailtrap',
 auth: {
        user: 'fbdebd3b7b5648',
        pass: '8e8886a64111eb'
    }
})
//var statictable="IPT_T";

module.exports = class IPDashboard {



    getUserAndCompany() {
      
         return sequelize.sync().then(() => {
    
                return sequelize.query("Select id,user_id,company_name from ADM_User_and_Company; ", {
                    type: sequelize.QueryTypes.SELECT
                });
         
        });
    
    }
    
    getCompany(userid) {
      
        return sequelize.sync().then(() => {
   
               return sequelize.query("Select company_name from ADM_User_and_Company where user_id='"+userid+"'; ", {
                   type: sequelize.QueryTypes.SELECT
               });
        
       });
       
   }

   getCompanyEmployee(cname) {
      
    return sequelize.sync().then(() => {

           return sequelize.query("Select emp_name,emp_id from users ; ", {
               type: sequelize.QueryTypes.SELECT
           });
    
   });
   
}

getEmployeeDetails(id) {
      
    return sequelize.sync().then(() => {

           return sequelize.query("Select emp_name,emp_id,emp_department from users where emp_id='"+id+"'; ", {
               type: sequelize.QueryTypes.SELECT
           });
    
   });
   
}
getUsersEmail(reqData) {
    return sequelize.sync().then(() => {
        return sequelize.query("select emp_email,emp_name,emp_accountexpires from  users where emp_id= '" + reqData +"'", { bind: ['active'], type: sequelize.QueryTypes.SELECT }
        );
    });
}
getEmployeeDetailsForView(id) {
      
    return sequelize.sync().then(() => {

           return sequelize.query("Select u.emp_name,u.emp_id,u.emp_department,u.emp_accountexpires,a.SAP_Id,a.Remark from users u left join ADUserLogHistory a on a.Emp_ID =u.emp_id where u.emp_id='"+id+"'; ", {
               type: sequelize.QueryTypes.SELECT
           });
    
   });
   
}
   
   


sendRequestorEmail(emailData){
    console.log("emailData",emailData);
   const mailOptions = {
     from: "vendor.portal@bharatforge.com", // sender address
     to: emailData['usersEmail'],
     cc: "akash.ingale@autoflowtech.com", // list of receivers
     subject: "Account expires date", // Subject line
     html: "Dear Sir/Madam,<br><br> Your account has expired.Please find below details <br><br><b>Employee Id :  </b>"+emailData['emp_id']+"<br><b>Employee Name :  </b>"+emailData['usersName']+".<br><b>Expire date :  </b>"+emailData['expireDate']+"<br><br> Regards,<br>Technical Team."
   //   +'Created By:- ' + emailData["currenUserName"] + ' (' + emailData["currenUserId"] + ') ' + '<br><br>' + 'Created At:- '+ emailData["currentTime"]// plain text body
   };
   
   transporter.sendMail(mailOptions, function (err, info) {
      if(err)
      {
        console.log("email",err);
    return false;
      }
      else
      {
        console.log("email",info);
    return true;
      }
   });
}

    saveUser(userData){
        return sequelize.sync().then(() => {
            return sequelize.query("insert into ADM_User_and_Company (user_id,company_name,Created_By)values("+ "'"+ userData.user_id+"','"+userData.company+"','"+userData.created_By+ "'"+") ", {
                type: sequelize.QueryTypes.INSERT
            });
        });
    }

    updateUser(userData){
        return sequelize.sync().then(() => {
            return sequelize.query("update ADM_User_and_Company set  user_id = '"+ userData.user_id+"',company_name='"+userData.company+"',Updated_By='"+userData.created_By+ "' where id='"+userData.id+"' ", {
                type: sequelize.QueryTypes.INSERT
            });
        });
    }
    
    updateUserDetails(userData){
        return sequelize.sync().then(() => {
            return sequelize.query("update users set  emp_accountexpires = '"+ userData.expireDate+"',Updated_By1='"+userData.updated_by+ "' where emp_id='"+userData.emp_id+"' ", {
                type: sequelize.QueryTypes.update
            }).then(()=>{
                if(userData.sapid==""|| userData.sapid=="undefined" || userData.sapid == "null"){
                    userData.sapid=""; 
                }
                return sequelize.query("insert into  ADUserLogHistory (Emp_ID,SAP_Id,Remark,Account_Expire_Date,UpdatedBy)values('"+userData.emp_id+"','"+userData.sapid+"','"+userData.remark+"','"+userData.expireDate+"','"+userData.updated_by+"') ", {
                    type: sequelize.QueryTypes.INSERT
                });
    
            })
     
          

            
        });
     
          

            
      

      
    }

    
    

    deleteAdUsers(Id) {
        return sequelize.sync().then(() => {
            return sequelize.query("delete from ADM_User_and_Company where id='"+ Id +"'", {
                type: sequelize.QueryTypes.SELECT
             })
        });
    }

    

  
}
