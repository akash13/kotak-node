const Sequelize = require('sequelize');
var con = require('./connection');


module.exports = class FormAPI {


    constructor() {

        this.FORMAPI = con.define('form_api_master', {
            form_api_id: {
                type: Sequelize.INTEGER,
                primaryKey: true,
                autoIncrement: true
            },
            form_id_fk: Sequelize.INTEGER,
            form_api_method: Sequelize.STRING,
            form_api_name: Sequelize.STRING,
            form_api_action: Sequelize.STRING,
        });
    }

    saveAPI(formApiData) {
        formApiData.forEach(element => {
            return sequelize.sync().then(() => {
                return this.FORMAPI.create({
                    form_api_action: element.ApiAction,
                    form_api_method: element.ApiGetMethod,
                    form_api_name: element.APiGetString,
                    form_id_fk: element.FormId
                });
            });
        });
    }

    getFormAPIs() {
        return sequelize.sync().then(() => {
            return this.FORMAPI.all();
        });
    }
};