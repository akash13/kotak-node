import {RouterModule, Routes} from "@angular/router";

import { FormComponent } from './form/form.component';
import { FormAPIComponent } from './formAPI/formapi.component';

const APP_ROUTES: Routes = [
   
    { path:'form', component: FormComponent},
    { path:'formapi', component: FormAPIComponent},

];

export const routing = RouterModule.forRoot(APP_ROUTES);