const Sequelize = require('sequelize');
var con = require('./connection');
var nodemailer = require('nodemailer');

var transporter = nodemailer.createTransport({
   // host: '172.16.100.15',
//port: 587,
     host: 'smtp.zoho.com',
     port: 465,
     secure: true,
     auth: {
         user: 'yogesh.sonawane@autoflowtech.com',
         pass: 'Yogesh@2019'
     },
    rateLimit: 100,

})

transporter.verify(function(error, success) {
    if (error) {
        console.log(error);
    } else {
        console.log("Server is ready to take our messages");
    }
});


var url = "http://localhost:4200"


module.exports = class EmailNotification {

    constructor() {

    }

    sendMail(strEmails, cc, draft, subject) {
        console.log("this for email-------")
        if (subject != '') {
            var subject = subject;
        } else {
            var subject = "Kotak Notification";
        }

        var mailOptions = {
            from: 'yogesh.sonawane@autoflowtech.com',
            to: strEmails,
            cc: cc,
            subject: subject,
            html: draft,
            rateLimit: 100,
        };

        console.log("mailoption------------>", mailOptions)
        transporter.sendMail(mailOptions, function(error, info) {
            if (error) {

            } else {
                console.log("Email Send-------------------");

            }
        });

    }

    sendMailForApproveOrRejct(Data, RoleType) {
        console.log("this for send mail cm ", Data, "this for type ", RoleType);
        if (RoleType === "CM") {
            if (Data.CMApproveFlag === 1) {
                console.log("this cm approval flag");
                //approval
                sequelize.sync().then(() => {
                    sequelize.query(
                        " select emailId, employeeName from master_employees where role ='GM'", {
                            bind: ["active"],
                            type: sequelize.QueryTypes.SELECT
                        }).then((gmPersonal) => {
                        console.log("email for gm----------->>>", gmPersonal);
                        console.log("apprve id ------->>>", Data.approvalRequestId);


                        sequelize.sync().then(() => {
                            sequelize.query(
                                "select ar.approvalRequestId, ar.requestNumber, ar.partDescription, ar.customerName,ar.partNumber,me.employeeName,me.emailId from approve_requests as ar inner join master_employees as me on me.employeeId = ar.createdBy where ar.approvalRequestId ='" + Data.approvalRequestId + "'", {
                                    bind: ["active"],
                                    type: sequelize.QueryTypes.SELECT
                                }).then((toPerson) => {
                                console.log("email for toPerson----------->>>", toPerson);
                                sequelize.query(
                                    " select employeeName,emailId from master_employees where employeeId = '" + Data.createdByCm + "'", {
                                        bind: ["active"],
                                        type: sequelize.QueryTypes.SELECT
                                    }).then((fromPerson) => {
                                    console.log("email for fromPerson----------->>>", fromPerson);
                                    var mails = toPerson[0].emailId + "," + fromPerson[0].emailId + "," + gmPersonal[0].emailId;
                                    console.log("kkkkkkkkkkkkkkkk--->>>", mails)
                                    var draft = "Dear Mr. " + gmPersonal[0].employeeName + ", <br><br>Kindly approve this request  <b>(" + toPerson[0].requestNumber + ")</b> to issue PO to the supplier to initiate development. <br>Pl. click here to view & approve the details  <br> <a href=\"" + url + "\">app.pas.com</a>";
                                    console.log("kkkkkkkkkkkkkkkk--->>>", draft)
                                    this.sendMail(mails, '', draft, "RPS has been approved by CM");
                                }).catch((err) => {
                                    console.log("data 1 ==========", err);
                                });
                            }).catch((err) => {
                                console.log("data 1 ==========", err);
                            });

                        });
                    });
                });

            } else {
                // reject
                console.log("this cm reject flag");
                console.log("apprve id -----reject-->>>", Data.approvalRequestId);

                sequelize.sync().then(() => {
                    sequelize.query(
                        "select ar.approvalRequestId, ar.requestNumber, ar.partDescription, ar.customerName,ar.partNumber,me.employeeName,me.emailId from approve_requests as ar inner join master_employees as me on me.employeeId = ar.createdBy where ar.approvalRequestId ='" + Data.approvalRequestId + "'", {
                            bind: ["active"],
                            type: sequelize.QueryTypes.SELECT
                        }).then((toPerson) => {
                        console.log("email for toPerson----------->>>", toPerson)
                        sequelize.query(
                            " select employeeName,emailId from master_employees where employeeId = '" + Data.createdByCm + "'", {
                                bind: ["active"],
                                type: sequelize.QueryTypes.SELECT
                            }).then((fromPerson) => {
                            console.log("email for fromPerson----------->>>", fromPerson);
                            var mails = toPerson[0].emailId + "," + fromPerson[0].emailId;
                            console.log("kkkkkkkkkkkkkkkk--->>>", mails)
                            var draft = "Dear Mr." + toPerson[0].employeeName + ": <br><br> The <b>Request Number : </b> " + toPerson[0].requestNumber + "is rejected by Mr. " + fromPerson[0].employeeName + "<br>  <b>Commodity manager remarks : </b> " + Data.CMRemarks;
                            console.log("kkkkkkkkkkkkkkkk--->>>", draft)
                            this.sendMail(mails, '', draft, "RPS has been rejected by CM");
                        }).catch((err) => {
                            console.log("data 1 ==========", err);
                        });
                    }).catch((err) => {
                        console.log("data 1 ==========", err);
                    });

                });
            }
        } else if (RoleType === "GM") {
            if (Data.GMApproveFlag === 1) {
                console.log("this GM approval flag", Data.GMApproveFlag);
                //approval
                sequelize.sync().then(() => {
                    sequelize.query(
                        " select emailId, employeeName from master_employees where role ='VP'", {
                            bind: ["active"],
                            type: sequelize.QueryTypes.SELECT
                        }).then((vpPersonal) => {
                        console.log("email for vp----------->>>", vpPersonal);
                        console.log("apprve id ------->>>", Data.approvalRequestId);
                        sequelize.sync().then(() => {
                            sequelize.query(
                                "select ar.approvalRequestId,createdByCm, ar.requestNumber, ar.partDescription, ar.customerName,ar.partNumber,me.employeeName,me.emailId from approve_requests as ar inner join master_employees as me on me.employeeId = ar.createdBy where ar.approvalRequestId ='" + Data.approvalRequestId + "'", {
                                    bind: ["active"],
                                    type: sequelize.QueryTypes.SELECT
                                }).then((toPerson) => {
                                console.log("email for toPerson----------->>>", toPerson);
                                sequelize.query(
                                    "select employeeName,emailId from master_employees where employeeId = '" + toPerson[0].createdByCm + "'", {
                                        bind: ["active"],
                                        type: sequelize.QueryTypes.SELECT
                                    }).then((fromCmPerson) => {
                                    sequelize.query(
                                        "select employeeName,emailId from master_employees where employeeId = '" + Data.createdByGm + "'", {
                                            bind: ["active"],
                                            type: sequelize.QueryTypes.SELECT
                                        }).then((fromPerson) => {
                                        console.log("email for fromPerson----------->>>", fromPerson);
                                        var mails = toPerson[0].emailId + "," + fromPerson[0].emailId + "," + fromCmPerson[0].emailId + "," + vpPersonal[0].emailId;
                                        console.log("kkkkkkkkkkkkkkkk--->>>", mails)
                                        var draft = "Dear Mr. " + vpPersonal[0].employeeName + ", <br><br>Kindly approve this request <b>(" + toPerson[0].requestNumber + ")</b> to issue PO to the supplier to initiate development. <br>  <a href=\"" + url + "\">app.pas.com</a> ";
                                        console.log("kkkkkkkkkkkkkkkk--->>>", draft)
                                        this.sendMail(mails, '', draft, "RPS has been approved by GM");
                                    }).catch((err) => {
                                        console.log("data 1 ==========", err);
                                    });
                                });

                            }).catch((err) => {
                                console.log("data 1 ==========", err);
                            });

                        });
                    });
                });

            } else {
                sequelize.sync().then(() => {
                    console.log("apprve id ------->>>", Data.approvalRequestId);
                    sequelize.sync().then(() => {
                        sequelize.query(
                            "select ar.approvalRequestId,createdByCm, ar.requestNumber, ar.partDescription, ar.customerName,ar.partNumber,me.employeeName,me.emailId from approve_requests as ar inner join master_employees as me on me.employeeId = ar.createdBy where ar.approvalRequestId ='" + Data.approvalRequestId + "'", {
                                bind: ["active"],
                                type: sequelize.QueryTypes.SELECT
                            }).then((toPerson) => {
                            console.log("email for toPerson----------->>>", toPerson);
                            sequelize.query(
                                "select employeeName,emailId from master_employees where employeeId = '" + toPerson[0].createdByCm + "'", {
                                    bind: ["active"],
                                    type: sequelize.QueryTypes.SELECT
                                }).then((fromCmPerson) => {
                                sequelize.query(
                                    "select employeeName,emailId from master_employees where employeeId = '" + Data.createdByGm + "'", {
                                        bind: ["active"],
                                        type: sequelize.QueryTypes.SELECT
                                    }).then((fromPerson) => {
                                    console.log("email for fromPerson----------->>>", fromPerson);
                                    var mails = toPerson[0].emailId + "," + fromPerson[0].emailId + "," + fromCmPerson[0].emailId;
                                    console.log("kkkkkkkkkkkkkkkk--->>>", mails)
                                    var draft = "RPS is Reject by " + fromPerson[0].employeeName + ": <br><br> <b>Request Number : </b> " + toPerson[0].requestNumber + "<br> <b>Part Number : </b> " + toPerson[0].partNumber + "<br> <b>Part Description : </b> " + toPerson[0].partDescription + "<br> <b>GM Remark : </b> " + Data.GMRemarks;
                                    console.log("kkkkkkkkkkkkkkkk--->>>", draft)
                                    this.sendMail(mails, '', draft, "RPS has been Rejected by GM");
                                }).catch((err) => {
                                    console.log("data 1 ==========", err);
                                });
                            });

                        }).catch((err) => {
                            console.log("data 1 ==========", err);
                        });

                    });

                });
            }
        } else if (RoleType === "VP") {
            if (Data.VPApproveFlag === 1) {
                console.log("this VP approval flag", Data.VPApproveFlag);
                //approval
                sequelize.sync().then(() => {
                    sequelize.query(
                        " select emailId, employeeName from master_employees where role ='VP'", {
                            bind: ["active"],
                            type: sequelize.QueryTypes.SELECT
                        }).then((vpPersonal) => {
                        console.log("email for vp----------->>>", vpPersonal);
                        console.log("apprve id ------->>>", Data.approvalRequestId);
                        sequelize.sync().then(() => {
                            sequelize.query(
                                "select ar.approvalRequestId,createdByCm, ar.requestNumber,ar.createdByGm, ar.partDescription, ar.customerName,ar.partNumber,me.employeeName,me.emailId from approve_requests as ar inner join master_employees as me on me.employeeId = ar.createdBy where ar.approvalRequestId ='" + Data.approvalRequestId + "'", {
                                    bind: ["active"],
                                    type: sequelize.QueryTypes.SELECT
                                }).then((toPerson) => {
                                console.log("email for toPerson----------->>>", toPerson);
                                sequelize.query(
                                    "select employeeName,emailId from master_employees where employeeId = '" + toPerson[0].createdByCm + "'", {
                                        bind: ["active"],
                                        type: sequelize.QueryTypes.SELECT
                                    }).then((fromCmPerson) => {
                                    sequelize.query(
                                        "select employeeName,emailId from master_employees where employeeId = '" + toPerson[0].createdByGm + "'", {
                                            bind: ["active"],
                                            type: sequelize.QueryTypes.SELECT
                                        }).then((fromPerson) => {
                                        console.log("email for fromPerson----------->>>", fromPerson);
                                        var mails = toPerson[0].emailId + "," + fromPerson[0].emailId + "," + fromCmPerson[0].emailId + "," + vpPersonal[0].emailId;
                                        console.log("kkkkkkkkkkkkkkkk--->>>", mails)
                                        var draft = "RPS is Approved by " + vpPersonal[0].employeeName + ": <br><br> <b>Request Number : </b> " + toPerson[0].requestNumber + "<br> <b>Part Number : </b> " + toPerson[0].partNumber + "<br> <b>Part Description : </b> " + toPerson[0].partDescription;
                                        console.log("kkkkkkkkkkkkkkkk--->>>", draft)
                                        this.sendMail(mails, '', draft, "RPS has been approved by VP");
                                    }).catch((err) => {
                                        console.log("data 1 ==========", err);
                                    });
                                });

                            }).catch((err) => {
                                console.log("data 1 ==========", err);
                            });

                        });
                    });
                });

            } else {
                sequelize.sync().then(() => {
                    console.log("apprve id ------->>>", Data.approvalRequestId);
                    sequelize.sync().then(() => {
                        sequelize.query(
                            " select emailId, employeeName from master_employees where role ='VP'", {
                                bind: ["active"],
                                type: sequelize.QueryTypes.SELECT
                            }).then((vpPersonal) => {
                            sequelize.query(
                                "select ar.approvalRequestId,createdByCm, ar.requestNumber,ar.createdByGm, ar.partDescription, ar.customerName,ar.partNumber,me.employeeName,me.emailId from approve_requests as ar inner join master_employees as me on me.employeeId = ar.createdBy where ar.approvalRequestId ='" + Data.approvalRequestId + "'", {
                                    bind: ["active"],
                                    type: sequelize.QueryTypes.SELECT
                                }).then((toPerson) => {
                                console.log("email for toPerson----------->>>", toPerson);
                                sequelize.query(
                                    "select employeeName,emailId from master_employees where employeeId = '" + toPerson[0].createdByCm + "'", {
                                        bind: ["active"],
                                        type: sequelize.QueryTypes.SELECT
                                    }).then((fromCmPerson) => {
                                    sequelize.query(
                                        "select employeeName,emailId from master_employees where employeeId = '" + toPerson[0].createdByGm + "'", {
                                            bind: ["active"],
                                            type: sequelize.QueryTypes.SELECT
                                        }).then((fromPerson) => {
                                        console.log("email for fromPerson----------->>>", fromPerson);
                                        var mails = toPerson[0].emailId + "," + fromPerson[0].emailId + "," + fromCmPerson[0].emailId + "," + vpPersonal[0].emailId;
                                        console.log("kkkkkkkkkkkkkkkk--->>>", mails)
                                        var draft = "RPS is Reject by " + vpPersonal[0].employeeName + ": <br><br> <b>Request Number : </b> " + toPerson[0].requestNumber + "<br> <b>Part Number : </b> " + toPerson[0].partNumber + "<br> <b>Part Description : </b> " + toPerson[0].partDescription + "<br> <b>VP Remark : </b> " + Data.VPRemarks;
                                        console.log("kkkkkkkkkkkkkkkk--->>>", draft)
                                        this.sendMail(mails, '', draft, "RPS has been Rejected by VP");
                                    }).catch((err) => {
                                        console.log("data 1 ==========", err);
                                    });
                                });

                            }).catch((err) => {
                                console.log("data 1 ==========", err);
                            });

                        });
                    });
                });
            }
        }

    }

    sendmailOnRaiseNPDRequest(revisedData, Data) {
        console.log("sendmailOnRaiseNPDRequest------------>",revisedData)
            var draft = "Task Name : "+ revisedData['taskName']+"<br>"+"Status : "+revisedData['status'];
            this.sendMail( Data[0]['T_CHR_Email'], '', draft, "Escalation-Task");
                    
    }
    sendmailOnRejections(mailOptions = {
        from: 'yogesh.sonawane@autoflowtech.com',
        to: 'ysonawane86@gmail.com',
        subject: 'test mail',
        html: 'html ',
        rateLimit: 100,

    }) {

        transporter.sendMail(mailOptions, function(error, info) {
            if (error) {
                console.log("Email error--------------------------", error)

            } else {
                console.log("Email Send--------------------------")

            }
        });
    }

    // sendmailOnRejections() {
    //     var mailOptions = {
    //         from: 'pune.gadmin@anandgroupindia.com',
    //         to: 'ysonawane86@gmail.com',
    //         subject: 'test mail',
    //         html: 'html ',
    //         rateLimit: 100,

    //     };
    //     transporter.sendMail(mailOptions, function(error, info) {
    //         if (error) {
    //             console.log("Email error--------------------------", error)

    //         } else {
    //             console.log("Email Send--------------------------")

    //         }
    //     });
    // }
}