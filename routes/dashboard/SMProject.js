var express = require('express');
var router = express.Router();
var bcrypt = require('bcryptjs');
var jwt = require('jsonwebtoken');
const Joi = require('joi');
var SMProject = require('../../model/SMProject')
var path = require('path')
var myLogger = require('../../middleware.js');

router.get('/getEmployeeList/:userid',myLogger, function (req, res, next) {
    var request = new SMProject();
    var userid = req.params.userid;


    // console.log("userid", userid);
    var totalcount = 0;
    request.getEmployeeList(userid).then((userData) => {

        buildResponse(res, 200, {
            error: false,
            message: 'Forms Fetched successfully',
            responseData: userData

        });
    });



});

router.get('/getDeprtmentList',myLogger, function (req, res, next) {
    var request = new SMProject();
   
    var totalcount = 0;
    request.getDeprtmentList().then((userData) => {

        buildResponse(res, 200, {
            error: false,
            message: 'Forms Fetched successfully',
            responseData: userData

        });
    });



});

router.get('/getLineList/:department',myLogger, function (req, res, next) {
    var request = new SMProject();
   var department=req.params.department;
    var totalcount = 0;
    request.getLineList(department).then((userData) => {

        buildResponse(res, 200, {
            error: false,
            message: 'Forms Fetched successfully',
            responseData: userData

        });
    });



});



router.get('/getFunctionList',myLogger, function (req, res, next) {
    var request = new SMProject();
   
    var totalcount = 0;
    request.getFunctionList().then((userData) => {

        buildResponse(res, 200, {
            error: false,
            message: 'Forms Fetched successfully',
            responseData: userData

        });
    });



});


router.get('/getDepartmentWiseReport/:department',myLogger, function (req, res, next) {
    var request = new SMProject();
   var department=req.params.department;
    var totalcount = 0;
    request.getDepartmentWiseReport(department).then((userData) => {

        buildResponse(res, 200, {
            error: false,
            message: 'Forms Fetched successfully',
            responseData: userData

        });
    });

});

router.get('/getLineWiseReport/:department/:line',myLogger, function (req, res, next) {
    var request = new SMProject();
   var department=req.params.department;
   var line=req.params.line;
  
    request.getLineWiseReport(department,line).then((userData) => {

        buildResponse(res, 200, {
            error: false,
            message: 'Forms Fetched successfully',
            responseData: userData

        });
    });

});

router.get('/getFunctionWiseReport/:function_id',myLogger, function (req, res, next) {
    var request = new SMProject();
   
   var function_id=req.params.function_id;
  
    request.getFunctionWiseReport(function_id).then((userData) => {

        buildResponse(res, 200, {
            error: false,
            message: 'Forms Fetched successfully',
            responseData: userData

        });
    });

});



router.get('/getDepartmentName/:userid',myLogger, function (req, res, next) {
    var request = new SMProject();
   var userid=req.params.userid;
    var totalcount = 0;
    request.getDepartmentName(userid).then((userData) => {

        buildResponse(res, 200, {
            error: false,
            message: 'Forms Fetched successfully',
            responseData: userData

        });
    });

});

router.get('/getLineName/:line',myLogger, function (req, res, next) {
    var request = new SMProject();
   var line=req.params.line;
    var totalcount = 0;
    request.getLineName(line).then((userData) => {

        buildResponse(res, 200, {
            error: false,
            message: 'Forms Fetched successfully',
            responseData: userData

        });
    });

});

router.get('/getfunctionName/:function_id',myLogger, function (req, res, next) {
    var request = new SMProject();
   var function_id=req.params.function_id;
    var totalcount = 0;
    request.getfunctionName(function_id).then((userData) => {

        buildResponse(res, 200, {
            error: false,
            message: 'Forms Fetched successfully',
            responseData: userData

        });
    });

});





router.get('/getEmployeeSkillList/:role_id/:fun_id',myLogger, function (req, res, next) {
    var request = new SMProject();
    var role_id = req.params.role_id;
    
    var fun_id = req.params.fun_id;

    
    var totalcount = 0;
    request.getEmployeeSkillList(role_id,fun_id).then((userData) => {

        buildResponse(res, 200, {
            error: false,
            message: 'Forms Fetched successfully',
            responseData: userData

        });
    });



});


router.get('/ListTrainingPlan/',myLogger, function (req, res, next) {
    var request = new SMProject();
  
    // console.log("emp_id", emp_id);
    var totalcount = 0;
    request.ListTrainingPlan().then((userData) => {

        buildResponse(res, 200, {
            error: false,
            message: 'Forms Fetched successfully',
            responseData: userData

        });
    });



});


router.get('/getEmployeeSkillList_Matrix_Table/:emp_id',myLogger, function (req, res, next) {
    var request = new SMProject();
    var emp_id = req.params.emp_id;
    var dept = req.params.dept;

    // console.log("emp_id", emp_id);
    var totalcount = 0;
    request.getEmployeeSkillList_Matrix_Table(emp_id).then((userData) => {

        buildResponse(res, 200, {
            error: false,
            message: 'Forms Fetched successfully',
            responseData: userData

        });
    });



});


router.post('/saveData',myLogger, function (req, res, next) {
    var request = new SMProject();
    var rows = req.body;
    //console.log("rowssssssss",rows);
    var ip = req.headers['x-forwarded-for'] || req.connection.remoteAddress;
    var count=0;
    var rowscount=0;
    rows.forEach(element => {
        rowscount=rowscount+element["details"].length;
        
        request.getEmployeeSkillData(element["fun_id"]).then((userData) => {
           
            if (userData.length > 0) {
                element["details"].forEach(elem => {
                    request.getEmployeeEachRoleId(element["ticketNo"]).then((roleIDData) => {
                        //console.log("roleIDData",roleIDData)
                                                   var role_id=roleIDData[0]["role_id"];
                                               
                    request.getEmployeeEachSkillData(element["ticketNo"], elem["skillId"],role_id).then((getEmployeeEachSkillData) => {
                       
                       
                    
                        //console.log("getEmployeeEachSkillData=",getEmployeeEachSkillData[0]["role_id"]);
                        if (getEmployeeEachSkillData.length > 0) {
                            request.getEmployeeSkillUpdateData(element["ticketNo"], elem["skillId"], elem["actualValue"],element["fun_id"],element["current_user"],element["current_date"],role_id,ip).then((getEmployeeSkillUpdateData) => {
                                count++;
                                
                                if(rowscount == count){
                                    buildResponse(res, 200, {
                                        error: false,
                                        message: 'Updated successfully'
                                
                                    });
                                }
                                
                            });
                        } else {
                            request.getEmployeeSkillInsertData(element["ticketNo"], elem["skillId"], elem["actualValue"],element["fun_id"],element["current_user"],element["current_date"],role_id,ip).then((getEmployeeSkillInsertData) => {
                               
                                count++;
                                if(rowscount == count){
                                    buildResponse(res, 200, {
                                        error: false,
                                        message: 'Updated successfully'
                                
                                    });
                                }
                            });
                        }
                    //////
                    
                });
            });
                });
               
            } else {
               
                element["details"].forEach(elem => {
                    count++;
                    
                    request.getEmployeeSkillInsertData(element["ticketNo"], elem["skillId"], elem["actualValue"],element["dept_id"],element["line_id"],element["fun_id"],element["current_user"],element["current_date"],ip).then((getEmployeeSkillInsertData) => {
                        
                        if(rowscount == count){
                            buildResponse(res, 200, {
                                error: false,
                                message: 'Updated successfully'
                        
                            });
                        }
                    });

                });

              
            }
           
           
        });

        
        
    });
    var totalcount = 0;

   

});


router.get('/getDashboardTotalCount/:tableName/:userid',myLogger, function (req, res, next) {
    var request = new IPDashboard();
    var tablenameval = req.params.tableName;
    var userid = req.params.userid;
    // var group=req.params.group;
    // var dept=req.params.dept;
    // console.log("in route total",group,dept);

    var totalcount = 0;
    //    for (var i=0;i< req.body.length;i++)
    //    {
    request.getDashboardTotalCount(tablenameval, userid).then((countData) => {
        request.getDashboardTotalCount
        //console.log("countdata=",countData)
        buildResponse(res, 200, {
            error: false,
            message: 'Forms Fetched successfully',
            listData: countData,
            countData: countData.length

        });
    });

    //}

});


router.get('/getDashboardPendingCount/:tableName/:columnname',myLogger, function (req, res, next) {
    var request = new IPDashboard();
    var tablenameval = req.params.tableName;
    var colomname = req.params.columnname;

    // var userid= req.params.userid;
    // var group=req.params.group;
    // var dept=req.params.dept;

    // console.log("tablenameval", tablenameval);
    var totalcount = 0;
    request.getDashboardPendingCount(tablenameval, colomname).then((countData) => {

        buildResponse(res, 200, {
            error: false,
            message: 'Forms Fetched successfully',
            listData: countData,
            countData: countData.length

        });
    });



});

router.get('/ShowTrainingData/:id',myLogger, function (req, res, next) {
    var request = new SMProject();
    var id = req.params.id;
   // var userid= req.params.userid;
    // var group=req.params.group;
    // var dept=req.params.dept;

    // console.log("tablenameval", tablenameval);
    var totalcount = 0;
    request.ShowTrainingData(id).then((countData) => {

        buildResponse(res, 200, {
            error: false,
            message: 'Forms Fetched successfully',
            listData: countData,
            countData: countData.length

        });
    });



});




router.get('/getDashboardCompletedCount/:tableName/:columnname',myLogger, function (req, res, next) {
    var request = new IPDashboard();
    var tablenameval = req.params.tableName;
    var colomname = req.params.columnname;

    // var userid= req.params.userid;
    // var group=req.params.group;
    // var dept=req.params.dept;

    var totalcount = 0;
    request.getDashboardCompletedCount(tablenameval, colomname).then((countData) => {

        buildResponse(res, 200, {
            error: false,
            message: 'Forms Fetched successfully',
            listData: countData,
            countData: countData.length


        });
    });



});

router.get('/getDashboardHoldCount/:tableName/:columnname',myLogger, function (req, res, next) {
    var request = new IPDashboard();
    var tablenameval = req.params.tableName;
    var colomname = req.params.columnname;

    // var userid= req.params.userid;
    // var group=req.params.group;
    // var dept=req.params.dept;

    var totalcount = 0;
    request.getDashboardHoldCount(tablenameval, colomname).then((countData) => {

        buildResponse(res, 200, {
            error: false,
            message: 'Forms Fetched successfully',
            listData: countData,
            countData: countData.length

        });
    });



});

router.get('/getDashboardIssueCount/:tableName/:columnname',myLogger, function (req, res, next) {
    var request = new IPDashboard();
    var tablenameval = req.params.tableName;
    var colomname = req.params.columnname;

    // var userid= req.params.userid;
    // var group=req.params.group;
    // var dept=req.params.dept;

    var totalcount = 0;
    request.getDashboardIssueCount(tablenameval, colomname).then((countData) => {

        buildResponse(res, 200, {
            error: false,
            message: 'Forms Fetched successfully',
            listData: countData,
            countData: countData.length

        });
    });



});

router.get('/getDashboardYTSCount/:tableName/:columnname',myLogger, function (req, res, next) {
    var request = new IPDashboard();
    var tablenameval = req.params.tableName;
    var colomname = req.params.columnname;

    // var userid= req.params.userid;
    // var group=req.params.group;
    // var dept=req.params.dept;
    var totalcount = 0;
    request.getDashboardYTSCount(tablenameval, colomname).then((countData) => {

        buildResponse(res, 200, {
            error: false,
            message: 'Forms Fetched successfully',
            listData: countData,
            countData: countData.length

        });
    });



});



router.get('/getDashboardRejectedCount/:tableName/:columnname',myLogger, function (req, res, next) {
    var request = new IPDashboard();
    var tablenameval = req.params.tableName;
    var colomname = req.params.columnname;

    // var userid= req.params.userid;
    // var group=req.params.group;
    // var dept=req.params.dept;

    var totalcount = 0;
    request.getDashboardRejectedCount(tablenameval, colomname).then((countData) => {

        buildResponse(res, 200, {
            error: false,
            message: 'Forms Fetched successfully',
            listData: countData,
            countData: countData.length

        });
    });



});



router.post('/saveRequest',myLogger, function (req, res, next) {
    var request = new IPDashboard();
    var requestData = {
        issueTitle: req.body.issueTitle,
        issueDescription: req.body.issueDescription,
        requestDate: req.body.requestDate,
        expectedCompletionDate: req.body.expectedCompletionDate,
        issueAttachement: req.body.issueAttachement,

    };
    request.saveRequest(requestData)
        .then((data) => {

            buildResponse(res, 200, {
                error: false,
                message: "Stored Successfully"
            });

        }).catch((err) => {
            buildResponse(res, 500, {
                error: true,
                message: "Fail to Store"
            });
        });

});

router.post('/UpdateRequest/:id',myLogger, function (req, res, next) {
    var reqId = req.params.id;
    var request = new IPDashboard();
    var requestData = {
        issueTitle: req.body.issueTitle,
        issueDescription: req.body.issueDescription,
        requestDate: req.body.requestDate,
        expectedCompletionDate: req.body.expectedCompletionDate,
        issueAttachement: req.body.issueAttachement,
    };
    request.UpdateRequest(requestData, reqId)
        .then((data) => {
            buildResponse(res, 200, {
                error: false,
                message: "Stored Successfully"
            });

        }).catch((err) => {
            buildResponse(res, 500, {
                error: true,
                message: "Fail to Store"
            });
        });

});

router.post('/saveRequestUpdates',myLogger, function (req, res, next) {
    var request = new IPDashboard();
    var requestData = {
        tantativeDate: req.body.tantativeDate,
        attachement: req.body.attachement,
        comment: req.body.comment,
        status: req.body.status,
        requestId: req.body.requestId,
    };
    request.saveRequestUpdates(requestData)
        .then((data) => {
            buildResponse(res, 200, {
                error: false,
                message: "Stored Successfully"
            });
        }).catch((err) => {
            buildResponse(res, 500, {
                error: true,
                message: "Fail to Store"
            });
        });

});

router.get('/deleteRequest/:id',myLogger, function (req, res, next) {
    var request = new IPDashboard();
    var reqData = req.params.id;
    request.deleteRequest(reqData)
        .then((userData) => {
            buildResponse(res, 200, {
                error: false,
                message: 'User Deleted successfully',
                UserData: userData
            });
        }).catch((err) => {
            buildResponse(res, 500, {
                error: false,
                message: 'Please Try Again',
            });
        });
});


router.get('/getRequest/:id',myLogger, function (req, res, next) {

    var request = new IPDashboard();
    var reqData = req.params.id;
    request.getRequest(reqData)
        .then((requestData) => {
            buildResponse(res, 200, {
                error: false,
                message: 'Request Fetched successfully',
                requestData: requestData
            });
        }).catch((err) => {
            buildResponse(res, 404, {
                error: false,
                message: 'Failed to Fetch',
            });
        });
});

router.get('/getUpdates/:id',myLogger, function (req, res, next) {

    var request = new IPDashboard();
    var reqData = req.params.id;
    request.getUpdates(reqData)
        .then((requestData) => {
            buildResponse(res, 200, {
                error: false,
                message: 'Updates for request fetch successfully',
                requestData: requestData
            });
        }).catch((err) => {
            buildResponse(res, 404, {
                error: false,
                message: 'Failed to Fetch',
            });
        });
});
router.get('/getRequestUpdatesById/:id',myLogger, function (req, res, next) {

    var request = new IPDashboard();
    var reqData = req.params.id;
    request.getRequestUpdatesById(reqData)
        .then((requestData) => {
            buildResponse(res, 200, {
                error: false,
                message: 'Updates for request fetch successfully',
                requestData: requestData
            });
        }).catch((err) => {
            buildResponse(res, 404, {
                error: false,
                message: 'Failed to Fetch',
            });
        });
});


router.get('/getFunctionEmployeeRoleList/:userId',myLogger, function (req, res, next) {
    var request = new SMProject();
   var userId=req.params.userId;
    var totalcount = 0;
    request.getFunctionEmployeeRoleList(userId).then((userData) => {

        buildResponse(res, 200, {
            error: false,
            message: 'Forms Fetched successfully',
            responseData: userData

        });
    });



});

router.post('/getFERSkillList',myLogger, function (req, res, next) {

    var data =[];
    var request = new SMProject();
    var data = req.body;
    if(data.function_id=='' && data.employee_id=='' && data.role_id==''){
        buildResponse(res, 200, {
            error: false,
            message: "No data!",
            TableData: data


        });
    }else{
        request.getFERSkillList(data).then((data) => {
            //console.log('datas',data);
             buildResponse(res, 200, {
                 error: false,
                 message: "Successfully IPT Display!",
                 TableData: data
     
     
             });
         })
    }
   
});


router.post('/getIndividualSkillsList',myLogger, function (req, res, next) {


    var request = new SMProject();
    var data = req.body;
   //console.log("data",data);
   request.getIndividualSkillsList(data).then((data) => {
        buildResponse(res, 200, {
            error: false,
            message: "Successfully IPT Display!",
            TableData: data


        });
    });
});

router.get('/getEducationReport/:emp_id',myLogger, function (req, res, next) { 
    var request = new SMProject();
   var emp_id=req.params.emp_id;
    var totalcount = 0;
    request.getEducationReport(emp_id).then((userData) => {

        buildResponse(res, 200, {
            error: false,
            message: 'Forms Fetched successfully',
            responseData: userData

        });
    });

});

router.get('/getEmployeesOfCurrentUser/:currentUser',myLogger, function (req, res, next) { 
    var request = new SMProject();
    var currentUser=req.params.currentUser;
    var totalcount = 0;
    request.getEmployeesOfCurrentUser(currentUser).then((userData) => {

        buildResponse(res, 200, {
            error: false,
            message: 'Forms Fetched successfully',
            responseData: userData

        });
    });

});


router.get('/getEmployeesSkills/:empid',myLogger, function (req, res, next) { 
    var request = new SMProject();
    var empid=req.params.empid;
    var totalcount = 0;
    request.getEmployeesSkills(empid).then((userData) => {

        buildResponse(res, 200, {
            error: false,
            message: 'Forms Fetched successfully',
            responseData: userData

        });
    });

});

router.post('/saveTrainingdata',myLogger, function (req, res, next) {
    var request = new SMProject();
    var data = req.body;
   console.log("datadadada",data);
   request.saveTrainingdata(data).then((data) => {
        buildResponse(res, 200, {
            error: false,
            message: "Successfully IPT Display!",
            TableData: data


        });
    });
});



router.get('/deleteRow/:id',myLogger, function (req, res, next) {
    var request = new SMProject();
    var reqData = req.params.id;
    request.deleteRow(reqData)
        .then((data) => {
            buildResponse(res, 200, {
                error: false,
                message: 'Record Deleted successfully',
                TableData: data
            });
        }).catch((err) => {
            buildResponse(res, 404, {
                error: false,
                message: 'Please Try Again',

            });
        });
});


router.post('/updateTrainingdata',myLogger, function (req, res, next) {
    var request = new SMProject();
    var data = req.body;
   console.log("datadadada",data);
   request.updateTrainingdata(data).then((data) => {
        buildResponse(res, 200, {
            error: false,
            message: "Successfully IPT Display!",
            TableData: data


        });
    });
});


function buildResponse(res, statusCode, responseData) {
    res.status(statusCode).json(responseData);
}

module.exports = router;
