var express = require('express');
var router = express.Router();
var bcrypt = require('bcryptjs');
var jwt = require('jsonwebtoken');
const Joi = require('joi');
var IPDashboard = require('../../model/IPDashboard')
var path = require('path')
var myLogger = require('../../middleware.js');
   
router.get('/getDashboardUserDept/:userid',myLogger, function (req, res, next) {
    var request = new IPDashboard();
    var userid = req.params.userid;
   

console.log("userid",userid);
  var totalcount=0;
    request.getDashboardUserDept(userid).then((userData) => {

        buildResponse(res, 200, {
            error: false,
            message: 'Forms Fetched successfully',
          responseData:userData
           
        });
    });



});

router.get('/getDashboardTotalCount/:tableName/:userid',myLogger, function (req, res, next) {
    var request = new IPDashboard();
var tablenameval = req.params.tableName;
var userid= req.params.userid;
// var group=req.params.group;
// var dept=req.params.dept;
// console.log("in route total",group,dept);
 
  var totalcount=0;
//    for (var i=0;i< req.body.length;i++)
//    {
    request.getDashboardTotalCount(tablenameval,userid).then((countData) => {
    request.getDashboardTotalCount
        //console.log("countdata=",countData)
        buildResponse(res, 200, {
            error: false,
            message: 'Forms Fetched successfully',
            listData:countData,
            countData: countData.length
           
        });
    });

  //}

});



router.get('/getDashboardPendingCount/:tableName/:columnname',myLogger, function (req, res, next) {
    var request = new IPDashboard();
    var tablenameval = req.params.tableName;
    var colomname=req.params.columnname;
    
// var userid= req.params.userid;
// var group=req.params.group;
// var dept=req.params.dept;

console.log("tablenameval",tablenameval);
  var totalcount=0;
    request.getDashboardPendingCount(tablenameval,colomname).then((countData) => {

        buildResponse(res, 200, {
            error: false,
            message: 'Forms Fetched successfully',
            listData:countData,
            countData: countData.length
           
        });
    });



});


router.get('/getDashboardCompletedCount/:tableName/:columnname',myLogger, function (req, res, next) {
    var request = new IPDashboard();
    var tablenameval = req.params.tableName;
    var colomname=req.params.columnname;
    
// var userid= req.params.userid;
// var group=req.params.group;
// var dept=req.params.dept;

  var totalcount=0;
    request.getDashboardCompletedCount(tablenameval,colomname).then((countData) => {

        buildResponse(res, 200, {
            error: false,
            message: 'Forms Fetched successfully',
            listData:countData,
            countData: countData.length
            

        });
    });



});

router.get('/getDashboardHoldCount/:tableName/:columnname',myLogger, function (req, res, next) {
    var request = new IPDashboard();
    var tablenameval = req.params.tableName;
    var colomname=req.params.columnname;
    
// var userid= req.params.userid;
// var group=req.params.group;
// var dept=req.params.dept;

  var totalcount=0;
    request.getDashboardHoldCount(tablenameval,colomname).then((countData) => {

        buildResponse(res, 200, {
            error: false,
            message: 'Forms Fetched successfully',
            listData:countData,
            countData: countData.length
           
        });
    });

 

});

router.get('/getDashboardIssueCount/:tableName/:columnname',myLogger, function (req, res, next) {
    var request = new IPDashboard();
    var tablenameval = req.params.tableName;
    var colomname=req.params.columnname;
    
// var userid= req.params.userid;
// var group=req.params.group;
// var dept=req.params.dept;

  var totalcount=0;
    request.getDashboardIssueCount(tablenameval,colomname).then((countData) => {

        buildResponse(res, 200, {
            error: false,
            message: 'Forms Fetched successfully',
            listData:countData,
            countData: countData.length
           
        });
    });

 

});

router.get('/getDashboardYTSCount/:tableName/:columnname',myLogger, function (req, res, next) {
    var request = new IPDashboard();
    var tablenameval = req.params.tableName;
    var colomname=req.params.columnname;

    // var userid= req.params.userid;
    // var group=req.params.group;
    // var dept=req.params.dept;
  var totalcount=0;
    request.getDashboardYTSCount(tablenameval,colomname).then((countData) => {

        buildResponse(res, 200, {
            error: false,
            message: 'Forms Fetched successfully',
            listData:countData,
            countData: countData.length
           
        });
    });

 

});



router.get('/getDashboardRejectedCount/:tableName/:columnname',myLogger, function (req, res, next) {
    var request = new IPDashboard();
    var tablenameval = req.params.tableName;
    var colomname=req.params.columnname;

    // var userid= req.params.userid;
    // var group=req.params.group;
    // var dept=req.params.dept;

    var totalcount = 0;
    request.getDashboardRejectedCount(tablenameval,colomname).then((countData) => {

        buildResponse(res, 200, {
            error: false,
            message: 'Forms Fetched successfully',
            listData: countData,
            countData: countData.length

        });
    });



});



router.post('/saveRequest',myLogger, function (req, res, next) {
    var request = new IPDashboard();
    var requestData = {
        issueTitle: req.body.issueTitle,
        issueDomain: req.body.domain,
        issueDescription: req.body.issueDescription,
        requestDate: req.body.requestDate,
        expectedCompletionDate: req.body.expectedCompletionDate,
        issueAttachement: req.body.issueAttachement,
        Created_By:req.body.Created_By,
        othersVal:req.body.othersVal,
        draftFlag:req.body.draftFlag,

    };
    console.log('requestData',requestData);
    request.saveRequest(requestData)
        .then((data) => {
            
            buildResponse(res, 200, {
                error: false,
                 message: "Stored Successfully"
              });

          }).catch((err) => {
          buildResponse(res, 200, {
              error: true,
             message: "Fail to Store"
            });
        });
   
});

router.post('/saveDraftRequest',myLogger, function (req, res, next) {
    var request = new IPDashboard();
    var requestData = {
        issueTitle: req.body.issueTitle,
        issueDomain: req.body.domain,
        issueDescription: req.body.issueDescription,
        requestDate: req.body.requestDate,
        expectedCompletionDate: req.body.expectedCompletionDate,
        issueAttachement: req.body.issueAttachement,
        Created_By:req.body.Created_By,
        othersVal:req.body.othersVal,
        draftFlag:req.body.draftFlag,
    
    };
    request.saveDraftRequest(requestData)
        .then((data) => {
            
            buildResponse(res, 200, {
                error: false,
                 message: "Stored Successfully"
              });

          }).catch((err) => {
          buildResponse(res, 200, {
              error: true,
             message: "Fail to Store"
            });
        });
   
});

router.post('/UpdateRequest/:id',myLogger, function (req, res, next) {
    var reqId=req.params.id;
    var request = new IPDashboard();
    var requestData = {
        issueTitle: req.body.issueTitle,
        issueDomain: req.body.domain,
        issueDescription: req.body.issueDescription,
        requestDate: req.body.requestDate,
        expectedCompletionDate: req.body.expectedCompletionDate,
        issueAttachement: req.body.issueAttachement,
        Created_By:req.body.Created_By,
        othersVal:req.body.othersVal,
        draftFlag:req.body.draftFlag,
        FK_IP_ID:req.body.FK_IP_ID,
    };
    request.UpdateRequest(requestData,reqId)
        .then((data) => {
            buildResponse(res, 200, {
                error: false,
                 message: "Stored Successfully"
              });

          }).catch((err) => {
          buildResponse(res, 200, {
              error: true,
             message: "Fail to Store"
            });
        });
   
});
router.post('/UpdateDraftRequest/:id',myLogger, function (req, res, next) {
    var reqId=req.params.id;
    var request = new IPDashboard();
    var requestData = {
        issueTitle: req.body.issueTitle,
        issueDomain: req.body.domain,
        issueDescription: req.body.issueDescription,
        requestDate: req.body.requestDate,
        expectedCompletionDate: req.body.expectedCompletionDate,
        issueAttachement: req.body.issueAttachement,
        Created_By:req.body.Created_By,
        othersVal:req.body.othersVal,
        draftFlag:req.body.draftFlag,
        FK_IP_ID:req.body.FK_IP_ID,
    };
    request.UpdateDraftRequest(requestData,reqId)
        .then((data) => {
            buildResponse(res, 200, {
                error: false,
                 message: "Stored Successfully"
              });

          }).catch((err) => {
          buildResponse(res, 200, {
              error: true,
             message: "Fail to Store"
            });
        });
   
});

router.post('/saveRequestUpdates',myLogger, function (req, res, next) {
    var request = new IPDashboard();
    var requestData = {
        tantativeDate: req.body.tantativeDate,
        attachement: req.body.attachement,
        comment: req.body.comment,
        status: req.body.status,
       requestId:req.body.requestId,
       Created_By:req.body.Created_By,
       RequestI:req.body.RequestI,
    };
    request.saveRequestUpdates(requestData)
        .then((data) => {
            buildResponse(res, 200, {
                error: false,
                 message: "Stored Successfully"
              });
          }).catch((err) => {
          buildResponse(res, 200, {
              error: true,
             message: "Fail to Store"
            });
        });
   
});
router.post('/saveRequestUpdatesByRequestor',myLogger, function (req, res, next) {
    var request = new IPDashboard();
    var requestData = {
       
        attachement: req.body.attachement,
        comment: req.body.comment,
       
       requestId:req.body.requestId,
       Created_By:req.body.Created_By,
       RequestI:req.body.RequestI,
    };
    request.saveRequestUpdatesByRequestor(requestData)
        .then((data) => {
            buildResponse(res, 200, {
                error: false,
                 message: "Stored Successfully"
              });
          }).catch((err) => {
          buildResponse(res, 200, {
              error: true,
             message: "Fail to Store"
            });
        });
   
});



router.get('/deleteRequest/:id',myLogger, function (req, res, next) {
    var request = new IPDashboard();
    var reqData = req.params.id;
    request.deleteRequest(reqData)
        .then((userData) => {
            buildResponse(res, 200, {
                error: false,
                message: 'User Deleted successfully',
                UserData: userData
            });
        }).catch((err) => {
            buildResponse(res, 500, {
                error: false,
                message: 'Please Try Again',
            });
        });
});

router.get('/deleteDraftRequest/:id',myLogger, function (req, res, next) {
    var request = new IPDashboard();
    var reqData = req.params.id;
    request.deleteDraftRequest(reqData)
        .then((userData) => {
            buildResponse(res, 200, {
                error: false,
                message: 'User Deleted successfully',
                UserData: userData
            });
        }).catch((err) => {
            buildResponse(res, 500, {
                error: false,
                message: 'Please Try Again',
            });
        });
});


router.get('/getRequest/:id',myLogger, function (req, res, next) {

    var request = new IPDashboard();
    var reqData = req.params.id;
    request.getRequest(reqData)
        .then((requestData) => {
            buildResponse(res, 200, {
                error: false,
                message: 'Request Fetched successfully',
                requestData: requestData
            });
        }).catch((err) => {
            buildResponse(res, 404, {
                error: false,
                message: 'Failed to Fetch',
            });
        });
});
router.get('/getViewDataById/:id',myLogger, function (req, res, next) {

    var request = new IPDashboard();
    var reqData = req.params.id;
    request.getViewDataById(reqData)
        .then((requestData) => {
            buildResponse(res, 200, {
                error: false,
                message: 'Request Fetched successfully',
                requestData: requestData
            });
        }).catch((err) => {
            buildResponse(res, 404, {
                error: false,
                message: 'Failed to Fetch',
            });
        });
});

router.get('/getDataById/:id',myLogger, function (req, res, next) {

    var request = new IPDashboard();
    var reqData = req.params.id;
    request.getDataById(reqData)
        .then((requestData) => {
            buildResponse(res, 200, {
                error: false,
                message: 'Request Fetched successfully',
                requestData: requestData
            });
        }).catch((err) => {
            buildResponse(res, 404, {
                error: false,
                message: 'Failed to Fetch',
            });
        });
});

router.get('/getUpdates/:id',myLogger, function (req, res, next) {

    var request = new IPDashboard();
    var reqData = req.params.id;
    request.getUpdates(reqData)
        .then((requestData) => {
            buildResponse(res, 200, {
                error: false,
                message: 'Updates for request fetch successfully',
                requestData: requestData
            });
        }).catch((err) => {
            buildResponse(res, 404, {
                error: false,
                message: 'Failed to Fetch',
            });
        });
});
router.get('/getRequestUpdatesById/:id',myLogger, function (req, res, next) {

    var request = new IPDashboard();
    var reqData = req.params.id;
    request.getRequestUpdatesById(reqData)
        .then((requestData) => {
            buildResponse(res, 200, {
                error: false,
                message: 'Updates for request fetch successfully',
                requestData: requestData
            });
        }).catch((err) => {
            buildResponse(res, 404, {
                error: false,
                message: 'Failed to Fetch',
            });
        });
});
router.get('/getApproverRequestUpdatesById/:id',myLogger, function (req, res, next) {

    var request = new IPDashboard();
    var reqData = req.params.id;
    request.getApproverRequestUpdatesById(reqData)
        .then((requestData) => {
            buildResponse(res, 200, {
                error: false,
                message: 'Updates for request fetch successfully',
                requestData: requestData
            });
        }).catch((err) => {
            buildResponse(res, 404, {
                error: false,
                message: 'Failed to Fetch',
            });
        });
});

router.get('/getDomains',myLogger, function (req, res, next) {
    console.log("hhhhhhh");
    var request = new IPDashboard();
    request.getDomains().then((domains) => {
        buildResponse(res, 200, {
            error: false,
            message: 'Domains Fetched successfully',
            responseData: domains,
        });
    }).catch((err) => {
        buildResponse(res, 200, {
            error: true,
            message: err,
        });
    });
});

router.post('/sendRequestorEmail',myLogger, function (req, res, next) {
    
   var request = new IPDashboard();
	console.log("body=",req.body);
    request.sendRequestorEmail(req.body);
	 buildResponse(res, 200, {
            error: false,
            message: 'Email Intimation Notification Send Successfully'
        });
});


router.post('/sendApproverEmail',myLogger, function (req, res, next) {
    
   var request = new IPDashboard();
	console.log("body=app",req.body);
    request.sendApproverEmail(req.body);
	 buildResponse(res, 200, {
            error: false,
            message: 'Email Intimation Notification Send Successfully'
        });
});

router.post('/sendRequestorUpdateEmail',myLogger, function (req, res, next) {
    
    var request = new IPDashboard();
     console.log("body=app",req.body);
     request.sendRequestorUpdateEmail(req.body);
      buildResponse(res, 200, {
             error: false,
             message: 'Email Intimation Notification Send Successfully'
         });
 });

router.post('/sendEditRequestorEmail',myLogger, function (req, res, next) {
    
    var request = new IPDashboard();
     console.log("body=",req.body);
     request.sendEditRequestorEmail(req.body);
      buildResponse(res, 200, {
             error: false,
             message: 'Email Intimation Notification Send Successfully'
         });
 });


router.get('/getUsersEmail/:userId',myLogger, function (req, res, next) {
    var request = new IPDashboard();
    var reqData = req.params.userId;
    request.getUsersEmail(reqData)
        .then((userData) => {
            buildResponse(res, 200, {
                error: false,
                message: 'User Email found successfully',
                UserData: userData
            });
        }).catch((err) => {
            buildResponse(res, 404, {
                error: false,
                message: 'Please Try Again',
            });
        });
});

router.get('/getRequestDashboardTotalCount/:tableName/:userid',myLogger, function (req, res, next) {
    var request = new IPDashboard();
var tablenameval = req.params.tableName;
var userid= req.params.userid;

  var totalcount=0;

    request.getRequestDashboardTotalCount(tablenameval,userid).then((countData) => {
    request.getRequestDashboardTotalCount
        //console.log("countdata=",countData)
        buildResponse(res, 200, {
            error: false,
            message: 'Forms Fetched successfully',
            listData:countData,
            countData: countData.length
           
        });
    });

  //}

});


router.get('/getRequestDashboardPendingCount/:tableName/:columnname/:userid',myLogger, function (req, res, next) {
    var request = new IPDashboard();
    var tablenameval = req.params.tableName;
    var colomname=req.params.columnname;
    var userid= req.params.userid;
    
// var userid= req.params.userid;
// var group=req.params.group;
// var dept=req.params.dept;

console.log("tablenameval",tablenameval);
  var totalcount=0;
    request.getRequestDashboardPendingCount(tablenameval,colomname,userid).then((countData) => {

        buildResponse(res, 200, {
            error: false,
            message: 'Forms Fetched successfully',
            listData:countData,
            countData: countData.length
           
        });
    });



});


router.get('/getRequestDashboardCompletedCount/:tableName/:columnname/:userid',myLogger, function (req, res, next) {
    var request = new IPDashboard();
    var tablenameval = req.params.tableName;
    var colomname=req.params.columnname;
    var userid= req.params.userid;
  

  var totalcount=0;
    request.getRequestDashboardCompletedCount(tablenameval,colomname,userid).then((countData) => {

        buildResponse(res, 200, {
            error: false,
            message: 'Forms Fetched successfully',
            listData:countData,
            countData: countData.length
         
        });
    });



});


router.get('/getRequestDashboardRejectedCount/:tableName/:columnname/:userid',myLogger, function (req, res, next) {
    var request = new IPDashboard();
    var tablenameval = req.params.tableName;
    var colomname=req.params.columnname;
    var userid= req.params.userid;
    var totalcount = 0;
    request.getRequestDashboardRejectedCount(tablenameval,colomname,userid).then((countData) => {
        buildResponse(res, 200, {
            error: false,
            message: 'Forms Fetched successfully',
            listData: countData,
            countData: countData.length
        });
    });

});
router.get('/getRequestDashboardDraftedCount/:tableName/:columnname/:userid',myLogger, function (req, res, next) {
    var request = new IPDashboard();
    var tablenameval = req.params.tableName;
    var colomname=req.params.columnname;
    var userid= req.params.userid;
    var totalcount = 0;
    request.getRequestDashboardDraftedCount(tablenameval,colomname,userid).then((countData) => {
        buildResponse(res, 200, {
            error: false,
            message: 'Forms Fetched successfully',
            listData: countData,
            countData: countData.length
        });
    });

});


router.get('/getRequestListDashboardTotalCount/:tableName/:userid',myLogger, function (req, res, next) {
    var request = new IPDashboard();
var tablenameval = req.params.tableName;
var userid= req.params.userid;

  var totalcount=0;

    request.getRequestListDashboardTotalCount(tablenameval,userid).then((countData) => {
    request.getRequestListDashboardTotalCount;
        buildResponse(res, 200, {
            error: false,
            message: 'Forms Fetched successfully',
            listData:countData,
            countData: countData.length
        });
    });
});

router.get('/getDashboardListTotalCount/:tableName/:userid',myLogger, function (req, res, next) {
    var request = new IPDashboard();
var tablenameval = req.params.tableName;
var userid= req.params.userid;
  var totalcount=0;
    request.getDashboardListTotalCount(tablenameval,userid).then((countData) => {
    request.getDashboardListTotalCount
        buildResponse(res, 200, {
            error: false,
            message: 'Forms Fetched successfully',
            listData:countData,
            countData: countData.length
        });
    });
});
router.get('/getRequestForViewById/:userid',myLogger, function (req, res, next) {
    var request = new IPDashboard();
//var tablenameval = req.params.tableName;
var userid= req.params.userid;
  var totalcount=0;
    request.getRequestForViewById(userid).then((countData) => {
    
        buildResponse(res, 200, {
            error: false,
            message: 'Query Fetched successfully',
            listData:countData,
            countData: countData.length
        });
    });
});



function buildResponse(res, statusCode, responseData) {
    res.status(statusCode).json(responseData);
}

module.exports = router;
