const Sequelize = require('sequelize');
var con = require('./connection');
const Op = Sequelize.Op;
var MyLogModel = require('../model/myLog');
var log = new MyLogModel();
Sequelize.DATE.prototype._stringify = function _stringify(date, options) {
    return this._applyTimezone(date, options).format('YYYY-MM-DD HH:mm:ss.SSS');
  };
module.exports = class Application {
    constructor() {
        this.APPLICATION = con.define('M_Meeting_Master', {

            T_INT_Meeting_Id: {
                type: Sequelize.INTEGER,
                primaryKey: true,
                autoIncrement: true
            },
            T_INT_Type_Of_Meeting: {
                type: Sequelize.INTEGER,
                allowNull: false,
               
            },
            T_CHR_Short_Name: {
                type: Sequelize.STRING(50),
                allowNull: false,
                unique: true
            },
            T_CHR_Full_Name: {
                type: Sequelize.STRING(50),
                allowNull: false,
                unique: true
            },
            T_CHR_Sales_Type: {
                type: Sequelize.STRING(10),
                allowNull: false
            },
            T_CHR_Meeting_Option: {
                type: Sequelize.STRING(10),
                allowNull: false
            },
            T_INT_Frequency: {
                type: Sequelize.INTEGER,
                allowNull: false
            },
            T_CHR_Channel_Deprtment: {
                type: Sequelize.STRING(50),
                allowNull: false
            },
            T_CHR_Meeting_Chair: {
                type: Sequelize.STRING(50),
                allowNull: false
            },
            T_CHR_Meeting_Attendies: {
                type: Sequelize.STRING(50),
                allowNull: false
            },
            T_CHR_Governing_Body: {
                type: Sequelize.STRING(50),
                allowNull: false
            },
           
            T_INT_Convener_Id: {
                type: Sequelize.STRING(20),
                allowNull: false
            },
            T_CHR_Status:{
                type: Sequelize.STRING(10),
                allowNull: true
            },
            T_DATE_Meeting_From_Date: {
                type: Sequelize.DATE,
                allowNull: false
            },
            T_DATE_Meeting_To_Date: {
                type: Sequelize.DATE,
                allowNull: false
            },

            T_CHR_Isdelete: {
                type: Sequelize.STRING(20),
                allowNull: true,
                defaultValue:'0'
            },

            T_CHR_Created_By: {
                type: Sequelize.STRING(20),
                allowNull: true
            },
            T_DATE_Created_Date_Time:{
                type: Sequelize.DATE,
                allowNull: true
            },
            T_CHR_Updated_By: {
                type: Sequelize.STRING(20),
                allowNull: true
            },
            T_DATE_Updated_Date_Time:{
                type: Sequelize.DATE,
                allowNull: true
            },
            T_CHR_deleted_By: {
                type: Sequelize.STRING(20),
                allowNull: true
            },
          
            T_DATE_Deleted_Date_Time: {
                type: Sequelize.DATE,
                allowNull: true
            },
            database_connection: {
                type: Sequelize.STRING,
                allowNull: true
            },
            
        });
    }
    saveApplication(applicationData) {
        console.log("++++++",applicationData)
        return sequelize.sync().then(() => {
            return this.APPLICATION.create({
        database_connection:applicationData.database_connection,
        T_INT_Type_Of_Meeting: applicationData.type_of_meeting,
        T_CHR_Short_Name: applicationData.short_name,
        T_CHR_Full_Name: applicationData.full_name,
        T_CHR_Sales_Type: applicationData.type_sales,
        T_CHR_Meeting_Option: applicationData.type_meeting,
        T_INT_Frequency: applicationData.frequency,
        T_CHR_Channel_Deprtment:applicationData.department.toString(),
        T_CHR_Meeting_Chair:applicationData.chair.toString(),
        T_CHR_Meeting_Attendies:applicationData.Attendees.toString(),
        T_CHR_Governing_Body:applicationData.governing_body.toString(),
        T_CHR_Status:'Enable',
        T_INT_Convener_Id: applicationData.convener_name,
        T_DATE_Meeting_From_Date: applicationData.effective_start_Date,
        T_DATE_Meeting_To_Date: applicationData.effective_end_Date,
        T_CHR_Created_By: applicationData.userId

            }).catch(function(errLog) {
              
                log.addLog('model :: application.js ' + errLog);
                var err;
                if (errLog == "SequelizeUniqueConstraintError: Validation error") {
         

                    err = {
                        data: 0,
                        errMsg: "Short name and full name should be unique"
                    }
                } else if (errLog != "SequelizeUniqueConstraintError: Validation error") {
                

                    err = {
                        data: 0,
                        errMsg: errLog["errors"][0]["message"]
                    }

                }
                return err;
            });
        }).catch(function(errLog) {

            
            log.addLog('model :: application.js ' + errLog);
        });
    }

    
    getApplications() {

        
        return sequelize.sync().then(() => {
            return sequelize.query("select mm.T_INT_Meeting_Id,mt.T_CHR_Type_Of_Meeting_Name,mm.T_CHR_Short_Name,mm.T_CHR_Full_Name,mf.T_CHR_Frequency_Name,"+
            " mm.T_CHR_Channel_Deprtment,mm.T_CHR_Governing_Body,mm.T_CHR_Channel_Deprtment from M_Meeting_Masters as mm inner join M_Type_Of_Meeting_Master as mt  "+
            " on mm.T_INT_Type_Of_Meeting=mt.T_INT_Type_Of_Meeting_Id inner join M_Frequency_Masters as mf "+
            " on mm.T_INT_Frequency = mf.T_INT_Frequency_Id where mm.T_CHR_Isdelete='0' ", { type: sequelize.QueryTypes.SELECT }); 
        });
    }

   
    getApplicationById(id) {
        return sequelize.sync().then(() => {
            return this.APPLICATION.findOne({
                where: {
                    T_INT_Meeting_Id: id
                },
            }).catch(errLog=>{
                log.addLog('model :: application.js ' + errLog);
                buildResponse(res, 500, {
                    error: true,
                    message: "server error"
                });

            })
        });
    }
    
    updateApplication(applicationData) {
        console.log("this update meeting ",applicationData);
        return sequelize.sync().then(() => {
            return this.APPLICATION.update({
                database_connection:applicationData.database_connection,
                T_INT_Type_Of_Meeting: applicationData.type_of_meeting,
                T_CHR_Short_Name: applicationData.short_name,
                T_CHR_Full_Name: applicationData.full_name,
                T_CHR_Sales_Type: applicationData.type_sales,
                T_CHR_Meeting_Option: applicationData.type_meeting,
                T_INT_Frequency: applicationData.frequency,
                T_CHR_Channel_Deprtment:applicationData.department.toString(),
                T_CHR_Meeting_Chair:applicationData.chair.toString(),
                T_CHR_Meeting_Attendies:applicationData.Attendees.toString(),
                T_CHR_Governing_Body:applicationData.governing_body.toString(),
                T_CHR_Status:applicationData.status,
                T_INT_Convener_Id: applicationData.convener_name,
                T_DATE_Meeting_From_Date: applicationData.effective_start_Date,
                T_DATE_Meeting_To_Date: applicationData.effective_end_Date,
                T_CHR_Created_By: applicationData.userId
            }, {
                where: {
                    T_INT_Meeting_Id: applicationData.meetingId
                }
            }).then((d)=>{
                return d;
            }).catch(function(err) {
                console.log("this for Validation meeting",err);

                if(err=="SequelizeUniqueConstraintError: Validation error"){
                    return 0;
                }   
            });
        }).catch(function(err) {
            console.log("this for err app meeting",err);
            return err;
        });
    }
    
    deleteApplication(applicationId) {
        return sequelize.sync().then(() => {
            return this.APPLICATION.update({
                T_CHR_Isdelete :'1'

            },
            {
                where: {
                    T_INT_Meeting_Id: applicationId
                }
            });
               
        });
    }
    
    getApplication(appId) {
        return sequelize.sync().then(() => {
            return this.APPLICATION.findAll({
                where: { 
                    app_id: {
                            [Op.or]: appId
                          }
                    }
                    
            });
        });
    }

    getMeetingDetails(data){
        var d = new Date();
        return sequelize.sync().then(() => {
            return sequelize.query("select top 50 mm.T_INT_Meeting_Id,mm.T_CHR_Short_Name,mm.T_CHR_Full_Name,mm.T_INT_Frequency,mm.T_CHR_Meeting_Attendies"+
            "  from M_Meeting_Masters as mm  inner join M_Frequency_Masters as mf on mm.T_INT_Frequency=mf.T_INT_Frequency_Id "+
            "  where mm.T_CHR_Isdelete='0' and mm.T_CHR_Status='Enable' and mf.T_CHR_Frequency_Status='Enable' and mm.T_INT_Convener_Id='"+data.userId+"' and mm.T_DATE_Meeting_From_Date <= '" + d.toLocaleDateString("en-US") + "' and  mm.T_DATE_Meeting_To_Date  >= '" + d.toLocaleDateString("en-US") + "'", { type: sequelize.QueryTypes.SELECT }); 
        });
    }

    
    getMeetingViewDetails(data){
        var d = new Date();
        return sequelize.sync().then(() => {
            return sequelize.query("SELECT tm.T_INT_Schedule_Meeting_Id "+
            " ,tm.T_INT_Meeting_Id "+
            " ,tm.T_INT_Meeting_Template_Id "+
            " ,tm.T_CHR_For_Month_Or_Quoarter "+
            " ,tm.T_DATE_Meeting_Date "+
            " ,tm.T_CHR_Meeting_Start_Time "+
            " ,tm.T_CHR_Meeting_End_Time "+
            " ,tm.T_CHR_Location "+
            " ,tm.T_CHR_Invitees "+
            " ,tm.T_CHR_Description "+
            " ,mt.T_CHR_Template_Name "+
            "  ,mm.T_CHR_Meeting_Attendies, "+
           " mm.T_CHR_Short_Name, "+
          "  mm.T_CHR_Full_Name,tmm.T_CHR_Type_Of_Meeting_Name,mt.T_CHR_Version,mm.T_INT_Convener_Id,mm.T_CHR_Meeting_Chair,u1.T_CHR_Name as convenerName "+
        " FROM TXN_Schedule_Meetings as tm inner join Meeting_Templates as mt on tm.T_INT_Meeting_Template_Id=mt.form_id "+
        " inner join M_Meeting_Masters as mm on mm.T_INT_Meeting_Id=tm.T_INT_Meeting_Id "+
        " inner join M_Type_Of_Meeting_Master as tmm on tmm.T_INT_Type_Of_Meeting_Id=mm.T_INT_Type_Of_Meeting "+
        " inner join M_User_Masters as u1 on u1.T_CHR_Employee_Id=mm.T_INT_Convener_Id "+
        "  where tm.T_INT_Schedule_Meeting_Id='"+data.scheduleId+"'", { type: sequelize.QueryTypes.SELECT }); 
        }).catch((err)=>{
            console.log("eeeeeeee=====",err)
        });
    }

    getMeetingDetailsForMeetingSchedule(data){
        var d = new Date();
        if (data.role == '2') {
            if (data.type == 'meetings') {
                return sequelize.sync().then(() => {
                    return sequelize.query("select top 50 mm.T_INT_Meeting_Id,mm.T_CHR_Short_Name,mm.T_CHR_Full_Name,mm.T_INT_Frequency,mm.T_CHR_Meeting_Attendies"+
                    "  from M_Meeting_Masters as mm  inner join M_Frequency_Masters as mf on mm.T_INT_Frequency=mf.T_INT_Frequency_Id "+
                    "  where mm.T_CHR_Isdelete='0' and mm.T_CHR_Status='Enable' and mf.T_CHR_Frequency_Status='Enable' and mm.T_INT_Convener_Id='"+data.userId+"' and mm.T_DATE_Meeting_From_Date <= '" + d.toLocaleDateString("en-US") + "' and  mm.T_DATE_Meeting_To_Date  >= '" + d.toLocaleDateString("en-US") + "' ", { type: sequelize.QueryTypes.SELECT });  
                });
            
            }else{
                return sequelize.sync().then(() => {
                    return sequelize.query("select top 50 mm.T_INT_Meeting_Id,mm.T_CHR_Short_Name,mm.T_CHR_Full_Name,mm.T_INT_Frequency,mm.T_CHR_Meeting_Attendies"+
                    "  from M_Meeting_Masters as mm  inner join M_Frequency_Masters as mf on mm.T_INT_Frequency=mf.T_INT_Frequency_Id "+
                    "  where mm.T_CHR_Isdelete='0' and mm.T_CHR_Status='Enable' and mf.T_CHR_Frequency_Status='Enable'  and mm.T_DATE_Meeting_From_Date <= '" + d.toLocaleDateString("en-US") + "' and  mm.T_DATE_Meeting_To_Date  >= '" + d.toLocaleDateString("en-US") + "' "+
                    " and ((mm.T_CHR_Meeting_Chair='" + data.userId + "'" +

                    " OR mm.T_CHR_Meeting_Chair LIKE '" + data.userId + "' + ',%' " +
                    " OR mm.T_CHR_Meeting_Chair LIKE '%,' + '" + data.userId + "' + ',%' " +
                    "  OR mm.T_CHR_Meeting_Chair LIKE '%,' + '" + data.userId + "') or (mm.T_CHR_Meeting_Attendies='" + data.userId + "' " +

                    "  OR mm.T_CHR_Meeting_Attendies LIKE '" + data.userId + "' + ',%' " +
                    " OR mm.T_CHR_Meeting_Attendies LIKE '%,' + '" + data.userId + "' + ',%' " +
                    " OR mm.T_CHR_Meeting_Attendies LIKE '%,' + '" + data.userId + "'))  ", { type: sequelize.QueryTypes.SELECT });  
                });
            }
        
      
    }else if(data.role=='3'){
        return sequelize.sync().then(() => {
            return sequelize.query("select top 50 mm.T_INT_Meeting_Id,mm.T_CHR_Short_Name,mm.T_CHR_Full_Name,mm.T_INT_Frequency,mm.T_CHR_Meeting_Attendies"+
            "  from M_Meeting_Masters as mm  inner join M_Frequency_Masters as mf on mm.T_INT_Frequency=mf.T_INT_Frequency_Id "+
            "  where mm.T_CHR_Isdelete='0' and mm.T_CHR_Status='Enable' and mf.T_CHR_Frequency_Status='Enable'  and mm.T_DATE_Meeting_From_Date <= '" + d.toLocaleDateString("en-US") + "' and  mm.T_DATE_Meeting_To_Date  >= '" + d.toLocaleDateString("en-US") + "' ", { type: sequelize.QueryTypes.SELECT });  
        });
    }
    }


    getMeetingType(data){
                return sequelize.sync().then(() => {
                    return sequelize.query("select T_INT_Type_Of_Meeting_Id,T_CHR_Type_Of_Meeting_Name from dbo.M_Type_Of_Meeting_Master", { type: sequelize.QueryTypes.SELECT });  
                });
            
    }
    getFilterMeetingDetails(data){
        return sequelize.sync().then(() => {
            return sequelize.query("select top 50 mm.T_INT_Meeting_Id,mm.T_CHR_Short_Name,mm.T_CHR_Full_Name,mm.T_INT_Frequency,mm.T_CHR_Meeting_Attendies"+
            "  from M_Meeting_Masters as mm  inner join M_Frequency_Masters as mf on mm.T_INT_Frequency=mf.T_INT_Frequency_Id "+
            "  where mm.T_CHR_Isdelete='0' and mm.T_CHR_Status='Enable' and mf.T_CHR_Frequency_Status='Enable' and mm.T_INT_Convener_Id='"+data.userId+"' and ( mm.T_CHR_Short_Name like '%"+data.value+"%' or mm.T_CHR_Full_Name like '%"+data.value+"%')", { type: sequelize.QueryTypes.SELECT }); 
        });
    }
};