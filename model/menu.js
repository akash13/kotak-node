const Sequelize = require('sequelize');
var con = require('./connection');
var randomstring = require("randomstring");
var bcrypt = require('bcryptjs');
var MyLogModel = require('../model/myLog');
var log = new MyLogModel();
const Op = Sequelize.Op;
var errMsg = "Server issue please try after sometime";
module.exports = class menu_master {

    constructor() {


        this.menu_master = con.define('menu_master', {
            Txn_ID: {
                type: Sequelize.INTEGER,
                primaryKey: true,
                autoIncrement: true
            },

            Added_By: {
                type: Sequelize.STRING(20),
                allowNull: true

            },
            Create_Date: {
                type: Sequelize.DATE,
                allowNull: true

            },

            Modified_By: {
                type: Sequelize.STRING(20),
                allowNull: true
            },

            Modified_Date: {
                type: Sequelize.DATE,
                allowNull: true
            },
            Menu_ID: {
                type: Sequelize.INTEGER,
                allowNull: true,
                primaryKey: true,
            },
            Menu_Name: {
                type: Sequelize.STRING(100),
                allowNull: true
            },
            Menu_Display_Name: {
                type: Sequelize.STRING(100),
                allowNull: false
            },
            Parent_Menu_ID: {
                type: Sequelize.INTEGER,
                allowNull: true
            },
            App_Menu_Available: {
                type: Sequelize.STRING(1),
                allowNull: true
            },
            Web_Menu_Available: {
                type: Sequelize.STRING(1),
                allowNull: true
            },
            Default_Access_App: {
                type: Sequelize.STRING(1),
                allowNull: true
            },
            Default_Access_Web: {
                type: Sequelize.STRING(1),
                allowNull: true
            },
            App_Menu_Status: {
                type: Sequelize.STRING(1),
                allowNull: true
            },
            Web_Menu_Status: {
                type: Sequelize.STRING(1),
                allowNull: true
            },

            RedirectURL: {
                type: Sequelize.STRING(500),
                allowNull: true
            },


        });
    }

    saveMenu(menuData) {
        return sequelize.sync().then(() => {
            return this.menu_master.create({

                Added_By: menuData.Added_By,
                Create_Date: menuData.Create_Date,
                Menu_ID: menuData.Menu_ID,
                Menu_Name: menuData.Menu_Name,
                Menu_Display_Name: menuData.Menu_Display_Name,
                Parent_Menu_ID: menuData.Parent_Menu_ID,
                App_Menu_Available: menuData.App_Menu_Available,
                Web_Menu_Available: menuData.Web_Menu_Available,
                Default_Access_App: menuData.Default_Access_App,
                Default_Access_Web: menuData.Default_Access_Web,
                App_Menu_Status: menuData.App_Menu_Status,
                Web_Menu_Status: menuData.Web_Menu_Status,
                RedirectURL: menuData.RedirectURL,

            }).catch(function (errLog) {
                console.log("errMsg:", errLog)
                log.addLog('model :: menu.js ' + errLog);
                var err;
                if (errLog == "SequelizeUniqueConstraintError: Validation error") {
                    err = {
                        data: 0,
                        errMsg: "menu name should be unique"
                    }
                } else if (errLog != "SequelizeUniqueConstraintError: Validation error") {
                    err = {
                        data: 0,
                        errMsg: errLog["errors"][0]["message"]
                    }

                }
                return err;
            });
        }).catch(function (errLog) {
            log.addLog('model :: menu.js ' + errLog);
        });
    }

    updateMenu(menuData) {
        return sequelize.sync().then(() => {
            return this.menu_master.update({
               
              
                Modified_By: menuData.Modified_By,
                Modified_Date: menuData.Modified_Date,
                Menu_ID: menuData.Menu_ID,
                Menu_Name: menuData.Menu_Name,
                Menu_Display_Name: menuData.Menu_Display_Name,
                Parent_Menu_ID: menuData.Parent_Menu_ID,
                App_Menu_Available: menuData.App_Menu_Available,
                Web_Menu_Available: menuData.Web_Menu_Available,
                Default_Access_App: menuData.Default_Access_App,
                Default_Access_Web: menuData.Default_Access_Web,
                App_Menu_Status: menuData.App_Menu_Status,
                Web_Menu_Status: menuData.Web_Menu_Status,
                RedirectURL: menuData.RedirectURL,


            }, {
                    where: {
                        Txn_ID: menuData.Txn_ID
                    }
                }).catch(function (errLog) {
                    console.log("errMsg:", errLog)
                    log.addLog('model :: menu.js ' + errLog);
                    var err;
                    if (errLog == "SequelizeUniqueConstraintError: Validation error") {
                        err = {
                            data: 0,
                            errMsg: "menu name should be unique"
                        }
                    } else if (errLog != "SequelizeUniqueConstraintError: Validation error") {
                        err = {
                            data: 0,
                            errMsg: errLog["errors"][0]["message"]
                        }

                    }
                    return err;
                });
        }).catch(function (errLog) {
            log.addLog('model :: menu.js ' + errLog);
        });
    }


    getMenu() {
        return sequelize.sync().then(() => {
            return this.menu_master.all().then((data) => {
                return data
            }).catch((errLog) => {
                var err = {
                    data: 0,
                    errMsg: errMsg
                }
                log.addLog('model :: menu.js' + errLog);
                return err;

            });
        }).catch((errLog) => {
            var err = {
                data: 0,
                errMsg: errMsg
            }
            log.addLog('model :: menu.js ' + errLog);
            return err;
        });
    }

    getMenuById(id) {
        return sequelize.sync().then(() => {
            return this.menu_master.findOne({
                where: {
                    Txn_ID: id
                }
            }).then((data) => {
                return data;
            }).catch((errLog) => {
                var err = {
                    data: 0,
                    errMsg: errMsg
                }
                log.addLog('model :: menu.js' + errLog);
                return err;

            });
        }).catch((errLog) => {
            var err = {
                data: 0,
                errMsg: errMsg
            }
            log.addLog('model :: menu.js ' + errLog);
            return err;
        });
    }

};