const Sequelize = require('sequelize');
var con = require('./connection');
var randomstring = require("randomstring");
var bcrypt = require('bcryptjs');
var MyLogModel = require('../model/myLog');
var log = new MyLogModel();
const Op = Sequelize.Op;
var errMsg = "Server issue please try after sometime";
module.exports = class Group_Menu_Mapping {

    constructor() {


        this.Group_Menu_Mapping = con.define('Group_Menu_Mapping', {
            Txn_ID: {
                type: Sequelize.INTEGER,
                primaryKey: true,
                autoIncrement: true
            },

            Added_By: {
                type: Sequelize.STRING(20),
                allowNull: true

            },
            Create_Date: {
                type: Sequelize.DATE,
                allowNull: true

            },

            Modified_By: {
                type: Sequelize.STRING(20),
                allowNull: true
            },

            Modified_Date: {
                type: Sequelize.DATE,
                allowNull: true
            },
            Group_ID: {
                type: Sequelize.INTEGER,
                allowNull: true,
                primaryKey: true,
            },
            Menu_ID: {
                type: Sequelize.INTEGER,
                allowNull: true,
                primaryKey: true,
            },
            Grp_Menu_Status: {
                type: Sequelize.STRING(1),
                allowNull: true
            },
            


        });
    }

    savegroupMenuMapping(groupMenuMappingData) {
        return sequelize.sync().then(() => {
            return this.Group_Menu_Mapping.create({

                Added_By: groupMenuMappingData.Added_By,
                Create_Date: groupMenuMappingData.Create_Date,
                Menu_ID: groupMenuMappingData.Menu_ID,
                Group_ID: groupMenuMappingData.Group_ID,
                Grp_Menu_Status: groupMenuMappingData.Grp_Menu_Status,

            }).catch(function (errLog) {
                console.log("errMsg:", errLog)
                log.addLog('model :: Group_Menu_Mapping.js ' + errLog);
                var err;
                if (errLog == "SequelizeUniqueConstraintError: Validation error") {
                    err = {
                        data: 0,
                        errMsg: "menu name should be unique"
                    }
                } else if (errLog != "SequelizeUniqueConstraintError: Validation error") {
                    err = {
                        data: 0,
                        errMsg: errLog["errors"][0]["message"]
                    }

                }
                return err;
            });
        }).catch(function (errLog) {
            console.log("errMsg:", errLog)
            log.addLog('model :: Group_Menu_Mapping.js ' + errLog);
        });
    }

    updateGroupMenuMapping(groupMenuMappingData) {
        return sequelize.sync().then(() => {
            return this.Group_Menu_Mapping.update({
               
              
                Modified_By: groupMenuMappingData.Modified_By,
                Modified_Date: groupMenuMappingData.Modified_Date,
                Menu_ID: groupMenuMappingData.Menu_ID,
                Group_ID: groupMenuMappingData.Group_ID,
                Grp_Menu_Status: groupMenuMappingData.Grp_Menu_Status,


            }, {
                    where: {
                        Txn_ID: groupMenuMappingData.Txn_ID
                    }
                }).catch(function (errLog) {
                    console.log("errMsg:", errLog)
                    log.addLog('model :: Group_Menu_Mapping.js ' + errLog);
                    var err;
                    if (errLog == "SequelizeUniqueConstraintError: Validation error") {
                        err = {
                            data: 0,
                            errMsg: "menu name should be unique"
                        }
                    } else if (errLog != "SequelizeUniqueConstraintError: Validation error") {
                        err = {
                            data: 0,
                            errMsg: errLog["errors"][0]["message"]
                        }

                    }
                    return err;
                });
        }).catch(function (errLog) {
            log.addLog('model :: Group_Menu_Mapping.js ' + errLog);
        });
    }


    getGroupMenuMapping() {
        return sequelize.sync().then(() => {
            return this.Group_Menu_Mapping.all().then((data) => {
                return data
            }).catch((errLog) => {
                var err = {
                    data: 0,
                    errMsg: errMsg
                }
                log.addLog('model :: Group_Menu_Mapping.js' + errLog);
                return err;

            });
        }).catch((errLog) => {
            var err = {
                data: 0,
                errMsg: errMsg
            }
            log.addLog('model :: Group_Menu_Mapping.js ' + errLog);
            return err;
        });
    }

    getGroupMenuMappingById(id) {
        return sequelize.sync().then(() => {
            return this.Group_Menu_Mapping.findOne({
                where: {
                    Txn_ID: id
                }
            }).then((data) => {
                return data;
            }).catch((errLog) => {
                var err = {
                    data: 0,
                    errMsg: errMsg
                }
                log.addLog('model :: Group_Menu_Mapping.js' + errLog);
                return err;

            });
        }).catch((errLog) => {
            var err = {
                data: 0,
                errMsg: errMsg
            }
            log.addLog('model :: Group_Menu_Mapping.js ' + errLog);
            return err;
        });
    }

};