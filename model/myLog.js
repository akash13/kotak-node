const SimpleNodeLogger = require('simple-node-logger'),
    opts = {
        logFilePath:'mylogfile.log',
        timestampFormat:'YYYY-MM-DD HH:mm:ss.SSS'
    },
log = SimpleNodeLogger.createSimpleLogger( opts );

module.exports = class MyLogModel {


    addLog(err){
        log.info(' About  ', err, ' accepted at ', new Date().toJSON());
    }
}