import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import {routing} from "./app.routing";
import { AppComponent } from "./app.component";
import {FormsModule} from "@angular/forms";
import {HTTP_INTERCEPTORS, HttpClientModule} from "@angular/common/http";
import {AuthService} from "./auth/auth.service";
import {EmployeeService} from "./auth/employee.service";
import {AuthInterceptor} from "./auth/auth.interceptor";
import { FormComponent } from './form/form.component';
import { FormAPIComponent } from './formAPI/formapi.component';
import { FormService } from './auth/form.service';

@NgModule({
    declarations: [
        AppComponent,
        FormComponent,
        FormAPIComponent,
    ],
    imports: [BrowserModule,
        FormsModule,
        HttpClientModule,
        routing],
    bootstrap: [AppComponent],
    providers: [FormService]
})
export class AppModule {

}