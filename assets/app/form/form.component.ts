import {Component, ElementRef, ViewChild} from '@angular/core';
import {NgForm} from "@angular/forms";
import {FormService} from "../auth/form.service";
import {Router} from "@angular/router";

@Component({
    selector: 'app-form',
    templateUrl: './form.component.html'
})
export class FormComponent {
   title ="formComponent";
   @ViewChild('f') formFORM: NgForm;
   errorMsg: String = "";

    constructor(private formService: FormService, private router: Router) {}

onSubmit() {
    this.formService.saveForm({
        'formName': this.formFORM.value.formname,
        'formJSON': this.formFORM.value.formjsonstring,
    }).subscribe(
        data => {
            if(data["error"]){
                this.errorMsg = data["message"];
            }  else {
                this.router.navigate(['/formapi']);
            }
        },
        error => this.errorMsg = "Please try again!"
    )
}


}

