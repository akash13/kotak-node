const Sequelize = require('sequelize');
var con = require('./connection');
var randomstring = require("randomstring");
var bcrypt = require('bcryptjs');
var MyLogModel = require('../model/myLog');
var log = new MyLogModel();
var EmailNotification = require('../model/emailNotification');

const Op = Sequelize.Op;
var errMsg = "Server issue please try after sometime";
Sequelize.DATE.prototype._stringify = function _stringify(date, options) {
    return this._applyTimezone(date, options).format('YYYY-MM-DD HH:mm:ss.SSS');

};
module.exports = class Department {

    constructor() {


        this.MEETINGTASK = con.define('meeting_task_master', {
            meeting_task_id: {
                type: Sequelize.INTEGER,
                primaryKey: true,
                autoIncrement: true
            },
            meeting_schedule_id: {
                type: Sequelize.INTEGER,
                allowNull: false,
            },
            task_name: {
                type: Sequelize.STRING(255),
                allowNull: false
            },
            priority: {
                type: Sequelize.STRING(255),
                allowNull: false
            },
            activity_status: {
                type: Sequelize.STRING(10),
                allowNull: false
            },
            plan_start_date: {
                type: Sequelize.DATE,
                allowNull: false
            },
            plan_end_date: {
                type: Sequelize.DATE,
                allowNull: false
            },
            assigned_by: {
                type: Sequelize.INTEGER,
                allowNull: false
            },
            assigned_to: {
                type: Sequelize.INTEGER,
                allowNull: false
            },
            actual_start_date: {
                type: Sequelize.DATE,
                allowNull: true
            },
            actual_end_date: {
                type: Sequelize.DATE,
                allowNull: true
            },
            efford_hours: {
                type: Sequelize.INTEGER,
                allowNull: true
            },
            activity_description: {
                type: Sequelize.STRING(255),
                allowNull: true
            },
            is_delete: {
                type: Sequelize.STRING(2),
                allowNull: false,
                defaultValue: '0'
            },
            deleted_by: {
                type: Sequelize.STRING(20),
                allowNull: true
            },
            deleted_date_time: {
                type: Sequelize.DATE,
                allowNull: true
            },
        },
            {
                timestamps: false,
                createdAt: false,
                UpdatedAt: false,
            }
        );
        this.MEETINGTASKLOGPROGRESS = con.define('TXN_Task_Progress', {
            T_INT_Log_Progress_Id: {
                type: Sequelize.INTEGER,
                primaryKey: true,
                autoIncrement: true
            },
            T_INT_Meeting_Task_Id: {
                type: Sequelize.INTEGER,
                allowNull: false,
            },
            T_INT_Schedule_Meeting_Id: {
                type: Sequelize.INTEGER,
                allowNull: false
            },
            T_CHR_Hours: {
                type: Sequelize.STRING(10),
                allowNull: false
            },
            T_CHR_Log_Activity_Status: {
                type: Sequelize.STRING(10),
                allowNull: false
            },
            T_DATE_Actual_Start_Date: {
                type: Sequelize.DATE,
                allowNull: true
            },
            T_DATE_Actual_End_Date: {
                type: Sequelize.DATE,
                allowNull: true
            },

            T_INT_Log_Completion_Percentage: {
                type: Sequelize.INTEGER,
                allowNull: false
            },
            T_CHR_Log_Comment: {
                type: Sequelize.TEXT,
                allowNull: false
            },
            T_CHR_Created_By: {
                type: Sequelize.STRING(20),
                allowNull: false
            },
            T_DATE_Created_Date_Time: {
                type: Sequelize.DATE,
                allowNull: true
            },
            T_CHR_Updated_By: {
                type: Sequelize.STRING(20),
                allowNull: true
            },
            T_DATE_Updated_Date_Time: {
                type: Sequelize.DATE,
                allowNull: true
            },
            T_CHR_Deleted_By: {
                type: Sequelize.STRING(20),
                allowNull: true,
            },
            T_DATE_Deleted_Date_Time: {
                type: Sequelize.DATE,
                allowNull: true
            },
            T_CHR_Isdelete: {
                type: Sequelize.STRING(2),
                allowNull: true
            },
        },
            {
                timestamps: false,
                createdAt: false,
                UpdatedAt: false,
            }
        );

        this.REVISEDHISTORY = con.define('T_Revised_History', {
            T_INT_Task_Revised_History_Id: {
                type: Sequelize.INTEGER,
                primaryKey: true,
                autoIncrement: true
            },
            T_INT_Meeting_Task_Id: {
                type: Sequelize.INTEGER,
                allowNull: false,
            },
            T_INT_Schedule_Meeting_Id: {
                type: Sequelize.INTEGER,
                allowNull: false
            },
            T_Date_Revised_Date: {
                type: Sequelize.STRING,
                allowNull: false
            },
            T_CHR_Revised_Comment: {
                type: Sequelize.STRING,
                allowNull: false
            },

            T_CHR_Revised_By: {
                type: Sequelize.STRING(20),
                allowNull: false
            },
            T_Date_Revised_Date_Time: {
                type: Sequelize.DATE,
                allowNull: true
            },

        },
            {
                timestamps: false,
                createdAt: false,
                UpdatedAt: false,
            }
        );

        this.ESCALATENOTIFYTO = con.define('T_Escalate_Notify_To', {
            T_INT_Escalate_Notify_To: {
                type: Sequelize.INTEGER,
                primaryKey: true,
                autoIncrement: true
            },
            T_INT_Meeting_Task_Id: {
                type: Sequelize.INTEGER,
                allowNull: false,
            },
            T_INT_Schedule_Meeting_Id: {
                type: Sequelize.INTEGER,
                allowNull: false
            },
            T_escalateInput: {
                type: Sequelize.STRING,
                allowNull: false
            },
            T_CHR_Escalate_Comment: {
                type: Sequelize.STRING,
                allowNull: false
            },

            T_CHR_Escalate_By: {
                type: Sequelize.STRING(20),
                allowNull: false
            },
            T_Date_Escalate_Date_Time: {
                type: Sequelize.DATE,
                allowNull: true
            },

        },
            {
                timestamps: false,
                createdAt: false,
                UpdatedAt: false,
            }
        );

    }

    saveMeeting_task(meeting_task_Data) {
        return sequelize.sync().then(() => {
            return this.MEETINGTASK.create({

                meeting_schedule_id: meeting_task_Data.meeting_schedule_id,
                task_name: meeting_task_Data.task_name,
                priority: meeting_task_Data.priority,
                activity_status: meeting_task_Data.activity_status,
                plan_start_date: meeting_task_Data.plan_start_date,
                plan_end_date: meeting_task_Data.plan_end_date,
                assigned_by: meeting_task_Data.assigned_by,
                assigned_to: meeting_task_Data.assigned_to,

            }).catch(function (errLog) {
                console.log("errMsg:", errLog)
                log.addLog('model :: meeting_task.js ' + errLog);
                var err;
                if (errLog == "SequelizeUniqueConstraintError: Validation error") {
                    err = {
                        data: 0,
                        errMsg: "Department name should be unique"
                    }
                } else if (errLog != "SequelizeUniqueConstraintError: Validation error") {
                    err = {
                        data: 0,
                        errMsg: errLog["errors"][0]["message"]
                    }
                }
                return err;
            });
        }).catch(function (errLog) {
            log.addLog('model :: meeting_task.js ' + errLog);
        });
    }


    logProgressSubmit(meeting_task_Data) {
        console.log("this for logprogresss--", meeting_task_Data);
        return sequelize.sync().then(() => {


            if (meeting_task_Data.status == '4' && meeting_task_Data.actualEndDate) {

                return this.MEETINGTASKLOGPROGRESS.create({
                    T_INT_Meeting_Task_Id: meeting_task_Data.meetingId,
                    T_INT_Schedule_Meeting_Id: meeting_task_Data.scheduleId,
                    T_CHR_Hours: meeting_task_Data.hours,
                    T_INT_Log_Completion_Percentage: meeting_task_Data.CompletionPercentage,
                    T_CHR_Log_Comment: meeting_task_Data.comment,
                    T_CHR_Log_Activity_Status: meeting_task_Data.status,
                    T_CHR_Created_By: meeting_task_Data.created_by,
                    T_DATE_Created_Date_Time: new Date(),
                    T_CHR_Isdelete: 0,
                }).then((data) => {

                    return this.MEETINGTASKLOGPROGRESS.findAll({
                        where: {
                            T_INT_Meeting_Task_Id: meeting_task_Data.meetingId,
                            T_INT_Schedule_Meeting_Id: meeting_task_Data.scheduleId,
                        }
                    }).then((d) => {
                        console.log("dddddddd=", d);
                        var today = new Date();
                        var dd = String(today.getDate()).padStart(2, '0');
                        var mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
                        var yyyy = today.getFullYear();

                        today = dd + '-' + mm + '-' + yyyy;
                        if (d.length > 1) {
                            return sequelize.query("update " + meeting_task_Data.tableName + " set T_CHR_Percentage = '"+meeting_task_Data.CompletionPercentage+"' , T_CHR_Activity_Status='" + meeting_task_Data.status + "', T_CHR_Actual_end_date='" + meeting_task_Data.actualEndDate + "'  where T_INT_Meeting_Task_Id = '" + meeting_task_Data.meetingId + "' and T_INT_Schedule_Meeting_Id='" + meeting_task_Data.scheduleId + "'");
                        } else {
                            return sequelize.query("update " + meeting_task_Data.tableName + " set T_CHR_Percentage = '"+meeting_task_Data.CompletionPercentage+"' , T_CHR_Activity_Status='" + meeting_task_Data.status + "',T_CHR_Actual_start_date='" + today + "', T_CHR_Actual_end_date'" + meeting_task_Data.actualEndDate + "' where T_INT_Meeting_Task_Id = '" + meeting_task_Data.meetingId + "' and T_INT_Schedule_Meeting_Id='" + meeting_task_Data.scheduleId + "'");
                        }
                    })


                }).catch(function (errLog) {
                    console.log("errMsg:", errLog)
                    log.addLog('model :: meeting_task.js ' + errLog);
                    var err;
                    if (errLog != "SequelizeUniqueConstraintError: Validation error") {
                        err = {
                            data: 0,
                            errMsg: errLog["errors"][0]["message"]
                        }
                    }
                    return err;
                });


            } else {
                return this.MEETINGTASKLOGPROGRESS.create({
                    T_INT_Meeting_Task_Id: meeting_task_Data.meetingId,
                    T_INT_Schedule_Meeting_Id: meeting_task_Data.scheduleId,
                    T_CHR_Hours: meeting_task_Data.hours,
                    T_INT_Log_Completion_Percentage: meeting_task_Data.CompletionPercentage,
                    T_CHR_Log_Comment: meeting_task_Data.comment,
                    T_CHR_Log_Activity_Status: meeting_task_Data.status,
                    T_CHR_Created_By: meeting_task_Data.created_by,
                    T_DATE_Created_Date_Time: new Date(),
                    T_CHR_Isdelete: 0,
                }).then((data) => {
                    return this.MEETINGTASKLOGPROGRESS.findAll({
                        where: {
                            T_INT_Meeting_Task_Id: meeting_task_Data.meetingId,
                            T_INT_Schedule_Meeting_Id: meeting_task_Data.scheduleId,
                        }
                    }).then((d) => {
                        console.log("dddddddd=", meeting_task_Data);
                        var today = new Date();
                        var dd = String(today.getDate()).padStart(2, '0');
                        var mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
                        var yyyy = today.getFullYear();

                        today = dd + '-' + mm + '-' + yyyy;
                        
                        if (d.length > 1) {
                            return sequelize.query("update " + meeting_task_Data.tableName + " set T_CHR_Percentage = '"+meeting_task_Data.CompletionPercentage+"' , T_CHR_Activity_Status='" + meeting_task_Data.status + "' where T_INT_Meeting_Task_Id = '" + meeting_task_Data.meetingId + "' and T_INT_Schedule_Meeting_Id='" + meeting_task_Data.scheduleId + "'");
                        } else {
                            
                            return sequelize.query("update " + meeting_task_Data.tableName + " set T_CHR_Percentage = '"+meeting_task_Data.CompletionPercentage+"' , T_CHR_Activity_Status='" + meeting_task_Data.status + "',T_CHR_Actual_start_date='" + today + "' where T_INT_Meeting_Task_Id = '" + meeting_task_Data.meetingId + "' and T_INT_Schedule_Meeting_Id='" + meeting_task_Data.scheduleId + "'");
                        }
                    })
                }).catch(function (errLog) {
                    console.log("errMsg:", errLog)
                    log.addLog('model :: meeting_task.js ' + errLog);
                    var err;
                    if (errLog != "SequelizeUniqueConstraintError: Validation error") {
                        err = {
                            data: 0,
                            errMsg: errLog["errors"][0]["message"]
                        }
                    }
                    return err;
                });
            }

        }).catch(function (errLog) {
            log.addLog('model :: meeting_task.js ' + errLog);
        });
    }

    updateMeeting_task(meeting_task_Data) {
        return sequelize.sync().then(() => {
            return this.MEETINGTASK.update({

                meeting_schedule_id: meeting_task_Data.meeting_schedule_id,
                task_name: meeting_task_Data.task_name,
                priority: meeting_task_Data.priority,
                activity_status: meeting_task_Data.activity_status,
                plan_start_date: meeting_task_Data.plan_start_date,
                plan_end_date: meeting_task_Data.plan_end_date,
                assigned_by: meeting_task_Data.assigned_by,
                assigned_to: meeting_task_Data.assigned_to,

            }, {
                    where: {
                        meeting_task_id: meeting_task_Data.meeting_task_id,
                    }
                }).catch(function (errLog) {
                    console.log("errMsg:", errLog)
                    log.addLog('model :: meeting_task.js ' + errLog);
                    var err;
                    if (errLog == "SequelizeUniqueConstraintError: Validation error") {
                        err = {
                            data: 0,
                            errMsg: "Department name should be unique"
                        }
                    } else if (errLog != "SequelizeUniqueConstraintError: Validation error") {
                        err = {
                            data: 0,
                            errMsg: errLog["errors"][0]["message"]
                        }

                    }
                    return err;
                });
        }).catch(function (errLog) {
            log.addLog('model :: meeting_task.js ' + errLog);
        });
    }

    editMeetingTask(meeting_task_Data) {
        console.log("this for edit meeting task deta", meeting_task_Data);
        return sequelize.sync().then(() => {
            return sequelize.query("update " + meeting_task_Data.tableName + " set T_CHR_Task_Name='" + meeting_task_Data.activityName + "',T_CHR_Priority='" + meeting_task_Data.Priority + "', T_CHR_Activity_Status='" + meeting_task_Data.activityStatus + "',T_Date_Plan_Start_Date='" + meeting_task_Data.planStartDate + "',T_Date_Plan_End_Date='" + meeting_task_Data.planEndDate + "',"
                + "T_CHR_Assigned_By='" + meeting_task_Data.assignedBy + "',T_CHR_Assigned_To='" + meeting_task_Data.assignedTo + "',"
                + "T_CHR_Activity_Description='" + meeting_task_Data.activityDescription + "',T_Date_Dependent_Activity='" + meeting_task_Data.selectDependentActivity + "',T_CHR_Updated_By='" + meeting_task_Data.Updated_By + "'"
                + " where T_INT_Meeting_Task_Id = '" + meeting_task_Data.meetingTaskId + "' and T_INT_Schedule_Meeting_Id='" + meeting_task_Data.scheduleMeetingId + "'");
        });
    }

    getMeetingWorkbenchFollowupsount(data) {
        //  console.log("this for meeting task chekc ",data,"offset=====",offsetRow,"fetchrow",fetchRow);
        var d = new Date();
        var condition = "";
        if (data.meetingName) {
            condition = condition + " and T_INT_Meeting_Id in (" + data.meetingName + ") ";

        }
        if (data.meetingType) {
            condition = condition + " and T_INT_Type_Of_Meeting in (" + data.meetingType + ") ";
        }

        if (data.role == '2') {
            if (data.type == 'Workbench') {
                return sequelize.sync().then(() => {
                    return sequelize.query("select TOP 1 (select COUNT(*) from MV_Meeting_Task where T_CHR_Activity_Status='1' and T_CHR_Assigned_To='" + data.userId + "' " + condition + "  ) as openTaskCount,"

                        + "(select COUNT(T_INT_Meeting_Task_Id) from MV_Meeting_Task where T_CHR_Activity_Status='2' and T_CHR_Assigned_To='" + data.userId + "' " + condition + " ) as OverdueTaskCount,"

                        + "(select COUNT(T_INT_Meeting_Task_Id) from MV_Meeting_Task where T_CHR_Activity_Status='4' and T_CHR_Assigned_To='" + data.userId + "' " + condition + " ) as CompletedTaskCount,"

                        + "(select COUNT(T_INT_Meeting_Task_Id) from MV_Meeting_Task where T_CHR_Activity_Status='3' and T_CHR_Assigned_To='" + data.userId + "' " + condition + " ) as DroppeTaskCount"
                        + " from MV_Meeting_Task").then((countdata) => {
                            return countdata;
                        });
                });
            } else {
                return sequelize.sync().then(() => {
                    return sequelize.query("select TOP 1 (select COUNT(T_INT_Meeting_Task_Id) from MV_Meeting_Task where T_CHR_Activity_Status='1' and T_CHR_Assigned_By='" + data.userId + "' " + condition + "  ) as openTaskCount,"

                        + "(select COUNT(T_INT_Meeting_Task_Id) from MV_Meeting_Task where T_CHR_Activity_Status='2' and T_CHR_Assigned_By='" + data.userId + "' " + condition + " ) as OverdueTaskCount,"

                        + "(select COUNT(T_INT_Meeting_Task_Id) from MV_Meeting_Task where T_CHR_Activity_Status='4' and T_CHR_Assigned_By='" + data.userId + "' " + condition + " ) as CompletedTaskCount,"

                        + "(select COUNT(T_INT_Meeting_Task_Id) from MV_Meeting_Task where T_CHR_Activity_Status='3' and T_CHR_Assigned_By='" + data.userId + "' " + condition + " ) as DroppeTaskCount"
                        + " from MV_Meeting_Task").then((countdata) => {
                            return countdata;
                        });
                });
            }
        }

    }
    

    deleteMeeting_task(meeting_task_Data) {
        console.log("+++++++++++++++++++++++", meeting_task_Data);
        return sequelize.sync().then(() => {
            return this.MEETINGTASK.update({
                is_delete: '1',
                deleted_by: meeting_task_Data.deleted_by,
                deleted_date_time: new Date()

            }, {
                    where: {
                        meeting_task_id: meeting_task_Data.meeting_task_id
                    }
                }).catch((errLog) => {
                    var err = {
                        data: 0,
                        errMsg: errMsg
                    }
                    log.addLog('model :: meeting_task.js 210 ' + errLog);
                    return err;
                });
        });
    }





    getMeeting_task(data, offsetRow, fetchRow) {
        console.log("this for meeting task chekc ", data, "offset=====", offsetRow, "fetchrow", fetchRow);
        var d = new Date();
        var condition = "";
        if (data.meetingName) {
            condition = condition + " and T_INT_Meeting_Id in (" + data.meetingName + ") ";

        }
        if (data.meetingType) {
            condition = condition + " and T_INT_Type_Of_Meeting in (" + data.meetingType + ") ";
        }
        if (data.searchText) {
            condition = condition + " and  (T_CHR_Task_Name LIKE '%" + data.searchText + "%' or T_CHR_Name LIKE '%" + data.searchText + "%' )";
        }

        if (data.role == '2') {
            if (data.type == 'Workbench') {
                return sequelize.sync().then(() => {
                    return sequelize.query("select T_CHR_Assigned_By,T_CHR_Assigned_To,T_CHR_Actual_start_date,T_CHR_Actual_end_date,T_CHR_Select_dependent_activity,T_CHR_Revised_due_date,T_INT_Meeting_Task_Id,T_INT_Schedule_Meeting_Id,T_CHR_Task_Name,T_CHR_Priority,T_CHR_Activity_Status,"
                        + "T_Date_Plan_Start_Date,T_Date_Plan_End_Date,T_CHR_Activity_Description,T_CHR_Name,T_CHR_Status_Name,"
                        + "T_Date_Dependent_Activity,T_CHR_Future_Refrence_1,T_CHR_Future_Refrence_2,T_CHR_Future_Refrence_3,T_CHR_Future_Refrence_4,"
                        + "T_CHR_Table_Name,T_CHR_Created_By,T_Date_Created_Date_Time,T_CHR_Updated_By,T_CHR_Updated_By,T_Date_Updated_Date_Time,"
                        + " T_CHR_Deleted_By,T_Date_Deleted_Date_Time ,T_CHR_Percentage  from MV_Meeting_Task where T_CHR_Activity_Status='1' and T_CHR_Assigned_To = '" + data.userId + "' " + condition + " " +
                        "ORDER BY " +
                        "T_Date_Created_Date_Time desc" +
                        " OFFSET " + offsetRow + " ROWS " +
                        " FETCH NEXT " + fetchRow + " ROWS ONLY ");
                });
            } else {
                return sequelize.sync().then(() => {
                    return sequelize.query("select T_CHR_Assigned_By,T_CHR_Assigned_To,T_CHR_Actual_start_date,T_CHR_Actual_end_date,T_CHR_Select_dependent_activity,T_CHR_Revised_due_date,T_INT_Meeting_Task_Id,T_INT_Schedule_Meeting_Id,T_CHR_Task_Name,T_CHR_Priority,T_CHR_Activity_Status,"
                        + "T_Date_Plan_Start_Date,T_Date_Plan_End_Date,T_CHR_Activity_Description,T_CHR_Name,T_CHR_Status_Name,"
                        + "T_Date_Dependent_Activity,T_CHR_Future_Refrence_1,T_CHR_Future_Refrence_2,T_CHR_Future_Refrence_3,T_CHR_Future_Refrence_4,"
                        + "T_CHR_Table_Name,T_CHR_Created_By,T_Date_Created_Date_Time,T_CHR_Updated_By,T_CHR_Updated_By,T_Date_Updated_Date_Time,"
                        + " T_CHR_Deleted_By,T_Date_Deleted_Date_Time ,T_CHR_Percentage  from MV_Meeting_Task where T_CHR_Activity_Status='1' and T_CHR_Assigned_By = '" + data.userId + "' " + condition + " " +
                        "ORDER BY " +
                        "T_Date_Created_Date_Time desc" +
                        " OFFSET " + offsetRow + " ROWS " +
                        " FETCH NEXT " + fetchRow + " ROWS ONLY ");
                });
            }
        }

    }


    getMeetingTaskDataExportToExcel(data, offsetRow, fetchRow) {
        console.log("this for meeting task chekc ", data, "offset=====", offsetRow, "fetchrow", fetchRow);
        var d = new Date();
        var condition = "";
        if (data.meetingName) {
            condition = condition + " and T_INT_Meeting_Id in (" + data.meetingName + ") ";

        }
        if (data.meetingType) {
            condition = condition + " and T_INT_Type_Of_Meeting in (" + data.meetingType + ") ";
        }
        if (data.searchText) {
            condition = condition + " and  (T_CHR_Task_Name LIKE '%" + data.searchText + "%' or T_CHR_Name LIKE '%" + data.searchText + "%' )";
        }

        if (data.role == '2') {
            if (data.type == 'Workbench') {
                return sequelize.sync().then(() => {
                    return sequelize.query("select T_CHR_Full_Name,T_CHR_Short_Name,T_CHR_Assigned_By,T_CHR_Assigned_To,T_CHR_Actual_start_date,T_CHR_Actual_end_date,T_CHR_Select_dependent_activity,T_CHR_Revised_due_date,T_INT_Meeting_Task_Id,T_INT_Schedule_Meeting_Id,T_CHR_Task_Name,T_CHR_Priority,T_CHR_Activity_Status,"
                        + "T_Date_Plan_Start_Date,T_Date_Plan_End_Date,T_CHR_Activity_Description,T_CHR_Name,T_CHR_Status_Name,"
                        + "T_Date_Dependent_Activity,T_CHR_Future_Refrence_1,T_CHR_Future_Refrence_2,T_CHR_Future_Refrence_3,T_CHR_Future_Refrence_4,"
                        + "T_CHR_Table_Name,T_CHR_Created_By,T_Date_Created_Date_Time,T_CHR_Updated_By,T_CHR_Updated_By,T_Date_Updated_Date_Time,"
                        + " T_CHR_Deleted_By,T_Date_Deleted_Date_Time ,T_CHR_Percentage  from MV_Meeting_Task where T_CHR_Activity_Status='1' and T_CHR_Assigned_To = '" + data.userId + "' " + condition + "  ");
                });
            } else {
                return sequelize.sync().then(() => {
                    return sequelize.query("select T_CHR_Full_Name,T_CHR_Short_Name,T_CHR_Assigned_By,T_CHR_Assigned_To,T_CHR_Actual_start_date,T_CHR_Actual_end_date,T_CHR_Select_dependent_activity,T_CHR_Revised_due_date,T_INT_Meeting_Task_Id,T_INT_Schedule_Meeting_Id,T_CHR_Task_Name,T_CHR_Priority,T_CHR_Activity_Status,"
                        + "T_Date_Plan_Start_Date,T_Date_Plan_End_Date,T_CHR_Activity_Description,T_CHR_Name,T_CHR_Status_Name,"
                        + "T_Date_Dependent_Activity,T_CHR_Future_Refrence_1,T_CHR_Future_Refrence_2,T_CHR_Future_Refrence_3,T_CHR_Future_Refrence_4,"
                        + "T_CHR_Table_Name,T_CHR_Created_By,T_Date_Created_Date_Time,T_CHR_Updated_By,T_CHR_Updated_By,T_Date_Updated_Date_Time,"
                        + " T_CHR_Deleted_By,T_Date_Deleted_Date_Time ,T_CHR_Percentage  from MV_Meeting_Task where T_CHR_Activity_Status='1' and T_CHR_Assigned_By = '" + data.userId + "' " + condition + "  ");
                });
            }
        }

    }


    getMeetingTaskoverdueList(data, offsetRow, fetchRow) {
        console.log("this for meeting task chekc ", data, "offset=====", offsetRow, "fetchrow", fetchRow);
        var d = new Date();
        var condition = "";
        if (data.meetingName) {
            condition = condition + " and T_INT_Meeting_Id in (" + data.meetingName + ") ";

        }
        if (data.meetingType) {
            condition = condition + " and T_INT_Type_Of_Meeting in (" + data.meetingType + ") ";
        }
        if (data.searchText) {
            condition = condition + " and  (T_CHR_Task_Name LIKE '%" + data.searchText + "%' or T_CHR_Name LIKE '%" + data.searchText + "%' )";
        }

        if (data.role == '2') {
            if (data.type == 'Workbench') {
                return sequelize.sync().then(() => {
                    return sequelize.query("select T_CHR_Assigned_By,T_CHR_Assigned_To,T_CHR_Actual_start_date,T_CHR_Actual_end_date,T_CHR_Select_dependent_activity,T_CHR_Revised_due_date,T_INT_Meeting_Task_Id,T_INT_Schedule_Meeting_Id,T_CHR_Task_Name,T_CHR_Priority,T_CHR_Activity_Status,"
                        + "T_Date_Plan_Start_Date,T_Date_Plan_End_Date,T_CHR_Activity_Description,T_CHR_Name,T_CHR_Status_Name,"
                        + "T_Date_Dependent_Activity,T_CHR_Future_Refrence_1,T_CHR_Future_Refrence_2,T_CHR_Future_Refrence_3,T_CHR_Future_Refrence_4,"
                        + "T_CHR_Table_Name,T_CHR_Created_By,T_Date_Created_Date_Time,T_CHR_Updated_By,T_CHR_Updated_By,T_Date_Updated_Date_Time,"
                        + " T_CHR_Deleted_By,T_Date_Deleted_Date_Time ,T_CHR_Percentage  from MV_Meeting_Task where T_CHR_Activity_Status='2' and T_CHR_Assigned_To = '" + data.userId + "' " + condition + " " +
                        "ORDER BY " +
                        "T_Date_Created_Date_Time desc" +
                        " OFFSET " + offsetRow + " ROWS " +
                        " FETCH NEXT " + fetchRow + " ROWS ONLY ");
                });
            } else {
                return sequelize.sync().then(() => {
                    return sequelize.query("select T_CHR_Assigned_By,T_CHR_Assigned_To,T_CHR_Actual_start_date,T_CHR_Actual_end_date,T_CHR_Select_dependent_activity,T_CHR_Revised_due_date,T_INT_Meeting_Task_Id,T_INT_Schedule_Meeting_Id,T_CHR_Task_Name,T_CHR_Priority,T_CHR_Activity_Status,"
                        + "T_Date_Plan_Start_Date,T_Date_Plan_End_Date,T_CHR_Activity_Description,T_CHR_Name,T_CHR_Status_Name,"
                        + "T_Date_Dependent_Activity,T_CHR_Future_Refrence_1,T_CHR_Future_Refrence_2,T_CHR_Future_Refrence_3,T_CHR_Future_Refrence_4,"
                        + "T_CHR_Table_Name,T_CHR_Created_By,T_Date_Created_Date_Time,T_CHR_Updated_By,T_CHR_Updated_By,T_Date_Updated_Date_Time,"
                        + " T_CHR_Deleted_By,T_Date_Deleted_Date_Time ,T_CHR_Percentage  from MV_Meeting_Task where T_CHR_Activity_Status='2' and T_CHR_Assigned_By = '" + data.userId + "' " + condition + " " +
                        "ORDER BY " +
                        "T_Date_Created_Date_Time desc" +
                        " OFFSET " + offsetRow + " ROWS " +
                        " FETCH NEXT " + fetchRow + " ROWS ONLY ");
                });
            }
        }

    }

    getMeetingTaskoverdueListaExportToExcel(data, offsetRow, fetchRow) {
        console.log("this for meeting task chekc ", data, "offset=====", offsetRow, "fetchrow", fetchRow);
        var d = new Date();
        var condition = "";
        if (data.meetingName) {
            condition = condition + " and T_INT_Meeting_Id in (" + data.meetingName + ") ";

        }
        if (data.meetingType) {
            condition = condition + " and T_INT_Type_Of_Meeting in (" + data.meetingType + ") ";
        }
        if (data.searchText) {
            condition = condition + " and  (T_CHR_Task_Name LIKE '%" + data.searchText + "%' or T_CHR_Name LIKE '%" + data.searchText + "%' )";
        }

        if (data.role == '2') {
            if (data.type == 'Workbench') {
                return sequelize.sync().then(() => {
                    return sequelize.query("select T_CHR_Assigned_By,T_CHR_Short_Name,T_CHR_Full_Name,T_CHR_Assigned_To,T_CHR_Actual_start_date,T_CHR_Actual_end_date,T_CHR_Select_dependent_activity,T_CHR_Revised_due_date,T_INT_Meeting_Task_Id,T_INT_Schedule_Meeting_Id,T_CHR_Task_Name,T_CHR_Priority,T_CHR_Activity_Status,"
                        + "T_Date_Plan_Start_Date,T_Date_Plan_End_Date,T_CHR_Activity_Description,T_CHR_Name,T_CHR_Status_Name,"
                        + "T_Date_Dependent_Activity,T_CHR_Future_Refrence_1,T_CHR_Future_Refrence_2,T_CHR_Future_Refrence_3,T_CHR_Future_Refrence_4,"
                        + "T_CHR_Table_Name,T_CHR_Created_By,T_Date_Created_Date_Time,T_CHR_Updated_By,T_CHR_Updated_By,T_Date_Updated_Date_Time,"
                        + " T_CHR_Deleted_By,T_Date_Deleted_Date_Time ,T_CHR_Percentage  from MV_Meeting_Task where T_CHR_Activity_Status='2' and T_CHR_Assigned_To = '" + data.userId + "' " + condition + " ");
                });
            } else {
                return sequelize.sync().then(() => {
                    return sequelize.query("select T_CHR_Assigned_By,T_CHR_Short_Name,T_CHR_Full_Name,T_CHR_Assigned_To,T_CHR_Actual_start_date,T_CHR_Actual_end_date,T_CHR_Select_dependent_activity,T_CHR_Revised_due_date,T_INT_Meeting_Task_Id,T_INT_Schedule_Meeting_Id,T_CHR_Task_Name,T_CHR_Priority,T_CHR_Activity_Status,"
                        + "T_Date_Plan_Start_Date,T_Date_Plan_End_Date,T_CHR_Activity_Description,T_CHR_Name,T_CHR_Status_Name,"
                        + "T_Date_Dependent_Activity,T_CHR_Future_Refrence_1,T_CHR_Future_Refrence_2,T_CHR_Future_Refrence_3,T_CHR_Future_Refrence_4,"
                        + "T_CHR_Table_Name,T_CHR_Created_By,T_Date_Created_Date_Time,T_CHR_Updated_By,T_CHR_Updated_By,T_Date_Updated_Date_Time,"
                        + " T_CHR_Deleted_By,T_Date_Deleted_Date_Time ,T_CHR_Percentage  from MV_Meeting_Task where T_CHR_Activity_Status='2' and T_CHR_Assigned_By = '" + data.userId + "' " + condition + " ");
                });
            }
        }

    }

    getMeetingTaskdiscardedList(data, offsetRow, fetchRow) {
        var d = new Date();
        var condition = "";
        if (data.meetingName) {
            condition = condition + " and T_INT_Meeting_Id in (" + data.meetingName + ") ";

        }
        if (data.meetingType) {
            condition = condition + " and T_INT_Type_Of_Meeting in (" + data.meetingType + ") ";
        }
        if (data.searchText) {
            condition = condition + " and  (T_CHR_Task_Name LIKE '%" + data.searchText + "%' or T_CHR_Name LIKE '%" + data.searchText + "%' )";
        }
        if (data.role == '2') {
            if (data.type == 'Workbench') {
                return sequelize.sync().then(() => {
                    return sequelize.query("select T_CHR_Assigned_By,T_CHR_Assigned_To,T_CHR_Actual_start_date,T_CHR_Actual_end_date,T_CHR_Select_dependent_activity,T_CHR_Revised_due_date,T_INT_Meeting_Task_Id,T_INT_Schedule_Meeting_Id,T_CHR_Task_Name,T_CHR_Priority,T_CHR_Activity_Status,"
                        + "T_Date_Plan_Start_Date,T_Date_Plan_End_Date,T_CHR_Activity_Description,T_CHR_Name,T_CHR_Status_Name,"
                        + "T_Date_Dependent_Activity,T_CHR_Future_Refrence_1,T_CHR_Future_Refrence_2,T_CHR_Future_Refrence_3,T_CHR_Future_Refrence_4,"
                        + "T_CHR_Table_Name,T_CHR_Created_By,T_Date_Created_Date_Time,T_CHR_Updated_By,T_CHR_Updated_By,T_Date_Updated_Date_Time,"
                        + " T_CHR_Deleted_By,T_Date_Deleted_Date_Time ,T_CHR_Percentage  from MV_Meeting_Task where T_CHR_Activity_Status='3' and T_CHR_Assigned_To = '" + data.userId + "' " + condition + " " +
                        "ORDER BY " +
                        "T_Date_Created_Date_Time desc" +
                        " OFFSET " + offsetRow + " ROWS " +
                        " FETCH NEXT " + fetchRow + " ROWS ONLY ");
                });
            } else {
                return sequelize.sync().then(() => {
                    return sequelize.query("select T_CHR_Assigned_By,T_CHR_Assigned_To,T_CHR_Actual_start_date,T_CHR_Actual_end_date,T_CHR_Select_dependent_activity,T_CHR_Revised_due_date,T_INT_Meeting_Task_Id,T_INT_Schedule_Meeting_Id,T_CHR_Task_Name,T_CHR_Priority,T_CHR_Activity_Status,"
                        + "T_Date_Plan_Start_Date,T_Date_Plan_End_Date,T_CHR_Activity_Description,T_CHR_Name,T_CHR_Status_Name,"
                        + "T_Date_Dependent_Activity,T_CHR_Future_Refrence_1,T_CHR_Future_Refrence_2,T_CHR_Future_Refrence_3,T_CHR_Future_Refrence_4,"
                        + "T_CHR_Table_Name,T_CHR_Created_By,T_Date_Created_Date_Time,T_CHR_Updated_By,T_CHR_Updated_By,T_Date_Updated_Date_Time,"
                        + " T_CHR_Deleted_By,T_Date_Deleted_Date_Time ,T_CHR_Percentage  from MV_Meeting_Task where T_CHR_Activity_Status='3' and T_CHR_Assigned_By = '" + data.userId + "' " + condition + " " +
                        "ORDER BY " +
                        "T_Date_Created_Date_Time desc" +
                        " OFFSET " + offsetRow + " ROWS " +
                        " FETCH NEXT " + fetchRow + " ROWS ONLY");
                });
            }
        }

    }

    getMeetingTaskdiscardedListaExportToExcel(data, offsetRow, fetchRow) {
        var d = new Date();
        var condition = "";
        if (data.meetingName) {
            condition = condition + " and T_INT_Meeting_Id in (" + data.meetingName + ") ";

        }
        if (data.meetingType) {
            condition = condition + " and T_INT_Type_Of_Meeting in (" + data.meetingType + ") ";
        }
        if (data.searchText) {
            condition = condition + " and  (T_CHR_Task_Name LIKE '%" + data.searchText + "%' or T_CHR_Name LIKE '%" + data.searchText + "%' )";
        }
        if (data.role == '2') {
            if (data.type == 'Workbench') {
                return sequelize.sync().then(() => {
                    return sequelize.query("select T_CHR_Assigned_By,T_CHR_Full_Name,T_CHR_Short_Name,T_CHR_Assigned_To,T_CHR_Actual_start_date,T_CHR_Actual_end_date,T_CHR_Select_dependent_activity,T_CHR_Revised_due_date,T_INT_Meeting_Task_Id,T_INT_Schedule_Meeting_Id,T_CHR_Task_Name,T_CHR_Priority,T_CHR_Activity_Status,"
                        + "T_Date_Plan_Start_Date,T_Date_Plan_End_Date,T_CHR_Activity_Description,T_CHR_Name,T_CHR_Status_Name,"
                        + "T_Date_Dependent_Activity,T_CHR_Future_Refrence_1,T_CHR_Future_Refrence_2,T_CHR_Future_Refrence_3,T_CHR_Future_Refrence_4,"
                        + "T_CHR_Table_Name,T_CHR_Created_By,T_Date_Created_Date_Time,T_CHR_Updated_By,T_CHR_Updated_By,T_Date_Updated_Date_Time,"
                        + " T_CHR_Deleted_By,T_Date_Deleted_Date_Time ,T_CHR_Percentage  from MV_Meeting_Task where T_CHR_Activity_Status='3' and T_CHR_Assigned_To = '" + data.userId + "' " + condition + "  ");
                });
            } else {
                return sequelize.sync().then(() => {
                    return sequelize.query("select T_CHR_Assigned_By,T_CHR_Full_Name,T_CHR_Short_Name,T_CHR_Assigned_To,T_CHR_Actual_start_date,T_CHR_Actual_end_date,T_CHR_Select_dependent_activity,T_CHR_Revised_due_date,T_INT_Meeting_Task_Id,T_INT_Schedule_Meeting_Id,T_CHR_Task_Name,T_CHR_Priority,T_CHR_Activity_Status,"
                        + "T_Date_Plan_Start_Date,T_Date_Plan_End_Date,T_CHR_Activity_Description,T_CHR_Name,T_CHR_Status_Name,"
                        + "T_Date_Dependent_Activity,T_CHR_Future_Refrence_1,T_CHR_Future_Refrence_2,T_CHR_Future_Refrence_3,T_CHR_Future_Refrence_4,"
                        + "T_CHR_Table_Name,T_CHR_Created_By,T_Date_Created_Date_Time,T_CHR_Updated_By,T_CHR_Updated_By,T_Date_Updated_Date_Time,"
                        + " T_CHR_Deleted_By,T_Date_Deleted_Date_Time ,T_CHR_Percentage  from MV_Meeting_Task where T_CHR_Activity_Status='3' and T_CHR_Assigned_By = '" + data.userId + "' " + condition + " ");
                });
            }
        }

    }


    getMeetingTaskViewList(id, Schedule_Meeting_Id) {
        return sequelize.sync().then(() => {
            return sequelize.query("select T_CHR_Assigned_By,T_CHR_Assigned_To,T_CHR_Assigned_To,T_CHR_Actual_start_date,T_CHR_Actual_end_date,T_CHR_Select_dependent_activity,T_CHR_Revised_due_date,T_INT_Meeting_Task_Id,T_INT_Schedule_Meeting_Id,T_CHR_Task_Name,T_CHR_Priority,T_CHR_Activity_Status,"
                + "T_Date_Plan_Start_Date,T_Date_Plan_End_Date,T_CHR_Activity_Description,T_CHR_Name,T_CHR_Status_Name,"
                + "T_Date_Dependent_Activity,T_CHR_Future_Refrence_1,T_CHR_Future_Refrence_2,T_CHR_Future_Refrence_3,T_CHR_Future_Refrence_4,"
                + "T_CHR_Table_Name,T_CHR_Created_By,T_Date_Created_Date_Time,T_CHR_Updated_By,T_CHR_Updated_By,T_Date_Updated_Date_Time,"
                + " T_CHR_Deleted_By,T_Date_Deleted_Date_Time ,T_CHR_Percentage  from MV_Meeting_Task where T_INT_Meeting_Task_Id = '" + id + "' and T_INT_Schedule_Meeting_Id = '" + Schedule_Meeting_Id + "'");
        });
    }

    getDependentActivityView(id, Schedule_Meeting_Id) {
        return sequelize.sync().then(() => {
            return sequelize.query("select T_CHR_Assigned_By,T_CHR_Assigned_To,T_CHR_Assigned_To,T_CHR_Actual_start_date,T_CHR_Actual_end_date,T_CHR_Select_dependent_activity,T_CHR_Revised_due_date,T_INT_Meeting_Task_Id,T_INT_Schedule_Meeting_Id,T_CHR_Task_Name,T_CHR_Priority,T_CHR_Activity_Status,"
                + "T_Date_Plan_Start_Date,T_Date_Plan_End_Date,T_CHR_Activity_Description,T_CHR_Name,T_CHR_Status_Name,"
                + "T_Date_Dependent_Activity,T_CHR_Future_Refrence_1,T_CHR_Future_Refrence_2,T_CHR_Future_Refrence_3,T_CHR_Future_Refrence_4,"
                + "T_CHR_Table_Name,T_CHR_Created_By,T_Date_Created_Date_Time,T_CHR_Updated_By,T_CHR_Updated_By,T_Date_Updated_Date_Time,"
                + " T_CHR_Deleted_By,T_Date_Deleted_Date_Time ,T_CHR_Percentage  from MV_Meeting_Task where T_INT_Meeting_Task_Id = '" + id + "' and T_INT_Schedule_Meeting_Id = '" + Schedule_Meeting_Id + "'");
        });
    }

    getRevisedHistory(id, Schedule_Meeting_Id) {
        console.log("22222222222===", id, Schedule_Meeting_Id)
        return sequelize.sync().then(() => {

            return sequelize.query("select tr.T_INT_Meeting_Task_Id,tr.T_INT_Schedule_Meeting_Id,tr.T_Date_Revised_Date,tr.T_CHR_Revised_Comment," +
                " tr.T_CHR_Revised_By,tr.T_Date_Revised_Date_Time,u.T_CHR_Name from T_Revised_Histories as tr inner join M_User_Masters as u on u.T_CHR_Employee_Id=tr.T_CHR_Revised_By where tr.T_INT_Meeting_Task_Id = '" + id + "' and tr.T_INT_Schedule_Meeting_Id = '" + Schedule_Meeting_Id + "'").then((data) => {
                    return data;
                });


        });
    }



    getMeetingTaskcompletedList(data, offsetRow, fetchRow) {
        var d = new Date();
        var condition = "";
        if (data.meetingName) {
            condition = condition + " and T_INT_Meeting_Id in (" + data.meetingName + ") ";

        }
        if (data.meetingType) {
            condition = condition + " and T_INT_Type_Of_Meeting in (" + data.meetingType + ") ";
        }

        if (data.searchText) {
            condition = condition + " and  (T_CHR_Task_Name LIKE '%" + data.searchText + "%' or T_CHR_Name LIKE '%" + data.searchText + "%' )";
        }
        if (data.role == '2') {
            if (data.type == 'Workbench') {
                return sequelize.sync().then(() => {
                    return sequelize.query("select T_CHR_Assigned_By,T_CHR_Assigned_To,T_CHR_Actual_start_date,T_CHR_Actual_end_date,T_CHR_Select_dependent_activity,T_CHR_Revised_due_date,T_INT_Meeting_Task_Id,T_INT_Schedule_Meeting_Id,T_CHR_Task_Name,T_CHR_Priority,T_CHR_Activity_Status,"
                        + "T_Date_Plan_Start_Date,T_Date_Plan_End_Date,T_CHR_Activity_Description,T_CHR_Name,T_CHR_Status_Name,"
                        + "T_Date_Dependent_Activity,T_CHR_Future_Refrence_1,T_CHR_Future_Refrence_2,T_CHR_Future_Refrence_3,T_CHR_Future_Refrence_4,"
                        + "T_CHR_Table_Name,T_CHR_Created_By,T_Date_Created_Date_Time,T_CHR_Updated_By,T_CHR_Updated_By,T_Date_Updated_Date_Time,"
                        + " T_CHR_Deleted_By,T_Date_Deleted_Date_Time ,T_CHR_Percentage  from MV_Meeting_Task where T_CHR_Activity_Status='4' and T_CHR_Assigned_To = '" + data.userId + "' " + condition + " " +
                        "ORDER BY " +
                        "T_Date_Created_Date_Time desc" +
                        " OFFSET " + offsetRow + " ROWS " +
                        " FETCH NEXT " + fetchRow + " ROWS ONLY ");
                });
            } else {
                return sequelize.sync().then(() => {
                    return sequelize.query("select T_CHR_Assigned_By,T_CHR_Assigned_To,T_CHR_Actual_start_date,T_CHR_Actual_end_date,T_CHR_Select_dependent_activity,T_CHR_Revised_due_date,T_INT_Meeting_Task_Id,T_INT_Schedule_Meeting_Id,T_CHR_Task_Name,T_CHR_Priority,T_CHR_Activity_Status,"
                        + "T_Date_Plan_Start_Date,T_Date_Plan_End_Date,T_CHR_Activity_Description,T_CHR_Name,T_CHR_Status_Name,"
                        + "T_Date_Dependent_Activity,T_CHR_Future_Refrence_1,T_CHR_Future_Refrence_2,T_CHR_Future_Refrence_3,T_CHR_Future_Refrence_4,"
                        + "T_CHR_Table_Name,T_CHR_Created_By,T_Date_Created_Date_Time,T_CHR_Updated_By,T_CHR_Updated_By,T_Date_Updated_Date_Time,"
                        + " T_CHR_Deleted_By,T_Date_Deleted_Date_Time ,T_CHR_Percentage  from MV_Meeting_Task where T_CHR_Activity_Status='4' and T_CHR_Assigned_By = '" + data.userId + "' " + condition + " " +
                        "ORDER BY " +
                        "T_Date_Created_Date_Time desc" +
                        " OFFSET " + offsetRow + " ROWS " +
                        " FETCH NEXT " + fetchRow + " ROWS ONLY");
                });
            }
        }
    }

    getMeetingTaskcompletedListaExportToExcel(data, offsetRow, fetchRow) {
        var d = new Date();
        var condition = "";
        if (data.meetingName) {
            condition = condition + " and T_INT_Meeting_Id in (" + data.meetingName + ") ";

        }
        if (data.meetingType) {
            condition = condition + " and T_INT_Type_Of_Meeting in (" + data.meetingType + ") ";
        }

        if (data.searchText) {
            condition = condition + " and  (T_CHR_Task_Name LIKE '%" + data.searchText + "%' or T_CHR_Name LIKE '%" + data.searchText + "%' )";
        }
        if (data.role == '2') {
            if (data.type == 'Workbench') {
                return sequelize.sync().then(() => {
                    return sequelize.query("select T_CHR_Assigned_By,T_CHR_Assigned_To,T_CHR_Actual_start_date,T_CHR_Actual_end_date,T_CHR_Select_dependent_activity,T_CHR_Revised_due_date,T_INT_Meeting_Task_Id,T_INT_Schedule_Meeting_Id,T_CHR_Task_Name,T_CHR_Priority,T_CHR_Activity_Status,"
                        + "T_Date_Plan_Start_Date,T_Date_Plan_End_Date,T_CHR_Activity_Description,T_CHR_Name,T_CHR_Status_Name,"
                        + "T_Date_Dependent_Activity,T_CHR_Future_Refrence_1,T_CHR_Future_Refrence_2,T_CHR_Future_Refrence_3,T_CHR_Future_Refrence_4,"
                        + "T_CHR_Table_Name,T_CHR_Created_By,T_Date_Created_Date_Time,T_CHR_Updated_By,T_CHR_Updated_By,T_Date_Updated_Date_Time,"
                        + " T_CHR_Deleted_By,T_Date_Deleted_Date_Time ,T_CHR_Percentage  from MV_Meeting_Task where T_CHR_Activity_Status='4' and T_CHR_Assigned_To = '" + data.userId + "' " + condition + "  ");
                });
            } else {
                return sequelize.sync().then(() => {
                    return sequelize.query("select T_CHR_Assigned_By,T_CHR_Assigned_To,T_CHR_Actual_start_date,T_CHR_Actual_end_date,T_CHR_Select_dependent_activity,T_CHR_Revised_due_date,T_INT_Meeting_Task_Id,T_INT_Schedule_Meeting_Id,T_CHR_Task_Name,T_CHR_Priority,T_CHR_Activity_Status,"
                        + "T_Date_Plan_Start_Date,T_Date_Plan_End_Date,T_CHR_Activity_Description,T_CHR_Name,T_CHR_Status_Name,"
                        + "T_Date_Dependent_Activity,T_CHR_Future_Refrence_1,T_CHR_Future_Refrence_2,T_CHR_Future_Refrence_3,T_CHR_Future_Refrence_4,"
                        + "T_CHR_Table_Name,T_CHR_Created_By,T_Date_Created_Date_Time,T_CHR_Updated_By,T_CHR_Updated_By,T_Date_Updated_Date_Time,"
                        + " T_CHR_Deleted_By,T_Date_Deleted_Date_Time ,T_CHR_Percentage  from MV_Meeting_Task where T_CHR_Activity_Status='4' and T_CHR_Assigned_By = '" + data.userId + "' " + condition + "  ");
                });
            }
        }
    }

    getLogProgress(data) {
        return sequelize.sync().then(() => {
            return sequelize.query("select T_INT_Log_Progress_Id,T_CHR_Log_Comment,T_CHR_Created_By,T_DATE_Created_Date_Time from TXN_Task_Progresses"
                + " where T_INT_Meeting_Task_Id= '" + data.id + "' ORDER BY T_DATE_Created_Date_Time DESC ");
        });
    }

    getLogProgressFormData(data) {
        return sequelize.sync().then(() => {
            return sequelize.query("select T_CHR_Activity_Status,T_CHR_Table_Name,T_CHR_Percentage,T_INT_Meeting_Task_Id,T_INT_Schedule_Meeting_Id from MV_Meeting_Task where T_INT_Meeting_Task_Id = '"+data.id+"' and T_INT_Schedule_Meeting_Id='"+data.scheduleId+"'",{type:sequelize.QueryTypes.SELECT});
        });
    }

    getDependentActivity(data) {
        console.log("this for depndent activity ", data);
        return sequelize.sync().then(() => {
            return sequelize.query("select T_CHR_Task_Name,T_INT_Meeting_Task_Id,T_INT_Schedule_Meeting_Id from MV_Meeting_Task");
        });
    }

    submitRevisedDate(revisedData) {

        console.log("RRRRRRRRRRRRRRRRRR===",revisedData)
        return this.REVISEDHISTORY.create({
            T_INT_Meeting_Task_Id: revisedData.meetingTaskId,
            T_INT_Schedule_Meeting_Id: revisedData.scheduleMeetingId,
            T_Date_Revised_Date: revisedData.revisedDueDate,
            T_CHR_Revised_Comment: revisedData.revisedDueDateComment,

            T_CHR_Revised_By: revisedData.Updated_By,
            T_Date_Revised_Date_Time: new Date(),

        }).then((data) => {

            var today = new Date();
            var dd = String(today.getDate()).padStart(2, '0');
            var mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
            var yyyy = today.getFullYear();

            today = dd + '-' + mm + '-' + yyyy;
            
            return sequelize.query("update " + revisedData.tableName + " set  T_CHR_Revised_due_date='" + revisedData.revisedDueDate + "' where T_INT_Meeting_Task_Id = '" + revisedData.meetingTaskId + "' and T_INT_Schedule_Meeting_Id='" + revisedData.scheduleMeetingId + "'");

        }).catch(function (errLog) {
            console.log("errMsg:", errLog)
            log.addLog('model :: meeting_task.js ' + errLog);
            var err;
            if (errLog != "SequelizeUniqueConstraintError: Validation error") {
                err = {
                    data: 0,
                    errMsg: errLog["errors"][0]["message"]
                }

            }
            return err;
        });

    }


    submitEscalateNotifyTo(revisedData) {

        return this.ESCALATENOTIFYTO.create({
            T_INT_Meeting_Task_Id: revisedData.meetingTaskId,
            T_INT_Schedule_Meeting_Id: revisedData.scheduleMeetingId,
            T_escalateInput: revisedData.escalateInput,
            T_CHR_Escalate_Comment: revisedData.commentInput,
            T_CHR_Escalate_By: revisedData.Updated_By,
            T_Date_Escalate_Date_Time: new Date(),

        }).then((data) => {

            console.log("this data after insert in ",data,"this for zoho data",revisedData);
            return sequelize.query("select T_CHR_Email from M_User_Masters where T_CHR_Isdelete='0' and T_CHR_Status='enable' and T_CHR_Employee_Id ='" + revisedData.escalateInput + "'",{type:sequelize.QueryTypes.SELECT}).then((data) => {
                console.log("this email for data ",data);
                if(data[0]['T_CHR_Email']){
                    console.log("this for if" )
                    let notify = new EmailNotification();
                    notify.sendmailOnRaiseNPDRequest(revisedData, data);
                     return data;
                }else{
                    console.log("this for else")

                    var err
                    
                    err = {
                        data: 0,
                        errMsg: "Email id is Not Available For This User"
                    }
                    return err
                }
            
            });

        }).catch(function (errLog) {
            console.log("errMsg:", errLog)
            log.addLog('model :: meeting_task.js ' + errLog);
            var err;
            if (errLog != "SequelizeUniqueConstraintError: Validation error") {
                err = {
                    data: 0,
                    errMsg: errLog["errors"][0]["message"]
                }

            }
            return err;
        });

    }

};