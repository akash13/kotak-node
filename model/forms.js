const Sequelize = require('sequelize');
var Sequelize1 = require('Sequelize');
var con = require('./connection');

var customCon = require('../model/customConnection');
//var customCon = require('./customConnection')('localhost', 'dbName', 'userName', 'password');
var Application = require('./application');
var MyLogModel = require('../model/myLog');
var log = new MyLogModel();
const Op = Sequelize.Op;
var errMsg = "Server issue please try after sometime";
Sequelize.DATE.prototype._stringify = function _stringify(date, options) {
    return this._applyTimezone(date, options).format('YYYY-MM-DD HH:mm:ss.SSS');
};

module.exports = class FormGroup {

    constructor() {
        this.FORMGROUP = con.define('Meeting_Templates', {
            form_id: {
                type: Sequelize.INTEGER,
                primaryKey: true,
                autoIncrement: true
            },
            T_INT_Meeting_Id: {
                type: Sequelize.INTEGER,
                allowNull: true
            },
            T_CHR_Action_Table_Name: {
                type: Sequelize.STRING(50),
                allowNull: true,

            },
            action_type: {
                type: Sequelize.STRING,
                allowNull: true,
                defaultValue: 'create'
            },
            T_CHR_Template_Name: {
                type: Sequelize.STRING(50),
                allowNull: true,
                unique: true
            },
            T_CHR_Template_Title: {
                type: Sequelize.STRING(50),
                allowNull: true
            },
            form_type: {
                type: Sequelize.STRING,
                allowNull: true,
                defaultValue: 'standard'

            },
            T_CHR_Version: {
                type: Sequelize.STRING(20),
                allowNull: true,

            },

            T_CHR_Form_Json: {
                type: Sequelize.TEXT,
                allowNull: true
            },
            T_CHR_Status: {
                type: Sequelize.STRING(10),
                allowNull: true,
                defaultValue: 'enable'
            },
            T_CHR_Created_By: {
                type: Sequelize.STRING(20),
                allowNull: true
            },
            T_DATE_Created_Date_Time: {
                type: Sequelize.DATE,
                allowNull: true
            },
            T_CHR_Updated_By: {
                type: Sequelize.STRING(20),
                allowNull: true
            },
            T_DATE_Updated_Date_Time: {
                type: Sequelize.DATE,
                allowNull: true
            },
            T_CHR_Isdelete: {
                type: Sequelize.STRING(2),
                allowNull: false,
                defaultValue: '0'
            },
            T_CHR_Deleted_By: {
                type: Sequelize.STRING(20),
                allowNull: true
            },
            T_DATE_Deleted_Date_Time: {
                type: Sequelize.DATE,
                allowNull: true
            },
        },
            {
                timestamps: false,
                createdAt: false,
                UpdatedAt: false,
            });





    }

    saveForm(formData) {
        return sequelize.sync().then(() => {
            return this.FORMGROUP.create({
                T_CHR_Template_Name: formData.Name,
                T_INT_Meeting_Id: formData.app_id,
                T_CHR_Action_Table_Name: formData.Table_Name,

                T_CHR_Template_Title: formData.Title,

                T_CHR_Version: formData.version,
                T_CHR_Form_Json: formData.formJson_string,
                T_CHR_Created_By: formData.userId,
                T_DATE_Created_Date_Time: new Date()
            }).then(function (x) {
                return x.form_id;
            }).catch(function (errLog) {
                console.log("errMsg:", errLog)
                log.addLog('model :: form.js ' + errLog);
                var err;
                if (errLog == "SequelizeUniqueConstraintError: Validation error") {
                    err = {
                        data: 0,
                        errMsg: "Form name,version should be unique"
                    }
                } else if (errLog != "SequelizeUniqueConstraintError: Validation error") {
                    err = {
                        data: 0,
                        errMsg: errLog["errors"][0]["message"]
                    }

                }
                return err;

            });
        }).catch(function (errLog) {
            log.addLog('model :: form.js ' + errLog);
        })

    }

    getForms(appId) {
        return sequelize.sync().then(() => {
            return sequelize.query("SELECT mt.form_id,mm.T_CHR_Short_Name, mt.T_CHR_Template_Title,mt.T_CHR_Template_Name,mt.T_CHR_Action_Table_Name,mt.T_CHR_Status, " +
                " mt.T_DATE_Created_Date_Time FROM M_Meeting_Masters  as mm " +
                " INNER JOIN Meeting_Templates as mt ON mm.T_INT_Meeting_Id = mt.T_INT_Meeting_Id where mt.T_INT_Meeting_Id = " + appId + " order by mt.T_DATE_Created_Date_Time desc;", {
                    type: sequelize.QueryTypes.SELECT
                });
        });
    }

    getFormById(requestData) {
        return sequelize.sync().then(() => {
            return this.FORMGROUP.findOne({
                where: {
                    form_id: requestData.formId

                },
            });
        });
    }

    getRenderFormData(reqData) {
        //console.log("getRenderFormData vvvvvvvv------->" + JSON.stringify(reqData));
        return sequelize.sync().then(() => {
            return sequelize.query("SELECT * FROM QWERTY", {
                type: sequelize.QueryTypes.SELECT
            });
        });
    }

    getRenderFormDataById(dataid, reqData, actionColumn) {

        // console.log("getRenderFormData vvvvvvvv--++" + dataid + " " + JSON.stringify(reqData));
        return sequelize.sync().then(() => {
            return sequelize.query("SELECT * FROM " + reqData + " Where " + actionColumn + " = " + "'" + dataid + "'" + ";", {
                type: sequelize.QueryTypes.SELECT
            });
        });
    }

    deleteRenderFormDataById(dataid, tableActionColumn, reqData) {
        return sequelize.sync().then(() => {
            return sequelize.query("DELETE FROM " + reqData + " WHERE " + tableActionColumn + " = " + "'" + dataid + "'" + ";", {
                type: sequelize.QueryTypes.DELETE
            });
        });
    }

    updateForm(requestData, formId) {

        return sequelize.sync().then(() => {
            return this.FORMGROUP.update({
                T_CHR_Template_Name: requestData.Name,
                T_CHR_Template_Title: requestData.Title,

                T_CHR_Action_Table_Name: requestData.Table_Name,

                T_CHR_Version: requestData.version,
                T_CHR_Form_Json: requestData.formJson_string,
                T_CHR_Status: requestData.status,
                T_CHR_Updated_By: requestData.updatedBy,
                T_DATE_Updated_Date_Time: new Date()
            }, {
                    where: {
                        form_id: formId
                    }
                }).catch(function (err) {
                    console.log("errMsg:", errLog)
                    log.addLog('model :: form.js ' + errLog);
                    var err;
                    if (errLog == "SequelizeUniqueConstraintError: Validation error") {
                        err = {
                            data: 0,
                            errMsg: "Form name,version should be unique"
                        }
                    } else if (errLog != "SequelizeUniqueConstraintError: Validation error") {
                        err = {
                            data: 0,
                            errMsg: errLog["errors"][0]["message"]
                        }

                    }
                    return err;

                });
        }).catch(function (errLog) {
            log.addLog('model :: form.js ' + errLog);
        });
    }

    deleteForm(formId) {
        return sequelize.sync().then(() => {
            this.FORMGROUP.destroy({
                where: {
                    form_id: formId
                }
            });

            return this.FORMAPIS.destroy({
                where: {
                    form_id: formId
                }
            });
        });
    }



    getApplicationForm(id) {
        return sequelize.sync().then(() => {
            return this.FORMGROUP.all({
                where: {
                    T_INT_Meeting_Id: id
                },
            });
        });
    }

    getFormForAccess() {
        return sequelize.sync().then(() => {
            return this.FORMGROUP.all();
        });
    }

    genrateApi(apiData) {
        //console.log("apiData=" + JSON.stringify(apiData.form_id));
        return sequelize.sync().then(() => {
            this.FORMAPIS.create({
                form_id: apiData.form_id,
                app_id: apiData.app_id,
                api_name: apiData.api_name,
                api_method: apiData.api_method,
                api_action: apiData.api_action,
            });
        });
    }

    getAPIS(form_id) {
        return sequelize.sync().then(() => {
            return this.FORMAPIS.findAll({
                where: {
                    form_id: form_id
                },
            });
        });
    }

    dynamicAPIAlongWithJSONString(formId) {
        return sequelize.sync().then(() => {
            return sequelize.query("SELECT form_id,T_CHR_Action_Table_Name,T_INT_Meeting_Id,T_CHR_Template_Name,T_CHR_Template_Title,T_CHR_Version,T_CHR_Form_Json FROM Meeting_Templates where form_id = " + formId + ";", {
                type: sequelize.QueryTypes.SELECT
            });
        });
    }
    tableDetailstable(table, columns, whereSELECT, data) {

        console.log("hiiiiiiiiiiiiiiiiiiiiiiiiiiiii=", table, columns, whereSELECT, data)

        var s = whereSELECT + "";
        if (s == "select") {
            this.getOtherConnection(data);

            return Sequelize1.sync().then(() => {
                return Sequelize1.query("SELECT " + columns + " FROM " + table + " ;", {
                    type: sequelize.QueryTypes.SELECT
                });
            });
        } else {



            s = s.substr(6, s.length);
            this.getOtherConnection(data);

            return Sequelize1.sync().then(() => {
                return Sequelize1.query("SELECT " + columns + " FROM " + table + " where " + s + " ;", {
                    type: sequelize.QueryTypes.SELECT
                });
            });


        }

    }

    DynamictableDetails(table, columns,scheduledId) {

        console.log("hiiiiiiiiiiiiiiiiiiiiiiiiiiiii=", table, columns)

        

            return sequelize.sync().then(() => {
                return sequelize.query("SELECT " + columns + " FROM " + table + " where T_INT_Schedule_Meeting_Id='"+scheduledId+"';", {
                    type: sequelize.QueryTypes.SELECT
                }).catch((err)=>{
                    console.log(err);
                });
            }).catch((err)=>{
                console.log(err);
            });
       

    }

    tableonchangeDetails(where, fromtable, columnnameto, coldefaultval, columnvalto, wherestring) {


        // console.log("len=",whereclause.length);
        return sequelize.sync().then(() => {
            if (wherestring.length > 1) {

                console.log("in iff");
                return sequelize.query("SELECT distinct   " + columnnameto + "," + columnvalto + " FROM " + fromtable + "   where " + where + "= '" + coldefaultval + "'" + wherestring + " ;", {
                    type: sequelize.QueryTypes.SELECT
                });
            } else {
                console.log("in else");
                return sequelize.query("SELECT distinct   " + columnnameto + "," + columnvalto + " FROM " + fromtable + "   where  " + where + "= '" + coldefaultval + "';", {
                    type: sequelize.QueryTypes.SELECT
                });
            }

        });
    }



    insertTableValues(tableName, columnName, valuename) {
        return sequelize.sync().then(() => {
            return sequelize.query("INSERT INTO " + tableName + " (" + columnName + ") VALUES(" + valuename + ")", {
                type: sequelize.QueryTypes.INSERT
            });
        });
    }


    formRenderDataSave(formId, renderForm, ip) {

        var formName = renderForm["formName"];
        var appId = renderForm["appId"];
        var createdBy = renderForm["createdBy"];
        var purposedTableName = renderForm["purposedTableName"];
        var columnString = "";
        var columnValueString = "";
        var scheduledId = renderForm["scheduledId"];

        renderForm["obj"].forEach(element => {
            columnString += element["name"] + ",";
            columnValueString += "'" + element["value"] + "',";
        });

        columnString = columnString.slice(0, -1);
        columnValueString = columnValueString.slice(0, -1);

        return sequelize.transaction().then(function (t) {
            return sequelize.query("INSERT INTO " + purposedTableName + " (" + columnString + ") VALUES(" + columnValueString + ")", {

                type: sequelize.QueryTypes.INSERT
            }, { transaction: t }).then(function () {

                return sequelize.query("select max(id) as id from " + purposedTableName, {
                    type: sequelize.QueryTypes.SELECT
                }, { transaction: t }).then(function (result) {
                    // console.log("result=", result[0]);

                    return sequelize.query("update " + purposedTableName + " set T_INT_Schedule_Meeting_Id='" + scheduledId + "', Created_By=" + "'" + createdBy + "' where id=" + result[0].id, {
                        type: sequelize.QueryTypes.UPDATE
                    }, { transaction: t }).then(function (resultdata) {


                        return sequelize.query("update TXN_Schedule_Meetings  set  T_CHR_In_Progress='1' where T_INT_Schedule_Meeting_Id='" + scheduledId + "'", {
                            type: sequelize.QueryTypes.UPDATE
                        }, { transaction: t }).then(function (resultdata) {

                            return resultdata;
                            t.commit()

                        }).catch(function (errLog) {

                            console.log("errMsg:", errLog)
                            log.addLog('model :: form.js ' + errLog);
                            var err;
                            if (errLog == "SequelizeUniqueConstraintError: Validation error") {
                                err = {
                                    data: 0,
                                    errMsg: "server error !"
                                }
                            } else if (errLog != "SequelizeUniqueConstraintError: Validation error") {
                                err = {
                                    data: 0,
                                    errMsg: errMsg
                                }

                            }
                            t.rollback()
                            return err;
                        });

                    }).catch(function (errLog) {
                        t.rollback()
                        console.log("errMsg:", errLog)
                        log.addLog('model :: form.js ' + errLog);
                        var err;
                        if (errLog == "SequelizeUniqueConstraintError: Validation error") {
                            err = {
                                data: 0,
                                errMsg: "server error !"
                            }
                        } else if (errLog != "SequelizeUniqueConstraintError: Validation error") {
                            err = {
                                data: 0,
                                errMsg: errMsg
                            }

                        }
                        return err;
                    });




                }).catch(function (errLog) {
                    t.rollback()
                    console.log("errMsg:", errLog)
                    log.addLog('model :: form.js ' + errLog);
                    var err;
                    if (errLog == "SequelizeUniqueConstraintError: Validation error") {
                        err = {
                            data: 0,
                            errMsg: "server error !"
                        }
                    } else if (errLog != "SequelizeUniqueConstraintError: Validation error") {
                        err = {
                            data: 0,
                            errMsg: errMsg
                        }

                    }
                    return err;
                });



            }).catch(function (errLog) {
                t.rollback()
                console.log("errMsg:", errLog)
                log.addLog('model :: form.js ' + errLog);
                var err;
                if (errLog == "SequelizeUniqueConstraintError: Validation error") {
                    err = {
                        data: 0,
                        errMsg: "server error !"
                    }
                } else if (errLog != "SequelizeUniqueConstraintError: Validation error") {
                    err = {
                        data: 0,
                        errMsg: errMsg
                    }

                }
                return err;
            });
        }).catch((errLog) => {
            t.rollback()
            log.addLog('model :: form.js ' + errLog);
            return errLog;

        });
    }

    formRenderDataDelete(formId, renderForm, ip) {

      
        var purposedTableName = renderForm["purposedTableName"];

        var scheduledId = renderForm["scheduledId"];

   

        return sequelize.transaction().then(function (t) {
            return sequelize.query("delete from " + purposedTableName + " where T_INT_Schedule_Meeting_Id='" + scheduledId + "'", {

                type: sequelize.QueryTypes.INSERT
            }, { transaction: t }).then(function () {

                return sequelize.query("update TXN_Schedule_Meetings  set  T_CHR_In_Progress='0' where T_INT_Schedule_Meeting_Id='" + scheduledId + "'", {
                    type: sequelize.QueryTypes.UPDATE
                }, { transaction: t }).then(function (resultdata) {

                    return resultdata;
                    t.commit()

                }).catch(function (errLog) {

                    console.log("errMsg:", errLog)
                    log.addLog('model :: form.js ' + errLog);
                    var err;
                    if (errLog == "SequelizeUniqueConstraintError: Validation error") {
                        err = {
                            data: 0,
                            errMsg: "server error !"
                        }
                    } else if (errLog != "SequelizeUniqueConstraintError: Validation error") {
                        err = {
                            data: 0,
                            errMsg: errMsg
                        }

                    }
                    t.rollback()
                    return err;
                });

            }).catch(function (errLog) {
                t.rollback()
                console.log("errMsg:", errLog)
                log.addLog('model :: form.js ' + errLog);
                var err;
                if (errLog == "SequelizeUniqueConstraintError: Validation error") {
                    err = {
                        data: 0,
                        errMsg: "server error !"
                    }
                } else if (errLog != "SequelizeUniqueConstraintError: Validation error") {
                    err = {
                        data: 0,
                        errMsg: errMsg
                    }

                }
                return err;
            });
        }).catch((errLog) => {
            t.rollback()
            log.addLog('model :: form.js ' + errLog);
            return errLog;

        });
    }

    formRenderDeleteDynamicTable(actionTableName, columName,scheduledId) {
        console.log("cccccccc=", actionTableName, columName)
        var purposedTableName = actionTableName;
      
        var scheduledId=scheduledId;
      
        return sequelize.sync().then(() => {
            return sequelize.query("delete from " + purposedTableName + " where T_INT_Schedule_Meeting_Id='"+scheduledId+"'", {

                type: sequelize.QueryTypes.INSERT
            }).then(function (data) {
                
                return data;
            }).catch(function (errLog) {
                console.log("errMsg:", errLog)
                log.addLog('model :: form.js ' + errLog);
                var err;
                if (errLog == "SequelizeUniqueConstraintError: Validation error") {
                    err = {
                        data: 0,
                        errMsg: "server error !"
                    }
                } else if (errLog != "SequelizeUniqueConstraintError: Validation error") {
                    err = {
                        data: 0,
                        errMsg: errMsg
                    }

                }
                return err;
            });
        }).catch((errLog) => {
            log.addLog('model :: form.js ' + errLog);
            return errLog;

        });

    }

    formRenderSaveDynamicTable(actionTableName, columName, columnValue,scheduledId) {
        console.log("cccccccc=", actionTableName, columName, columnValue)
        var purposedTableName = actionTableName;
        var columnString = "";
        var columnValueString = "";
        var arr = columnValue.split(",")
        arr.forEach(element => {
            //  columnString += element["name"] + ",";
            columnValueString += "'" + element + "',";
        });
        columnValueString += "'" + scheduledId + "',";
        columnValueString = columnValueString.slice(0, -1);
        return sequelize.sync().then(() => {
            return sequelize.query("INSERT INTO " + purposedTableName + " ( " + columName + " , T_INT_Schedule_Meeting_Id) VALUES(" + columnValueString + ")", {

                type: sequelize.QueryTypes.INSERT
            }).then(function (data) {
                
                return data;
            }).catch(function (errLog) {
                console.log("errMsg:", errLog)
                log.addLog('model :: form.js ' + errLog);
                var err;
                if (errLog == "SequelizeUniqueConstraintError: Validation error") {
                    err = {
                        data: 0,
                        errMsg: "server error !"
                    }
                } else if (errLog != "SequelizeUniqueConstraintError: Validation error") {
                    err = {
                        data: 0,
                        errMsg: errMsg
                    }

                }
                return err;
            });
        }).catch((errLog) => {
            log.addLog('model :: form.js ' + errLog);
            return errLog;

        });

    }

    formRenderDataUpdate(formId, scheduledId, renderForm, ip) {
        var praposed_table_name = renderForm["praposed_table_name"];
        var ActionColumn = renderForm["ActionColumn"]
        var columnString = "";
        var columnValueString = "";
        var date = new Date().toJSON().slice(0, 10).replace(/-/g, '-');
        // console.log("date", date);

        var Updated_By = renderForm["createdBy"];

        renderForm["obj"].forEach(element => {
            columnValueString += element["name"] + "='" + element["value"] + "',";
        });
        columnValueString = columnValueString.slice(0, -1);


        return sequelize.sync().then(() => {
            return sequelize.query("UPDATE " + praposed_table_name + " SET " + columnValueString + " WHERE T_INT_Schedule_Meeting_Id = " + "'" + scheduledId + "'" + "; ", {
                type: sequelize.QueryTypes.UPDATE
            }).then(function () {
                return sequelize.query("update " + praposed_table_name + " set  Updated_By=" + "'" + Updated_By + "'" + ", Updated_At=" + "'" + date + "'" + " WHERE T_INT_Schedule_Meeting_Id ='" + scheduledId + "'; ", {
                    type: sequelize.QueryTypes.UPDATE
                }).then(function (resultdata) {
                    //console.log("resultdata=", resultdata);
                    return resultdata;
                }).catch(function (errLog) {

                    console.log("errMsg:", errLog)
                    log.addLog('model :: form.js ' + errLog);
                    var err;
                    if (errLog == "SequelizeUniqueConstraintError: Validation error") {
                        err = {
                            data: 0,
                            errMsg: "server error !"
                        }
                    } else if (errLog != "SequelizeUniqueConstraintError: Validation error") {
                        err = {
                            data: 0,
                            errMsg: errMsg
                        }

                    }
                   
                    return err;
                });
            }).catch(function (errLog) {

                console.log("errMsg:", errLog)
                log.addLog('model :: form.js ' + errLog);
                var err;
                if (errLog == "SequelizeUniqueConstraintError: Validation error") {
                    err = {
                        data: 0,
                        errMsg: "server error !"
                    }
                } else if (errLog != "SequelizeUniqueConstraintError: Validation error") {
                    err = {
                        data: 0,
                        errMsg: errMsg
                    }

                }
              
                return err;
            });

        }).catch(function (errLog) {

            console.log("errMsg:", errLog)
            log.addLog('model :: form.js ' + errLog);
            var err;
            if (errLog == "SequelizeUniqueConstraintError: Validation error") {
                err = {
                    data: 0,
                    errMsg: "server error !"
                }
            } else if (errLog != "SequelizeUniqueConstraintError: Validation error") {
                err = {
                    data: 0,
                    errMsg: errMsg
                }

            }
           
            return err;
        });

    }
    
    getSelectedAppForm(AppId) {
        return sequelize.sync().then(() => {
            return this.FORMGROUP.findAll({
                where: {
                    T_INT_Meeting_Id: {
                        [Op.in]: AppId
                    }
                }
            });
        });
    }

    getMenuForm(menu_id) {
        return sequelize.sync().then(() => {
            return sequelize.query("SELECT 	fm.* FROM menu_masters as mm inner join  form_masters as fm on(mm.form_id=fm.form_id) WHERE mm.menu_id=" + menu_id + " ;", {
                type: sequelize.QueryTypes.SELECT
            });
        }).then(function (x) {
            return [x[0].T_CHR_Action_Table_Name, x[0].form_id, x[0].action_type, x[0].proposed_Table_Name, x[0].T_CHR_Template_Title];
        });
    };



    getFormData(T_CHR_Action_Table_Name, userId) {

        return sequelize.sync().then(() => {
            return sequelize.query("SELECT id from " + T_CHR_Action_Table_Name + " where Created_By= '" + userId + "';", {
                type: sequelize.QueryTypes.SELECT
            });
        });
    }



    getFormDataForUpdate(T_CHR_Action_Table_Name) {
        return sequelize.sync().then(() => {
            return sequelize.query("SELECT id from " + T_CHR_Action_Table_Name + ";", {
                type: sequelize.QueryTypes.SELECT
            });
        });
    }

    getDashboardTotalCount(tablename) {

        return sequelize.sync().then(() => {
            return sequelize.query("SELECT * from " + tablename + ";", {
                type: sequelize.QueryTypes.SELECT
            });
        });
    }

    getDashboardPendingCount(tablename) {
        return sequelize.sync().then(() => {
            return sequelize.query("SELECT * from " + tablename + " " + "where Project_Current_Status='10' or Project_Current_Status='11' or Project_Current_Status='12' or status='Pending'  ", {
                type: sequelize.QueryTypes.SELECT
            });
        });
    }


    getDashboardRejectedCount(tablename) {
        return sequelize.sync().then(() => {
            return sequelize.query("SELECT * from " + tablename + " " + "where Status='Rejected';", {
                type: sequelize.QueryTypes.SELECT
            });
        });
    }

    getDashboardCompletedCount(tablename) {
        return sequelize.sync().then(() => {
            return sequelize.query("SELECT * from " + tablename + " " + "where Status='Approved' ", {
                type: sequelize.QueryTypes.SELECT
            });
        });
    }

    getTableName() {
        //console.log("data table",typeof(data));
        return sequelize.sync().then(() => {
            return sequelize.query("select TABLE_NAME from INFORMATION_SCHEMA.TABLES ;", { type: sequelize.QueryTypes.SELECT });
        });
    }

    getTableNames(data) {
        console.log("data table", typeof (data));
        if (Object.keys(data).length === 0) {
            return sequelize.sync().then(() => {
                return sequelize.query("select TABLE_NAME from INFORMATION_SCHEMA.TABLES ;", { type: sequelize.QueryTypes.SELECT });
            });
        } else {
            this.getOtherConnection(data);

            return Sequelize1.sync().then(() => {
                return Sequelize1.query("select TABLE_NAME from INFORMATION_SCHEMA.TABLES ;", { type: sequelize.QueryTypes.SELECT });
            });
        }

    }

    getTableColumn(tableName, data) {

        if (Object.keys(data).length === 0) {
            return sequelize.sync().then(() => {
                return sequelize.query("select COLUMN_NAME from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME='" + tableName + "' ;", { type: sequelize.QueryTypes.SELECT });
            });
        } else {
            this.getOtherConnection(data);

            return Sequelize1.sync().then(() => {
                return Sequelize1.query("select COLUMN_NAME from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME='" + tableName + "' ;", { type: sequelize.QueryTypes.SELECT });
            });
        }
    }


    getActionTableColumns(tableName) {

        return sequelize.sync().then(() => {
            return sequelize.query("select COLUMN_NAME from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME='" + tableName + "' ;", { type: sequelize.QueryTypes.SELECT });
        });

    }

    getViewNames() {
        return sequelize.sync().then(() => {
            return sequelize.query("select TABLE_NAME from INFORMATION_SCHEMA.VIEWS ;", { type: sequelize.QueryTypes.SELECT });
        });
    }

    getViewName(data) {

        if (Object.keys(data).length === 0) {
            return sequelize.sync().then(() => {
                return sequelize.query("select TABLE_NAME from INFORMATION_SCHEMA.VIEWS ;", { type: sequelize.QueryTypes.SELECT });
            });
        } else {
            this.getOtherConnection(data);

            return Sequelize1.sync().then(() => {
                return Sequelize1.query("select TABLE_NAME from INFORMATION_SCHEMA.VIEWS ;", { type: sequelize.QueryTypes.SELECT });
            });
        }
    }

    getTableColumns(tableName) {
        return sequelize.sync().then(() => {
            return sequelize.query("select COLUMN_NAME from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME='" + tableName + "' ;", { type: sequelize.QueryTypes.SELECT });
        });
    }

    getTableColumnsLabelData(tableName, columnNameLabel, columnNameValue, data) {
        this.getOtherConnection(data);

        return Sequelize1.sync().then(() => {

            return Sequelize1.query("select  distinct  top 100 " + columnNameLabel + "," + columnNameValue + " from " + tableName + " ;", { type: sequelize.QueryTypes.SELECT });
        });
    }

    getTableColumnsLabelDataDynamiCTable(tableName, columnNameLabel, columnNameValue, where,searchText) {
       
        console.log("wwwwwwww=",where,searchText);
        if(where=="where "){
            console.log("innnnnnnnnnnnn")
            where="";
            if(searchText=="not"){
                
            }else{
                where="where "+columnNameLabel+" like '%"+searchText+"%'"
            }
        }else{
            if(searchText=="not"){
            }else{
                where=where +" and "+columnNameLabel+" like '%"+searchText+"%'"
            }
        }

        return sequelize.sync().then(() => {

            return sequelize.query("select  distinct  top 50 " + columnNameLabel + "," + columnNameValue + " from " + tableName +" "+ where , { type: sequelize.QueryTypes.SELECT });
        });
    }

    getTableColumnsLabelDataPrevious(tableName, columnNameLabel, columnNameValue, data, arr) {
        this.getOtherConnection(data);


        return Sequelize1.sync().then(() => {

            return Sequelize1.query("select  distinct  top 100 " + columnNameLabel + "," + columnNameValue + " from " + tableName + " where " + columnNameValue + " in (" + arr + ")  or " + columnNameLabel + "  in  (" + arr + ");", { type: sequelize.QueryTypes.SELECT });
        });
    }

    getTableColumnsLabelDataFilter(tableName, columnNameLabel, columnNameValue, data, value) {
        this.getOtherConnection(data);

        return Sequelize1.sync().then(() => {

            return Sequelize1.query("select  distinct   " + columnNameLabel + "," + columnNameValue + " from " + tableName + "  where  " + columnNameLabel + "  like '%" + value + "%' ;", { type: sequelize.QueryTypes.SELECT });
        });
    }

    getTableColumnsLabelDatawhere(tableName, columnNameLabel, columnNameValue, whereString) {

        return sequelize.sync().then(() => {
            return sequelize.query("select  distinct top 100 " + columnNameLabel + "," + columnNameValue + " from " + tableName + "  where  " + whereString + " ;", { type: sequelize.QueryTypes.SELECT });
        });
    }
    getTableColumnsLabelDatawherePrevious(tableName, columnNameLabel, columnNameValue, whereString, selectarr) {

        return sequelize.sync().then(() => {
            return sequelize.query("select  distinct top 100 " + columnNameLabel + "," + columnNameValue + " from " + tableName + "  where  (" + whereString + ") and  " + columnNameValue + " in (" + selectarr + ");", { type: sequelize.QueryTypes.SELECT });
        });
    }
    getTableColumnsLabelDatawhereFilter(tableName, columnNameLabel, columnNameValue, whereString, value) {

        return sequelize.sync().then(() => {
            return sequelize.query("select  distinct top 100 " + columnNameLabel + "," + columnNameValue + "  from " + tableName + "  where  (" + whereString + ") and " + columnNameLabel + " like '%" + value + "%' ;", { type: sequelize.QueryTypes.SELECT });
        });
    }
    getTableColumnsValueData(tableName, columnName) {
        return sequelize.sync().then(() => {
            return sequelize.query("select  distinct  top 100 " + columnName + " from " + tableName + " ;", { type: sequelize.QueryTypes.SELECT });
        });
    }



    tableDetails(table, columns) {
        return sequelize.sync().then(() => {
            return sequelize.query("SELECT distinct  " + columns + " FROM " + table + ";", {
                type: sequelize.QueryTypes.SELECT
            });
        });
    }

    getTableData(table, columns, wherestring, data) {

        this.getOtherConnection(data);

        console.log("query :SELECT distinct   " + columns + " FROM " + table + " where " + wherestring + ";");
        return Sequelize1.sync().then(() => {
            if (wherestring == 'select') {
                return Sequelize1.query("SELECT distinct   " + columns + " FROM " + table + ";", {

                    type: sequelize.QueryTypes.SELECT
                });
            } else {
                return Sequelize1.query("SELECT distinct   " + columns + " FROM " + table + " where " + wherestring + ";", {
                    type: sequelize.QueryTypes.SELECT
                });
            }

        });
    }

    getMaxCount(tableName, columnName, data) {

        this.getOtherConnection(data);

        return Sequelize1.sync().then(() => {
            return Sequelize1.query("select max(" + columnName + ") as count from " + tableName + " ;", { type: sequelize.QueryTypes.SELECT });
        });


    }

    getMaxCountAdvanced(tableName, columnName, whrcon) {
        return sequelize.sync().then(() => {
            var array = columnName.split(",");
            var data = "";
            //   for(var i=0;i<array.length;i++){



            return sequelize.query("select " + columnName + "  from " + tableName + "" + "  where " + "" + "" + whrcon + ";", { type: sequelize.QueryTypes.SELECT });
            // console.log("i=",i,"len=",array.length);
            //                 if(i==array.length-1){
            //                     condition+="max("+array[i]+")";
            //                 }else{
            //                 condition+="max("+array[i]+"),";
            //             }
            // }
            //console.log("akakakakakak",sequelize.query("select max(" + columnName + ") as count from " + tableName + " ;", { type: sequelize.QueryTypes.SELECT }));

        });


    }

    getDatabaseNames() {
        return sequelize.sync().then(() => {
            return sequelize.query("select connection_id,connection_name from connection_masters ;", { type: sequelize.QueryTypes.SELECT });
        });
    }
    getDatabaseById(appId) {
        return sequelize.sync().then(() => {
            return sequelize.query("select * from connection_masters where connection_id=1", { type: sequelize.QueryTypes.SELECT });
        });
    }
    foundOrNot() {
        return sequelize.sync().then(() => {
            return sequelize.query("SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_TYPE='BASE TABLE' ;", { type: sequelize.QueryTypes.SELECT });
        });
    }


    getPMSProjectMaster(userId) {
        return sequelize.sync().then(() => {
            return sequelize.query("select * from PMS_Project_Master", { type: sequelize.QueryTypes.SELECT });
        });
    }
    getOtherConnection(data) {
        var custCon = new customCon();
        var con1;
        var hoste;
        var databasee;
        var usernamee;
        var passworde;
        var porte;
        data['responseData'].forEach(element => {
            hoste = element['host'];
            databasee = element['database'];
            usernamee = element['username'];
            passworde = element['password'];
            porte = element['port'];
        });
        return Sequelize1 = custCon.connectDatabase(hoste, databasee, usernamee, passworde, porte);
    }

    getParentTableColumns(tableName) {
        return sequelize.sync().then(() => {
            return sequelize.query("select COLUMN_NAME from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME='" + tableName + "' ;", { type: sequelize.QueryTypes.SELECT });
        });
    }

    getMaxVerion(app_id_param) {
        return sequelize.sync().then(() => {
            return sequelize.query("select form_id as form_id from Meeting_Templates where T_INT_Meeting_Id='" + app_id_param + "' ;", { type: sequelize.QueryTypes.SELECT });
        });
    }

    getMeetingTemplate(data) {
        return sequelize.sync().then(() => {
            return sequelize.query("select mt.form_id,mt.T_CHR_Template_Name,mm.T_CHR_Meeting_Attendies from Meeting_Templates as mt inner join M_Meeting_Masters as mm on mm.T_INT_Meeting_Id=mt.T_INT_Meeting_Id where mt.T_INT_Meeting_Id='" + data.meetingId + "'  ;", { type: sequelize.QueryTypes.SELECT });
        });
    }
}


    // formRenderDataSave(formId, renderForm) {
    //     console.log("hiiii");
    //     var formName = renderForm["formName"];
    //     var appId = renderForm["appId"];
    //     var purposedTableName = renderForm["purposedTableName"];
    //     var columnString = "";
    //     var columnValueString = "";
    //     //Done By KD
    //     var listColunm = [];
    //     var listValue = [];
    //     var newcolumnValueString = "";
    //     var newValues = "";
    //     var newValues1 = "";

    //     var stName = "";
    //     var ob = [];
    //     var ob2 = [];

    //     var jsonvalu = "{";
    //     var jsonvalu2 = "{";
    //     renderForm["obj"].forEach(element => {


    //         if (listColunm.indexOf(element["name"]) !== -1) {} else {
    //             newcolumnValueString += element["name"] + ",";
    //             listColunm.push(element["name"]);

    //         }
    //         if (typeof (element["value"]) == typeof (ob)) {
    //             listValue = element["value"];
    //             listValue.forEach(element1 => {

    //                 jsonvalu += "\"" + element["name"] + "\":\"" + element1 + "\"},";
    //                 jsonvalu = jsonvalu.slice(0, -1);
    //                 console.log("JSON===============", jsonvalu);
    //                 var jsonobject = JSON.parse(jsonvalu);
    //                 console.log("jsonobject===============", jsonobject);

    //                 ob2.push(jsonobject);
    //                 jsonobject = {};
    //                 jsonvalu = "";
    //                 jsonvalu = jsonvalu2;

    //             });
    //             console.log("Array=======", ob2);
    //         } else {
    //             jsonvalu += "\"" + element["name"] + "\":\"" + element["value"] + "\",";
    //             jsonvalu2 = jsonvalu;
    //             newValues += "'" + element["value"] + "',";
    //             newValues1 += "'" + element["value"] + "',";
    //         }

    //         columnString += element["name"] + ",";
    //         columnValueString += "'" + element["value"] + "',";
    //     });
    //     console.log("newcolumnValueString===========", newValues);

    //     columnString = columnString.slice(0, -1);
    //     columnValueString = columnValueString.slice(0, -1);
    //     console.log("columnString=" + columnString);
    //     console.log("columnValueString=" + columnValueString);
    //     var newData = [];

    //     var str = "";

    //     listValue.forEach(element => {
    //         //  newValues += "'" + element + "')(";

    //         str += newValues + "'" + element + "'),(";
    //         console.log("==================", str);

    //         // newValues = newValues.slice(0, -1);
    //         newValues = "";
    //         newValues = newValues1;
    //     });
    //     console.log("INSERT INTO " + purposedTableName + " (" + columnString + ") VALUES(" + str.slice(0, -2) + "", );
    //     return sequelize.sync().then(() => {
    //         sequelize.query("INSERT INTO " + purposedTableName + " (" + columnString + ") VALUES(" + str.slice(0, -2) + "", {
    //             type: sequelize.QueryTypes.INSERT
    //         })
    //     });




    //     // sequelize.query("INSERT INTO " + purposedTableName + " (" + columnString + ") VALUES(" + newValues + ")", {
    //     //     type: sequelize.QueryTypes.INSERT
    //     // })
    //     newValues = "";
    //     newValues = newValues1;
    //     return newData
    // }
