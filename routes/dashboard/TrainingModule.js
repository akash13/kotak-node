var express = require('express');
var multer = require('multer');
var path = require('path');
var router = express.Router();
var bcrypt = require('bcryptjs');
var jwt = require('jsonwebtoken');
const Joi = require('joi');
var TrainingRoute = require('../../model/TrainingModule')
var app = express();
var path = require('path')
var options = {
    wsdl_headers: {
        'Authorization': 'Basic ' + new Buffer('piwebusr' + ':' + 'piproxy123').toString('base64'),
    }
};
app.use(express.static(path.join(__dirname, 'uploads')));


router.get('/getEmployee', function (req, res, next) {

    var request = new TrainingRoute();
    var module = req.params.module;
    var level = req.params.level;
    var subject = req.params.subject;
    request.getEmployee().then((data) => {
        buildResponse(res, 200, {
            error: false,
            message: "Successfully display!",
            responseData: data
        });
    });
});


router.post('/getReportTDL', function (req, res, next) {
    var user = new TrainingRoute();
    var data = req.body;
  //  console.log("data", data);
    user.getReportTDL(data).then((data) => {
        buildResponse(res, 200, {
            error: false,
            message: "Successfully display!",
            TableData: data


        });
    });
});

router.get('/getLAB/', function (req, res, next) {

    var request = new TrainingRoute();

    request.getLAB().then((data) => {
        buildResponse(res, 200, {
            error: false,
            message: "Successfully display!",
            responseData: data
        });
    });
});

router.get('/getModule/:level', function (req, res, next) {

    var request = new TrainingRoute();
    var level = req.params.level;
    request.getModule(level).then((data) => {
        buildResponse(res, 200, {
            error: false,
            message: "Successfully display!",
            responseData: data
        });
    });
});

router.get('/getSubject/:module/:level', function (req, res, next) {

    var request = new TrainingRoute();
    var module = req.params.module;
    var level = req.params.level;
    request.getSubject(module, level).then((data) => {
        buildResponse(res, 200, {
            error: false,
            message: "Successfully display!",
            responseData: data
        });
    });
});

router.get('/getuserpdfdata/:id', function (req, res, next) {

    var request = new TrainingRoute();

    var id = req.params.id;
    request.getuserpdfdata(id).then((data) => {
        buildResponse(res, 200, {
            error: false,
            message: "Successfully display!",
            responseData: data
        });
    });
});


router.get('/getLevel/', function (req, res, next) {

    var request = new TrainingRoute();

    request.getLevel().then((data) => {
        buildResponse(res, 200, {
            error: false,
            message: "Successfully display!",
            responseData: data
        });
    });
});



router.get('/getUploadedFileDetails/:id', function (req, res, next) {

    var request = new TrainingRoute();
    var id = req.params.id;
    request.getUploadedFileDetails(id).then((data) => {
        buildResponse(res, 200, {
            error: false,
            message: "Successfully display!",
            responseData: data
        });
    });
});



router.get('/getListSubModLevdata/:id', function (req, res, next) {

    var request = new TrainingRoute();
    var id = req.params.id;
    request.getListSubModLevdata(id).then((redata) => {
        buildResponse(res, 200, {
            error: false,
            message: "Successfully display!",
            responseData: redata
        });
    });
});

router.get('/getListDialogdata/:id', function (req, res, next) {

    var request = new TrainingRoute();
    var id = req.params.id;
    request.getListDialogdata(id).then((redata) => {
        buildResponse(res, 200, {
            error: false,
            message: "Successfully display!",
            responseData: redata
        });
    });
});

router.get('/deleteUploadedFile/:id/:filename', function (req, res, next) {

    var request = new TrainingRoute();

    var fs = require('fs');
    var filename ='./uploads/'+ req.params.filename;
 
    console.log('filename',filename)
// delete file named 'sample.txt'
fs.unlink(filename, function (err) {
    if (err) throw err;
    // if no error, file has been deleted successfully
    console.log('File deleted!');
}); 
    var id = req.params.id;
    request.deleteUploadedFile(id).then((redata) => {
        buildResponse(res, 200, {
            error: false,
            message: "Successfully display!",
            responseData: redata
        });
    });
});


router.post('/updateCompletionDate', function (req, res, next) {

    var request = new TrainingRoute();
    var data = req.body;
    request.updateCompletionDate(data).then((resdata) => {
        buildResponse(res, 200, {
            error: false,
            message: "Successfully Inserted!",
            responseData: resdata
        });
    });
});
router.post('/deleteexcelRow', function (req, res, next) {

    var request = new TrainingRoute();
    var data = req.body;
    request.deleteexcelRow(data).then((resdata) => {
        buildResponse(res, 200, {
            error: false,
            message: "Successfully Inserted!",
            responseData: resdata
        });
    });
});

router.get('/bydefaultaccessto/:element/:value', function (req, res, next) {

    var request = new TrainingRoute();
    var id = req.params.element;
    var value = req.params.value;
    
    request.bydefaultaccessto(id,value).then((data) => {

    
       
      // console.log("data", data);
        buildResponse(res, 200, {
            error: false,
            message: "Successfully display!",
            responseData: data
        });
    });  
});


router.post('/SubmitData', function (req, res, next) {

    var request = new TrainingRoute();
    var data = req.body;
    request.SubmitData(data).then((data) => {
        buildResponse(res, 200, {
            error: false,
            message: "Successfully Inserted!",
            responseData: data
        });
    });
});




router.get('/getdetailsForEmployeeDashboardForSubject/:id', function (req, res, next) {

    var request = new TrainingRoute();
    var id = req.params.id;
    var FinalArray = [];
    request.getdetailsForEmployeeDashboardForSubject(id).then((data) => {

    
       
       //console.log("data", data);
        buildResponse(res, 200, {
            error: false,
            message: "Successfully display!",
            responseData: data
        });
    });  
});
router.get('/getdetailsForEmployeeDashboard/:id', function (req, res, next) {

    var request = new TrainingRoute();
    var id = req.params.id;
    var FinalArray = [];
    request.getdetailsForEmployeeDashboard(id).then((data) => {

    
       
       //console.log("data", data);
        buildResponse(res, 200, {
            error: false,
            message: "Successfully display!",
            responseData: data
        });
    });  
});



router.get('/getModuleDataForEmployeeDashboard/:level/:module', function (req, res, next) {

    var request = new TrainingRoute();
    var levelid = req.params.level;
    var moduleid = req.params.module;
    request.getModuleDataForEmployeeDashboard(levelid,moduleid).then((data) => {
console.log("module=",data);
            
        buildResponse(res, 200, {
            error: false,
            message: "Successfully display!",
            responseData: data
        });
    });  
});
router.get('/getSubjectDataForEmployeeDashboard/:level/:module/:subject', function (req, res, next) {

    var request = new TrainingRoute();
    var levelid = req.params.level;
    var moduleid = req.params.module;
    var subjectid = req.params.subject;

    request.getSubjectDataForEmployeeDashboard(levelid,moduleid,subjectid).then((data) => {

        console.log("subject=",data);   
        buildResponse(res, 200, {
            error: false,
            message: "Successfully display!",
            responseData: data
        });
    });  
});

router.post('/uploadEnrollData', function (req, res, next) {


    var request = new TrainingRoute();
    var id = req;

    console.log("data", req.file);
    // insertDocuments(db, 'public/images/uploads/' + req.file.filename, () => {
    //     db.close();
    //     res.json({'message': 'File uploaded successfully'});
    // });
});
// request.uploadEnrollData(id).then((data) => {
//     console.log("data",data);
//     buildResponse(res, 200, {
//         error: false,
//         message: "Successfully display!",
//         responseData: data
//     });
// });


router.get('/getdetailsforUpdateEnrollData/:id', function (req, res, next) {

    var request = new TrainingRoute();
    var id = req.params.id;
    request.getdetailsforUpdateEnrollData(id).then((data) => {
        console.log("data", data);
        buildResponse(res, 200, {
            error: false,
            message: "Successfully display!",
            responseData: data
        });
    });
});
router.get('/getdefaultdetailsForEmployeeDashboard/:id', function (req, res, next) {

    var request = new TrainingRoute();
    var id = req.params.id;
    request.getdefaultdetailsForEmployeeDashboard(id).then((data) => {
        console.log("data", data);
        buildResponse(res, 200, {
            error: false,
            message: "Successfully display!",
            responseData: data
        });
    });
});

router.get('/getdetails', function (req, res, next) {

    var request = new TrainingRoute();

    request.getdetails().then((data) => {
        console.log("data", data);
        buildResponse(res, 200, {
            error: false,
            message: "Successfully display!",
            responseData: data
        });
    });
});

router.get('/getdetails', function (req, res, next) {

    var request = new TrainingRoute();

    request.getdetails().then((data) => {
        console.log("data", data);
        buildResponse(res, 200, {
            error: false,
            message: "Successfully display!",
            responseData: data
        });
    });
});
router.get('/getFileDirectoryList', function (req, res, next) {

    var request = new TrainingRoute();

    const testFolder =  './uploads/';
    const fs = require('fs');
    var data=[];

    fs.readdirSync(testFolder).forEach(file => {
      console.log("===",file);
      data.push(file);
    });
    
    buildResponse(res, 200, {
                error: false,
                message: "Successfully display!",
                responseData: data
            });
    // request.getFileDirectoryList().then((data) => {
    //     console.log("data", data);
    //     buildResponse(res, 200, {
    //         error: false,
    //         message: "Successfully display!",
    //         responseData: data
    //     });
    // });
});



router.get('/getdetailsTrainingData', function (req, res, next) {

    var request = new TrainingRoute();

    request.getdetailsTrainingData().then((data) => {
        console.log("data", data);
        buildResponse(res, 200, {
            error: false,
            message: "Successfully display!",
            responseData: data
        });
    });
});


router.post('/SubmitEnrollData', function (req, res, next) {

    var request = new TrainingRoute();
    var data = req.body;
    request.SubmitEnrollData(data).then((data) => {
        buildResponse(res, 200, {
            error: false,
            message: "Successfully Inserted!",
            responseData: data
        });
    });
});





router.post('/UpdateEnrollData', function (req, res, next) {

    var request = new TrainingRoute();
    var data = req.body;
    request.UpdateEnrollData(data).then((data) => {
        buildResponse(res, 200, {
            error: false,
            message: "Successfully Inserted!",
            responseData: data
        });
    });
});

router.post('/SubmitUploadData', function (req, res, next) {

    var request = new TrainingRoute();
    var data = req.body;
    request.SubmitUploadData(data).then((data) => {
        buildResponse(res, 200, {
            error: false,
            message: "Successfully Inserted!",
            responseData: data
        });
    });
});
router.post('/SubmitbulkUploadData', function (req, res, next) {

    var request = new TrainingRoute();
    var data = req.body;
    request.SubmitbulkUploadData(data).then((data) => {

        console.log("data=",data);
        buildResponse(res, 200, {
            error: false,
            message: "Successfully Inserted!",
            responseData: data
        }).catch((err) => {
            buildResponse(res, 304, {
                error: true,
               message: "Can not Insert Duplicate file"
              });
          });
    });
});

//////////////////////////////// Declaration ////////////////
var multer = require('multer')

const UPLOADDIR = './uploads';

let storageupload = multer.diskStorage({
  

    destination: (req, file, cb) => {
        console.log(" file received", file.fieldname);
        cb(null, UPLOADDIR);
    },
    filename: (req, file, cb) => {
        console.log("No file received", file.fieldname);

        cb(null, file.originalname);
    }
});
let upload = multer({

    storage: storageupload
});
router.post('/uploadTrainingFolder/', /*upload.single('photo')*/ upload.array("uploads[]", 12), function (req, res) {
//console.log('req.files',req.files);


var request = new TrainingRoute();
    var data = req.files;
    var count=0;
var errordata=[];
    //console.log("data=",data);
var date=new Date();
    // if (!req.files) {
    //     return res.send({
    //         success: false,
    //     });
    // } else {
    //     return res.send({
    //         success: true,
    //         files: req.files
    //     })

    data.forEach((file)=>{
       
    request.SubmitbulkUploadData(file,date).then((resdata) => {
        count++;
        console.log('file.originalname',file.originalname);
        console.log(file.originalname!=resdata);
        if(file.originalname==resdata){
            errordata.push(resdata);
        }
        // console.log('resdata',resdata);
         
            
            console.log('errordata=',errordata)
        //  //   var index=errordata.indexOf(1);
        //     var arrindex=errordata.indexOf('1');

        //     console.log('arrindex',arrindex);
            // if( arrindex !=-1){
            //     //errordata.splice(index,1)
            //     errordata.splice(arrindex,1)
            // }
     

if(count==req.files.length){

    buildResponse(res, 200, {
        error: true,
        message: "File Already Uploaded",
        responseData: errordata,
    });
}
    });
});

        // console.log("data=",resdata);
        // if (resdata == 0) {
        //     buildResponse(res, 200, {
        //         error: true,
        //         message: "File Already Uploaded",
        //         responseData: resdata,
        //     });
        // }
        // buildResponse(res, 200, {
        //     error: false,
        //     message: "Uploaded Successfully",
        //     responseData: resdata,
        // });

    // }).catch((err) => {
    //     buildResponse(res, 200, {
    //         error: true,
    //         message: "File Already Uploaded",
    //         responseData: resdata,
    //     });
    // });          

      
    

    

  
});


router.get('/getfileData/:lab', function (req, res, next) {

    var request = new TrainingRoute();
    var lab = req.params.lab;

});
router.get('/getCountForFiles/:level/:module/:subject', function (req, res, next) {

    var request = new TrainingRoute();
    var level = req.params.level;
    var module = req.params.module;
    var subject = req.params.subject;


    request.getCountForFiles(level, module, subject).then((data) => {
        buildResponse(res, 200, {
            error: false,
            message: "Successfully display!",
            responseData: data
        });
    });
});
router.get('/getClassRoom/:lab', function (req, res, next) {

    var request = new TrainingRoute();
    var lab = req.params.lab;
    request.getClassRoom(lab).then((data) => {
        buildResponse(res, 200, {
            error: false,
            message: "Successfully display!",
            responseData: data
        });
    });
});
function buildResponse(res, statusCode, responseData) {
    res.status(statusCode).json(responseData);
}
var insertDocuments = function (db, filePath, callback) {
    var collection = db.collection('user');
    collection.insertOne({ 'imagePath': filePath }, (err, result) => {
        assert.equal(err, null);
        callback(result);
    });
}

module.exports = router;