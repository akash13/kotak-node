var express = require('express');
var router = express.Router();
var bcrypt = require('bcryptjs');
var jwt = require('jsonwebtoken');
const Joi = require('joi');
var employeeBranchMappingMaster = require('../model/employee_Branch_Mapping');

var myLogger = require('../middleware.js');
var MyLogModel = require('../model/myLog');
var log = new MyLogModel();





const schema = Joi.object().keys({
 
    Added_By: Joi.string().max(20).required().allow(''),
    Create_Date: Joi.string().allow(''),
    Employee_Code: Joi.number().required().allow(''),
    Branch_Code:Joi.number().required().allow(''),
    Br_Loc_Map_Status: Joi.string().max(1).required().allow(''),



});


const schema1 = Joi.object().keys({

 
  
    Employee_Code: Joi.number().required().allow(''),
    Branch_Code:Joi.number().required().allow(''),
    Br_Loc_Map_Status: Joi.string().max(1).required().allow(''),
    Txn_ID: Joi.number().required().allow(''),

});

router.post('/saveEmployeeBranchMapping', function (req, res, next) {
    var employee_Branch_Mapping_Master = new employeeBranchMappingMaster();
    var employeeBranchMappingData = {
        Added_By: req.body.Added_By,
        Create_Date: req.body.Create_Date,
        Employee_Code: req.body.Employee_Code,
        Branch_Code: req.body.Branch_Code,
        Br_Loc_Map_Status: req.body.Br_Loc_Map_Status,
    
    };
    
    Joi.validate({
        Added_By: req.body.Added_By,
        Create_Date: req.body.Create_Date,
        Employee_Code: req.body.Employee_Code,
        Branch_Code: req.body.Branch_Code,
        Br_Loc_Map_Status: req.body.Br_Loc_Map_Status,
    }, schema, function (err, schemeEmployeeBranchMappingData) {
        if (err) {
            console.log(err)
            buildResponse(res, 200, {
                error: true,
                message: err["message"]
            });
        }
        else {
            employee_Branch_Mapping_Master.saveEmployeeBranchMapping(employeeBranchMappingData)
                .then((data) => {
                 
                    if (data["data"] == 0) {
                        buildResponse(res, 200, {
                            error: true,
                            message: data["errMsg"]
                        });
                    } else {
                        buildResponse(res, 200, {
                            error: false,
                            message: "Stored Successfully"
                        });

                    }

                }).catch((errLog) => {

                    log.addLog('route :: Employee_Branch_Mapping.js ' + errLog);
                    buildResponse(res, 500, {
                        error: true,
                        message: "server error"
                    });
                });
        }
    });
});

router.post('/updateEmployeeBranchMapping', function (req, res, next) {
    var employee_Branch_Mapping_Master = new employeeBranchMappingMaster();

    var employeeBranchMappingData = {
       
        Employee_Code: req.body.Employee_Code,
        Branch_Code: req.body.Branch_Code,
        Br_Loc_Map_Status: req.body.Br_Loc_Map_Status,
        Txn_ID: req.body.Txn_ID,
    };

    Joi.validate({
        Employee_Code: req.body.Employee_Code,
        Branch_Code: req.body.Branch_Code,
        Br_Loc_Map_Status: req.body.Br_Loc_Map_Status,
        Txn_ID: req.body.Txn_ID,


    }, schema1, function (err, schemeemployeeBranchMappingData) {
        if (err) {

            buildResponse(res, 200, {
                error: true,
                message: err["message"]
            });
        }
        else {
            employee_Branch_Mapping_Master.updateEmployeeBranchMapping(employeeBranchMappingData)
                .then((data) => {
                    console.log(data, "-----------")
                    if (data["data"] == 0) {
                        buildResponse(res, 200, {
                            error: true,
                            message: data["errMsg"]
                        });
                    } else {
                        buildResponse(res, 200, {
                            error: false,
                            message: "Stored Successfully"
                        });

                    }

                }).catch((errLog) => {

                    log.addLog('route :: Employee_Branch_Mapping.js ' + errLog);
                    buildResponse(res, 500, {
                        error: true,
                        message: "server error"
                    });
                });
        }
    });
});

router.post('/getEmployeeBranchMapping', function (req, res, next) {
    var employee_Branch_Mapping_Master = new employeeBranchMappingMaster();

    employee_Branch_Mapping_Master.getEmployeeBranchMapping().then((employeeBranchMappingData) => {

        if (employeeBranchMappingData["data"] == '0') {
            buildResponse(res, 200, {
                error: false,
                message: employeeBranchMappingData["errMsg"],

            });
        } else {
            buildResponse(res, 200, {
                error: false,
                message: "Successfully display!",
                employeeBranchMappingData: employeeBranchMappingData
            });
        }

    });

});

router.post('/getEmployeeBranchMappingById', function (req, res, next) {
    var employee_Branch_Mapping_Master = new employeeBranchMappingMaster();

    var Txn_ID = req.body.Txn_ID;
    employee_Branch_Mapping_Master.getEmployeeBranchMappingById(Txn_ID).then((employeeBranchMappingData) => {
      
        if (employeeBranchMappingData["data"] == '0') {
            buildResponse(res, 200, {
                error: false,
                message: employeeBranchMappingData["errMsg"],

            });
        } else {
            buildResponse(res, 200, {
                error: false,
                message: "Successfully display!",
                employeeBranchMappingData: employeeBranchMappingData
            });
        }

    });

});



function buildResponse(res, statusCode, responseData) {
    res.status(statusCode).json(responseData);
}

module.exports = router;