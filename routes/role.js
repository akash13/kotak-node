var express = require('express');
var router = express.Router();
var bcrypt = require('bcryptjs');
var jwt = require('jsonwebtoken');
const Joi = require('joi');
var Role = require('../model/role');

var myLogger = require('../middleware.js');
var MyLogModel = require('../model/myLog');
var log = new MyLogModel();

const schema = Joi.object().keys({
    role_name: Joi.string().regex(/^[a-zA-Z ]*$/, '. Special symbols and numbers are not allow').max(20).required().not(''),
    role_status: Joi.string().max(10).required().not(''),
    created_by: Joi.string().allow(''),
    updated_by: Joi.string().allow(''),
    deleted_by: Joi.string().allow(''),

});

router.post('/saveRole', myLogger, function (req, res, next) {
    var role = new Role();
    var roleData = {
         role_name: req.body.role_name,
         role_status: req.body.role_status,
         created_by: req.body.created_by,
   };
   console.log("ddddddddddddddddd",roleData);
    Joi.validate({
        role_name: req.body.role_name,
        role_status: req.body.role_status,
        created_by: req.body.created_by,
     
     
    }, schema, function (err, roleData) {
        if (err) {

            buildResponse(res, 200, {
                error: true,
                message: err["message"]
            });
        }
        else {
            role.saveRole(roleData)
                .then((data) => {
                   console.log(data,"-----------")
                    if (data["data"] == 0) {
                        buildResponse(res, 200, {
                            error: true,
                            message: data["errMsg"]
                        });
                    } else {
                        buildResponse(res, 200, {
                            error: false,
                            message: "Stored Successfully"
                        });

                    }

                }).catch((errLog) => {
                   
                    log.addLog('route :: role.js ' + errLog);
                    buildResponse(res, 500, {
                        error: true,
                        message: "server error"
                    });
                });
        }
    });
});

router.post('/updateRole', myLogger, function (req, res, next) {
    console.log("ddddddddddddddddddddddddddddddd",req.body);
    var role = new Role();
    
    var roleData = {
        role_id:req.body.role_id,
        role_name: req.body.role_name,
        role_status: req.body.role_status,
        updated_by: req.body.updated_by,
   };
   console.log("------------------",roleData),
    Joi.validate({
        role_name: req.body.role_name,
        role_status: req.body.role_status,
        updated_by: req.body.updated_by,
     
     
    }, schema, function (err, schemaRoleData) {
        if (err) {

            buildResponse(res, 200, {
                error: true,
                message: err["message"]
            });
        }
        else {
            role.updateRole(roleData)
                .then((data) => {
                   console.log(data,"-----------")
                    if (data["data"] == 0) {
                        buildResponse(res, 200, {
                            error: true,
                            message: data["errMsg"]
                        });
                    } else {
                        buildResponse(res, 200, {
                            error: false,
                            message: "Stored Successfully"
                        });

                    }

                }).catch((errLog) => {
                   
                    log.addLog('route :: role.js ' + errLog);
                    buildResponse(res, 500, {
                        error: true,
                        message: "server error"
                    });
                });
        }
    });
});

router.get('/getRoles', myLogger, function (req, res, next) {
    var role = new Role();

    role.getRoles().then((data) => {
        
        if (data["data"] == '0') {
            buildResponse(res, 200, {
                error: false,
                message: data["errMsg"],

            });
        } else {
            buildResponse(res, 200, {
                error: false,
                message: "Successfully display!",
                data: data
            });
        }

    });

});

router.post('/deleteRole', myLogger, function (req, res, next) {
    var role = new Role();
    var roleData={
        role_id:req.body.role_id,
        deleted_by:req.body.deleted_by
    };
    console.log("111111111111111",roleData)
    //var reqData = req.body.role_id;
    role.deleteRole(roleData)
        .then((data) => {
            buildResponse(res, 200, {
                error: false,
                message: 'Role Deleted successfully',
                data: data
            });
        }).catch((errLog) => {
            log.addLog('route :: role.js ' + errLog);
            buildResponse(res, 404, {
                error: false,
                message: 'Please Try Again',
            });
        });
});

function buildResponse(res, statusCode, responseData) {
    res.status(statusCode).json(responseData);
}

module.exports = router;