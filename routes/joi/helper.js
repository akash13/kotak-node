const Joi = require('joi');

var schema = {
    requestId: Joi.required(),
    requestName: Joi.required(),
};
module.exports = function(data) {

    var err = Joi.validate(data, schema);
    console.dir(err ? err : 'Valid!');
}