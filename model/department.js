const Sequelize = require('sequelize');
var con = require('./connection');
var randomstring = require("randomstring");
var bcrypt = require('bcryptjs');
var MyLogModel = require('../model/myLog');
var log = new MyLogModel();
const Op = Sequelize.Op;
var errMsg = "Server issue please try after sometime";
Sequelize.DATE.prototype._stringify = function _stringify(date, options) {
    return this._applyTimezone(date, options).format('YYYY-MM-DD HH:mm:ss.SSS');
  };
module.exports = class Department {

    constructor() {

      
        this.DEPARTMENT = con.define('M_Department_Master', {
            T_INT_Department_Id: {
                type: Sequelize.INTEGER,
                primaryKey: true,
                autoIncrement: true
            },
           
            T_CHR_Department_Name: {
                type: Sequelize.STRING(20),
                allowNull: false,
                unique: true
            },
            T_CHR_Department_Status: {
                type: Sequelize.STRING(10),
                allowNull: false,
               
            },
            T_CHR_Created_By: {
                type: Sequelize.STRING(20),
                allowNull: false,
                defaultValue:'0'
               
            },
            T_DATE_Created_Date_Time: {
                type: Sequelize.DATE,
                allowNull: true
            },
            T_CHR_Updated_By: {
                type: Sequelize.STRING(20),
                allowNull: true
            },
            T_DATE_Updated_Date_Time: {
                type: Sequelize.DATE,
                allowNull: true
            },
            T_CHR_Isdelete: {
                type: Sequelize.STRING(2),
                allowNull: false,
                defaultValue:'0'
               
            },
            T_CHR_Deleted_By: {
                type: Sequelize.STRING(20),
                allowNull: true
            },
            T_DATE_Deleted_Date_Time: {
                type: Sequelize.DATE,
                allowNull: true
            },
        },
        {
            timestamps:false,
            createdAt: false,
            UpdatedAt: false,
        }
        );
    }

    saveDepartment(deptData) {
        return sequelize.sync().then(() => {
            return this.DEPARTMENT.create({
              
                T_CHR_Department_Name: deptData.department_name,
                T_CHR_Department_Status:deptData.department_status,
                T_CHR_Created_By: deptData.created_by,
                T_DATE_Created_Date_Time:new Date()

                
              
            }).catch(function (errLog) {
                console.log("errMsg:", errLog)
                log.addLog('model :: department.js ' + errLog);
                var err;
                if (errLog == "SequelizeUniqueConstraintError: Validation error") {
                    err = {
                        data: 0,
                        errMsg: "Department name should be unique"
                    }
                } else if (errLog != "SequelizeUniqueConstraintError: Validation error") {
                    err = {
                        data: 0,
                        errMsg: errLog["errors"][0]["message"]
                    }

                }
                return err;
            });
        }).catch(function (errLog) {
            log.addLog('model :: department.js ' + errLog);
        });
    }

    updateDepartment(deptData) {
        return sequelize.sync().then(() => {
            return this.DEPARTMENT.update({
              
                T_CHR_Department_Name: deptData.department_name,
                T_CHR_Department_Status:deptData.department_status,
                T_CHR_Updated_By: deptData.updated_by,
                T_DATE_Updated_Date_Time:new Date()
              
            },{
                where:{
                    T_INT_Department_Id:deptData.department_id
                }
            }).catch(function (errLog) {
                console.log("errMsg:", errLog)
                log.addLog('model :: department.js ' + errLog);
                var err;
                if (errLog == "SequelizeUniqueConstraintError: Validation error") {
                    err = {
                        data: 0,
                        errMsg: "Department name should be unique"
                    }
                } else if (errLog != "SequelizeUniqueConstraintError: Validation error") {
                    err = {
                        data: 0,
                        errMsg: errLog["errors"][0]["message"]
                    }

                }
                return err;
            });
        }).catch(function (errLog) {
            log.addLog('model :: department.js ' + errLog);
        });
    }
    getDepartments(){
        return sequelize.sync().then(() => {
            return this.DEPARTMENT.all({
                where:{
                    T_CHR_Isdelete:'0'
                }
            }).then((data) => {
                return data
            }).catch((errLog) => {
                var err = {
                    data: 0,
                    errMsg: errMsg
                }
                log.addLog('model :: department.js 153' + errLog);
                return err;
                
            });
        }).catch((errLog) => {
            var err = {
                data: 0,
                errMsg: errMsg
            }
            log.addLog('model :: department.js 162 ' + errLog);
            return err;
        });
    }

    deleteDepartment(deptData) {
        return sequelize.sync().then(() => {
            return this.DEPARTMENT.update({
                T_CHR_Isdelete:'1',
                T_CHR_Deleted_By:deptData.deleted_by,
                T_DATE_Deleted_Date_Time:new Date()

            },{
                where: {
                    T_INT_Department_Id: deptData.department_id
                }
            }).catch((errLog) => {
                var err = {
                    data: 0,
                    errMsg: errMsg
                }
                log.addLog('model :: department.js 169 ' + errLog);
                return err;
            });
        });
    }

};