const Sequelize = require('sequelize');
var con = require('./connection');
var Application = require('./application');
const Op = Sequelize.Op;
//var statictable="IPT_T";

module.exports = class dbConnection {

    constructor() {
        this.CONNECTION = con.define('connection_master', {
            connection_id: {
                type: Sequelize.INTEGER,
                primaryKey: true,
                autoIncrement: true
            },
            connection_name: {
                type: Sequelize.STRING,
                allowNull: true
                
            },
            dialect: {
                type: Sequelize.STRING,
                allowNull: true
                
            },
            database: {
                type: Sequelize.STRING,
                allowNull: true
            },
            username: {
                type: Sequelize.TEXT,
                allowNull: true
            },
            host: {
                type: Sequelize.STRING,
                allowNull: true
            },
            
            port: {
                type: Sequelize.STRING,
                allowNull: true
            },
            
            password: {
                type: Sequelize.STRING,
                allowNull: true
            },
            
            logging: {
                type: Sequelize.STRING,
                allowNull: true
            },
            encrypt: {
                type: Sequelize.STRING,
                allowNull: true
            },
            instanceName: {
                type: Sequelize.STRING,
                allowNull: true
            },
        });
    }


    saveConnection(connectionData) {
        return sequelize.sync().then(() => {
            return this.CONNECTION.create({
                connection_name:connectionData.connection_name,
                dialect: connectionData.dialect,
                database: connectionData.database,
                username: connectionData.username,
                host: connectionData.host,
                port: connectionData.port,
                password: connectionData.password,
                logging: connectionData.logging,
                encrypt: connectionData.encrypt,
                instanceName: connectionData.instanceName,

            }).catch(function(err) {
                if(err=="SequelizeUniqueConstraintError: Validation error"){
                    return 0;
                } 
            });
        }).catch(function(err) {
            return err;
        });
    }


    getConnection() {
        return sequelize.sync().then(() => {
            return this.CONNECTION.all();
        });
    }

    getConnectionById(id) {
        return sequelize.sync().then(() => {
            return this.CONNECTION.findOne({
                where: {
                    connection_id: id
                },
            });
        });
    }

    updateConnection(connectionData, ConnectionId) {
        
        return sequelize.sync().then(() => {
            return this.CONNECTION.update({
                connection_name:connectionData.connection_name,
                dialect: connectionData.dialect,
                database: connectionData.database,
                username: connectionData.username,
                host: connectionData.host,
                port: connectionData.port,
                password: connectionData.password,
                logging: connectionData.logging,
                encrypt: connectionData.encrypt,
                instanceName: connectionData.instanceName,
            }, {
                where: {
                    connection_id: ConnectionId
                }
            }).catch(function(err) {
                if(err=="SequelizeUniqueConstraintError: Validation error"){
                    return 0;
                } 
            });
        }).catch(function(err) {
            return err;
        });
    }

    deleteConnections(ConnectionId) {
        return sequelize.sync().then(() => {
            return this.CONNECTION.destroy({
                where: {
                    connection_id: ConnectionId
                }
            });
        });
    }

}
