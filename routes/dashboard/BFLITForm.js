var express = require('express');
var router = express.Router();

var BFLForm = require('../../model/BFLITForm');

var soap = require('strong-soap').soap;

var myLogger = require('../../middleware.js');
var options = {
    wsdl_headers: {
        'Authorization': 'Basic ' + new Buffer('piwebusr' + ':' + 'piproxy123').toString('base64'),
    }
};



router.get('/getuserDivision/:id',myLogger, function (req, res, next) {
    //console.log("in mroute", req.params.id)
    var request = new BFLForm();
    var id = req.params.id;
    request.getuserDivision(id).then((data) => {
        buildResponse(res, 200, {
            error: false,
            message: "Successfully display!",
            responseData: data
        });
    });
});


router.get('/getUserDetailsForPirForm/:id',myLogger, function (req, res, next) {
    var request = new BFLForm();
    var id = req.params.id;
    request.getUserDetailsForPirForm(id).then((data) => {
        buildResponse(res, 200, {
            error: false,
            message: "Successfully display!",
            responseData: data
        });
    });
});


router.get('/CheckForBanUser/:currentUser',myLogger, function (req, res, next) {
  
    var request = new BFLForm();
    var id = req.params.currentUser;
    request.CheckForBanUser(id).then((data) => {
        buildResponse(res, 200, {
            error: false,
            message: "Successfully display!",
            responseData: data
        });
    });
});


router.get('/getDashboardTotalCount/:userid',myLogger, function (req, res, next) {
    var request = new BFLForm();
    var userid = req.params.userid;
    // var group=req.params.group;
    // var dept=req.params.dept;
    // log("in route total",group,dept);

    var totalcount = 0;
    //    for (var i=0;i< req.body.length;i++)
    //    {
    request.getDashboardTotalCount(userid).then((countData) => {
        request.getDashboardTotalCount
        //log("countdata=",countData)
        buildResponse(res, 200, {
            error: false,
            message: 'Forms Fetched successfully',
            listData: countData,
            countData: countData.length

        });
    });

    //}

});



router.get('/getDashboardPendingCount/:userid',myLogger, function (req, res, next) {
    var request = new BFLForm();
    var userid = req.params.userid;
    // var group=req.params.group;
    // var dept=req.params.dept;
    // log("in route total",group,dept);

    var totalcount = 0;
    //    for (var i=0;i< req.body.length;i++)
    //    {
    request.getDashboardPendingCount(userid).then((countData) => {
        request.getDashboardPendingCount
        //log("countdata=",countData)
        buildResponse(res, 200, {
            error: false,
            message: 'Forms Fetched successfully',
            listData: countData,
            countData: countData.length

        });
    });

    //}

});




router.get('/getCostingUser',myLogger, function (req, res, next) {
    
    var request = new BFLForm();
    // var id = req.params.id;
    request.getCostingUser().then((data) => {
        buildResponse(res, 200, {
            error: false,
            message: "Successfully display!",
            responseData: data
        });
    });
});






router.get('/getITFormList/:id',myLogger, function (req, res, next) {
    
    var request = new BFLForm();
    var id = req.params.id;
    request.getITFormList(id).then((data) => {
        buildResponse(res, 200, {
            error: false,
            message: "Successfully display!",
            responseData: data
        });
    }).catch((err) => {
        buildResponse(res, 200, {
            error: false,
            message: 'Error fetching',
        });
    });
});


router.get('/getOrgansationList',myLogger, function (req, res, next) {
    
    var request = new BFLForm();
    // var id = req.params.id;
    request.getOrgansationList().then((data) => {
        buildResponse(res, 200, {
            error: false,
            message: "Successfully display!",
            responseData: data
        });
    }).catch((err) => {
        buildResponse(res, 200, {
            error: false,
            message: 'Error in Connecting',
        });
    });
});
router.get('/getFormType',myLogger, function (req, res, next) {
    
    var request = new BFLForm();
    // var id = req.params.id;
    request.getFormType().then((data) => {
        buildResponse(res, 200, {
            error: false,
            message: "Successfully display!",
            responseData: data
        });
    }).catch((err) => {
        buildResponse(res, 200, {
            error: false,
            message: 'Error in Connecting',
        });
    });
});

router.get('/getemployee/:userid', function (req, res, next) {
    var request = new BFLForm();
    var userid = req.params.userid;
   
    //console.log("in route");
    request.getemployee(userid).then((data) => {
        buildResponse(res, 200, {
            error: false,
            message: "Successfully display!",
            responseData: data
        });
        // console.log("awardData=",responseData);
    }).catch((err) => {
        buildResponse(res, 200, {
            error: false,
            message: 'Error in Connecting',
        });
    });


});
router.get('/getemployeeOnInput/:userid', function (req, res, next) {
    var request = new BFLForm();
    var userid = req.params.userid;
   
    request.employeeOnInput(userid).then((data) => {
        buildResponse(res, 200, {
            error: false,
            message: "Successfully display!",
            responseData: data
        });
        // console.log("awardData=",responseData);
    });


});
router.get('/getDepartment/:id',myLogger, function (req, res, next) {
    // console.log('in store',req.params.id)
    var request = new BFLForm();
     var organisationname = req.params.id;
    request.getDepartment(organisationname).then((data) => {
        buildResponse(res, 200, {
            error: false,
            message: "Successfully display!",
            responseData: data
        });
    }).catch((err) => {
        buildResponse(res, 200, {
            error: false,
            message: 'Error in Connecting',
        });
    });
});
router.get('/getApproverdetails/:id',myLogger, function (req, res, next) {
    // console.log('in store',req.params.id)
    var request = new BFLForm();
     var deptname = req.params.id;
    request.getApproverdetails(deptname).then((data) => {
        buildResponse(res, 200, {
            error: false,
            message: "Successfully display!",
            responseData: data
        });
    }).catch((err) => {
        buildResponse(res, 200, {
            error: false,
            message: 'Error in Connecting',
        });
    });
});






router.get('/getCostingDepartmentID/:id',myLogger, function (req, res, next) {
    // console.log('in store',req.params.id)
    var request = new BFLForm();
     var division = req.params.id;
    request.getCostingDepartmentID(division).then((data) => {
        buildResponse(res, 200, {
            error: false,
            message: "Successfully display!",
            responseData: data
        });
    });
});
router.get('/getPlantwiseStore/:id',myLogger, function (req, res, next) {
    // console.log('in store',req.params.id)
    var request = new BFLForm();
     var plant = req.params.id;
    request.getPlantwiseStore(plant).then((data) => {
        buildResponse(res, 200, {
            error: false,
            message: "Successfully display!",
            responseData: data
        });
    });
});



router.get('/getDetailsByCostNo/:costNo',myLogger, function (req, res, next) {
  //  console.log("in mroute", req.params.id)
    var request = new BFLForm();
    var id = req.params.costNo;
    //console.log("idididididid",id);
    var url = "http://bfpis.kalyanicorp.com:50000/dir/wsdl?p=ic/0c136b6ff75a3a348b208c0fb98d2c6c";
    var RequestData = {
        MT_ISSUESLIP_READ_REQ: { KOSTL: id ,AUFNR:'',FOR_SYS:'RMGP'}
    }
    soap.createClient(url, options, function (err, client) {
        if (err) {
            callback(err);
            return;
        }
       // console.log('Client is ready');
        client.setSecurity(new soap.BasicAuthSecurity('piwebusr', 'piproxy123'));
        client.SI_ISSUESLIP_READ_COSTCENTER_APPROVERService.HTTP_Port.SI_ISSUESLIP_READ_COSTCENTER_APPROVER(RequestData, function (err, response) {
            if (err) {
              //  console.log("err", err);
            } else {
                // console.log("response", response);
                buildResponse(res, 200, {
                    error: false,
                    message: "Successfully display!",
                    responseData: response
                });
            }
        });

    });
});

router.get('/getDetailsByInternalNo/:InternalNo',myLogger, function (req, res, next) {
   // console.log("in mroute", req.params.id)
    var request = new BFLForm();
    var id = req.params.InternalNo;
    //console.log("idididididid",id);
    var url = "http://bfpis.kalyanicorp.com:50000/dir/wsdl?p=ic/0c136b6ff75a3a348b208c0fb98d2c6c";
    var RequestData = {
        MT_ISSUESLIP_READ_REQ: { KOSTL: '' ,AUFNR:id,FOR_SYS:''}
    }
    soap.createClient(url, options, function (err, client) {
        if (err) {
            callback(err);
            return;
        }
       // console.log('Client is ready');
        client.setSecurity(new soap.BasicAuthSecurity('piwebusr', 'piproxy123'));
        client.SI_ISSUESLIP_READ_COSTCENTER_APPROVERService.HTTP_Port.SI_ISSUESLIP_READ_COSTCENTER_APPROVER(RequestData, function (err, response) {
            if (err) {
               // console.log("err", err);
            } else {
                // console.log("response", response);
                buildResponse(res, 200, {
                    error: false,
                    message: "Successfully display!",
                    responseData: response
                });
            }
        });

    });
});




router.post('/sendRequestorEmail',myLogger, function (req, res, next) {

    var request = new BFLForm();
    // console.log("body=",req.body);
    request.sendRequestorEmail(req.body);
    buildResponse(res, 200, {
        error: false,
        message: 'Email Intimation Notification Send Successfully'
    });
});




router.post('/getMatfield',myLogger, function (req, res, next) {
    //var matno = 'X09307001376,X09307001370';
    var responseArray = [];
    var data = req.body;
    var url = "http://bfpis.kalyanicorp.com:50000/dir/wsdl?p=ic/b1d61f724eb73e2bbfb9faa616968eb8";
    //console.log('ididididid', data);
    var RequestData = {
        MT_ISSUESLIP_MATERIAL_DATA_REQ:
        {
            MATERIAL_TAB: {
                MATERIAL_DATA: {
                    MATERIAL_NO: data.material_code,
                    PLANT: data.plant,
                    STORAGE_LOCATION: data.store,
                }
            }
        }
    }
    soap.createClient(url, options, function (err, client) {
        if (err) {
            callback(err);
            return;
        }
        client.setSecurity(new soap.BasicAuthSecurity('piwebusr', 'piproxy123'));
        client.SI_ISSUESLIP_MATERIAL_DATAService.HTTP_Port.SI_ISSUESLIP_MATERIAL_DATA(RequestData, function (err, response, envelope) {
            if (err) {
                //console.log("err", err);

                buildResponse(res, 200, {
                    error: true,
                    message: "errr"
                });
            } else {
                //console.log("response", response);
                buildResponse(res, 200, {
                    error: false,
                    message: "record fetch succcessfull",
                    responseData: response
                });
            }
        });
    });
});

router.post('/getReportDashboardDTA',myLogger, function (req, res, next) {

    var request = new BFLForm();
    var data = req.body;
    //console.log("dataaaaa", data);

    request.getReportDashboardDTA(data)
        .then((data) => {
            buildResponse(res, 200, {
                error: false,
                message: "Record Inserted Successfully",
                responseData: data
            });

        }).catch((err) => {
            buildResponse(res, 200, {
                error: true,
                message: "Fail to Store"
            });
        });

})


router.post('/submitData',myLogger, function (req, res, next) {

    var request = new BFLForm();
    var data = req.body;
    //console.log("dataaaaa", data);

    request.submitData(data)
        .then((data) => {
            buildResponse(res, 200, {
                error: false,
                message: "Record Inserted Successfully",
                responseData: data
            });

        }).catch((err) => {
            buildResponse(res, 200, {
                error: true,
                message: "Fail to Store"
            });
        });

})


router.post('/UpdateDataByRequestor',myLogger, function (req, res, next) {

    var request = new BFLForm();
    var data = req.body;
    request.UpdateDataByRequestor(data)
        .then((data) => {
            buildResponse(res, 200, {
                error: false,
                message: "Record Updated Successfully",
                responseData: data
            });

        }).catch((err) => {
            buildResponse(res, 200, {
                error: true,
                message: "Fail to Updated"
            });
        });

});


router.post('/UpdateRequestStatus',myLogger, function (req, res, next) {

    var request = new BFLForm();
    var data = req.body;
   console.log("dataaaaa", data);

    request.UpdateRequestStatus(data)
        .then((data) => {
            buildResponse(res, 200, {
                error: false,
                message: "Record Updated Successfully"
            });

        }).catch((err) => {
            buildResponse(res, 200, {
                error: true,
                message: "Fail to Store"
            });
        });

});

router.post('/UpdateRMGPRejectData',myLogger, function (req, res, next) {

    var request = new BFLForm();
    var data = req.body;
    //console.log("dataaaaa", data);

    request.UpdateRMGPRejectData(data)
        .then((data) => {
            buildResponse(res, 200, {
                error: false,
                message: "Record Updated Successfully"
            });

        }).catch((err) => {
            buildResponse(res, 200, {
                error: true,
                message: "Fail to Store"
            });
        });

});


router.get('/getMaxIDFORRMGpRequestNo/:fiscalyear/:rmgptype',myLogger, function (req, res, next) {
    var request = new BFLForm();
     var fiscalyear= req.params.fiscalyear;
     var type= req.params.rmgptype;
    request.getMaxIDFORRMGpRequestNo(fiscalyear,type).then((countData) => {
        buildResponse(res, 200, {
            error: false,
            message: 'Forms Fetched successfully',
            listData: countData,
            countData: countData.length

        });
    });

    //}

});

router.get('/getExciseDepartmentID/',myLogger, function (req, res, next) {
    var request = new BFLForm();
   
    request.getExciseDepartmentID().then((countData) => {
        buildResponse(res, 200, {
            error: false,
            message: 'Forms Fetched successfully',
            listData: countData,
            countData: countData.length

        });
    });

    //}

});
router.get('/getCheckoutDepartmentID/:division',myLogger, function (req, res, next) {
    var request = new BFLForm();
    var division= req.params.division;
    request.getCheckoutDepartmentID(division).then((countData) => {
        buildResponse(res, 200, {
            error: false,
            message: 'Forms Fetched successfully',
            listData: countData,
            countData: countData.length

        });
    });

    //}

});

router.get('/getCheckInDepartmentID/:division',myLogger, function (req, res, next) {
    var request = new BFLForm();
    var division= req.params.division;
    request.getCheckInDepartmentID(division).then((countData) => {
        buildResponse(res, 200, {
            error: false,
            message: 'Forms Fetched successfully',
            listData: countData,
            countData: countData.length

        });
    });

    //}

});


router.get('/getDataOnUpdateForm/:id',myLogger, function (req, res, next) {
    var request = new BFLForm();
    var reqData = req.params.id;
    request.getDataOnUpdateForm(reqData)
        .then((userData) => {
            buildResponse(res, 200, {
                error: false,
                message: 'Data Fetch Successfully',
                UserData: userData
            });
        }).catch((err) => {
            buildResponse(res, 500, {
                error: false,
                message: 'Please Try Again',
            });
        });
});


router.get('/getDataOfCurrentRecord/:id',myLogger, function (req, res, next) {
    var request = new BFLForm();
    var reqData = req.params.id;
    request.getDataOfCurrentRecord(reqData)
        .then((userData) => {
            buildResponse(res, 200, {
                error: false,
                message: 'Data Fetch Successfully',
                UserData: userData
            });
        }).catch((err) => {
            buildResponse(res, 500, {
                error: false,
                message: 'Please Try Again',
            });
        });
});


router.get('/getMasterITVERIFICATION', function (req, res, next) {
    var request = new BFLForm();
    request.getMasterITVERIFICATION().then((data) => {
        buildResponse(res, 200, {
            error: false,
            message: "Successfully get Master ITVERIFICATION!",
            responseData: data
        });
    });
});

router.get('/getMasterITADMIN', function (req, res, next) {
    var request = new BFLForm();
    request.getMasterITADMIN().then((data) => {
        buildResponse(res, 200, {
            error: false,
            message: "Successfully getMaster IT ADMIN!",
            responseData: data
        });
    });
});
router.get('/getMasterITHEAD', function (req, res, next) {
    var request = new BFLForm();
    request.getMasterITHEAD().then((data) => {
        buildResponse(res, 200, {
            error: false,
            message: "Successfully getMaster IT HEAD!",
            responseData: data
        });
    });
});

router.get('/getMasterITHELPDESK', function (req, res, next) {
    var request = new BFLForm();
    request.getMasterITHELPDESK().then((data) => {
        buildResponse(res, 200, {
            error: false,
            message: "Successfully getMaster IT HEAD!",
            responseData: data
        });
    });
});




function buildResponse(res, statusCode, responseData) {
    res.status(statusCode).json(responseData);
}

module.exports = router;