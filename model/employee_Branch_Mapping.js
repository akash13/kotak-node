const Sequelize = require('sequelize');
var con = require('./connection');
var randomstring = require("randomstring");
var bcrypt = require('bcryptjs');
var MyLogModel = require('../model/myLog');
var log = new MyLogModel();
const Op = Sequelize.Op;
var errMsg = "Server issue please try after sometime";
module.exports = class Employee_Branch_Mapping {

    constructor() {


        this.Employee_Branch_Mapping = con.define('Employee_Branch_Mapping', {
            Txn_ID: {
                type: Sequelize.INTEGER,
                primaryKey: true,
                autoIncrement: true
            },

            Added_By: {
                type: Sequelize.STRING(20),
                allowNull: true

            },
            Create_Date: {
                type: Sequelize.DATE,
                allowNull: true

            },

            Employee_Code: {
                type: Sequelize.STRING(20),
                allowNull: true,
                primaryKey:true,
            },

            Branch_Code: {
                type: Sequelize.STRING(30),
                allowNull: true,
                primaryKey:true,
            },
            Br_Loc_Map_Status: {
                type: Sequelize.STRING(1),
                allowNull: true
            },
           
            


        });
    }

    saveEmployeeBranchMapping(employeeBranchMappingData) {
        return sequelize.sync().then(() => {
            return this.Employee_Branch_Mapping.create({

                Added_By:employeeBranchMappingData.Added_By,
                Create_Date:employeeBranchMappingData.Create_Date,
                Employee_Code:employeeBranchMappingData.Employee_Code,
                Branch_Code:employeeBranchMappingData.Branch_Code,
                Br_Loc_Map_Status:employeeBranchMappingData.Br_Loc_Map_Status,

            }).catch(function (errLog) {
               
                log.addLog('model :: Employee_Branch_Mapping.js ' + errLog);
                var err;
                if (errLog == "SequelizeUniqueConstraintError: Validation error") {
                    err = {
                        data: 0,
                        errMsg: "menu name should be unique"
                    }
                } else if (errLog != "SequelizeUniqueConstraintError: Validation error") {
                    err = {
                        data: 0,
                        errMsg: errLog["errors"][0]["message"]
                    }

                }
                return err;
            });
        }).catch(function (errLog) {
           
            log.addLog('model :: Employee_Branch_Mapping.js ' + errLog);
        });
    }

    updateEmployeeBranchMapping(employeeBranchMappingData) {
        return sequelize.sync().then(() => {
            return this.Employee_Branch_Mapping.update({
                Employee_Code: employeeBranchMappingData.Employee_Code,
                Branch_Code: employeeBranchMappingData.Branch_Code,
                Br_Loc_Map_Status: employeeBranchMappingData.Br_Loc_Map_Status,
                Txn_ID: employeeBranchMappingData.Txn_ID,
        


            }, {
                    where: {
                        Txn_ID: employeeBranchMappingData.Txn_ID
                    }
                }).catch(function (errLog) {
                   
                    log.addLog('model :: Employee_Branch_Mapping.js ' + errLog);
                    var err;
                    if (errLog == "SequelizeUniqueConstraintError: Validation error") {
                        err = {
                            data: 0,
                            errMsg: "menu name should be unique"
                        }
                    } else if (errLog != "SequelizeUniqueConstraintError: Validation error") {
                        err = {
                            data: 0,
                            errMsg: errLog["errors"][0]["message"]
                        }

                    }
                    return err;
                });
        }).catch(function (errLog) {
            log.addLog('model :: Employee_Branch_Mapping.js ' + errLog);
        });
    }


    getEmployeeBranchMapping() {
        return sequelize.sync().then(() => {
            return this.Employee_Branch_Mapping.all().then((data) => {
                return data
            }).catch((errLog) => {
                var err = {
                    data: 0,
                    errMsg: errMsg
                }
                log.addLog('model :: Employee_Branch_Mapping.js' + errLog);
                return err;

            });
        }).catch((errLog) => {
            var err = {
                data: 0,
                errMsg: errMsg
            }
            log.addLog('model :: Employee_Branch_Mapping.js ' + errLog);
            return err;
        });
    }

    getEmployeeBranchMappingById(id) {
        return sequelize.sync().then(() => {
            return this.Employee_Branch_Mapping.findOne({
                where: {
                    Txn_ID: id
                }
            }).then((data) => {
                return data;
            }).catch((errLog) => {
                var err = {
                    data: 0,
                    errMsg: errMsg
                }
                log.addLog('model :: Employee_Branch_Mapping.js' + errLog);
                return err;

            });
        }).catch((errLog) => {
            var err = {
                data: 0,
                errMsg: errMsg
            }
            log.addLog('model :: Employee_Branch_Mapping.js ' + errLog);
            return err;
        });
    }

};