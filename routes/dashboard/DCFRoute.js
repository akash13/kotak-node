var express = require('express');
var router = express.Router();
var DCFModel = require('../../model/DCFModel.js');
var soap = require('strong-soap').soap;
var myLogger = require('../../middleware.js');
var options = {
    wsdl_headers: {
        'Authorization': 'Basic ' + new Buffer('piwebusr' + ':' + 'piproxy123').toString('base64'),
    }
};



router.get('/getuserDivision/:id',myLogger, function (req, res, next) {
    console.log("in mroute", req.params.id)
    var request = new DCFModel();
    var id = req.params.id;
    request.getuserDivision(id).then((data) => {
        buildResponse(res, 200, {
            error: false,
            message: "Successfully display!",
            responseData: data
        });
    });
});


router.get('/getUserDetailsForPirForm/:id',myLogger, function (req, res, next) {
    console.log("in mroute", req.params.id)
    var request = new DCFModel();
    var id = req.params.id;
    request.getUserDetailsForPirForm(id).then((data) => {
        buildResponse(res, 200, {
            error: false,
            message: "Successfully display!",
            responseData: data
        });
    });
});


router.get('/CheckForBanUser/:currentUser',myLogger, function (req, res, next) {
  
    var request = new DCFModel();
    var id = req.params.currentUser;
    request.CheckForBanUser(id).then((data) => {
        buildResponse(res, 200, {
            error: false,
            message: "Successfully display!",
            responseData: data
        });
    });
});


router.get('/getDashboardTotalCount/:userid',myLogger, function (req, res, next) {
    var request = new DCFModel();
    var userid = req.params.userid;
    // var group=req.params.group;
    // var dept=req.params.dept;
    // log("in route total",group,dept);

    var totalcount = 0;
    //    for (var i=0;i< req.body.length;i++)
    //    {
    request.getDashboardTotalCount(userid).then((countData) => {
        request.getDashboardTotalCount
        //log("countdata=",countData)
        buildResponse(res, 200, {
            error: false,
            message: 'Forms Fetched successfully',
            listData: countData,
            countData: countData.length

        });
    });

    //}

});


// router.get('/getDashboardPendingCount/:userid',myLogger, function (req, res, next) {
//     var request = new DCFModel();
//     var userid = req.params.userid;
//     // var group=req.params.group;
//     // var dept=req.params.dept;
//     // log("in route total",group,dept);

//     var totalcount = 0;
//     //    for (var i=0;i< req.body.length;i++)
//     //    {
//     request.getDashboardPendingCount(userid).then((countData) => {
//         request.getDashboardPendingCount
//         //log("countdata=",countData)
//         buildResponse(res, 200, {
//             error: false,
//             message: 'Forms Fetched successfully',
//             listData: countData,
//             countData: countData.length

//         });
//     });

//     //}

// });


router.get('/getDashboardPendingCount/:id/:currentuser', myLogger, function (req, res, next) {
    var request = new DCFModel();
    var currentUser = req.params.currentuser;
    var division = req.params.id;
    var count = 0, hcount = 0, ccount = 0, ecount = 0, ocount = 0, icount = 0, acount = 0;
    var totalcount = 0;
    var listDataTotal = [];
    var listhodData = [];
    var listcostingData = [];
    var listexciseData = [];
    var listoutData = [];
    var listinData = [];
    var listackData = [];

    //    for (var i=0;i< req.body.length;i++)
    //    {

    request.getDashboardPendingCount(division, currentUser).then((finaldata) => {

       // console.log("finaldata---", finaldata['listhodPendingData']);

      
            request.gethodLISTDATA(finaldata['listhodPendingData'], division, currentUser).then((hoddata) => {

           
               if(hoddata.length){

                hoddata.forEach(element=>{
                                    listDataTotal.push(element);
                                });
                                
               }
          

                request.getcostingLISTDATA(finaldata['listcostingPendingData'],division, currentUser).then((costingdata) => {

                    if(costingdata.length){
                        costingdata.forEach(element=>{
                            listDataTotal.push(element);
                        });
                       }
                  
                
        
                    request.getexciseLISTDATA(finaldata['listexcisePendingData'],division, currentUser).then((excisedata) => {
        
                        if(excisedata.length){
                            excisedata.forEach(element=>{
                                listDataTotal.push(element);
                            });
                           }
                      
            
                           
                            request.getcheckoutLISTDATA(finaldata['listoutPendingData'],division, currentUser).then((checkoutdata) => {
                                if(checkoutdata.length){
                                    checkoutdata.forEach(element=>{
                                        listDataTotal.push(element);
                                    });
                                    
                                   }
                           
                              
                                    finaldata['listPendingData']=listDataTotal;
                                   // console.log("finaldata==",finaldata['listPendingData']);
                          buildResponse(res, 200, {
                            error: false,
                            message: 'Forms Fetched successfully',
                            listData: finaldata,
                        
                        
                        })
                                  
                            
                              
                            })
                         
                    
                           
                          
                        
                       
                      
                    })
                  
                  
                })
              
               

            });
        





    }).catch((err) => {
        buildResponse(res, 200, {
            error: false,
            message: 'Error',
        });
    });

    //}

});



router.get('/getDashboardCheckinCount/:userid/:division',myLogger, function (req, res, next) {
    var request = new DCFModel();
    var userid = req.params.userid;
    var division=req.params.division;
    // var dept=req.params.dept;
    // log("in route total",group,dept);

    var totalcount = 0;
    //    for (var i=0;i< req.body.length;i++)
    //    {
    request.getDashboardCheckinCount(userid,division).then((countData) => {
    
      //  console.log("countdata=",countData)
        buildResponse(res, 200, {
            error: false,
            message: 'Forms Fetched successfully',
            listData: countData,
            countData: countData.length

        });
    });

    //}

});

router.get('/getDashboardAckCount/:userid/:division',myLogger, function (req, res, next) {
    var request = new DCFModel();
    var userid = req.params.userid;
    var division=req.params.division;
    // var dept=req.params.dept;
    // log("in route total",group,dept);

    var totalcount = 0;
    //    for (var i=0;i< req.body.length;i++)
    //    {
    request.getDashboardAckCount(userid,division).then((countData) => {
    
      //  console.log("countdata=",countData)
        buildResponse(res, 200, {
            error: false,
            message: 'Forms Fetched successfully',
            listData: countData,
            countData: countData.length

        });
    });

    //}

});




router.get('/getCostingUser',myLogger, function (req, res, next) {
    
    var request = new DCFModel();
    // var id = req.params.id;
    request.getCostingUser().then((data) => {
        buildResponse(res, 200, {
            error: false,
            message: "Successfully display!",
            responseData: data
        });
    });
});






router.get('/getRMGPList',myLogger, function (req, res, next) {
    
    var request = new DCFModel();
    // var id = req.params.id;
    request.getRMGPList().then((data) => {
        buildResponse(res, 200, {
            error: false,
            message: "Successfully display!",
            responseData: data
        });
    });
});

router.get('/getUnitList',myLogger, function (req, res, next) {
    
    var request = new DCFModel();
    // var id = req.params.id;
    request.getUnitList().then((data) => {
        buildResponse(res, 200, {
            error: false,
            message: "Successfully display!",
            responseData: data
        });
    });
});
router.get('/getsupplierDetails/:id',myLogger, function (req, res, next) {
    // console.log('in store',req.params.id)
    var request = new DCFModel();
     var suppliercode = req.params.id;
    request.getsupplierDetails(suppliercode).then((data) => {
        buildResponse(res, 200, {
            error: false,
            message: "Successfully display!",
            responseData: data
        });
    });
});





router.get('/getCostingDepartmentID/:id',myLogger, function (req, res, next) {
    // console.log('in store',req.params.id)
    var request = new DCFModel();
     var division = req.params.id;
    request.getCostingDepartmentID(division).then((data) => {
        buildResponse(res, 200, {
            error: false,
            message: "Successfully display!",
            responseData: data
        });
    });
});
router.get('/getPlantwiseStore/:id',myLogger, function (req, res, next) {
    // console.log('in store',req.params.id)
    var request = new DCFModel();
     var plant = req.params.id;
    request.getPlantwiseStore(plant).then((data) => {
        buildResponse(res, 200, {
            error: false,
            message: "Successfully display!",
            responseData: data
        });
    });
});



router.get('/getDetailsByCostNo/:costNo',myLogger, function (req, res, next) {
    console.log("in mroute", req.params.id)
    var request = new DCFModel();
    var id = req.params.costNo;
    //console.log("idididididid",id);
    var url = "http://bfpis.kalyanicorp.com:50000/dir/wsdl?p=ic/0c136b6ff75a3a348b208c0fb98d2c6c";
    var RequestData = {
        MT_ISSUESLIP_READ_REQ: { KOSTL: id ,AUFNR:'',FOR_SYS:'RMGP'}
    }
    soap.createClient(url, options, function (err, client) {
        if (err) {
            callback(err);
            return;
        }
        console.log('Client is ready');
        client.setSecurity(new soap.BasicAuthSecurity('piwebusr', 'piproxy123'));
        client.SI_ISSUESLIP_READ_COSTCENTER_APPROVERService.HTTP_Port.SI_ISSUESLIP_READ_COSTCENTER_APPROVER(RequestData, function (err, response) {
            if (err) {
                console.log("err", err);
            } else {
                // console.log("response", response);
                buildResponse(res, 200, {
                    error: false,
                    message: "Successfully display!",
                    responseData: response
                });
            }
        });

    });
});

router.get('/getDetailsByInternalNo/:InternalNo',myLogger, function (req, res, next) {
    console.log("in mroute", req.params.id)
    var request = new DCFModel();
    var id = req.params.InternalNo;
    //console.log("idididididid",id);
    var url = "http://bfpis.kalyanicorp.com:50000/dir/wsdl?p=ic/0c136b6ff75a3a348b208c0fb98d2c6c";
    var RequestData = {
        MT_ISSUESLIP_READ_REQ: { KOSTL: '' ,AUFNR:id,FOR_SYS:''}
    }
    soap.createClient(url, options, function (err, client) {
        if (err) {
            callback(err);
            return;
        }
        console.log('Client is ready');
        client.setSecurity(new soap.BasicAuthSecurity('piwebusr', 'piproxy123'));
        client.SI_ISSUESLIP_READ_COSTCENTER_APPROVERService.HTTP_Port.SI_ISSUESLIP_READ_COSTCENTER_APPROVER(RequestData, function (err, response) {
            if (err) {
                console.log("err", err);
            } else {
                // console.log("response", response);
                buildResponse(res, 200, {
                    error: false,
                    message: "Successfully display!",
                    responseData: response
                });
            }
        });

    });
});




router.post('/sendRequestorEmail',myLogger, function (req, res, next) {

    var request = new DCFModel();
    // console.log("body=",req.body);
    request.sendRequestorEmail(req.body);
    buildResponse(res, 200, {
        error: false,
        message: 'Email Intimation Notification Send Successfully'
    });
});




router.post('/getMatfield',myLogger, function (req, res, next) {
    //var matno = 'X09307001376,X09307001370';
    var responseArray = [];
    var data = req.body;
    var url = "http://bfpis.kalyanicorp.com:50000/dir/wsdl?p=ic/b1d61f724eb73e2bbfb9faa616968eb8";
    console.log('ididididid', data);
    var RequestData = {
        MT_ISSUESLIP_MATERIAL_DATA_REQ:
        {
            MATERIAL_TAB: {
                MATERIAL_DATA: {
                    MATERIAL_NO: data.material_code,
                    PLANT: data.plant,
                    STORAGE_LOCATION: data.store,
                }
            }
        }
    }
    soap.createClient(url, options, function (err, client) {
        if (err) {
            callback(err);
            return;
        }
        client.setSecurity(new soap.BasicAuthSecurity('piwebusr', 'piproxy123'));
        client.SI_ISSUESLIP_MATERIAL_DATAService.HTTP_Port.SI_ISSUESLIP_MATERIAL_DATA(RequestData, function (err, response, envelope) {
            if (err) {
                console.log("err", err);

                buildResponse(res, 200, {
                    error: true,
                    message: "errr"
                });
            } else {
                console.log("response", response);
                buildResponse(res, 200, {
                    error: false,
                    message: "record fetch succcessfull",
                    responseData: response
                });
            }
        });
    });
});




router.post('/submitData',myLogger, function (req, res, next) {

    var request = new DCFModel();
    var data = req.body;
    //console.log("dataaaaa", data);

    request.submitData(data)
        .then((data) => {
            buildResponse(res, 200, {
                error: false,
                message: "Record Inserted Successfully",
                responseData: data
            });

        }).catch((err) => {
            buildResponse(res, 200, {
                error: true,
                message: "Fail to Store"
            });
        });

})


router.post('/UpdateData',myLogger, function (req, res, next) {

    var request = new DCFModel();
    var data = req.body;
   // console.log("dataaaaa", data);

    request.UpdateData(data)
        .then((responseData) => {
            buildResponse(res, 200, {
                error: false,
                message: "Record Updated Successfully"
            });

        }).catch((err) => {
            buildResponse(res, 200, {
                error: true,
                message: "Fail to Store"
            });
        });

});

router.post('/UpdateDCFRejectData',myLogger, function (req, res, next) {

    var request = new DCFModel();
    var data = req.body;
    console.log("dataaaaa", data);

    request.UpdateDCFRejectData(data)
        .then((data) => {
            buildResponse(res, 200, {
                error: false,
                message: "Record Updated Successfully"
            });

        }).catch((err) => {
            buildResponse(res, 200, {
                error: true,
                message: "Fail to Store"
            });
        });

});


router.get('/getMaxIDFORRequestNo/:fiscalyear/:rmgptype',myLogger, function (req, res, next) {
    var request = new DCFModel();
     var fiscalyear= req.params.fiscalyear;
     var type= req.params.rmgptype;
    request.getMaxIDFORRequestNo(fiscalyear,type).then((countData) => {
        buildResponse(res, 200, {
            error: false,
            message: 'Forms Fetched successfully',
            listData: countData,
            countData: countData.length

        });
    });

    //}

});

router.get('/getExciseDepartmentID/',myLogger, function (req, res, next) {
    var request = new DCFModel();
   
    request.getExciseDepartmentID().then((countData) => {
        buildResponse(res, 200, {
            error: false,
            message: 'Forms Fetched successfully',
            listData: countData,
            countData: countData.length

        });
    });

    //}

});
router.get('/getCheckoutDepartmentID/:division',myLogger, function (req, res, next) {
    var request = new DCFModel();
    var division= req.params.division;
    request.getCheckoutDepartmentID(division).then((countData) => {
        buildResponse(res, 200, {
            error: false,
            message: 'Forms Fetched successfully',
            listData: countData,
            countData: countData.length

        });
    });

    //}

});

router.get('/getCheckInDepartmentID/:division',myLogger, function (req, res, next) {
    var request = new DCFModel();
    var division= req.params.division;
    request.getCheckInDepartmentID(division).then((countData) => {
        buildResponse(res, 200, {
            error: false,
            message: 'Forms Fetched successfully',
            listData: countData,
            countData: countData.length

        });
    });

    //}

});


router.get('/getDataOnUpdateForm/:id',myLogger, function (req, res, next) {
    var request = new DCFModel();
    var reqData = req.params.id;
    request.getDataOnUpdateForm(reqData)
        .then((userData) => {
            buildResponse(res, 200, {
                error: false,
                message: 'Data Fetch Successfully',
                UserData: userData
            });
        }).catch((err) => {
            buildResponse(res, 500, {
                error: false,
                message: 'Please Try Again',
            });
        });
});



//     For sheduler ////////////////////////////////////


var schedule = require('node-schedule');
var rule = new schedule.RecurrenceRule();
rule.dayOfWeek = [0, new schedule.Range(0, 6)];
rule.hour = 16;
rule.minute = 38;

// var j = schedule.scheduleJob(rule, function(){
// },getdata);

// function getdata(req,res,next){
router.get('/getDataForEmailJobScheduling', function (req, res, next) {
    var request = new DCFModel();
    var dateNow = new Date();
    var dateSingleDigit = dateNow.getDate();
    dd = dateSingleDigit < 10 ? '0' + dateSingleDigit : dateSingleDigit;
    var monthSingleDigit = dateNow.getMonth() + 1;
    mm = monthSingleDigit < 10 ? '0' + monthSingleDigit : monthSingleDigit;
    var yy = dateNow.getFullYear().toString();
    var currentDate = dd + '-' + mm + '-' + yy;
   

           
          

    request.getDataForEmailJobScheduling().then(data => {
      
        data.forEach((element,index) => {
           console.log(element);
           
           console.log("index",index);
           
                
       
            var firstDate = dateNow;
            var  secondDate =element.RETURN_DATE;
            // d.getDate() + '-' + (d.getMonth()+1) + '-' + d.getFullYear()
            var ONEDAY = 1000 * 60 * 60 * 24;
            // Convert both dates to milliseconds
            var date1_ms = firstDate.getTime();
            var date2_ms = secondDate.getTime();
            // Calculate the difference in milliseconds
            var difference_ms = Math.abs(date1_ms - date2_ms);

            // Convert back to days and return
            var differentDays= Math.round(difference_ms/ONEDAY);
             console.log("diff days",differentDays);

             if(differentDays>15 && differentDays<20){
console.log("in 15");

var emailData={
    To:element.emp_email,
    
    'RMGPNo':element.REQUEST_NO,
    'SupplierCode':element.SUPPLIER_CODE,
    'SupplierName':element.SUPPLIER_NAME,
    Subject:"Reminder Mail:"+element.REQUEST_NO +" # from Supplier "+ element.SUPPLIER_NAME +" exceeded return date",
    Regards:'Technical Team',
}
request.sendNotificationEmail(emailData);
buildResponse(res, 200, {
       error: false,
       message: 'Email Intimation Notification Send Successfully'
   });
             }
             if(differentDays>20 && differentDays>15 ){
                console.log("in 20");
                var emailData={
                    To:element.emp_email,
                    Cc:element.APPROVAL_MAILID,
                    'RMGPNo':element.REQUEST_NO,
                    'SupplierCode':element.SUPPLIER_CODE,
                    'SupplierName':element.SUPPLIER_NAME,
                    Subject:"Reminder Mail:"+element.REQUEST_NO +" # from Supplier "+ element.SUPPLIER_NAME +" exceeded return date",
                    Regards:'Technical Team',
                }
                request.sendNotificationEmail(emailData);
                buildResponse(res, 200, {
                       error: false,
                       message: 'Email Intimation Notification Send Successfully'
                   });
             }
            
            //console.log("element.RETURN_DATE",element.RETURN_DATE);
   
  //timeDifference = Math.abs(secondDate.getTime() - firstDate.getTime());

// console.log("timediff",timeDifference);
// // //alert(timeDifference)          
  

  //          alert(differentDays);
        //     if (element.cad_lic_decision != 3) {
        //         if(currentDate > element.Created_At) {
        //             console.log("1");
        //             var emailData={
        //                 To:element.cadlicemail,
        //                 Cc:element.modelleremail,
        //                 'Die Number':element.die_number,
        //                 'Die Element':element.die_element,
        //                 'Model Location':element.model_location,
        //                 Subject:"Reminder Mail:"+element.die_number +" # "+ element.die_element +"  Submitted for approval",
        //                 Regards:'IT Team',
        //             }
        //             request.sendRequestorEmail(emailData);
        //             buildResponse(res, 200, {
        //                    error: false,
        //                    message: 'Email Intimation Notification Send Successfully'
        //                });
        //         }
        //     }else if(element.designer_decision!=3){
        //         if(currentDate > element.Updated_At) {
        //             console.log("2");
        //             var emailData={
        //                 To:element.designeremail,
        //                 Cc:element.modelleremail+","+element.cadlicemail,
        //                 'Die Number':element.die_number,
        //                 'Die Element':element.die_element,
        //                 'Model Location':element.model_location,
        //                 Subject:"Reminder Mail:"+element.die_number +" # "+ element.die_element +"  Submitted for approval",
        //                 Regards:'IT Team',
        //             }
        //             request.sendRequestorEmail(emailData);
        //             buildResponse(res, 200, {
        //                    error: false,
        //                    message: 'Email Intimation Notification Send Successfully'
        //                });
        //         } 
        //     }else if(element.designer_lic_decision!=3){
        //         if(currentDate > element.Updated_At) {
        //             console.log("3");
        //             var emailData={
        //                 To:element.designerlicemail,
        //                 Cc:element.modelleremail+","+element.cadlicemail+','+element.designeremail,
        //                 'Die Number':element.die_number,
        //                 'Die Element':element.die_element,
        //                 'Model Location':element.model_location,
        //                 Subject:"Reminder Mail: "+element.die_number +" # "+ element.die_element +"  Submitted for approval",
        //                 Regards:'IT Team',
        //             }
        //             request.sendRequestorEmail(emailData);
        //             buildResponse(res, 200, {
        //                    error: false,
        //                    message: 'Email Intimation Notification Send Successfully'
        //                });
        //         } 
        //     }
        // });
        // console.log("datadata=",data);
    });

})
 });




////////////////////////////////////////////////////////




function buildResponse(res, statusCode, responseData) {
    res.status(statusCode).json(responseData);
}

module.exports = router;