const Sequelize = require('sequelize');
var con = require('./connection');
const Op = Sequelize.Op;
var groupobj = {};
module.exports = class Master {
    constructor() {
        this.M_AWARD_RECOGNITION = con.define('M_AWARD_RECOGNITION', {
            PK_AWARD_ID: {
                type: Sequelize.INTEGER,
                primaryKey: true,
                autoIncrement: true
            },
            AWARD_HEADING: {
                type: Sequelize.STRING,
                allowNull: true,

            },

            AWARD_DESCRIPTION: {
                type: Sequelize.TEXT,
                allowNull: true
            },
            CREATED_BY: {
                type: Sequelize.TEXT,
                allowNull: true
            },
            CREATED_DATE: {
                type: Sequelize.DATE,
                allowNull: true
            },
            Status: {
                type: Sequelize.TEXT,
                allowNull: true
            }
        });
        this.M_AWARD_RECOGNITION_DTLs = con.define('M_AWARD_RECOGNITION_DTLs', {


            PK_AWRD_DTL_ID: {
                type: Sequelize.INTEGER,
                primaryKey: true,
                autoIncrement: true
            },
            FK_AWRD_ID: {
                type: Sequelize.STRING,
                allowNull: true,

            },

            IMAGE_NAME: {
                type: Sequelize.TEXT,
                allowNull: true
            },

        });
    }

    savnewData(newsdata) {
        return sequelize.sync().then(() => {
            console.log("newsdata", awardData);
            return this.Master_Award.create({
                AWARD_HEADING: awardData.awardName,

                AWARD_DESCRIPTION: awardData.awardDescription,


            })
        }).catch(function (err) {
            return err;
        });
    }
    saveData(awardData) {

        console.log("in save==", awardData);
        return sequelize.sync().then(() => {



            return this.M_AWARD_RECOGNITION.create({
                AWARD_HEADING: awardData.awardName,

                AWARD_DESCRIPTION: awardData.awardDescription,
                CREATED_BY: awardData.createdby,
                CREATED_DATE: awardData.date,

                Status: awardData.status,


            }).then(function (x) {
                console.log("id==", x);
                return x.PK_AWARD_ID;
            });

            //console.log("id==",PK_AWARD_ID);

        }).catch(function (err) {
            return err;
        });
    }


    saveimagedata(imageData, id) {
      //  console.log("in image data", imageData);
        return sequelize.sync().then(() => {
          //  console.log("in image data sync", id);
          //  console.log("Query=", "INSERT INTO  M_AWARD_RECOGNITION_DTLs (FK_AWRD_ID,IMAGE_NAME) VALUES(" + id + ",'" + imageData + "')");
            return sequelize.query("INSERT INTO  M_AWARD_RECOGNITION_DTLs (FK_AWRD_ID,IMAGE_NAME) VALUES(" + id + ",'" + imageData + "')", {

                type: sequelize.QueryTypes.INSERT
            })

        }).catch(function (err) {
            return err;
        });
    }

    getbirthdayData() {

        console.log("in get bday");
        return sequelize.sync().then(() => {
            return sequelize.query("select FULL_NAME, CAST(DATE_OF_BIRTH AS DATE),MOBILE_NO,DEPARTMENT from V_INTRANET_PORTAL where DATEPART(DD, DATE_OF_BIRTH) = DATEPART(DD, GETDATE() ) and DATEPART(MM, DATE_OF_BIRTH) = DATEPART(MM, GETDATE() ) ");
        });


    }

    getAwardData() {
        return sequelize.sync().then(() => {
            return this.M_AWARD_RECOGNITION.all();
        });
    }

    getAwardDataSignin() {
        return sequelize.sync().then(() => {
            return sequelize.query(" select ma.PK_AWARD_ID,ma.AWARD_HEADING,ma.AWARD_DESCRIPTION,ma.Status, ma.createdAt, ma.Status from M_AWARD_RECOGNITIONs ma  where ma.Status='enable' order by ma.PK_AWARD_ID desc ", {

                type: sequelize.QueryTypes.SELECT
            })



        });
    }


    getApplicationById(id) {
        return sequelize.sync().then(() => {
            return sequelize.query(" select ma.AWARD_HEADING,ma.AWARD_DESCRIPTION,ma.Status,md.PK_AWRD_DTL_ID,md.IMAGE_NAME,ma.createdAt from M_AWARD_RECOGNITIONs ma left join M_AWARD_RECOGNITION_DTLs md on ma.PK_AWARD_ID =md.FK_AWRD_ID where ma.PK_AWARD_ID='" + id + "'", {

                type: sequelize.QueryTypes.SELECT
            })



        });
    }


    getAllAwards() {
        var cnt = 0;
        var groupObj = {};
        var imageKey = "imageName";
        var arr = [];
        return sequelize.sync().then(() => {
            return sequelize.query("select ma.AWARD_HEADING,ma.PK_AWARD_ID, ma.createdAt, ma.AWARD_DESCRIPTION,ma.createdAt from M_AWARD_RECOGNITIONs ma order by PK_AWARD_ID desc", {
                type: sequelize.QueryTypes.SELECT
            });
        }).catch(function (err) {
            return err;
        });

    }



    getAllImagesWithAwardId() {
        // console.log("asfdghishudfsdfsdf", this.getAllAwards());
        return sequelize.sync().then(() => {
            return sequelize.query("select  image_name, FK_AWRD_ID from M_AWARD_RECOGNITION_DTLs", {
                type: sequelize.QueryTypes.SELECT
            });

        }).catch(function (err) {
            return err;
        });

    }

    getImageData(id) {
        console.log("id=", id);
        return sequelize.sync().then(() => {
            return this.M_AWARD_RECOGNITION_DTLs.all({
                where: {
                    FK_AWRD_ID: id
                },
            });
        });
    }



    getNewsDataById(id) {
        return sequelize.sync().then(() => {
            return this.M_AWARD_RECOGNITION.findOne({
                where: {
                    PK_AWARD_ID: id
                },
            });
        });
    }
    updateApplication(awardData, Id) {
        // console.log("data",awardData);
        //console.log("id=",Id);
        return sequelize.sync().then(() => {
            return this.M_AWARD_RECOGNITION.update({
                AWARD_HEADING: awardData.awardName,

                AWARD_DESCRIPTION: awardData.awardDescription,
                Status: awardData.Status
            }, {
                    where: {
                        PK_AWARD_ID: Id
                    }
                }).catch(function (err) {
                    if (err == "SequelizeUniqueConstraintError: Validation error") {
                        return 0;
                    }
                });
        }).catch(function (err) {
            return err;
        });
    }

    checkfordata(Id){
        var count=0;
        return sequelize.query("select * from M_AWARD_RECOGNITION_DTLs where FK_AWRD_ID ='"+Id+"' ", {
           
            type: sequelize.QueryTypes.select
         })
    }
    deleteAward(Id) {
        console.log("inside model");
        return sequelize.sync().then(() => {
            return this.M_AWARD_RECOGNITION.destroy({
                where: {
                    PK_AWARD_ID: Id
                }
            }).then(()=>{
                console.log("query;=delete from M_AWARD_RECOGNITION_DTLs where FK_AWRD_ID ='"+Id+"'");
                return sequelize.query("delete from M_AWARD_RECOGNITION_DTLs where FK_AWRD_ID ='"+Id+"' ", {
                    type: sequelize.QueryTypes.destroy
                 })

            })

        });
    }
    deleteImage(Id) {
        return sequelize.sync().then(() => {
            return this.M_AWARD_RECOGNITION_DTLs.destroy({
                where: {
                    FK_AWRD_ID: Id
                }
            });
        });
    }
    getApplication(appId) {
        return sequelize.sync().then(() => {
            return this.APPLICATION.findAll({
                where: {
                    app_id: {
                        [Op.or]: appId
                    }
                }

            });
        });
    }

};