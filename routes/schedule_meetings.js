var express = require('express');
var router = express.Router();
var bcrypt = require('bcryptjs');
var jwt = require('jsonwebtoken');
const Joi = require('joi');
var schedule_meetings = require('../model/schedule_meetings');
var schedule_meetings_master = new schedule_meetings();
var myLogger = require('../middleware.js');
var MyLogModel = require('../model/myLog');
var log = new MyLogModel();

const schema = Joi.object().keys({
    
    meeting_id: Joi.number().required().not(''),
    meeting_template_id:Joi.number().required().not(''),
    frequency:Joi.number().required().not(''),
    meeting_date:Joi.string().max(10).allow(),
    meeting_start_time:Joi.string().max(10).allow(),
    meeting_end_time:Joi.string().max(10).allow(),
    location:Joi.string().max(50).allow(),
    attendies:Joi.number().required().not(''),
    description:Joi.string().max(255).allow(),
    //is_publish
    //completed_flag
    created_by:Joi.number().required().not(''),
   // created_date_time
    
    //updated_date_time
  // deleted_by:Joi.number().required().not(''),
    // deleted_date_time

    
     
});

const schemaUpdate = Joi.object().keys({
    meeting_id: Joi.number().required().not(''),
    meeting_template_id:Joi.number().required().not(''),
    frequency:Joi.number().required().not(''),
    meeting_date:Joi.string().max(20).allow(),
    meeting_start_time:Joi.string().max(10).allow(),
    meeting_end_time:Joi.string().max(10).allow(),
    location:Joi.string().max(50).allow(),
    attendies:Joi.number().required().not(''),
    description:Joi.string().max(255).allow(),
    schedule_meeting_id:Joi.number().required().not(''),
    //is_publish
    //completed_flag
  
   // created_date_time
    updated_by:Joi.number().required().not(''),
    //updated_date_time
   // deleted_by
    // deleted_date_time
     
});

router.post('/saveScheduleMeetings', myLogger, function (req, res, next) {
    
    var ScheduleMeetingsData = {
        meeting_id: req.body.meeting_id,
        meeting_template_id:req.body.meeting_template_id,
        frequency:req.body.frequency,
        meeting_date:req.body.meeting_date,
        meeting_start_time:req.body.meeting_start_time,
        meeting_end_time:req.body.meeting_end_time,
        location:req.body.location,
        attendies:req.body.attendies,
        description:req.body.description,
        created_by:req.body.created_by,
   };
  

    Joi.validate({
        meeting_id: req.body.meeting_id,
        meeting_template_id:req.body.meeting_template_id,
        frequency:req.body.frequency,
        meeting_date:req.body.meeting_date,
        meeting_start_time:req.body.meeting_start_time,
        meeting_end_time:req.body.meeting_end_time,
        location:req.body.location,
        attendies:req.body.attendies,
        description:req.body.description,
        created_by:req.body.created_by,
     
    }, schema, function (err, ScheduleMeetingsData) {
        if (err) {

            console.log("error found");
            buildResponse(res, 200, {
                error: true,
                message: err["message"]
            });
        }
        else {
            schedule_meetings_master.saveScheduleMeetings(ScheduleMeetingsData)
                .then((data) => {
                   console.log(data,"-----------")
                    if (data["data"] == 0) {
                        buildResponse(res, 200, {
                            error: true,
                            message: data["errMsg"]
                        });
                    } else {
                        buildResponse(res, 200, {
                            error: false,
                            message: "Stored Successfully"
                        });

                    }

                }).catch((errLog) => {
                   
                    log.addLog('route :: schedule_meetings.js ' + errLog);
                    buildResponse(res, 500, {
                        error: true,
                        message: "server error"
                    });
                });
        }
    });
});

router.post('/updateScheduleMeetings', myLogger, function (req, res, next) {
    
    var ScheduleMeetingsData = {
        meeting_id: req.body.meeting_id,
        meeting_template_id:req.body.meeting_template_id,
        frequency:req.body.frequency,
        meeting_date:req.body.meeting_date,
        meeting_start_time:req.body.meeting_start_time,
        meeting_end_time:req.body.meeting_end_time,
        location:req.body.location,
        attendies:req.body.attendies,
        description:req.body.description,
       
        updated_by:req.body.updated_by,
        schedule_meeting_id:req.body.schedule_meeting_id,
   };
   
//console.log("dt-------------",ScheduleMeetingsData);
    Joi.validate({
        meeting_id: req.body.meeting_id,
        meeting_template_id:req.body.meeting_template_id,
        frequency:req.body.frequency,
        meeting_date:req.body.meeting_date,
        meeting_start_time:req.body.meeting_start_time,
        meeting_end_time:req.body.meeting_end_time,
        location:req.body.location,
        attendies:req.body.attendies,
        description:req.body.description,
       
        updated_by:req.body.updated_by,
        schedule_meeting_id:req.body.schedule_meeting_id
     
     
    }, schemaUpdate, function (err, ScheduleMeetingsData) {
        //console.log("dt-------------",ScheduleMeetingsData.schedule_meeting_id);
        if (err) {

            buildResponse(res, 200, {
                error: true,
                message: err["message"]
            });
        }
        else {
            schedule_meetings_master.updateScheduleMeetings(ScheduleMeetingsData)
                .then((data) => {
                  // console.log(data,"-----------")
                    if (data["data"] == 0) {
                        buildResponse(res, 200, {
                            error: true,
                            message: data["errMsg"]
                        });
                    } else {
                        buildResponse(res, 200, {
                            error: false,
                            message: "Stored Successfully"
                        });

                    }

                }).catch((errLog) => {
                   
                    log.addLog('route :: schedule_meetings.js ' + errLog);
                    buildResponse(res, 500, {
                        error: true,
                        message: "server error"
                    });
                });
        }
    });
});

router.get('/getScheduleMeetings', myLogger, function (req, res, next) {
    

    schedule_meetings_master.getScheduleMeetings().then((deptData) => {
        
        if (deptData["data"] == '0') {
            buildResponse(res, 200, {
                error: false,
                message: deptData["errMsg"],

            });
        } else {
            buildResponse(res, 200, {
                error: false,
                message: "Successfully display!",
                deptData: deptData
            });
        }

    });

});

router.post('/deleteScheduleMeetings', myLogger, function (req, res, next) {
    //var department = new Department();
    var schedule_meeting_data = {
        schedule_meeting_id:req.body.schedule_meeting_id,
        deleted_by:req.body.deleted_by             
    };
    //console.log(schedule_meeting_id);
    schedule_meetings_master.deleteScheduleMeetings(schedule_meeting_data)
        .then((deptData) => {
            buildResponse(res, 200, {
                error: false,
                message: 'Department Deleted successfully',
                deptData: deptData
            });
        }).catch((errLog) => {
            log.addLog('route :: schedule_meetings.js ' + errLog);
            buildResponse(res, 404, {
                error: false,
                message: 'Please Try Again',
            });
        });
});

function buildResponse(res, statusCode, responseData) {
    res.status(statusCode).json(responseData);
}

module.exports = router;