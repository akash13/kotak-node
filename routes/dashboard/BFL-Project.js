var express = require('express');
var router = express.Router();
var bcrypt = require('bcryptjs');
var jwt = require('jsonwebtoken');
const Joi = require('joi');
var KSDashboard = require('../../model/KSDashboard');
var path = require('path')

var myLogger = require('../../middleware.js');

router.get('/getDashboardTotalCount/:tableName',myLogger, function (req, res, next) {
    var request = new KSDashboard();
var tablenameval = req.params.tableName;
console.log("in route",tablenameval);
 
  var totalcount=0;
//    for (var i=0;i< req.body.length;i++)
//    {
    
    request.getDashboardTotalCount(tablenameval).then((countData) => {

  //      console.log("countdata=",countData)
        buildResponse(res, 200, {
            error: false,
            message: 'Forms Fetched successfully',
            listData:countData,
            countData: countData.length
           
        });
    });

  //}

});


router.get('/getDashboardPendingCount/:tableName/:columnname',myLogger, function (req, res, next) {
    var request = new KSDashboard();
     var tablenameval = req.params.tableName;
    var colomname=req.params.columnname;

//console.log("tablenameval",tablenameval);
  var totalcount=0;
    request.getDashboardPendingCount(tablenameval,colomname).then((countData) => {

        buildResponse(res, 200, {
            error: false,
            message: 'Forms Fetched successfully',
            listData:countData,
            countData: countData.length
           
        });
    });



});


router.get('/getDashboardCompletedCount/:tableName/:columnname',myLogger, function (req, res, next) {
    var request = new KSDashboard();
    var tablenameval = req.params.tableName;
    var colomname=req.params.columnname;

  var totalcount=0;
    request.getDashboardCompletedCount(tablenameval,colomname).then((countData) => {

        buildResponse(res, 200, {
            error: false,
            message: 'Forms Fetched successfully',
            listData:countData,
            countData: countData.length
            

        });
    });



});

router.get('/getDashboardHoldCount/:tableName/:columnname',myLogger, function (req, res, next) {
    var request = new KSDashboard();
    var tablenameval = req.params.tableName;
    var colomname=req.params.columnname;

  var totalcount=0;
    request.getDashboardHoldCount(tablenameval,colomname).then((countData) => {

        buildResponse(res, 200, {
            error: false,
            message: 'Forms Fetched successfully',
            listData:countData,
            countData: countData.length
           
        });
    });

 

});

router.get('/getDashboardIssueCount/:tableName/:columnname',myLogger, function (req, res, next) {
    var request = new KSDashboard();
    var tablenameval = req.params.tableName;
    var colomname=req.params.columnname;
    console.log("colomname=",colomname);

  var totalcount=0;
    request.getDashboardIssueCount(tablenameval,colomname).then((countData) => {

        buildResponse(res, 200, {
            error: false,
            message: 'Forms Fetched successfully',
            listData:countData,
            countData: countData.length
           
        });
    });

 

});

router.get('/getDashboardYTSCount/:tableName/:columnname',myLogger, function (req, res, next) {
    var request = new KSDashboard();
    var tablenameval = req.params.tableName;
    var colomname=req.params.columnname;

  var totalcount=0;
    request.getDashboardYTSCount(tablenameval,colomname).then((countData) => {

        buildResponse(res, 200, {
            error: false,
            message: 'Forms Fetched successfully',
            listData:countData,
            countData: countData.length
           
        });
    });

 

});



router.get('/getDashboardRejectedCount/:tableName/:columnname',myLogger, function (req, res, next) {
    var request = new KSDashboard();
    var tablenameval = req.params.tableName;
    var colomname=req.params.columnname;

    var totalcount = 0;
    request.getDashboardRejectedCount(tablenameval,colomname).then((countData) => {

        buildResponse(res, 200, {
            error: false,
            message: 'Forms Fetched successfully',
            listData: countData,
            countData: countData.length

        });
    });



});

function buildResponse(res, statusCode, responseData) {
    res.status(statusCode).json(responseData);
}

module.exports = router;
