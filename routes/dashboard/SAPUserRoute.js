var express = require('express');
var router = express.Router();
var SAPInfoModel = require('../../model/SAPUserModel');

var myLogger = require('../../middleware.js');
var options = {
    wsdl_headers: {
        'Authorization': 'Basic ' + new Buffer('piwebusr' + ':' + 'piproxy123').toString('base64'),
    }
};


router.get('/getUserDetailsForPirForm/:id', myLogger, function (req, res, next) {
    console.log("in mroute", req.params.id)
    var request = new SAPInfoModel();
    var id = req.params.id;
    request.getUserDetailsForPirForm(id).then((data) => {
        buildResponse(res, 200, {
            error: false,
            message: "Successfully display!",
            responseData: data
        });
    });
});


router.get('/getSAPUserList/', myLogger, function (req, res, next) {
    //console.log("in mroute", req.params.id)
    var request = new SAPInfoModel();
    //var id = req.params.id;
    request.getSAPUserList().then((data) => {
        buildResponse(res, 200, {
            error: false,
            message: "Successfully display!",
            responseData: data
        });
    });
});


router.get('/deleteSAPRec/id', myLogger, function (req, res, next) {
    //console.log("in mroute", req.params.id)
    var request = new SAPInfoModel();
    var id = req.params.id;
    request.deleteSAPRec(id).then((data) => {

        console.log('data==',data);
        buildResponse(res, 200, {
            error: false,
            message: "Successfully display!",
            responseData: data
        });
    });
});





router.get('/getApprovalMatrix/', myLogger, function (req, res, next) {
    //console.log("in mroute", req.params.id)
    var request = new SAPInfoModel();
    //var id = req.params.id;
    request.getApprovalMatrix().then((data) => {

        console.log('data==',data);
        buildResponse(res, 200, {
            error: false,
            message: "Successfully display!",
            responseData: data
        });
    });
});


router.post('/submitData', myLogger, function (req, res, next) {

    var request = new SAPInfoModel();
    var data = req.body;
    console.log("dataaaaa", data);

    request.submitData(data)
        .then((data) => {
            buildResponse(res, 200, {
                error: false,
                message: "Record Inserted Successfully",
                responseData: data
            });

        }).catch((err) => {
            buildResponse(res, 200, {
                error: true,
                message: "Fail to Store"
            });
        });

})



router.post('/updateData', myLogger, function (req, res, next) {

    var request = new SAPInfoModel();
    var data = req.body;
    console.log("dataaaaa", data);

    request.updateData(data)
        .then((data) => {
            buildResponse(res, 200, {
                error: false,
                message: "Record Inserted Successfully",
                responseData: data
            });

        }).catch((err) => {
            buildResponse(res, 200, {
                error: true,
                message: "Fail to Store"
            });
        });

})



router.get('/getDetailsOnUpdateForm/:id', myLogger, function (req, res, next) {

    var request = new SAPInfoModel();
    var id = req.params.id;

    request.getDetailsOnUpdateForm(id)
        .then((data) => {
            buildResponse(res, 200, {
                error: false,
                message: "Record Inserted Successfully",
                responseData: data
            });

        }).catch((err) => {
            buildResponse(res, 200, {
                error: true,
                message: "Fail to Store"
            });
        });

})




function buildResponse(res, statusCode, responseData) {
    res.status(statusCode).json(responseData);
}

module.exports = router;