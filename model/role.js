const Sequelize = require('sequelize');
var con = require('./connection');
var randomstring = require("randomstring");
var bcrypt = require('bcryptjs');
var MyLogModel = require('../model/myLog');
var log = new MyLogModel();
const Op = Sequelize.Op;
var errMsg = "Server issue please try after sometime";

Sequelize.DATE.prototype._stringify = function _stringify(date, options) {
    return this._applyTimezone(date, options).format('YYYY-MM-DD HH:mm:ss.SSS');
  };
module.exports = class Department {

    constructor() {

    
        this.ROLE_Master = con.define('M_Role_Master', {
            T_INT_Role_Id: {
                type: Sequelize.INTEGER,
                primaryKey: true,
                autoIncrement: true
            },
           
            T_CHR_Role_Name: {
                type: Sequelize.STRING(20),
                allowNull: false
            },
           
            
            T_CHR_Role_Status: {
                type: Sequelize.STRING(10),
                allowNull: false
            },

            
         
            T_CHR_Isdelete: {
                type: Sequelize.STRING(2),
                allowNull: false,
                defaultValue:'0'
               
            },
            T_CHR_Created_By: {
                type: Sequelize.STRING(20),
                allowNull: true
            },
            T_DATE_Created_Date_Time: {
                type: Sequelize.DATE,
                allowNull: true
            },
            T_CHR_Updated_By: {
                type: Sequelize.STRING(20),
                allowNull: true
            },
            T_DATE_Updated_Date_Time: {
                type: Sequelize.DATE,
                allowNull: true
            },
            T_CHR_Deleted_By: {
                type: Sequelize.STRING(20),
                allowNull: true
            },
            T_DATE_Deleted_Date_Time: {
                type: Sequelize.DATE,
                allowNull: true
            }
        },
             {
                 timestamps:false,
                createdAt: false,
                UpdatedAt: false,
            },
             
            
        );
    }

    saveRole(roleData) {
        return sequelize.sync().then(() => {
            return this.ROLE_Master.create({
              
                T_CHR_Role_Name: roleData.role_name,
                T_CHR_Role_Status:roleData.role_status,
                T_CHR_Created_By: roleData.created_by,

                T_DATE_Updated_Date_Time:new Date()
              
            }).catch(function (errLog) {
                console.log("errMsg:", errLog)
                log.addLog('model :: Role.js ' + errLog);
                var err;
                if (errLog == "SequelizeUniqueConstraintError: Validation error") {
                    err = {
                        data: 0,
                        errMsg: "Role name should be unique"
                    }
                } else if (errLog != "SequelizeUniqueConstraintError: Validation error") {
                    err = {
                        data: 0,
                        errMsg: errLog["errors"][0]["message"]
                    }

                }
                return err;
            });
        }).catch(function (errLog) {
            log.addLog('model :: Role.js ' + errLog);
        });
    }

    updateRole(roleData) {
        console.log("===========",roleData)
        return sequelize.sync().then(() => {
            return this.ROLE_Master.update({
              
                T_CHR_Role_Name: roleData.role_name,
                T_CHR_Role_Status:roleData.role_status,
                T_CHR_Created_By: roleData.updated_by,
                T_DATE_Updated_Date_Time:new Date()
              
            },{
                where:{
                    T_INT_Role_Id:roleData.role_id
                }
            }).catch(function (errLog) {
                console.log("errMsg:", errLog)
                log.addLog('model :: Role.js ' + errLog);
                var err;
                if (errLog == "SequelizeUniqueConstraintError: Validation error") {
                    err = {
                        data: 0,
                        errMsg: "Role name should be unique"
                    }
                } else if (errLog != "SequelizeUniqueConstraintError: Validation error") {
                    err = {
                        data: 0,
                        errMsg: errLog["errors"][0]["message"]
                    }

                }
                return err;
            });
        }).catch(function (errLog) {
            log.addLog('model :: ROle.js ' + errLog);
        });
    }
    getRoles(){
        console.log("in role table-------------------------------");
        return sequelize.sync().then(() => {
            return this.ROLE_Master.findAll({
                where:{ T_CHR_Isdelete:'0' }
            }).then((data) => {
                return data
            }).catch((errLog) => {
                var err = {
                    data: 0,
                    errMsg: errMsg
                }
                log.addLog('model :: Role.js 160' + errLog);
                return err;
                
            });
        }).catch((errLog) => {
            var err = {
                data: 0,
                errMsg: errMsg
            }
            log.addLog('model :: role.js 169 ' + errLog);
            return err;
        });
    }

    deleteRole(roleData) {
        return sequelize.sync().then(() => {
            return this.ROLE_Master.update({
                is_delete:'1',
                deleted_by:roleData.deleted_by,
                deleted_date_time:new Date()
            },{
                where: {
                    role_id: roleData.role_id
                }
            }).catch((errLog) => {
                var err = {
                    data: 0,
                    errMsg: errMsg
                }
                log.addLog('model :: role.js 184 ' + errLog);
                return err;
            });
        });
    }

};