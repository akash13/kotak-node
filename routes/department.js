var express = require('express');
var router = express.Router();
var bcrypt = require('bcryptjs');
var jwt = require('jsonwebtoken');
const Joi = require('joi');
var Department = require('../model/department');

var myLogger = require('../middleware.js');
var MyLogModel = require('../model/myLog');
var log = new MyLogModel();

const schemaCreate = Joi.object().keys({
    department_name: Joi.string().regex(/^[a-zA-Z ]*$/, '. Special symbols and numbers are not allow').max(20).required().not(''),
    department_status: Joi.string().max(10).required().not(''),
    created_by: Joi.string().not(''),
  
});

const schemaUpdate = Joi.object().keys({
    department_name: Joi.string().regex(/^[a-zA-Z ]*$/, '. Special symbols and numbers are not allow').max(20).required().not(''),
    department_status: Joi.string().max(10).required().not(''),
   
    updated_by: Joi.string().allow(''),
});

router.post('/saveDepartment', myLogger, function (req, res, next) {
    var department = new Department();
    var deptData = {
        department_name: req.body.department_name,
        department_status: req.body.department_status,
        created_by: req.body.created_by,
   };

    Joi.validate({
        department_name: req.body.department_name,
        department_status: req.body.department_status,
        created_by: req.body.created_by,
     
     
    }, schemaCreate, function (err, deptData) {
        if (err) {

            buildResponse(res, 200, {
                error: true,
                message: err['details'][0]["message"]
            });
        }
        else {
            department.saveDepartment(deptData)
                .then((data) => {
                   console.log(data,"-----------")
                    if (data["data"] == 0) {
                        buildResponse(res, 200, {
                            error: true,
                            message: data["errMsg"]
                        });
                    } else {
                        buildResponse(res, 200, {
                            error: false,
                            message: "Stored Successfully"
                        });

                    }

                }).catch((errLog) => {
                   
                    log.addLog('route :: department.js ' + errLog);
                    buildResponse(res, 500, {
                        error: true,
                        message: "server error"
                    });
                });
        }
    });
});

router.post('/updateDepartment', myLogger, function (req, res, next) {
    var department = new Department();
    var deptData = {
        department_id:req.body.department_id,
        department_name: req.body.department_name,
        department_status: req.body.department_status,
        updated_by: req.body.updated_by,
   };
console.log(deptData,"---------")
    Joi.validate({
        department_name: req.body.department_name,
        department_status: req.body.department_status,
        updated_by: req.body.updated_by,
     
     
    }, schemaUpdate, function (err, schemaDeptData) {
        if (err) {

            buildResponse(res, 200, {
                error: true,
                message: err['details'][0]["message"]
            });
        }
        else {
            department.updateDepartment(deptData)
                .then((data) => {
                   console.log(data,"-----------")
                    if (data["data"] == 0) {
                        buildResponse(res, 200, {
                            error: true,
                            message: data["errMsg"]
                        });
                    } else {
                        buildResponse(res, 200, {
                            error: false,
                            message: "Updated Successfully"
                        });

                    }

                }).catch((errLog) => {
                   
                    log.addLog('route :: department.js ' + errLog);
                    buildResponse(res, 500, {
                        error: true,
                        message: "server error"
                    });
                });
        }
    });
});

router.get('/getDepartments', myLogger, function (req, res, next) {
    var department = new Department();

    department.getDepartments().then((data) => {
        
        if (data["data"] == '0') {
            buildResponse(res, 200, {
                error: false,
                message: data["errMsg"],

            });
        } else {
            buildResponse(res, 200, {
                error: false,
                message: "Successfully display!",
                data: data
            });
        }

    });

});

router.post('/deleteDepartment', myLogger, function (req, res, next) {
    var department = new Department();
    var deptData={
        department_id:req.body.department_id,
        deleted_by:req.body.deleted_by
    };
    //var reqData = req.body.dept_id;
    department.deleteDepartment(deptData)
        .then((data) => {
            buildResponse(res, 200, {
                error: false,
                message: 'Department Deleted successfully',
                data: data
            });
        }).catch((errLog) => {
            log.addLog('route :: department.js ' + errLog);
            buildResponse(res, 404, {
                error: false,
                message: 'Please Try Again',
            });
        });
});

function buildResponse(res, statusCode, responseData) {
    res.status(statusCode).json(responseData);
}

module.exports = router;