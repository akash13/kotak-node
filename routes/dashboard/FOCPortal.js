var express = require('express');
var router = express.Router();
var Master = require('../../model/FOCPortal');
var myLogger = require('../../middleware.js');
var imageData = [];

const Sequelize = require('sequelize');
sequelizeother = new Sequelize({
    dialect: 'mssql',
    username: 'FOCConnect',
    host: 'SQLDBSRV.KALYANICORP.COM',
    port: '1433',
    password: 'foc@bfl123',
    database: 'PLM_SAP',
    logging: true,
    options: {
        encrypt: false // Use this if you're on Windows Azure
        , instanceName: 'SQLEXPRESS'
    }
});


router.get('/getcadmodelstatus', myLogger, function (req, res, next) {
    var request = new Master();
    //console.log("in route");
    request.getcadmodelstatus().then((data) => {
        buildResponse(res, 200, {
            error: false,
            message: "Successfully display!",
            responseData: data
        });
        // console.log("awardData=",responseData);
    });


});

router.get('/getemployee', myLogger, function (req, res, next) {
    var request = new Master();
    //console.log("in route");
    request.getemployee().then((data) => {
        buildResponse(res, 200, {
            error: false,
            message: "Successfully display!",
            responseData: data
        });
        // console.log("awardData=",responseData);
    }).catch((err) => {
        buildResponse(res, 200, {
            error: false,
            message: 'Error in display',
        });
    });


});
router.get('/vname', myLogger, function (req, res, next) {
    var request = new Master();
    //console.log("in route");
    request.vname().then((data) => {
        buildResponse(res, 200, {
            error: false,
            message: "Successfully display!",
            responseData: data
        });
        // console.log("awardData=",responseData);
    });


});

router.get('/getAeroSpaceVendor', myLogger, function (req, res, next) {
    var request = new Master();
    //console.log("in route");
    request.getAeroSpaceVendor().then((data) => {
        buildResponse(res, 200, {
            error: false,
            message: "Successfully display!",
            responseData: data
        });
        // console.log("awardData=",responseData);
    });


});

router.get('/getemployeeforUpdatePage', myLogger, function (req, res, next) {
    var request = new Master();
    //console.log("in route");
    request.getemployeeforUpdatePage().then((data) => {
        buildResponse(res, 200, {
            error: false,
            message: "Successfully display!",
            responseData: data
        });
        // console.log("awardData=",responseData);
    });


});

router.get('/getemployeeOnInput/:userid', myLogger, function (req, res, next) {
    var request = new Master();
    var userid = req.params.userid;
    //console.log("in route");
    request.employeeOnInput(userid).then((data) => {
        buildResponse(res, 200, {
            error: false,
            message: "Successfully display!",
            responseData: data
        });
        // console.log("awardData=",responseData);
    });


});
router.get('/GetRecordId/:id', myLogger, function (req, res, next) {
    var request = new Master();
    var focid = req.params.id;
    //console.log("in route");
    request.GetRecordId(focid).then((data) => {
        buildResponse(res, 200, {
            error: false,
            message: "Successfully display!",
            responseData: data
        });
        // console.log("awardData=",responseData);
    });


});


router.get('/ShowFOCData/:id', myLogger, function (req, res, next) {
    var request = new Master();
    var id = req.params.id;
    //console.log("in route");
    request.ShowFOCData(id).then((data) => {
        buildResponse(res, 200, {
            error: false,
            message: "Successfully display!",
            responseData: data
        });
        // console.log("awardData=",responseData);
    });


});


router.get('/getData/', myLogger, function (req, res, next) {
    var request = new Master();

    //console.log("in route");
    request.getData().then((data) => {
        buildResponse(res, 200, {
            error: false,
            message: "Successfully display!",
            responseData: data
        });
        // console.log("awardData=",responseData);
    });


});





router.post('/saveFOCData', myLogger, function (req, res, next) {


    //console.log("inside cad route");
    var request = new Master();
    var AWARD_ID = 0;
    var vname;
    if (req.body.aerospace == true) {
        this.vname = req.body.vnameforaerospace;
    } else {
        this.vname = req.body.vname;
    }
    var FOCData = {

        FocPortal: req.body.FocPortal,
        Title: req.body.Title,
        EntryDate: req.body.EntryDate,
        Reason: req.body.Reason,
        vname: this.vname,

        aerospace: req.body.aerospace,
        material: req.body.material,
        description: req.body.description,
        uom: req.body.uom,
        quantity: req.body.quantity,
        transporterdet: req.body.transporterdet,
        vehicleno: req.body.vehicleno,
        lrno: req.body.lrno,
        lrdate: req.body.lrdate,
        buyername: req.body.buyername,
        department: req.body.department,
        indentername: req.body.indentername,
        regularization: req.body.regularization,
        returnby: req.body.returnby,
        returndate: req.body.returndate,

        createdByname: req.body.createdByname,
        today: req.body.date,
        SupplierName: req.body.SupplierName,
        SuppliershortName: req.body.SuppliershortName,
        SupplierSAPId: this.vname,

    };



    request.saveFOCData(FOCData).then((data) => {
       // console.log('dddddd', data);
        if (FOCData.aerospace == true) {
           // console.log("in iff");
            sequelizeother.authenticate().then(() => {
                    return sequelizeother.query("insert into T_FOC (id,Title,MaterialPurpose,Date,Inwarding_Reason,Vendor_Name,For_Aerospace,Material_Name,Material_Description,UOM,Quantity,Transporter_Details,Vehicle_Number,LR_Number,LR_Date,Buyer_Name,Department,Indenter_Name,Regularization_FOC_Material_Dept,Return_Date_Returnable_Materail,Return_By,Created_By,Created_Date,Supplier_Name,Supplier_SAPID,Supplier_ShortName) values('" + data[0]['id'] + "','" + FOCData.Title + "','" + FOCData.FocPortal + "','" + FOCData.EntryDate + "','" + FOCData.Reason + "','" + FOCData.vname + "','" + FOCData.aerospace + "','" + FOCData.material + "','" + FOCData.description + "','" + FOCData.uom + "','" + FOCData.quantity + "','" + FOCData.transporterdet + "','" + FOCData.vehicleno + "','" + FOCData.lrno + "','" + FOCData.lrdate + "','" + FOCData.buyername + "','" + FOCData.department + "', '" + FOCData.indentername + "', '" + FOCData.regularization + "', '" + FOCData.returndate + "', '" + FOCData.returnby + "','" + FOCData.createdByname + "','" + FOCData.today + "','" + FOCData.SupplierName + "','" + FOCData.SupplierSAPId + "','" + FOCData.SuppliershortName + "')", {
                        type: sequelizeother.QueryTypes.INSERT
                    }).then((x) => {
                        buildResponse(res, 200, {
                            error: false,
                            message: "Saved Successfully!",
                            responseData: data
                        });
                    }).catch((err) => {
                        buildResponse(res, 200, {
                            error: false,
                            message: 'Error in Inserting Data TO SAP Table ',
                        });
                    });
                }).catch(err => {
                    buildResponse(res, 200, {
                        error: true,
                        message: 'Unable to connect to the database: FOCConnect',
                        responseData: err
                    });
                   // console.error('Unable to connect to the database:', err);
                });
        } else {
            buildResponse(res, 200, {
                error: false,
                message: "Saved Successfully!",
                responseData: data
            });
        }

    }).catch((err) => {
            buildResponse(res, 200, {
                error: true,
                message: "Fail to Store reason->"+err
            });
        });
});


router.post('/UpdateFOCData', myLogger, function (req, res, next) {



    var request = new Master();
    var AWARD_ID = 0;
    var vname;
    if (req.body.aerospace == true) {
        this.vname = req.body.vnameforaerospace;
    } else {
        this.vname = req.body.vname;
    }
    var FOCData = {


        FocPortal: req.body.FocPortal,
        Title: req.body.Title,
        EntryDate: req.body.EntryDate,
        Reason: req.body.Reason,
        vname: this.vname,
        oldAerospace: req.body.oldAerospace,
        aerospace: req.body.aerospace,
        material: req.body.material,
        description: req.body.description,
        uom: req.body.uom,
        quantity: req.body.quantity,
        transporterdet: req.body.transporterdet,
        vehicleno: req.body.vehicleno,
        lrno: req.body.lrno,
        lrdate: req.body.lrdate,
        buyername: req.body.buyername,
        department: req.body.department,
        indentername: req.body.indentername,
        regularization: req.body.regularization,
        returnby: req.body.returnby,
        returndate: req.body.returndate,

        createdByname: req.body.createdByname,
        today: req.body.date,

        SupplierName: req.body.SupplierName,
        SuppliershortName: req.body.SuppliershortName,
        SupplierSAPId: this.vname,


        id: req.body.id,




    };

    console.log('FOCData', FOCData);



    request.UpdateFOCData(FOCData).then((data) => {


        if (FOCData.oldAerospace == 'false' && FOCData.aerospace == true) {
            console.log("in iff");
            sequelizeother
                .authenticate()
                .then(() => {

                    return sequelizeother.query("select Title,MaterialPurpose from T_FOC where id='" + FOCData.id + "'", {
                        type: sequelizeother.QueryTypes.SELECT
                    }).then((selectdata) => {
                        console.log("selectdata", selectdata);

                        if (selectdata.length > 5) {
                            return sequelizeother.query("update T_FOC  set Delete_Flag ='',MaterialPurpose='" + FOCData.FocPortal + "',Date='" + FOCData.EntryDate + "',Inwarding_Reason='" + FOCData.Reason + "',Vendor_Name='" + FOCData.vname + "',For_Aerospace='" + FOCData.aerospace + "',Material_Name='" + FOCData.material + "',Material_Description='" + FOCData.description + "',UOM='" + FOCData.uom + "',Quantity='" + FOCData.quantity + "'" + ",Transporter_Details= '" + FOCData.transporterdet + "',Vehicle_Number='" + FOCData.vehicleno + "',LR_Number='" + FOCData.lrno + "',LR_Date='" + FOCData.lrdate + "',Buyer_Name='" + FOCData.buyername + "',Department='" + FOCData.department + "',Indenter_Name='" + FOCData.indentername + "',Regularization_FOC_Material_Dept='" + FOCData.regularization + "',Return_Date_Returnable_Materail='" + FOCData.returndate + "', Return_By='" + FOCData.returnby + "'" + ",Modified_By='" + FOCData.createdByname + "',Modified_Date= '" + FOCData.today + "',Other_DeptValue='" + FOCData.Other_DeptValue + "',Supplier_Name='" + FOCData.Supplier_Name + "',Supplier_SAPID='" + FOCData.Supplier_SAPID + "',Supplier_ShortName='" + FOCData.Supplier_ShortName + "'  where ID ='" + FOCData.id + "' ", {
                                type: sequelizeother.QueryTypes.INSERT
                            }).then((x) => {

                                buildResponse(res, 200, {
                                    error: false,
                                    message: "insertted into remote db server Successfully!",
                                    responseData: x
                                });
                            }).catch((err) => {
                                buildResponse(res, 404, {
                                    error: false,
                                    message: 'insertted into remote db server Failed',
                                });
                            });
                        } else {
                            return sequelizeother.query("insert into T_FOC (id,Title,MaterialPurpose,Date,Inwarding_Reason,Vendor_Name,For_Aerospace,Material_Name,Material_Description,UOM,Quantity,Transporter_Details,Vehicle_Number,LR_Number,LR_Date,Buyer_Name,Department,Indenter_Name,Regularization_FOC_Material_Dept,Return_Date_Returnable_Materail,Return_By,Created_By,Created_Date,Supplier_Name,Supplier_SAPID,Supplier_ShortName) values('" + FOCData.id + "','" + FOCData.Title + "','" + FOCData.FocPortal + "','" + FOCData.EntryDate + "','" + FOCData.Reason + "','" + FOCData.vname + "','" + FOCData.aerospace + "','" + FOCData.material + "','" + FOCData.description + "','" + FOCData.uom + "','" + FOCData.quantity + "','" + FOCData.transporterdet + "','" + FOCData.vehicleno + "','" + FOCData.lrno + "','" + FOCData.lrdate + "','" + FOCData.buyername + "','" + FOCData.department + "', '" + FOCData.indentername + "', '" + FOCData.regularization + "', '" + FOCData.returndate + "', '" + FOCData.returnby + "','" + FOCData.createdByname + "','" + FOCData.today + "','" + FOCData.SupplierName + "','" + FOCData.SupplierSAPId + "','" + FOCData.SuppliershortName + "')", {
                                type: sequelizeother.QueryTypes.INSERT
                            }).then((x) => {

                                buildResponse(res, 200, {
                                    error: false,
                                    message: "insertted into remote db server Successfully!",
                                    responseData: x
                                });
                            }).catch((err) => {
                                buildResponse(res, 404, {
                                    error: false,
                                    message: 'insertted into remote db server Failed',
                                });
                            });
                        }
                    });



                })
                .catch(err => {
                    console.error('Unable to connect to the database:', err);

                });


        } else if (FOCData.oldAerospace == 'true' && FOCData.aerospace == false) {
            sequelizeother
                .authenticate()
                .then(() => {
                    return sequelizeother.query("update T_FOC  set Delete_Flag ='X',For_Aerospace='" + FOCData.aerospace + "'  where ID ='" + FOCData.id + "' ", {
                        type: sequelizeother.QueryTypes.INSERT
                    }).then((x) => {

                        buildResponse(res, 200, {
                            error: false,
                            message: "insertted into remote db server Successfully!",
                            responseData: x
                        });
                    }).catch((err) => {
                        buildResponse(res, 404, {
                            error: false,
                            message: 'insertted into remote db server Failed',
                        });
                    });




                })
                .catch(err => {
                    console.error('Unable to connect to the database:', err);

                });
        } else if (FOCData.oldAerospace == '' && FOCData.aerospace == true) {
            sequelizeother
                .authenticate()
                .then(() => {
                    return sequelizeother.query("insert into T_FOC (id,Title,MaterialPurpose,Date,Inwarding_Reason,Vendor_Name,For_Aerospace,Material_Name,Material_Description,UOM,Quantity,Transporter_Details,Vehicle_Number,LR_Number,LR_Date,Buyer_Name,Department,Indenter_Name,Regularization_FOC_Material_Dept,Return_Date_Returnable_Materail,Return_By,Created_By,Created_Date,Supplier_Name,Supplier_SAPID,Supplier_ShortName) values('" + FOCData.id + "','" + FOCData.Title + "','" + FOCData.FocPortal + "','" + FOCData.EntryDate + "','" + FOCData.Reason + "','" + FOCData.vname + "','" + FOCData.aerospace + "','" + FOCData.material + "','" + FOCData.description + "','" + FOCData.uom + "','" + FOCData.quantity + "','" + FOCData.transporterdet + "','" + FOCData.vehicleno + "','" + FOCData.lrno + "','" + FOCData.lrdate + "','" + FOCData.buyername + "','" + FOCData.department + "', '" + FOCData.indentername + "', '" + FOCData.regularization + "', '" + FOCData.returndate + "', '" + FOCData.returnby + "','" + FOCData.createdByname + "','" + FOCData.today + "','" + FOCData.SupplierName + "','" + FOCData.SupplierSAPId + "','" + FOCData.SuppliershortName + "')", {
                        type: sequelizeother.QueryTypes.INSERT
                    }).then((x) => {

                        buildResponse(res, 200, {
                            error: false,
                            message: "insertted into remote db server Successfully!",
                            responseData: x
                        });
                    }).catch((err) => {
                        buildResponse(res, 404, {
                            error: false,
                            message: 'insertted into remote db server Failed',
                        });
                    });



                })
                .catch(err => {
                    console.error('Unable to connect to the database:', err);

                });
        } else {
            // console.log('else')
            buildResponse(res, 200, {
                error: false,
                message: "insertted into remote db server Successfully!",
                responseData: data
            });
        }

    })
        .catch((err) => {
            buildResponse(res, 200, {
                error: true,
                message: "Fail to Store"
            });
        });
});





router.get('/getDashboardTotalCount/:userid', myLogger, function (req, res, next) {
    var request = new Master();
    var userid = req.params.userid;
    // var group=req.params.group;
    // var dept=req.params.dept;
    // log("in route total",group,dept);

    var totalcount = 0;
    //    for (var i=0;i< req.body.length;i++)
    //    {
    request.getDashboardTotalCount(userid).then((countData) => {
        request.getDashboardTotalCount
        //log("countdata=",countData)
        buildResponse(res, 200, {
            error: false,
            message: 'Forms Fetched successfully',
            listData: countData,
            countData: countData.length

        });
    });

    //}

});



router.get('/getDashboardPendingCount/:userid', myLogger, function (req, res, next) {
    var request = new Master();
    var userid = req.params.userid;
    // var group=req.params.group;
    // var dept=req.params.dept;
    // log("in route total",group,dept);

    var totalcount = 0;
    //    for (var i=0;i< req.body.length;i++)
    //    {
    request.getDashboardPendingCount(userid).then((countData) => {
        request.getDashboardPendingCount
        //log("countdata=",countData)
        buildResponse(res, 200, {
            error: false,
            message: 'Forms Fetched successfully',
            listData: countData,
            countData: countData.length

        });
    });

    //}

});

router.get('/deleteRequest/:id', myLogger, function (req, res, next) {
    var request = new Master();
    var reqData = req.params.id;
    request.deleteRequest(reqData)
        .then((userData) => {
            buildResponse(res, 200, {
                error: false,
                message: 'User Deleted successfully',
                UserData: userData
            });
        }).catch((err) => {
            buildResponse(res, 500, {
                error: false,
                message: 'Please Try Again',
            });
        });
});
router.get('/getDashboardRejectedCount/:userid', myLogger, function (req, res, next) {
    var request = new Master();
    var userid = req.params.userid;
    // var group=req.params.group;
    // var dept=req.params.dept;
    // log("in route total",group,dept);

    var totalcount = 0;
    //    for (var i=0;i< req.body.length;i++)
    //    {
    request.getDashboardRejectedCount(userid).then((countData) => {
        request.getDashboardRejectedCount
        //log("countdata=",countData)
        buildResponse(res, 200, {
            error: false,
            message: 'Forms Fetched successfully',
            listData: countData,
            countData: countData.length

        });
    });

    //}

});

router.get('/getDashboardCompletedCount/:userid', myLogger, function (req, res, next) {
    var request = new Master();
    var userid = req.params.userid;
    // var group=req.params.group;
    // var dept=req.params.dept;
    // log("in route total",group,dept);

    var totalcount = 0;
    //    for (var i=0;i< req.body.length;i++)
    //    {
    request.getDashboardCompletedCount(userid).then((countData) => {
        request.getDashboardCompletedCount
        // console.log("countdata=",countData)
        buildResponse(res, 200, {
            error: false,
            message: 'Forms Fetched successfully',
            listData: countData,
            countData: countData.length

        });
    });

    //}

});


router.get('/getDepartment/', myLogger, function (req, res, next) {
    var request = new Master();

    //console.log("in route");
    request.getDepartmentName().then((data) => {
        buildResponse(res, 200, {
            error: false,
            message: "Email Sent Successfully",
            responseData: data
        });
        // console.log("awardData=",responseData);
    });


});


//  }





function buildResponse(res, statusCode, responseData) {
    res.status(statusCode).json(responseData);
}

module.exports = router;