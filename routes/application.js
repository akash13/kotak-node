var express = require('express');
var router = express.Router();
var bcrypt = require('bcryptjs');
var jwt = require('jsonwebtoken');
const Joi = require('joi');
var Application = require('../model/application');
var myLogger = require('../middleware.js');
var MyLogModel = require('../model/myLog');
var log = new MyLogModel();

const schema = Joi.object().keys({
    database_connection: Joi.string().required().not(''),
    type_of_meeting: Joi.number().required().not(''),
    short_name: Joi.string().required().max(50).not(''),
    full_name: Joi.string().required().max(50).not(''),
    type_sales: Joi.string().required().max(10).not(''),
    type_meeting:Joi.string().required().max(10).not(''),
    frequency: Joi.number().required().not(''),
    department: Joi.required().not(''),
    chair: Joi.required().not(''),
    Attendees: Joi.required().not(''),
    governing_body: Joi.required().not(''),
    convener_name: Joi.string().required().not(''),
    effective_start_Date: Joi.string().required().not(''),
    effective_end_Date: Joi.string().required().not(''),
    created_by: Joi.string().required().not(''),
    
});

const schema1 = Joi.object().keys({
    database_connection: Joi.string().required().not(''),
    type_of_meeting: Joi.number().required().not(''),
    short_name: Joi.string().required().max(50).not(''),
    full_name: Joi.string().required().max(50).not(''),
    type_sales: Joi.string().required().max(10).not(''),
    type_meeting:Joi.string().required().max(10).not(''),
    frequency: Joi.number().required().not(''),
    department: Joi.required().not(''),
    chair: Joi.required().not(''),
    Attendees: Joi.required().not(''),
    governing_body: Joi.required().not(''),
    status: Joi.required().not(''),
    convener_name: Joi.string().required().not(''),
    effective_start_Date: Joi.string().required().not(''),
    effective_end_Date: Joi.string().required().not(''),
    created_by: Joi.string().required().not(''),
    meetingId: Joi.string().required().not(''),

});


router.post('/saveApplication', myLogger, function (req, res, next) {

    var request = new Application();
    var applicationData = {
        databaseConnections: req.body.databaseConnections,
        type_of_meeting: req.body.type_of_meeting,
        short_name: req.body.short_name,
        full_name: req.body.full_name,
        type_sales: req.body.type_sales,
        type_meeting:req.body.type_meeting,
        frequency: req.body.frequency,
        department: req.body.department,
        chair: req.body.chair,
        participating_members: req.body.participating_members,
        governing_body: req.body.governing_body,
        status: req.body.status,
        convener_name: req.body.convener_name,
        effective_start_Date: req.body.effective_start_Date,
        effective_end_Date: req.body.effective_end_Date,
        created_by: req.body.userId,
    };
    console.log("req.body.databaseConnections",applicationData)
    Joi.validate({
        database_connection: req.body.databaseConnections,
        type_of_meeting: req.body.type_of_meeting,
        short_name: req.body.short_name,
        full_name: req.body.full_name,
        type_sales: req.body.type_sales,
        type_meeting:req.body.type_meeting,
        frequency: req.body.frequency,
        department: req.body.department,
        chair: req.body.chair,
        Attendees: req.body.participating_members,
        governing_body: req.body.governing_body,
        
        convener_name: req.body.convener_name,
        effective_start_Date: req.body.effective_start_Date,
        effective_end_Date: req.body.effective_end_Date,
        created_by: req.body.userId
    }, schema, function (err, applicationData) {
        if (err) {
            console.log("============",err)
            buildResponse(res, 200, {
                error: true,
                message: err["message"]
            });
        }
        else {
            request.saveApplication(applicationData)
                .then((data) => {
                    console.log("this for save user ",data, "-----------")

                    if (data["data"] == 0) {
                        buildResponse(res, 200, {
                            error: true,
                            message: data["errMsg"]
                        });
                    } else {

                        
                        buildResponse(res, 200, {
                            error: false,
                            message: "Stored Successfully"
                        });

                    }

                }).catch((errLog) => {
                    log.addLog('route :: application.js ' + errLog);
                    buildResponse(res, 500, {
                        error: true,
                        message: "server error"
                    });
                });
        }
    })


});

router.get('/getApplications', myLogger, function (req, res, next) {
    var request = new Application();

    request.getApplications().then((applicationData) => {
        buildResponse(res, 200, {
            error: false,
            message: "Successfully display!",
            ApplicationData: applicationData
        });
    });
});



router.get('/getApplication/:id', myLogger, function (req, res, next) {
    var request = new Application();
    var reqData = req.params.id;
    request.getApplicationById(reqData)
        .then((applicationData) => {

            buildResponse(res, 200, {
                error: false,
                message: 'Meeting Fetched successfully',
                ApplicationData: applicationData
            });
        }).catch((errLog) => {

            log.addLog('route :: application.js ' + errLog);
                    buildResponse(res, 500, {
                        error: true,
                        message: "server error"
                    });

           
        });
});

router.post('/updateApplication', myLogger, function (req, res, next) {
    var ApplicationId = req.params.id;
    var request = new Application();
    console.log("this for update application data",req.body);

    var applicationData = {
       database_connection: req.body.databaseConnections,
        type_of_meeting: req.body.type_of_meeting,
        short_name: req.body.short_name,
        full_name: req.body.full_name,
        type_sales: req.body.type_sales,
        type_meeting:req.body.type_meeting,
        frequency: req.body.frequency,
        department: req.body.department,
        chair: req.body.chair,
        Attendees: req.body.participating_members,
        governing_body: req.body.governing_body,
        status: req.body.status,
        convener_name: req.body.convener_name,
        effective_start_Date: req.body.effective_start_Date,
        effective_end_Date: req.body.effective_end_Date,
        created_by: req.body.userId,
        meetingId: req.body.meetingId,
    };   Joi.validate({
        database_connection: req.body.databaseConnections,
        type_of_meeting: req.body.type_of_meeting,
        short_name: req.body.short_name,
        full_name: req.body.full_name,
        type_sales: req.body.type_sales,
        type_meeting:req.body.type_meeting,
        frequency: req.body.frequency,
        department: req.body.department,
        chair: req.body.chair,
        Attendees: req.body.participating_members,
        governing_body: req.body.governing_body,
        status: req.body.status,
        convener_name: req.body.convener_name,
        effective_start_Date: req.body.effective_start_Date,
        effective_end_Date: req.body.effective_end_Date,
        created_by: req.body.userId,
        meetingId: req.body.meetingId,

    }, schema1, function (err, applicationData) {
        if (err) {
            console.log("============",err)
            buildResponse(res, 200, {
                error: true,
                message: err["message"]
            });
        }else{
            console.log("this for update",applicationData);
            request.updateApplication(applicationData)
            .then((data) => {
                console.log("this update meeting error data",data);
                if (data == 0) {
                    buildResponse(res, 200, {
                        error: true,
                        message: "Meeting Name should be unique"
                    });
                }
                buildResponse(res, 200, {
                    error: false,
                    message: "Meeting Updated Successfully"
                });
            }).catch((err) => {
                buildResponse(res, 200, {
                    error: true,
                    message: "server error",
                });
            });
        }
    })
});

router.get('/deleteApplication/:id', myLogger, function (req, res, next) {
    var request = new Application();
    var reqData = req.params.id;
    request.deleteApplication(reqData)
        .then((applicationData) => {
            buildResponse(res, 200, {
                error: false,
                message: 'Meeting Deleted successfully',
                ApplicationData: applicationData
            });
        }).catch((err) => {
            buildResponse(res, 404, {
                error: false,
                message: 'Please Try Again',

            });
        });
});

router.get('/getApplicationForm/:id', myLogger, function (req, res, next) {
    var request = new Application();
    var reqData = req.params.id;
    request.getApplicationForm(id).then((applicationData) => {
        buildResponse(res, 200, {
            error: false,
            message: "Successfully display!",
            ApplicationForm: applicationData
        });

    });
});


router.post('/getMeetingDetails', myLogger, function (req, res, next) {
    var request = new Application();

    request.getMeetingDetails(req.body).then((data) => {
        buildResponse(res, 200, {
            error: false,
            message: "Successfully display!",
            data: data
        });
    });
});

router.post('/getMeetingViewDetails', myLogger, function (req, res, next) {
    var request = new Application();

    request.getMeetingViewDetails(req.body).then((data) => {
        buildResponse(res, 200, {
            error: false,
            message: "Successfully display!",
            data: data
        });
    });
});


router.post('/getMeetingDetailsForMeetingSchedule', myLogger, function (req, res, next) {
    var request = new Application();

    request.getMeetingDetailsForMeetingSchedule(req.body).then((data) => {
        buildResponse(res, 200, {
            error: false,
            message: "Successfully display!",
            data: data
        });
    });
});


router.post('/getMeetingType', myLogger, function (req, res, next) {
    var request = new Application();

    request.getMeetingType(req.body).then((data) => {
        buildResponse(res, 200, {
            error: false,
            message: "Successfully display!",
            data: data
        });
    });
});



router.post('/getFilterMeetingDetails', myLogger, function (req, res, next) {
    var request = new Application();

    request.getFilterMeetingDetails(req.body).then((data) => {
        buildResponse(res, 200, {
            error: false,
            message: "Successfully display!",
            data: data
        });
    });
});



function buildResponse(res, statusCode, responseData) {
    res.status(statusCode).json(responseData);
}

module.exports = router;