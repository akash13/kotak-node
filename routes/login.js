var express = require('express');
var router = express.Router();
var bcrypt = require('bcryptjs');
var randomstring = require("randomstring");
var jwt = require('jsonwebtoken');
const Joi = require('joi');
var User = require('../model/user');
var ActiveDirectory = require('activedirectory2');
var CryptoJS = require('crypto-js');
var rp = require('request-promise');
var MyLogModel = require('../model/myLog');
var log = new MyLogModel();
var config = {
    url: 'ldap://bflcorp.com',
    baseDN: 'dc=bflcorp,dc=com',
    username: 'adaccess@bflcorp.com',
    password: '@dAces4bfl',
}
var ad = new ActiveDirectory(config);

const schema1 = Joi.object().keys({

    username: Joi.required().not(''),
    password: Joi.required().not(''),

});

const changePassword1 = Joi.object().keys({

    currentPassword: Joi.required().not(''),
    newPassword: Joi.required().not(''),
    confirmPassword: Joi.required().not(''),
    emp_id: Joi.required().not(''),
});



// router.get('/ssoCheck', function (req, res, next) {
//     let os = require('os')
//     console.log(os.userInfo());
//     buildResponse(res, 200, {
//         error: true,
//         message: "Invalid Credentials,Please check and try again!",
//         userData:os.userInfo()
//     });
// })
router.post('/signinForVIntranet', function (req, res, next) {
    //console.log("viraj");
    var user = new User();
    var userData = {
        username: CryptoJS.AES.decrypt(req.body.username, 'ATFTUSER').toString(CryptoJS.enc.Utf8),
        password: CryptoJS.AES.decrypt(req.body.password, 'ATFTPASS').toString(CryptoJS.enc.Utf8)
    };


    user.signinForVIntranet(userData).then((data) => {
        // console.log("vintadata",data);
        if (data != null) {
            buildResponse(res, 200, {
                error: false,
                message: " Welcome to Bharat Forge!",
                Empdata: data['data'],
                token: data['token']
            });
        } else if (data == null) {
            //console.log("in nulllll",data);
            buildResponse(res, 200, {
                error: true,
                message: "Invalid Credentials,Please check and try again!"
            });
        }
    }).catch((err) => {
        buildResponse(res, 500, {
            error: true,
            message: "An error occurred at V-Intranet login !"
        });
    });

});


router.get('/setlogouttokentonull/:currentUser', function (req, res, next) {
    // console.log("in mroute", req.params.currentUser)
    var user = new User();
    var id = req.params.currentUser;
    user.setlogouttokentonull(id).then((data) => {
        buildResponse(res, 200, {
            error: false,
            message: "Successfully display!",
            responseData: data
        });
    });
});


router.post('/changePassword', function (req, res, next) {

    var user = new User();

    var userData = {
        currentPassword: req.body.currentPassword,
        newPassword: req.body.newPassword,
        confirmPassword: req.body.confirmPassword,
        emp_id: req.body.emp_id
    };
    Joi.validate({
        currentPassword: req.body.currentPassword,
        newPassword: req.body.newPassword,
        confirmPassword: req.body.confirmPassword,
        emp_id: req.body.emp_id


    }, changePassword1, function (err, userData) {
        if (err) {
            buildResponse(res, 200, {
                error: true,
                message: err['details'][0]["message"]
            });
        } else {
            console.log("this for change pass", userData)
            user.getUserByEmpIdForChangePass(userData, bcrypt.hashSync(req.body.currentPassword)).then((data) => {
                console.log("this user data ", req.body.newPass, "==", req.body.confirmPass);

                if (data != null) {
                    if (req.body.newPassword == req.body.confirmPassword) {
                        console.log("this data ", req.body.emp_id)
                        user.newPassword(req.body.emp_id, bcrypt.hashSync(req.body.newPassword)).then(() => {
                            buildResponse(res, 200, {
                                error: false,
                                data: data,
                                message: "Password Change Successfully...!!!"
                            })
                        })
                    } else {
                        buildResponse(res, 200, {
                            error: true,
                            data: data,
                            message: "New password and confirm password don't match...!!!"
                        })
                    }
                } else {

                    buildResponse(res, 200, {
                        error: true,
                        data: data,
                        message: "Current Password not correct...!!!"
                    })
                }

            }).catch((err) => {
                console.log("111111111", err)
                buildResponse(res, 200, {
                    error: true,
                    message: "An error occurred!"
                });
            });
        }

    });



});



router.post('/signin', function (req, res, next) {
    var user = new User();
    /*  var userData = {
         username: CryptoJS.AES.decrypt(req.body.username, 'ATFTUSER').toString(CryptoJS.enc.Utf8),
         password: CryptoJS.AES.decrypt(req.body.password, 'ATFTPASS').toString(CryptoJS.enc.Utf8)
     }; */
    var userData = {
        username: req.body.username,
        password: req.body.password
    };
    Joi.validate({

        username: req.body.username,
        password: req.body.password

    }, schema1, function (err, userData) {
        if (err) {
            buildResponse(res, 200, {
                error: true,
                message: err['details'][0]["message"]
            });
        } else {
            user.getUserByEmpId(userData.username).then((data) => {

                var username = 'bflcorp\\' + userData.username;
                if (data && data['dataValues']['T_CHR_emp_authentication_type'] == 'Windows AD') {
                    console.log("11111111111111");
                    if (data['dataValues']['T_CHR_Status'] == 'enable') {

                        var options = {
                            method: 'POST',
                            uri: 'https://klibusuat.mykotaklife.com/services/Authentication.svc/Authenticate',
                            body:
                            {
                                "User_id": userData.username, "password": userData.password, "Domain": "employeestg.mykotaklife.com", "Source": "FIG"
                            },
                            headers: {
                                "Esb_ID": "W57ixTbOO+M9NLPRKScUd20LUUMVDxkuQR/Zb53WXQNmIWx7jZ7j9kBZolQ6TT0A",
                                "key": "SHA512"

                            },
                            json: true // Automatically stringifies the body to JSON
                        };

                        rp(options)
                            .then(function (parsedBody) {

                                console.log("1111=", parsedBody["status"])

                                if (parsedBody["status"] == "SUCCESS") {
                                    var userToken = randomstring.generate({
                                        length: 24,
                                        charset: 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789'
                                    });
                                    var tokenData = { emp_id: data['dataValues']['T_CHR_Employee_Id'], token: userToken, data: data };
                                    //user.isuserTokenExist(tokenData);
                                    console.log('tokenData;', tokenData);
                                    var user = new User();
    
                                    user.insertForADLogin(tokenData).then((data) => {
                                        //console.log('data for AD',data)
    
                                        buildResponse(res, 200, {
                                            error: false,
                                            message: " Welcome !",
                                            Empdata: tokenData['data'],
                                            token: tokenData['token']
                                        });
    
    
                                    })

                                } else {

                                    buildResponse(res, 200, {
                                        error: true,
                                        message: "Invalids Credentials,Please check and try again!",

                                    });

                                }
                            })

                       
                    } else {

                        buildResponse(res, 200, {
                            error: true,
                            message: "You are not allowed to login, Please contact to the Administrator!"
                        });
                    }
                } else if (data && data['dataValues']['T_CHR_emp_authentication_type'] == 'Enterprise') {
                    console.log("22222222222222=======");
                    user.signIn(userData).then((data) => {

                        if (data != null) {
                            buildResponse(res, 200, {
                                error: false,

                                Empdata: data['data'],
                                token: data['token']
                            });
                        } else {
                            buildResponse(res, 200, {
                                error: true,
                                message: "Invalids Credentials,Please check and try again!"
                            });
                        }
                    }).catch((err) => {


                        buildResponse(res, 200, {
                            error: true,
                            message: "An errors occurred!"
                        });
                    });
                } else {
                    var options = {
                        method: 'POST',
                        uri: 'https://klibusuat.mykotaklife.com/services/Authentication.svc/Authenticate',
                        body:
                        {
                            "User_id": userData.username, "password": userData.password, "Domain": "employeestg.mykotaklife.com", "Source": "FIG"
                        },
                        headers: {
                            "Esb_ID": "W57ixTbOO+M9NLPRKScUd20LUUMVDxkuQR/Zb53WXQNmIWx7jZ7j9kBZolQ6TT0A",
                            "key": "SHA512"

                        },
                        json: true // Automatically stringifies the body to JSON
                    };

                    rp(options)
                        .then(function (parsedBody) {

                            console.log("1111=", parsedBody["status"])

                            if (parsedBody["status"] == "SUCCESS") {

                                console.log("in ffffffffffff")
                                var userData = {
                                    empId: parsedBody["information"]["alias"],
                                    empName: parsedBody["information"]["display_name"],
                                    empEmail: parsedBody["information"]["email_id"],
                                    empContact: '',
                                    empPassword: '',
                                    empTelephone: '',
                                    empStatus: 'enable',
                                    empRole: '2',
                                    empDesignation: parsedBody["information"]["user_designation"],
                                    empDepartment: parsedBody["information"]["user_department"],
                                    createdBy: '',
                                    emp_authentication_type: 'Windows AD'

                                };
                                user.saveUser(userData)
                                    .then((data) => {

                                        if (data["data"] == 0) {

                                            buildResponse(res, 200, {
                                                error: true,
                                                message: data["errMsg"]
                                            });
                                        } else {

                                            user.getUserByEmpId(parsedBody["information"]["alias"]).then((dataEmp) => {

                                                var userToken = randomstring.generate({
                                                    length: 24,
                                                    charset: 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789'
                                                });
                                                var tokenData = { emp_id: data['dataValues']['T_CHR_Employee_Id'], token: userToken, data: dataEmp };

                                                console.log('tokenData;', tokenData);
                                                var user = new User();

                                                user.insertForADLogin(tokenData).then((data) => {
                                                    //console.log('data for AD',data)

                                                    buildResponse(res, 200, {
                                                        error: false,
                                                        message: " Welcome!",
                                                        Empdata: tokenData['data'],
                                                        token: tokenData['token']
                                                    });


                                                })
                                            });

                                            /*   buildResponse(res, 200, {
                                                  error: false,
                                                  message: "Stored Successfully"
                                              }); */

                                        }

                                    }).catch((errLog) => {

                                        log.addLog('route :: login.js ' + errLog);
                                        buildResponse(res, 500, {
                                            error: true,
                                            message: "server error"
                                        });
                                    });


                            } else {
                                buildResponse(res, 200, {
                                    error: true,
                                    message: "Invalids Credentials,Please check and try again!",

                                });
                            }

                        })
                        .catch(function (errLog) {

                            log.addLog('route :: login.js ' + errLog);
                            buildResponse(res, 500, {
                                error: true,
                                message: "server error"
                            });
                            buildResponse(res, 200, {
                                error: true,
                                message: "Server error! Please contact to administrator.",

                            });
                            console.log("1111", errLog)
                            // POST failed...
                        });
                }
            }).catch((err) => {
                console.log("111111111", err)
                buildResponse(res, 200, {
                    error: true,
                    message: "An error occurred!"
                });
            });


        }

    });



    // user.signIn(userData).then((data) => {
    //     if (data != null) {
    //         buildResponse(res, 200, {
    //             error: false,
    //             message: " Welcome to Bharat Forge!",
    //             Empdata: data['data'],
    //             token: data['token']
    //         });
    //     } else {
    //         user.signinForVIntranet(userData).then((data) => {
    //             // console.log("vintadata",data);
    //             if (data != null) {
    //                 buildResponse(res, 200, {
    //                     error: false,
    //                     message: " Welcome to Bharat Forge!",
    //                     Empdata: data['data'],
    //                     token: data['token']
    //                 });
    //             } else if (data == null) {
    //                 //console.log("in nulllll",data);
    //                 buildResponse(res, 200, {
    //                     error: true,
    //                     message: "Invalid Credentials,Please check and try again!"
    //                 });
    //             }
    //         }).catch((err) => {
    //             buildResponse(res, 500, {
    //                 error: true,
    //                 message: "An error occurred at V-Intranet login !"
    //             });
    //         });
    //     }
    // }).catch((err) => {
    //     buildResponse(res, 200, {
    //         error: true,
    //         message: "An errors occurred!"
    //     });
    // });
    //  }
    // }).catch((err) => {
    //     buildResponse(res, 200, {
    //         error: true,
    //         message: "An error occurred!"
    //     });
    // });


});

router.post('/isToken', function (req, res, next) {
    var user = new User();

    // console.log("in is token");
    var userData = {
        token: req.body.token,
        emp_id: req.body.emp_id
    };
    user.getUserToken(userData).then((data) => {
        if (data.length > 0) {
            buildResponse(res, 200, {
                error: false,
                message: "Sucess"
            });
        } else {
            buildResponse(res, 200, {
                error: true,
                message: "An error occurred!"
            });
        }
    }).catch((err) => {
        buildResponse(res, 500, {
            error: true,
            message: "An error occurred!"
        });

    });
});





function buildResponse(res, statusCode, responseData) {
    res.status(statusCode).json(responseData);
}

module.exports = router;