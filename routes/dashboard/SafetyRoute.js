var express = require('express');
var router = express.Router();

var SafetyForm = require('../../model/SafetyModel');

var myLogger = require('../../middleware.js');



router.post('/sendRequestorEmail', function (req, res, next) {

    var request = new SafetyForm();
    // console.log("body=",req.body);
    request.sendRequestorEmail(req.body);
    buildResponse(res, 200, {
        error: false,
        message: 'Email Intimation Notification Send Successfully'
    });
});


router.get('/getCurrentRecordData/:ItemStockID', myLogger, function (req, res, next) {
    var request = new SafetyForm();
    var ItemStockID = req.params.ItemStockID;




    console.log('ItemStockID==',ItemStockID)
    request.getCurrentRecordData(ItemStockID).then((data) => {
        buildResponse(res, 200, {
            error: false,
            message: 'DATA Fetched successfully',
            responseData: data,
        });
    }).catch((err) => {
        buildResponse(res, 400, {
            error: true,
            message: 'Failed to Fetch',
        });
    });
});


router.get('/getInitaitedByPermitOfficer/', function (req, res, next) {
    var request = new SafetyForm();
 
    request.getInitaitedByPermitOfficer().then((data) => {
        buildResponse(res, 200, {
            error: false,
            message: "Successfully display!",
            responseData: data
        });
        // console.log("awardData=",responseData);
    });


});



router.get('/getInProcessCompletePending/', function (req, res, next) {
    var request = new SafetyForm();
 
    request.getInProcessCompletePending().then((data) => {
        buildResponse(res, 200, {
            error: false,
            message: "Successfully display!",
            responseData: data
        });
        // console.log("awardData=",responseData);
    });


});


router.get('/getSafetyTotalCount/', function (req, res, next) {
    var request = new SafetyForm();
 
    request.getSafetyTotalCount().then((data) => {
        buildResponse(res, 200, {
            error: false,
            message: "Successfully display!",
            responseData: data
        });
        // console.log("awardData=",responseData);
    });


});

router.get('/getTotalWorkPermitList/', function (req, res, next) {
    var request = new SafetyForm();
 
    request.getTotalWorkPermitList().then((data) => {
        buildResponse(res, 200, {
            error: false,
            message: "Successfully display!",
            responseData: data
        });
        // console.log("awardData=",responseData);
    });


});



router.get('/getHSELEaderList/:deptid', function (req, res, next) {
    var request = new SafetyForm();
 var reqid=req.params.deptid;
 
    request.getHSELEaderList(reqid).then((data) => {
        buildResponse(res, 200, {
            error: false,
            message: "Successfully display!",
            responseData: data
        });
        // console.log("awardData=",responseData);
    });


});
router.get('/getDepartmentList/', function (req, res, next) {
    var request = new SafetyForm();
 
    request.getDepartmentList().then((data) => {
        buildResponse(res, 200, {
            error: false,
            message: "Successfully display!",
            responseData: data
        });
        // console.log("awardData=",responseData);
    });


});
router.get('/getemployeeOnInput/', function (req, res, next) {
    var request = new SafetyForm();
 
    request.employeeOnInput().then((data) => {
        buildResponse(res, 200, {
            error: false,
            message: "Successfully display!",
            responseData: data
        });
        // console.log("awardData=",responseData);
    });


});

router.get('/getUserDetailsForPirForm/:id', myLogger, function (req, res, next) {
   
    var request = new SafetyForm();
    var id = req.params.id;
    request.getUserDetailsForPirForm(id).then((data) => {
        buildResponse(res, 200, {
            error: false,
            message: "Successfully display!",
            responseData: data
        });
    });
});



router.post('/submitData', myLogger, function (req, res, next) {
    var request = new SafetyForm();
    var data = req.body;
    request.submitData(data)
        .then((data) => {

            buildResponse(res, 200, {
                error: false,
                message: "Record Inserted Successfully",
                responseData: data
            });
        }).catch((err) => {
            console.log('err===',err);
            buildResponse(res, 400, {
                error: true,
                message: err
            });
        });
});


router.post('/UpdateSafetyCoordinatorData', myLogger, function (req, res, next) {
    var request = new SafetyForm();
    var data = req.body;
    request.UpdateSafetyCoordinatorData(data)
        .then((data) => {

            buildResponse(res, 200, {
                error: false,
                message: "Record Inserted Successfully",
                responseData: data
            });
        }).catch((err) => {
            console.log('err===',err);
            buildResponse(res, 400, {
                error: true,
                message: err
            });
        });
});


router.post('/UpdateElectricalDeptData', myLogger, function (req, res, next) {
    var request = new SafetyForm();
    var data = req.body;
    request.UpdateElectricalDeptData(data)
        .then((data) => {

            buildResponse(res, 200, {
                error: false,
                message: "Record Inserted Successfully",
                responseData: data
            });
        }).catch((err) => {
            console.log('err===',err);
            buildResponse(res, 400, {
                error: true,
                message: err
            });
        });
});


router.post('/UpdateCivilDeptData', myLogger, function (req, res, next) {
    var request = new SafetyForm();
    var data = req.body;
    request.UpdateCivilDeptData(data)
        .then((data) => {

            buildResponse(res, 200, {
                error: false,
                message: "Record Inserted Successfully",
                responseData: data
            });
        }).catch((err) => {
            console.log('err===',err);
            buildResponse(res, 400, {
                error: true,
                message: err
            });
        });
});


router.post('/UpdateHSEData', myLogger, function (req, res, next) {
    var request = new SafetyForm();
    var data = req.body;
    request.UpdateHSEData(data)
        .then((data) => {

            buildResponse(res, 200, {
                error: false,
                message: "Record Inserted Successfully",
                responseData: data
            });
        }).catch((err) => {
            console.log('err===',err);
            buildResponse(res, 400, {
                error: true,
                message: err
            });
        });
});


router.get('/getUserEmail/:id', myLogger, function (req, res, next) {
    var request = new SafetyForm();
    var id = req.params.id;
    request.getUserEmail(id).then((data) => {
        if (data['0']['emp_email'] != null) {
            buildResponse(res, 200, {
                error: false,
                message: "Successfully display!",
                responseData: data
            });
        } else {
            request.getUserVintraTicket(id).then((data) => {
                buildResponse(res, 200, {
                    error: false,
                    message: "Successfully display!",
                    responseData: data
                });
            })
        }
    });
});

router.post('/UpdateSafetyData', myLogger, function (req, res, next) {
    var request = new SafetyForm();
    var data = req.body;
    request.UpdateSafetyData(data)
        .then((data) => {

            buildResponse(res, 200, {
                error: false,
                message: "Record Inserted Successfully",
                responseData: data
            });
        }).catch((err) => {
            console.log('err===',err);
            buildResponse(res, 400, {
                error: true,
                message: err
            });
        });
});

router.get('/getSafetyOfficer', myLogger, function (req, res, next) {
    var request = new SafetyForm();
   
    request.getSafetyOfficer()
        .then((data) => {

            buildResponse(res, 200, {
                error: false,
                message: "Record Inserted Successfully",
                responseData: data
            });
        }).catch((err) => {
            console.log('err===',err);
            buildResponse(res, 400, {
                error: true,
                message: err
            });
        });
});

function buildResponse(res, statusCode, responseData) {
    res.status(statusCode).json(responseData);
}

module.exports = router;