var express = require('express');
var router = express.Router();
var bcrypt = require('bcryptjs');
var jwt = require('jsonwebtoken');
const Joi = require('joi');
var groupMenuMappingMaster = require('../model/group_Menu_Mapping');

var myLogger = require('../middleware.js');
var MyLogModel = require('../model/myLog');
var log = new MyLogModel();





const schema = Joi.object().keys({
    Menu_ID: Joi.number().required().allow(''),
    Added_By: Joi.string().max(20).required().allow(''),
    Create_Date: Joi.string().allow(''),
    Group_ID: Joi.number().required().allow(''),
    Grp_Menu_Status: Joi.string().max(1).required().allow(''),



});


const schema1 = Joi.object().keys({

    Menu_ID: Joi.number().required().allow(''),
    Modified_By: Joi.string().max(20).required().allow(''),
    Modified_Date: Joi.string().allow(''),
    Group_ID: Joi.number().required().allow(''),
    Grp_Menu_Status: Joi.string().max(1).required().allow(''),
    Txn_ID: Joi.number().required().allow(''),

});

router.post('/saveGroupMenuMapping', function (req, res, next) {
    var group_Menu_Mapping_Master = new groupMenuMappingMaster();
    var groupMenuMappingData = {
        Added_By: req.body.Added_By,
        Create_Date: req.body.Create_Date,
        Menu_ID: req.body.Menu_ID,
        Group_ID: req.body.Group_ID,
        Grp_Menu_Status: req.body.Grp_Menu_Status,
     


    };
    
    Joi.validate({
        Added_By: req.body.Added_By,
        Create_Date: req.body.Create_Date,
        Menu_ID: req.body.Menu_ID,
        Group_ID: req.body.Group_ID,
        Grp_Menu_Status: req.body.Grp_Menu_Status,
    }, schema, function (err, schemegroupMenuMappingData) {
        if (err) {
            console.log(err)
            buildResponse(res, 200, {
                error: true,
                message: err["message"]
            });
        }
        else {
            group_Menu_Mapping_Master.savegroupMenuMapping(groupMenuMappingData)
                .then((data) => {
                 
                    if (data["data"] == 0) {
                        buildResponse(res, 200, {
                            error: true,
                            message: data["errMsg"]
                        });
                    } else {
                        buildResponse(res, 200, {
                            error: false,
                            message: "Stored Successfully"
                        });

                    }

                }).catch((errLog) => {

                    log.addLog('route :: Group_Menu_Mapping.js ' + errLog);
                    buildResponse(res, 500, {
                        error: true,
                        message: "server error"
                    });
                });
        }
    });
});

router.post('/updateGroupMenuMapping', function (req, res, next) {
    var group_Menu_Mapping_Master = new groupMenuMappingMaster();

    var groupMenuMappingData = {
        Menu_ID: req.body.Menu_ID,
        Modified_By: req.body.Modified_By,
        Modified_Date: req.body.Modified_Date,
   
        Group_ID: req.body.Group_ID,
        Grp_Menu_Status: req.body.Grp_Menu_Status,
        Txn_ID: req.body.Txn_ID,


    };

    Joi.validate({
        Menu_ID: req.body.Menu_ID,
        Modified_By: req.body.Modified_By,
        Modified_Date: req.body.Modified_Date,
     
        Group_ID: req.body.Group_ID,
        Grp_Menu_Status: req.body.Grp_Menu_Status,
        Txn_ID: req.body.Txn_ID,


    }, schema1, function (err, schemegroupMenuMappingData) {
        if (err) {

            buildResponse(res, 200, {
                error: true,
                message: err["message"]
            });
        }
        else {
            group_Menu_Mapping_Master.updateGroupMenuMapping(groupMenuMappingData)
                .then((data) => {
                    console.log(data, "-----------")
                    if (data["data"] == 0) {
                        buildResponse(res, 200, {
                            error: true,
                            message: data["errMsg"]
                        });
                    } else {
                        buildResponse(res, 200, {
                            error: false,
                            message: "Stored Successfully"
                        });

                    }

                }).catch((errLog) => {

                    log.addLog('route :: Group_Menu_Mapping.js ' + errLog);
                    buildResponse(res, 500, {
                        error: true,
                        message: "server error"
                    });
                });
        }
    });
});

router.post('/getGroupMenuMapping', function (req, res, next) {
    var group_Menu_Mapping_Master = new groupMenuMappingMaster();

    group_Menu_Mapping_Master.getGroupMenuMapping().then((groupMenuMappingData) => {

        if (groupMenuMappingData["data"] == '0') {
            buildResponse(res, 200, {
                error: false,
                message: groupMenuMappingData["errMsg"],

            });
        } else {
            buildResponse(res, 200, {
                error: false,
                message: "Successfully display!",
                groupMenuMappingData: groupMenuMappingData
            });
        }

    });

});

router.post('/getGroupMenuMappingById', function (req, res, next) {
    var group_Menu_Mapping_Master = new groupMenuMappingMaster();

    var Txn_ID = req.body.Txn_ID;
    group_Menu_Mapping_Master.getGroupMenuMappingById(Txn_ID).then((groupMenuMappingData) => {
      
        if (groupMenuMappingData["data"] == '0') {
            buildResponse(res, 200, {
                error: false,
                message: groupMenuMappingData["errMsg"],

            });
        } else {
            buildResponse(res, 200, {
                error: false,
                message: "Successfully display!",
                groupMenuMappingData: groupMenuMappingData
            });
        }

    });

});



function buildResponse(res, statusCode, responseData) {
    res.status(statusCode).json(responseData);
}

module.exports = router;