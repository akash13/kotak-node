var express = require('express');
var router = express.Router();
var bcrypt = require('bcryptjs');
var jwt = require('jsonwebtoken');
const Joi = require('joi');
var Meeting_Task = require('../model/meeting_task');

var myLogger = require('../middleware.js');
var MyLogModel = require('../model/myLog');
var log = new MyLogModel();

const schema = Joi.object().keys({
    
    meeting_schedule_id: Joi.number().required().not(''),
    task_name: Joi.string().max(255).required().not(''),
    priority: Joi.string().max(255).required().not(''),
    activity_status:Joi.string().max(10).required().not(''),
    plan_start_date:Joi.string().required().not(''),
    plan_end_date:Joi.string().required().not(''),
    assigned_by:Joi.number().required().not(''),
    assigned_to:Joi.number().required().not(''),

});

const editTaskschema = Joi.object().keys({
        tableName : Joi.string().required().not(''),
        scheduleMeetingId : Joi.required().not(''),
        meetingTaskId : Joi.required().not(''),
        activityName: Joi.string().required().not(''),
        Priority: Joi.string().required().not(''),
        activityStatus: Joi.required().not(''),
        planStartDate: Joi.string().required().not(''),
        planEndDate: Joi.string().required().not(''),
        assignedBy: Joi.string().required().not(''),
        assignedTo: Joi.string().required().not(''),
        activityDescription: Joi.string().allow(''),
        selectDependentActivity: Joi.string().required().not(''),
        Updated_By:Joi.string().required().not(''),
    

});

revisedDueDateSchema= Joi.object().keys({
    tableName : Joi.string().required().not(''),
    scheduleMeetingId : Joi.required().not(''),
    meetingTaskId : Joi.required().not(''),
   
    revisedDueDate: Joi.string().required().not(''),
   
    revisedDueDateComment: Joi.string().max(255).required().not(''),
    Updated_By:Joi.string().required().not(''),


});

escalateNotifyToSchema= Joi.object().keys({
    tableName : Joi.string().required().not(''),
    scheduleMeetingId : Joi.required().not(''),
    meetingTaskId : Joi.required().not(''),
    escalateInput: Joi.string().required().not(''),
    commentInput: Joi.string().max(255).required().not(''),
    Updated_By:Joi.string().required().not(''),
});




const schemaLogProgress = Joi.object().keys({
    meetingId: Joi.number().required().not(''),
    scheduleId: Joi.number().required().not(''),
    hours: Joi.number().allow(''),
    CompletionPercentage: Joi.number().max(100).required().not(''),
    actualStartDate: Joi.date().allow(''),
    actualEndDate: Joi.date().allow(''),
    comment: Joi.string().max(255).allow(''),
    status: Joi.number().required().not(''),
    created_by: Joi.string().required().not(''),

});

router.post('/saveMeeting_task', myLogger, function (req, res, next) {
    var meeting_task = new Meeting_Task();
    var meeting_task_Data = {
        meeting_schedule_id: req.body.meeting_schedule_id,
        task_name: req.body.task_name,
        priority: req.body.priority,
        activity_status:req.body.activity_status,
        plan_start_date:req.body.plan_start_date,
        plan_end_date: req.body.plan_end_date,
        assigned_by: req.body.assigned_by,
        assigned_to: req.body.assigned_to,
        
   };

    Joi.validate({
        meeting_schedule_id: req.body.meeting_schedule_id,
        task_name: req.body.task_name,
        priority: req.body.priority,
        activity_status:req.body.activity_status,
        plan_start_date:req.body.plan_start_date,
        plan_end_date: req.body.plan_end_date,
        assigned_by: req.body.assigned_by,
        assigned_to: req.body.assigned_to,
        
     
    }, schema, function (err, meeting_task_Data) {
        if (err) {

            buildResponse(res, 200, {
                error: true,
                message: err["message"]
            });
        }
        else {
            meeting_task.saveMeeting_task(meeting_task_Data)
                .then((data) => {
                   console.log(data,"-----------")
                    if (data["data"] == 0) {
                        buildResponse(res, 200, {
                            error: true,
                            message: data["errMsg"]
                        });
                    } else {
                        buildResponse(res, 200, {
                            error: false,
                            message: "Stored Successfully"
                        });

                    }

                }).catch((errLog) => {
                   
                    log.addLog('route :: meeting_task.js ' + errLog);
                    buildResponse(res, 500, {
                        error: true,
                        message: "server error"
                    });
                });
        }
    });
});

router.post('/logProgressSubmit', myLogger, function (req, res, next) {
    var meeting_task = new Meeting_Task();
    console.log("rrrrrrrrr==",req.body)
    var logProgressData = {
        meetingId: req.body.meetingId,
        scheduleId: req.body.scheduleId,
        hours: req.body.hours,
        CompletionPercentage: req.body.CompletionPercentage,
        actualStartDate : '',
        actualEndDate: req.body.actualEndDate,
        comment: req.body.comment,
        status: req.body.status,
        created_by: req.body.created_by,
        tableName:req.body.tableName
   };

   if(req.body.status=='4' && req.body.actualEndDate==''){
    buildResponse(res, 200, {
        error: true,
        message: "Please select actual end date"
    });

   }else{
    Joi.validate({
        meetingId: req.body.meetingId,
        scheduleId: req.body.scheduleId,
        hours: req.body.hours,
        CompletionPercentage: req.body.CompletionPercentage,
        
        actualEndDate: req.body.actualEndDate,
        comment: req.body.comment,
        status: req.body.status,
        created_by: req.body.created_by,
     
    }, schemaLogProgress, function (err, logProgressDataSchema) {
        if (err) {

            buildResponse(res, 200, {
                error: true,
                message: err['details'][0]["message"]
            });
        }
        else {
            meeting_task.logProgressSubmit(logProgressData)
                .then((data) => {
                   console.log(data,"-----------")
                    if (data["data"] == 0) {
                        buildResponse(res, 200, {
                            error: true,
                            message: data["errMsg"]
                        });
                    } else {
                        buildResponse(res, 200, {
                            error: false,
                            message: "Stored Successfully"
                        });

                    }
                }).catch((errLog) => {
                   
                    log.addLog('route :: department.js ' + errLog);
                    buildResponse(res, 500, {
                        error: true,
                        message: "server error"
                    });
                });
        }
    });
   }
    
});


// router.get('/getMeetingTaskData',myLogger function (req, res, next) {
//     console.log("inside route");
//     var request = new Master();

//     // console.log("reqdata",reqData);
//     request.getMeetingTaskData()
//         .then((meetingTaskdata) => {

//             buildResponse(res, 200, {
//                 error: false,
//                 message: 'Application Fetched successfully',
//                 responseData: meetingTaskdata

//             });
//         }).catch((err) => {
//             buildResponse(res, 404, {
//                 error: false,
//                 message: 'Failed to Fetch',

//             });
//         });
// });


router.post('/updateMeeting_task', myLogger, function (req, res, next) {
    var meeting_task = new Meeting_Task();
    var meeting_task_Data = {
        meeting_schedule_id: req.body.meeting_schedule_id,
        task_name: req.body.task_name,
        priority: req.body.priority,
        activity_status:req.body.activity_status,
        plan_start_date:req.body.plan_start_date,
        plan_end_date: req.body.plan_end_date,
        assigned_by: req.body.assigned_by,
        assigned_to: req.body.assigned_to,
        meeting_task_id:req.body.meeting_task_id,
        
   };
console.log(meeting_task_Data,"---------")
    Joi.validate({
       
        meeting_schedule_id: req.body.meeting_schedule_id,
        task_name: req.body.task_name,
        priority: req.body.priority,
        activity_status:req.body.activity_status,
        plan_start_date:req.body.plan_start_date,
        plan_end_date: req.body.plan_end_date,
        assigned_by: req.body.assigned_by,
        assigned_to: req.body.assigned_to,
     
    }, schema, function (err, schemastatusData) {
        if (err) {

            buildResponse(res, 200, {
                error: true,
                message: err["message"]
            });
        }
        else {
            meeting_task.updateMeeting_task(meeting_task_Data)
                .then((data) => {
                   console.log(data,"-----------")
                    if (data["data"] == 0) {
                        buildResponse(res, 200, {
                            error: true,
                            message: data["errMsg"]
                        });
                    } else {
                        buildResponse(res, 200, {
                            error: false,
                            message: "Stored Successfully"
                        });

                    }

                }).catch((errLog) => {
                   
                    log.addLog('route :: user.js ' + errLog);
                    buildResponse(res, 500, {
                        error: true,
                        message: "server error"
                    });
                });
        }
    });
});

router.post('/getMeeting_task', myLogger, function (req, res, next) {
    var meeting_task = new Meeting_Task();
    var userData = req.body;
    var pageNumber = req.body.pageNumber;
    var NumberOfRows = 10;

    var offsetRow = (pageNumber - 1) * 10;

    var fetchRow = NumberOfRows * pageNumber;
    meeting_task.getMeeting_task(userData,offsetRow,fetchRow).then((statusData) => {
        buildResponse(res, 200, {
            error: false,
            message: "Stored Successfully",
            statusData: statusData,
            numberOfRows:NumberOfRows*(pageNumber-1)
        });

    }).catch((errLog) => {

        log.addLog('route :: Meeting.js ' + errLog);
        buildResponse(res, 500, {
            error: true,
            message: "server error"
        });
    });

});

router.post('/getMeetingTaskDataExportToExcel', myLogger, function (req, res, next) {
    var meeting_task = new Meeting_Task();
    var userData = req.body;
    var pageNumber = req.body.pageNumber;
    var NumberOfRows = 10

    var offsetRow = (pageNumber - 1) * 10;

    var fetchRow = NumberOfRows * pageNumber;
    meeting_task.getMeetingTaskDataExportToExcel(userData,offsetRow,fetchRow).then((statusData) => {
        buildResponse(res, 200, {
            error: false,
            message: "Stored Successfully",
            statusData: statusData
        });

    }).catch((errLog) => {

        log.addLog('route :: Meeting.js ' + errLog);
        buildResponse(res, 500, {
            error: true,
            message: "server error"
        });
    });

});


router.post('/getMeetingTaskoverdueList', myLogger, function (req, res, next) {
    var meeting_task = new Meeting_Task();
    var userData = req.body;
    var pageNumber = req.body.pageNumber;
    var NumberOfRows = 10

    var offsetRow = (pageNumber - 1) * 10;

    var fetchRow = NumberOfRows * pageNumber;

    var pageNumber = req.body.pageNumber;
    var NumberOfRows = 10

    var offsetRow = (pageNumber - 1) * 10;

    var fetchRow = NumberOfRows * pageNumber;

    meeting_task.getMeetingTaskoverdueList(userData,offsetRow,fetchRow).then((statusData) => {
        
        if (statusData["data"] == '0') {
            buildResponse(res, 200, {
                error: false,
                message: statusData["errMsg"],

            });
        } else {
            buildResponse(res, 200, {
                error: false,
                message: "Successfully display!",
                statusData: statusData,
                numberOfRows:NumberOfRows*(pageNumber-1)

            });
        }
    });
});

router.post('/getMeetingTaskoverdueListaExportToExcel', myLogger, function (req, res, next) {
    var meeting_task = new Meeting_Task();
    var userData = req.body;
    var pageNumber = req.body.pageNumber;
    var NumberOfRows = 10

    var offsetRow = (pageNumber - 1) * 10;

    var fetchRow = NumberOfRows * pageNumber;

    var pageNumber = req.body.pageNumber;
    var NumberOfRows = 10

    var offsetRow = (pageNumber - 1) * 10;

    var fetchRow = NumberOfRows * pageNumber;

    meeting_task.getMeetingTaskoverdueListaExportToExcel(userData,offsetRow,fetchRow).then((statusData) => {
        
        if (statusData["data"] == '0') {
            buildResponse(res, 200, {
                error: false,
                message: statusData["errMsg"],

            });
        } else {
            buildResponse(res, 200, {
                error: false,
                message: "Successfully display!",
                statusData: statusData
            });
        }
    });
});

router.post('/getMeetingTaskdiscardedList', myLogger, function (req, res, next) {
    var meeting_task = new Meeting_Task();
    var userData = req.body;
    var pageNumber = req.body.pageNumber;
    var NumberOfRows = 10
    var offsetRow = (pageNumber - 1) * 10;
    var fetchRow = NumberOfRows * pageNumber;

    meeting_task.getMeetingTaskdiscardedList(userData,offsetRow,fetchRow).then((statusData) => {
        
        if (statusData["data"] == '0') {
            buildResponse(res, 200, {
                error: false,
                message: statusData["errMsg"],
            });
        } else {
            buildResponse(res, 200, {
                error: false,
                message: "Successfully display!",
                statusData: statusData,
                numberOfRows:NumberOfRows*(pageNumber-1)

            });
        }
    });
});


router.post('/getMeetingTaskdiscardedListaExportToExcel', myLogger, function (req, res, next) {
    var meeting_task = new Meeting_Task();
    var userData = req.body;
    var pageNumber = req.body.pageNumber;
    var NumberOfRows = 10
    var offsetRow = (pageNumber - 1) * 10;
    var fetchRow = NumberOfRows * pageNumber;

    meeting_task.getMeetingTaskdiscardedListaExportToExcel(userData,offsetRow,fetchRow).then((statusData) => {
        
        if (statusData["data"] == '0') {
            buildResponse(res, 200, {
                error: false,
                message: statusData["errMsg"],
            });
        } else {
            buildResponse(res, 200, {
                error: false,
                message: "Successfully display!",
                statusData: statusData
            });
        }
    });
});

router.post('/getMeetingTaskViewList', myLogger, function (req, res, next) {
    console.log("this for meeting view ",req.body.id);
    var meeting_task = new Meeting_Task();
    var id = req.body.id;
    var Schedule_Meeting_Id = req.body.Schedule_Meeting_Id
 
    meeting_task.getMeetingTaskViewList(id,Schedule_Meeting_Id).then((statusData) => {
        
        if (statusData["data"] == '0') {
            buildResponse(res, 200, {
                error: false,
                message: statusData["errMsg"],
            });
        } else {
            buildResponse(res, 200, {
                error: false,
                message: "Successfully display!",
                statusData: statusData,


            });
        }
    });
});

router.post('/getDependentActivityView', myLogger, function (req, res, next) {
    console.log("this for meeting view ",req.body.id);
    var meeting_task = new Meeting_Task();
    var id = req.body.id;
    var Schedule_Meeting_Id = req.body.Schedule_Meeting_Id
 
    meeting_task.getDependentActivityView(id,Schedule_Meeting_Id).then((statusData) => {
        
        if (statusData["data"] == '0') {
            buildResponse(res, 200, {
                error: false,
                message: statusData["errMsg"],
            });
        } else {
            buildResponse(res, 200, {
                error: false,
                message: "Successfully display!",
                statusData: statusData
            });
        }
    });
});


router.post('/getRevisedHistory', myLogger, function (req, res, next) {
    
    var meeting_task = new Meeting_Task();
    var id = req.body.id;
    var Schedule_Meeting_Id = req.body.Schedule_Meeting_Id
 
    meeting_task.getRevisedHistory(id,Schedule_Meeting_Id).then((statusData) => {
        console.log("ssssssss====",statusData)
        if (statusData["data"] == '0') {
            buildResponse(res, 200, {
                error: false,
                message: statusData["errMsg"],
            });
        } else {
            buildResponse(res, 200, {
                error: false,
                message: "Successfully display!",
                statusData: statusData
            });
        }
    });
});


router.post('/getMeetingTaskcompletedList', myLogger, function (req, res, next) {
    var meeting_task = new Meeting_Task();
    var userData = req.body;
    var pageNumber = req.body.pageNumber;
    var NumberOfRows = 10;
    var offsetRow = (pageNumber - 1) * 10;
    var fetchRow = NumberOfRows * pageNumber;
    meeting_task.getMeetingTaskcompletedList(userData,offsetRow,fetchRow).then((statusData) => {
        
        if (statusData["data"] == '0') {
            buildResponse(res, 200, {
                error: false,
                message: statusData["errMsg"],

            });
        } else {
            buildResponse(res, 200, {
                error: false,
                message: "Successfully display!",
                statusData: statusData,
                numberOfRows:NumberOfRows*(pageNumber-1)

            });
        }
    });
});

router.post('/getMeetingTaskcompletedListaExportToExcel', myLogger, function (req, res, next) {
    var meeting_task = new Meeting_Task();
    var userData = req.body;
    var pageNumber = req.body.pageNumber;
    var NumberOfRows = 10;
    var offsetRow = (pageNumber - 1) * 10;
    var fetchRow = NumberOfRows * pageNumber;
    meeting_task.getMeetingTaskcompletedListaExportToExcel(userData,offsetRow,fetchRow).then((statusData) => {
        
        if (statusData["data"] == '0') {
            buildResponse(res, 200, {
                error: false,
                message: statusData["errMsg"],

            });
        } else {
            buildResponse(res, 200, {
                error: false,
                message: "Successfully display!",
                statusData: statusData
            });
        }
    });
});


router.post('/getMeetingWorkbenchFollowupsount', myLogger, function (req, res, next) {
    var meeting_task = new Meeting_Task();
    console.log("this for body meeting ",req.body);
    var userData = req.body;
    meeting_task.getMeetingWorkbenchFollowupsount(userData)
    .then((data2) => {
     console.log("this meeting route",data2);
     if(data2[0].length > 0){
        buildResponse(res, 200, {
            message: "Stored Successfully",
           openTaskCount: data2[0][0]["openTaskCount"],
           OverdueTaskCount: data2[0][0]["OverdueTaskCount"],
           CompletedTaskCount: data2[0][0]["CompletedTaskCount"],
           DroppeTaskCount: data2[0][0]["DroppeTaskCount"],
           });
     }else{
        buildResponse(res, 200, {
            message: "Stored Successfully",
           openTaskCount: 0,
           OverdueTaskCount: 0,
           CompletedTaskCount: 0,
           DroppeTaskCount: 0,
           });
     }
    
}).catch((errLog) => {
    log.addLog('route :: Meeting.js ' + errLog);
    buildResponse(res, 500, {
        error: true,
        message: "server error"
    });
});
});



router.post('/deleteMeeting_task', myLogger, function (req, res, next) {
    var meeting_task = new Meeting_Task();
    var meeting_task_Data={
        meeting_task_id:req.body.meeting_task_id,
        deleted_by:req.body.deleted_by
    };
    console.log("+++++++++++++++++++++++",meeting_task_Data);
    //var reqData = req.body.status_id;
    meeting_task.deleteMeeting_task(meeting_task_Data)
        .then((statusData) => {
            buildResponse(res, 200, {
                error: false,
                message: 'meeting task Deleted successfully',
                statusData: statusData
            });
        }).catch((errLog) => {
            log.addLog('route :: meeting_task.js ' + errLog);
            buildResponse(res, 404, {
                error: false,
                message: 'Please Try Again',
            });
        });
});



router.post('/getLogProgress', myLogger, function (req, res, next) {
    var meeting_task = new Meeting_Task();
    var userData = req.body;

    meeting_task.getLogProgress(userData).then((statusData) => {
        buildResponse(res, 200, {
            error: false,
            message: "Stored Successfully",
            statusData: statusData
        });

    }).catch((errLog) => {

        log.addLog('route :: Meeting.js ' + errLog);
        buildResponse(res, 500, {
            error: true,
            message: "server error"
        });
    });

});


router.post('/getLogProgressFormData', myLogger, function (req, res, next) {
    var meeting_task = new Meeting_Task();
    var userData = req.body;

    meeting_task.getLogProgressFormData(userData).then((statusData) => {
        buildResponse(res, 200, {
            error: false,
            message: "Stored Successfully",
            statusData: statusData
        });

    }).catch((errLog) => {

        log.addLog('route :: Meeting.js ' + errLog);
        buildResponse(res, 500, {
            error: true,
            message: "server error"
        });
    });

});




router.post('/getDependentActivity', myLogger, function (req, res, next) {
    var meeting_task = new Meeting_Task();
    var userData = req.body;

    meeting_task.getDependentActivity(userData).then((statusData) => {
        buildResponse(res, 200, {
            error: false,
            message: "Stored Successfully",
            statusData: statusData
        });

    }).catch((errLog) => {

        log.addLog('route :: Meeting.js ' + errLog);
        buildResponse(res, 500, {
            error: true,
            message: "server error"
        });
    });

});


router.post('/editMeetingTask', myLogger, function (req, res, next) {
    var meeting_task = new Meeting_Task();
    var meeting_task_Data = {

        tableName : req.body.tableName,
        scheduleMeetingId : req.body.scheduleMeetingId,
        meetingTaskId : req.body.meetingTaskId,
        activityName: req.body.activityName,
        Priority: req.body.Priority,
        activityStatus: req.body.activityStatus,
        planStartDate: req.body.planStartDate,
        planEndDate: req.body.planEndDate,
        assignedBy: req.body.assignedBy,
        assignedTo: req.body.assignedTo,
        revisedDueDate: req.body.revisedDueDate,
        activityDescription: req.body.activityDescription,
        selectDependentActivity: req.body.selectDependentActivity,
        Updated_By:req.body.Updated_By,
   };
console.log(meeting_task_Data,"---------")
    Joi.validate({
       
        tableName : req.body.tableName,
        scheduleMeetingId : req.body.scheduleMeetingId,
        meetingTaskId : req.body.meetingTaskId,
        activityName: req.body.activityName,
        Priority: req.body.Priority,
        activityStatus: req.body.activityStatus,
        planStartDate: req.body.planStartDate,
        planEndDate: req.body.planEndDate,
        assignedBy: req.body.assignedBy,
        assignedTo: req.body.assignedTo,
      
        activityDescription: req.body.activityDescription,
        selectDependentActivity: req.body.selectDependentActivity,
        Updated_By:req.body.Updated_By,

    }, editTaskschema, function (err, schemastatusData) {
        if (err) {

            buildResponse(res, 200, {
                error: true,
                message: err["message"]
            });
        }
        else {
            meeting_task.editMeetingTask(meeting_task_Data)
                .then((data) => {
                   console.log(data,"-----------")
                    if (data["data"] == 0) {
                        buildResponse(res, 200, {
                            error: true,
                            message: data["errMsg"]
                        });
                    } else {
                        buildResponse(res, 200, {
                            error: false,
                            message: "Stored Successfully"
                        });

                    }

                }).catch((errLog) => {
                   
                    log.addLog('route :: user.js ' + errLog);
                    buildResponse(res, 500, {
                        error: true,
                        message: "server error"
                    });
                });
        }
    });
});



router.post('/submitRevisedDate', myLogger, function (req, res, next) {
    var meeting_task = new Meeting_Task();
    var meeting_task_Data = {

        tableName : req.body.tableName,
        scheduleMeetingId : req.body.scheduleMeetingId,
        meetingTaskId : req.body.meetingTaskId,
        revisedDueDate:req.body.revisedDueDate,
        revisedDueDateComment:req.body.revisedDueDateComment,
        Updated_By:req.body.Updated_By,
   };
console.log(meeting_task_Data,"---------")
    Joi.validate({
       
        tableName : req.body.tableName,
        scheduleMeetingId : req.body.scheduleMeetingId,
        meetingTaskId : req.body.meetingTaskId,
        revisedDueDate:req.body.revisedDueDate,
        revisedDueDateComment:req.body.revisedDueDateComment,
        Updated_By:req.body.Updated_By,

    }, revisedDueDateSchema, function (err, schemastatusData) {
        if (err) {
            buildResponse(res, 200, {
                error: true,
                message: err["message"]
            });
        }
        else {
            meeting_task.submitRevisedDate(meeting_task_Data)
                .then((data) => {
                   console.log(data,"-----------")
                    if (data["data"] == 0) {
                        buildResponse(res, 200, {
                            error: true,
                            message: data["errMsg"]
                        });
                    } else {
                        buildResponse(res, 200, {
                            error: false,
                            message: "Stored Successfully"
                        });

                    }

                }).catch((errLog) => {
                   
                    log.addLog('route :: meeting task.js ' + errLog);
                    buildResponse(res, 500, {
                        error: true,
                        message: "server error"
                    });
                });
        }
    });
});


router.post('/submitEscalateNotifyTo', myLogger, function (req, res, next) {
    var meeting_task = new Meeting_Task();
    var meeting_task_Data = {

        tableName : req.body.tableName,
        scheduleMeetingId : req.body.scheduleMeetingId,
        meetingTaskId : req.body.meetingTaskId,
        escalateInput:req.body.escalateInput,
        commentInput:req.body.commentInput,
        taskName: req.body.taskName,
        status: req.body.status,
        Updated_By:req.body.Updated_By,
   };
console.log(meeting_task_Data,"---------")
    Joi.validate({
       
        tableName : req.body.tableName,
        scheduleMeetingId : req.body.scheduleMeetingId,
        meetingTaskId : req.body.meetingTaskId,
        escalateInput:req.body.escalateInput,
        commentInput:req.body.commentInput,
        Updated_By:req.body.Updated_By,

    }, escalateNotifyToSchema, function (err, schemastatusData) {
        if (err) {

            buildResponse(res, 200, {
                error: true,
                message: err["message"]
            });
        }
        else {
            meeting_task.submitEscalateNotifyTo(meeting_task_Data)
                .then((data) => {
                   console.log(data,"-----------")
                    if (data["data"] == 0) {
                        buildResponse(res, 200, {
                            error: true,
                            message: data["errMsg"]
                        });
                    } else {
                        buildResponse(res, 200, {
                            error: false,
                            message: "Stored Successfully"
                        });

                    }

                }).catch((errLog) => {
                   
                    log.addLog('route :: meeting task.js ' + errLog);
                    buildResponse(res, 500, {
                        error: true,
                        message: "server error"
                    });
                });
        }
    });
});



function buildResponse(res, statusCode, responseData) {
    res.status(statusCode).json(responseData);
}

module.exports = router;