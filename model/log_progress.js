const Sequelize = require('sequelize');
var con = require('./connection');
var randomstring = require("randomstring");
var bcrypt = require('bcryptjs');
var MyLogModel = require('../model/myLog');
var log = new MyLogModel();
const Op = Sequelize.Op;
var errMsg = "Server issue please try after sometime";
Sequelize.DATE.prototype._stringify = function _stringify(date, options) {
    return this._applyTimezone(date, options).format('YYYY-MM-DD HH:mm:ss.SSS');
  };
module.exports = class Department {

    constructor() {

      
        this.LOGPROGRESS = con.define('log_progress_master', {
            log_progress_id: {
                type: Sequelize.INTEGER,
                primaryKey: true,
                autoIncrement: true
            },
           
            meeting_task_id: {
                type: Sequelize.INTEGER,
                allowNull: false,
            },
            meeting_schedule_id: {
                type: Sequelize.INTEGER,
                allowNull: false,
               
            },
            log_hours: {
                type: Sequelize.INTEGER,
                allowNull: false,
               
            },
            log_activity_status: {
                type: Sequelize.STRING(10),
                allowNull: false
            },
            log_completion_percentage: {
                type: Sequelize.INTEGER,
                allowNull: true
            },
            log_comment: {
                type: Sequelize.STRING(255),
                allowNull: true
            },
            created_by: {
                type: Sequelize.STRING(20),
                allowNull: true,
                
               
            },
            created_date_time: {
                type: Sequelize.DATE,
                allowNull: true
            },
            updated_by: {
                type: Sequelize.STRING(20),
                allowNull: true
            },
            updated_date_time: {
                type: Sequelize.DATE,
                allowNull: true
            },
            is_delete: {
                type: Sequelize.STRING(2),
                allowNull: false,
                defaultValue:'0'
               
            },
            deleted_by: {
                type: Sequelize.STRING(20),
                allowNull: true
            },
            deleted_date_time: {
                type: Sequelize.DATE,
                allowNull: true
            },
        },
        {
            timestamps:false,
            createdAt: false,
            UpdatedAt: false,
        }
        );
    }

    log_progress(log_progress_Data) {
        return sequelize.sync().then(() => {
            return this.LOGPROGRESS.create({
              
                meeting_task_id: log_progress_Data.meeting_task_id,
                meeting_schedule_id:log_progress_Data.meeting_schedule_id,
                created_by: log_progress_Data.created_by,
                log_hours:log_progress_Data.log_hours,
                log_activity_status:log_progress_Data.log_activity_status,
                created_date_time:new Date()

                
              
            }).catch(function (errLog) {
                console.log("errMsg:", errLog)
                log.addLog('model :: log_progress.js ' + errLog);
                var err;
                if (errLog == "SequelizeUniqueConstraintError: Validation error") {
                    err = {
                        data: 0,
                        errMsg: "Department name should be unique"
                    }
                } else if (errLog != "SequelizeUniqueConstraintError: Validation error") {
                    err = {
                        data: 0,
                        errMsg: errLog["errors"][0]["message"]
                    }

                }
                return err;
            });
        }).catch(function (errLog) {
            log.addLog('model :: log_progress.js ' + errLog);
        });
    }

    updateLog_progress(log_progress_Data) {
        return sequelize.sync().then(() => {
            return this.LOGPROGRESS.update({
              
                meeting_task_id: log_progress_Data.meeting_task_id,
                meeting_schedule_id: log_progress_Data.meeting_schedule_id,
                
                log_hours:log_progress_Data.log_hours,
                log_activity_status:log_progress_Data.log_activity_status,
                updated_by: log_progress_Data.updated_by,
                updated_date_time:new Date()
              
            },{
                where:{
                    log_progress_id:log_progress_Data.log_progress_id
                }
            }).catch(function (errLog) {
                console.log("errMsg:", errLog)
                log.addLog('model :: log_progress.js ' + errLog);
                var err;
                if (errLog == "SequelizeUniqueConstraintError: Validation error") {
                    err = {
                        data: 0,
                        errMsg: "Department name should be unique"
                    }
                } else if (errLog != "SequelizeUniqueConstraintError: Validation error") {
                    err = {
                        data: 0,
                        errMsg: errLog["errors"][0]["message"]
                    }

                }
                return err;
            });
        }).catch(function (errLog) {
            log.addLog('model :: log_progress.js ' + errLog);
        });
    }
    getLog_progress(){
        return sequelize.sync().then(() => {
            return this.LOGPROGRESS.all().then((data) => {
                return data
            }).catch((errLog) => {
                var err = {
                    data: 0,
                    errMsg: errMsg
                }
                log.addLog('model :: log_progress.js 160' + errLog);
                return err;
                
            });
        }).catch((errLog) => {
            var err = {
                data: 0,
                errMsg: errMsg
            }
            log.addLog('model :: log_progress.js 169 ' + errLog);
            return err;
        });
    }

    deleteLog_progress(log_progress_Data) {
        console.log("+++++++++++++++++++++++",log_progress_Data);
        return sequelize.sync().then(() => {
            return this.LOGPROGRESS.update({
                is_delete:'1',
                deleted_by:log_progress_Data.deleted_by,
                deleted_date_time:new Date()

            },{
                where: {
                    log_progress_id: log_progress_Data.log_progress_id
                }
            }).catch((errLog) => {
                var err = {
                    data: 0,
                    errMsg: errMsg
                }
                log.addLog('model :: log_progress.js 169 ' + errLog);
                return err;
            });
        });
    }

};