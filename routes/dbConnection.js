var express = require('express');
var router = express.Router();
var bcrypt = require('bcryptjs');
var jwt = require('jsonwebtoken');
const Joi = require('joi');
var dbConnection = require('../model/dbConnection');
var myLogger = require('../middleware.js');


const schema = Joi.object().keys({
    connection_name: Joi.required().not(''),
    dialect: Joi.required().not(''),
    database: Joi.required().not(''),
    username: Joi.required().not(''),
    host: Joi.required().not(''),
    port: Joi.required().not(''),
    password: Joi.required().not(''),
    logging: Joi.required().not(''),
    encrypt: Joi.required().not(''),
    instanceName: Joi.required().not(''),
});


router.post('/saveconnection',myLogger, function (req, res, next) {

    var request = new dbConnection();
    var connectionData = {
        name: req.body.name,
        dialect: req.body.dialect,
        database: req.body.database,
        username: req.body.username,
        host: req.body.host,
        port: req.body.port,
        password: req.body.password,
        logging: req.body.logging,
        encrypt: req.body.encrypt,
        instanceName: req.body.instanceName
    };

    console.log("connectionData=",connectionData);
    Joi.validate({
        connection_name: req.body.name,
        dialect: req.body.dialect,
        database: req.body.database,
        username: req.body.username,
        host: req.body.host,
        port: req.body.port,
        password: req.body.password,
        logging: req.body.logging,
        encrypt: req.body.encrypt,
        instanceName: req.body.instanceName
    }, schema, function (err, connectionData) {
        if (err) {
            buildResponse(res, 200, {
                error: true,
                message: "All fields are required"
            });
        }
        else {
            request.saveConnection(connectionData)
                .then((data) => {
                    if (data == 0) {
                        buildResponse(res, 200, {
                            error: true,
                            message: "Fields should be unique"
                        });
                    }
                    buildResponse(res, 200, {
                        error: false,
                        message: "Connection Created Successfully"
                    });

                }).catch((err) => {
                    buildResponse(res, 200, {
                        error: true,
                        message: "Fail to Store"
                    });
                });
        }
    })


});


router.get('/getConnection',myLogger, function (req, res, next) {
    var request = new dbConnection();

    request.getConnection().then((applicationData) => {
        buildResponse(res, 200, {
            error: false,
            message: "Successfully display!",
            ApplicationData: applicationData
        });
    });
});

router.get('/getConnection/:id',myLogger, function (req, res, next) {
    var request = new dbConnection();
    var reqData = req.params.id;
    request.getConnectionById(reqData)
        .then((applicationData) => {
            buildResponse(res, 200, {
                error: false,
                message: 'Connection Fetched successfully',
                ApplicationData: applicationData
            });
        }).catch((err) => {
            buildResponse(res, 404, {
                error: false,
                message: 'Failed to Fetch',

            });
        });
});


router.post('/updateConnection/:id',myLogger, function (req, res, next) {
    var ConnectionId = req.params.id;
    var request = new dbConnection();

    var connectionData = {
        connection_name: req.body.name,
        dialect: req.body.dialect,
        database: req.body.database,
        username: req.body.username,
        host: req.body.host,
        port: req.body.port,
        password: req.body.password,
        logging: req.body.logging,
        encrypt: req.body.encrypt,
        instanceName: req.body.instanceName
    };
            request.updateConnection(connectionData, ConnectionId)
                .then((data) => {
                    if (data == 0) {
                        buildResponse(res, 200, {
                            error: true,
                            message: "Connection Name should be unique"
                        });
                    }
                    buildResponse(res, 200, {
                        error: false,
                        message: "Connection Updated Successfully"
                    });
                }).catch((err) => {
                    buildResponse(res, 200, {
                        error: true,
                        message: "Fail to Update",
                    });
                });
        });


        router.get('/deleteConnections/:id',myLogger, function (req, res, next) {
            var request = new dbConnection();
            var reqData = req.params.id;
            request.deleteConnections(reqData)
                .then((applicationData) => {
                    buildResponse(res, 200, {
                        error: false,
                        message: 'Connection Deleted successfully',
                        ApplicationData: applicationData
                    });
                }).catch((err) => {
                    buildResponse(res, 404, {
                        error: false,
                        message: 'Please Try Again',
        
                    });
                });
        }); 

function buildResponse(res, statusCode, responseData) {
    res.status(statusCode).json(responseData);
}

module.exports = router;
