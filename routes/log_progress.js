var express = require('express');
var router = express.Router();
var bcrypt = require('bcryptjs');
var jwt = require('jsonwebtoken');
const Joi = require('joi');
var Log_progress = require('../model/log_progress');

var myLogger = require('../middleware.js');
var MyLogModel = require('../model/myLog');
var log = new MyLogModel();

const schema = Joi.object().keys({
    meeting_task_id: Joi.number().required().not(''),
    meeting_schedule_id: Joi.number().required().not(''),
    log_hours: Joi.number().allow(''),
    log_activity_status: Joi.string().max(10).not(''),
    log_completion_percentage:Joi.number().allow(''),
    log_comment:Joi.string().max(255).allow(''),
    created_by:Joi.number().allow(''),
});

router.post('/log_progress', myLogger, function (req, res, next) {
    var log_progress = new Log_progress();
    var log_progress_Data = {
        meeting_task_id: req.body.meeting_task_id,
        meeting_schedule_id: req.body.meeting_schedule_id,
        created_by: req.body.created_by,
        log_hours:req.body.log_hours,
        log_activity_status:req.body.log_activity_status
   };

    Joi.validate({
        meeting_task_id: req.body.meeting_task_id,
        meeting_schedule_id: req.body.meeting_schedule_id,
        created_by: req.body.created_by,
        log_hours:req.body.log_hours,
        log_activity_status:req.body.log_activity_status
     
    }, schema, function (err, log_progress_Data) {
        if (err) {

            buildResponse(res, 200, {
                error: true,
                message: err["message"]
            });
        }
        else {
            log_progress.log_progress(log_progress_Data)
                .then((data) => {
                   console.log(data,"-----------")
                    if (data["data"] == 0) {
                        buildResponse(res, 200, {
                            error: true,
                            message: data["errMsg"]
                        });
                    } else {
                        buildResponse(res, 200, {
                            error: false,
                            message: "Stored Successfully"
                        });

                    }

                }).catch((errLog) => {
                   
                    log.addLog('route :: log_progress.js ' + errLog);
                    buildResponse(res, 500, {
                        error: true,
                        message: "server error"
                    });
                });
        }
    });
});

router.post('/updateLog_progress', myLogger, function (req, res, next) {
    var log_progress = new Log_progress();
    var log_progress_Data = {
        meeting_task_id: req.body.meeting_task_id,
        meeting_schedule_id: req.body.meeting_schedule_id,
        updated_by: req.body.updated_by,
        log_hours:req.body.log_hours,
        log_activity_status:req.body.log_activity_status,
        log_progress_id:req.body.log_progress_id
   };
console.log(log_progress_Data,"---------")
    Joi.validate({
        meeting_task_id: req.body.meeting_task_id,
        meeting_schedule_id: req.body.meeting_schedule_id,
        //updated_by: req.body.updated_by,
        log_hours:req.body.log_hours,
        log_activity_status:req.body.log_activity_status
     
     
    }, schema, function (err, schemastatusData) {
        if (err) {

            buildResponse(res, 200, {
                error: true,
                message: err["message"]
            });
        }
        else {
            log_progress.updateLog_progress(log_progress_Data)
                .then((data) => {
                   console.log(data,"-----------")
                    if (data["data"] == 0) {
                        buildResponse(res, 200, {
                            error: true,
                            message: data["errMsg"]
                        });
                    } else {
                        buildResponse(res, 200, {
                            error: false,
                            message: "Stored Successfully"
                        });

                    }

                }).catch((errLog) => {
                   
                    log.addLog('route :: user.js ' + errLog);
                    buildResponse(res, 500, {
                        error: true,
                        message: "server error"
                    });
                });
        }
    });
});

router.get('/getLog_progress', myLogger, function (req, res, next) {
    var log_progress = new Log_progress();

    log_progress.getLog_progress().then((statusData) => {
        
        if (statusData["data"] == '0') {
            buildResponse(res, 200, {
                error: false,
                message: statusData["errMsg"],

            });
        } else {
            buildResponse(res, 200, {
                error: false,
                message: "Successfully display!",
                statusData: statusData
            });
        }

    });

});

router.post('/deleteLog_progress', myLogger, function (req, res, next) {
    var log_progress = new Log_progress();
    var log_progress_Data={
        log_progress_id:req.body.log_progress_id,
        deleted_by:req.body.deleted_by
    };
    console.log("+++++++++++++++++++++++",log_progress_Data);
    //var reqData = req.body.status_id;
    log_progress.deleteLog_progress(log_progress_Data)
        .then((statusData) => {
            buildResponse(res, 200, {
                error: false,
                message: 'Status Deleted successfully',
                statusData: statusData
            });
        }).catch((errLog) => {
            log.addLog('route :: log_progress.js ' + errLog);
            buildResponse(res, 404, {
                error: false,
                message: 'Please Try Again',
            });
        });
});

function buildResponse(res, statusCode, responseData) {
    res.status(statusCode).json(responseData);
}

module.exports = router;