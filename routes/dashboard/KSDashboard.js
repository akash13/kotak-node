var express = require('express');
var router = express.Router();
var bcrypt = require('bcryptjs');
var jwt = require('jsonwebtoken');
const Joi = require('joi');
var KSDashboard = require('../../model/KSDashboard');
var path = require('path')
var myLogger = require('../../middleware.js');

   
router.get('/getDashboardUserDept/:userid',myLogger, function (req, res, next) {
    var request = new KSDashboard();
    var userid = req.params.userid;
   

//console.log("userid",userid);
  var totalcount=0;
 
    request.getDashboardUserDept(userid).then((userData) => {

        buildResponse(res, 200, {
            error: false,
            message: 'Forms Fetched successfully',
          responseData:userData
           
        });
    });



});

router.get('/getDashboardTotalCount/:tableName/:userid/:group/:dept',myLogger, function (req, res, next) {
    var request = new KSDashboard();
var tablenameval = req.params.tableName;
console.log("in route",tablenameval);
var userid= req.params.userid;
var group=req.params.group;
var dept=req.params.dept;
 
  var totalcount=0;
//    for (var i=0;i< req.body.length;i++)
//    {
    
    request.getDashboardTotalCount(tablenameval,userid,group,dept).then((countData) => {

  //      console.log("countdata=",countData)
        buildResponse(res, 200, {
            error: false,
            message: 'Forms Fetched successfully',
            listData:countData,
            countData: countData.length
           
        });
    });

  //}

});


router.get('/getDashboardPendingCount/:tableName/:columnname/:userid/:group/:dept',myLogger, function (req, res, next) {
    var request = new KSDashboard();
     var tablenameval = req.params.tableName;
    var colomname=req.params.columnname;
    var userid= req.params.userid;
var group=req.params.group;
var dept=req.params.dept;

//console.log("tablenameval",tablenameval);
  var totalcount=0;
    request.getDashboardPendingCount(tablenameval,colomname,userid,group,dept).then((countData) => {

        buildResponse(res, 200, {
            error: false,
            message: 'Forms Fetched successfully',
            listData:countData,
            countData: countData.length
           
        });
    });



});


router.get('/getDashboardCompletedCount/:tableName/:columnname/:userid/:group/:dept',myLogger, function (req, res, next) {
    var request = new KSDashboard();
    var tablenameval = req.params.tableName;
    var colomname=req.params.columnname;
    var userid= req.params.userid;
var group=req.params.group;
var dept=req.params.dept;

  var totalcount=0;
    request.getDashboardCompletedCount(tablenameval,colomname,userid,group,dept).then((countData) => {

        buildResponse(res, 200, {
            error: false,
            message: 'Forms Fetched successfully',
            listData:countData,
            countData: countData.length
            

        });
    });



});

router.get('/getDashboardHoldCount/:tableName/:columnname/:userid/:group/:dept',myLogger, function (req, res, next) {
    var request = new KSDashboard();
    var tablenameval = req.params.tableName;
    var colomname=req.params.columnname;
    var userid= req.params.userid;
var group=req.params.group;
var dept=req.params.dept;

  var totalcount=0;
    request.getDashboardHoldCount(tablenameval,colomname,userid,group,dept).then((countData) => {

        buildResponse(res, 200, {
            error: false,
            message: 'Forms Fetched successfully',
            listData:countData,
            countData: countData.length
           
        });
    });

 

});

router.get('/getDashboardIssueCount/:tableName/:columnname/:userid/:group/:dept',myLogger, function (req, res, next) {
    var request = new KSDashboard();
    var tablenameval = req.params.tableName;
    var colomname=req.params.columnname;
    console.log("colomname=",colomname);var userid= req.params.userid;
    var group=req.params.group;
    var dept=req.params.dept;

  var totalcount=0;
    request.getDashboardIssueCount(tablenameval,colomname,userid,group,dept).then((countData) => {

        buildResponse(res, 200, {
            error: false,
            message: 'Forms Fetched successfully',
            listData:countData,
            countData: countData.length
           
        });
    });

 

});

router.get('/getDashboardYTSCount/:tableName/:columnname/:userid/:group/:dept',myLogger, function (req, res, next) {
    var request = new KSDashboard();
    var tablenameval = req.params.tableName;
    var colomname=req.params.columnname;
    var userid= req.params.userid;
var group=req.params.group;
var dept=req.params.dept;

  var totalcount=0;
    request.getDashboardYTSCount(tablenameval,colomname,userid,group,dept).then((countData) => {

        buildResponse(res, 200, {
            error: false,
            message: 'Forms Fetched successfully',
            listData:countData,
            countData: countData.length
           
        });
    });

 

});



router.get('/getDashboardRejectedCount/:tableName/:columnname/:userid/:group/:dept',myLogger, function (req, res, next) {
    var request = new KSDashboard();
    var tablenameval = req.params.tableName;
    var colomname=req.params.columnname;
    var userid= req.params.userid;
    var group=req.params.group;
    var dept=req.params.dept;
    var totalcount = 0;
    request.getDashboardRejectedCount(tablenameval,colomname,userid,group,dept).then((countData) => {

        buildResponse(res, 200, {
            error: false,
            message: 'Forms Fetched successfully',
            listData: countData,
            countData: countData.length

        });
    });



});

function buildResponse(res, statusCode, responseData) {
    res.status(statusCode).json(responseData);
}

module.exports = router;
