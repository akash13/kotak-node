var express = require('express');
var router = express.Router();
var bcrypt = require('bcryptjs');
var jwt = require('jsonwebtoken');
const Joi = require('joi');
var ChannelDesignationUserLevelSetupMaster = require('../model/channel_Designation_UserLevel_Setup');

var myLogger = require('../middleware.js');
var MyLogModel = require('../model/myLog');
var log = new MyLogModel();





const schema = Joi.object().keys({
    Channel_Code: Joi.string().max(20).required().allow(''),
    Sub_Channel_Code: Joi.string().max(20).required().allow(''),
    Designation_Code: Joi.string().max(20).required().allow(''),
    User_Level: Joi.string().max(3).required().allow(''),
    Business_Login: Joi.string().max(2).required().allow(''),
    ETLI_Employee: Joi.string().max(2).required().allow(''),
    DisplayDesignation: Joi.string().max(50).required().allow(''),
    DisplayDesignationDescription: Joi.string().max(50).required().allow(''),
});

const schema1 = Joi.object().keys({
    Channel_Code: Joi.string().max(20).required().allow(''),
    Sub_Channel_Code: Joi.string().max(20).required().allow(''),
    Designation_Code: Joi.string().max(20).required().allow(''),
    User_Level: Joi.string().max(3).required().allow(''),
    Business_Login: Joi.string().max(2).required().allow(''),
    ETLI_Employee: Joi.string().max(2).required().allow(''),
    DisplayDesignation: Joi.string().max(50).required().allow(''),
    DisplayDesignationDescription: Joi.string().max(50).required().allow(''),
    Txn_ID : Joi.number().required().allow(''),
});


router.post('/saveChannelDesignationUserLevelSetup', function (req, res, next) {
    var Channel_Designation_UserLevel_Setup_master = new ChannelDesignationUserLevelSetupMaster();
    var ChannelDesignationUserLevelSetupmasterData = {
        Channel_Code: req.body.Channel_Code,
        Sub_Channel_Code: req.body.Sub_Channel_Code,
        Designation_Code: req.body.Designation_Code,
        User_Level: req.body.User_Level,
        Business_Login: req.body.Business_Login,
        ETLI_Employee: req.body.ETLI_Employee,
        DisplayDesignation: req.body.DisplayDesignation,
        DisplayDesignationDescription: req.body.DisplayDesignationDescription,



    };

    Joi.validate({
        Channel_Code: req.body.Channel_Code,
        Sub_Channel_Code: req.body.Sub_Channel_Code,
        Designation_Code: req.body.Designation_Code,
        User_Level: req.body.User_Level,
        Business_Login: req.body.Business_Login,
        ETLI_Employee: req.body.ETLI_Employee,
        DisplayDesignation: req.body.DisplayDesignation,
        DisplayDesignationDescription: req.body.DisplayDesignationDescription,
    }, schema, function (err, schemeChannelDesignationUserLevelSetupmasterData) {
        if (err) {
            
            buildResponse(res, 200, {
                error: true,
                message: err["message"]
            });
        }
        else {
            Channel_Designation_UserLevel_Setup_master.saveChannelDesignationUserLevelSetup(ChannelDesignationUserLevelSetupmasterData)
                .then((data) => {
                   
                    if (data["data"] == 0) {
                        buildResponse(res, 200, {
                            error: true,
                            message: data["errMsg"]
                        });
                    } else {
                        buildResponse(res, 200, {
                            error: false,
                            message: "Stored Successfully"
                        });

                    }

                }).catch((errLog) => {

                    log.addLog('route :: Channel_Designation_UserLevel_Setup.js ' + errLog);
                    buildResponse(res, 500, {
                        error: true,
                        message: "server error"
                    });
                });
        }
    });
});

router.post('/updateChannelDesignationUserLevelSetup', function (req, res, next) {
    var Channel_Designation_UserLevel_Setup_master = new ChannelDesignationUserLevelSetupMaster();
    var ChannelDesignationUserLevelSetupmasterData = {
        Channel_Code: req.body.Channel_Code,
        Sub_Channel_Code: req.body.Sub_Channel_Code,
        Designation_Code: req.body.Designation_Code,
        User_Level: req.body.User_Level,
        Business_Login: req.body.Business_Login,
        ETLI_Employee: req.body.ETLI_Employee,
        DisplayDesignation: req.body.DisplayDesignation,
        DisplayDesignationDescription: req.body.DisplayDesignationDescription,
        Txn_ID: req.body.Txn_ID



    };


    Joi.validate({
        Channel_Code: req.body.Channel_Code,
        Sub_Channel_Code: req.body.Sub_Channel_Code,
        Designation_Code: req.body.Designation_Code,
        User_Level: req.body.User_Level,
        Business_Login: req.body.Business_Login,
        ETLI_Employee: req.body.ETLI_Employee,
        DisplayDesignation: req.body.DisplayDesignation,
        DisplayDesignationDescription: req.body.DisplayDesignationDescription,
        Txn_ID: req.body.Txn_ID,


    }, schema1, function (err, schemeChannelDesignationUserLevelSetupmasterData) {
        if (err) {

            buildResponse(res, 200, {
                error: true,
                message: err["message"]
            });
        }
        else {
            Channel_Designation_UserLevel_Setup_master.updateChannelDesignationUserLevelSetup(ChannelDesignationUserLevelSetupmasterData)
                .then((data) => {
                    
                    if (data["data"] == 0) {
                        buildResponse(res, 200, {
                            error: true,
                            message: data["errMsg"]
                        });
                    } else {
                        buildResponse(res, 200, {
                            error: false,
                            message: "Stored Successfully"
                        });

                    }

                }).catch((errLog) => {

                    log.addLog('route :: Channel_Designation_UserLevel_Setup.js ' + errLog);
                    buildResponse(res, 500, {
                        error: true,
                        message: "server error"
                    });
                });
        }
    });
});

router.post('/getChannelDesignationUserLevelSetup', function (req, res, next) {
    var Channel_Designation_UserLevel_Setup_master = new ChannelDesignationUserLevelSetupMaster();

    Channel_Designation_UserLevel_Setup_master.getChannelDesignationUserLevelSetup().then((ChannelDesignationUserLevelSetupmasterData) => {

        if (ChannelDesignationUserLevelSetupmasterData["data"] == '0') {
            buildResponse(res, 200, {
                error: false,
                message: ChannelDesignationUserLevelSetupmasterData["errMsg"],

            });
        } else {
            buildResponse(res, 200, {
                error: false,
                message: "Successfully display!",
                ChannelDesignationUserLevelSetupmasterData: ChannelDesignationUserLevelSetupmasterData
            });
        }

    });

});

router.post('/getChannelDesignationUserLevelSetupById', function (req, res, next) {
    var Channel_Designation_UserLevel_Setup_master = new ChannelDesignationUserLevelSetupMaster();
    var Txn_ID = req.body.Txn_ID;
    Channel_Designation_UserLevel_Setup_master.getChannelDesignationUserLevelSetupById(Txn_ID).then((ChannelDesignationUserLevelSetupmasterData) => {
        
        if (ChannelDesignationUserLevelSetupmasterData["data"] == '0') {
            buildResponse(res, 200, {
                error: false,
                message: ChannelDesignationUserLevelSetupmasterData["errMsg"],

            });
        } else {
            buildResponse(res, 200, {
                error: false,
                message: "Successfully display!",
                ChannelDesignationUserLevelSetupmasterData: ChannelDesignationUserLevelSetupmasterData
            });
        }

    });

});



function buildResponse(res, statusCode, responseData) {
    res.status(statusCode).json(responseData);
}

module.exports = router;