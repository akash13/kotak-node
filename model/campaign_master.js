const Sequelize = require('sequelize');
var con = require('./connection');
var randomstring = require("randomstring");
var bcrypt = require('bcryptjs');
var MyLogModel = require('../model/myLog');
var log = new MyLogModel();
const Op = Sequelize.Op;
var errMsg = "Server issue please try after sometime";
module.exports = class campaign_master {

    constructor() {


        this.campaign_master = con.define('campaign_master', {
            Txn_ID: {
                type: Sequelize.INTEGER,
                primaryKey: true,
                autoIncrement: true
            },

            Added_By: {
                type: Sequelize.STRING(20),
                allowNull: true
               
            },
            Create_Date: {
                type: Sequelize.DATE,
                allowNull: true

            },

            Modified_By: {
                type: Sequelize.STRING(20),
                allowNull: true
            },

            Modified_Date: {
                type: Sequelize.DATE,
                allowNull: true
            },
            Channel_Code: {
                type: Sequelize.STRING(20),
                allowNull: true,
                primaryKey: true,
            },
            SubChannel_Code: {
                type: Sequelize.STRING(20),
                allowNull: true
            },
            Campaign_Code: {
                type: Sequelize.STRING(20),
                allowNull: false
            },
            Campaign_Name: {
                type: Sequelize.STRING(100),
                allowNull: true
            },
            Campaign_Desc: {
                type: Sequelize.STRING(150),
                allowNull: true
            },
            Campaign_Remarks: {
                type: Sequelize.STRING(300),
                allowNull: true
            },
            Campaign_Start_Date: {
                type: Sequelize.DATE,
                allowNull: true
            },
            Campaign_End_Date: {
                type: Sequelize.DATE,
                allowNull: true
            },
            Campaign_Status: {
                type: Sequelize.STRING(2),
                allowNull: true
            },

        });
    }

    saveCampaign(campaignData) {
        return sequelize.sync().then(() => {
            return this.campaign_master.create({

                Added_By: campaignData.Added_By,
                Create_Date: campaignData.Create_Date,
               
                Channel_Code: campaignData.Channel_Code,
                SubChannel_Code: campaignData.SubChannel_Code,
                Campaign_Code: campaignData.Campaign_Code,
                Campaign_Name: campaignData.Campaign_Name,
                Campaign_Desc: campaignData.Campaign_Desc,
                Campaign_Remarks: campaignData.Campaign_Remarks,
                Campaign_Remarks: campaignData.Campaign_Remarks,
                Campaign_Start_Date: campaignData.Campaign_Start_Date,
                Campaign_End_Date: campaignData.Campaign_End_Date,
                Campaign_Status: campaignData.Campaign_Status


            }).catch(function (errLog) {
                console.log("errMsg:", errLog)
                log.addLog('model :: campaign_master.js ' + errLog);
                var err;
                if (errLog == "SequelizeUniqueConstraintError: Validation error") {
                    err = {
                        data: 0,
                        errMsg: "Department name should be unique"
                    }
                } else if (errLog != "SequelizeUniqueConstraintError: Validation error") {
                    err = {
                        data: 0,
                        errMsg: errLog["errors"][0]["message"]
                    }

                }
                return err;
            });
        }).catch(function (errLog) {
            log.addLog('model :: campaign_master.js ' + errLog);
        });
    }

    updateCampaign(campaignData) {
        return sequelize.sync().then(() => {
            return this.campaign_master.update({

              
                Modified_By: campaignData.Modified_By,
                Modified_Date: campaignData.Modified_Date,
                Channel_Code: campaignData.Channel_Code,
                SubChannel_Code: campaignData.SubChannel_Code,
                Campaign_Code: campaignData.Campaign_Code,
                Campaign_Name: campaignData.Campaign_Name,
                Campaign_Desc: campaignData.Campaign_Desc,
                Campaign_Remarks: campaignData.Campaign_Remarks,
                Campaign_Remarks: campaignData.Campaign_Remarks,
                Campaign_Start_Date: campaignData.Campaign_Start_Date,
                Campaign_End_Date: campaignData.Campaign_End_Date,
                Campaign_Status: campaignData.Campaign_Status


            },{
                where:{
                    Txn_ID:campaignData.Txn_ID
                }
            }).catch(function (errLog) {
                console.log("errMsg:", errLog)
                log.addLog('model :: campaign_master.js ' + errLog);
                var err;
                if (errLog == "SequelizeUniqueConstraintError: Validation error") {
                    err = {
                        data: 0,
                        errMsg: "Department name should be unique"
                    }
                } else if (errLog != "SequelizeUniqueConstraintError: Validation error") {
                    err = {
                        data: 0,
                        errMsg: errLog["errors"][0]["message"]
                    }

                }
                return err;
            });
        }).catch(function (errLog) {
            log.addLog('model :: campaign_master.js ' + errLog);
        });
    }

   
    getCampaign() {
        return sequelize.sync().then(() => {
            return this.campaign_master.all().then((data) => {
                return data
            }).catch((errLog) => {
                var err = {
                    data: 0,
                    errMsg: errMsg
                }
                log.addLog('model :: campaign_master.js 160' + errLog);
                return err;

            });
        }).catch((errLog) => {
            var err = {
                data: 0,
                errMsg: errMsg
            }
            log.addLog('model :: campaign_master.js 169 ' + errLog);
            return err;
        });
    }

    getCampaignById(id) {
        return sequelize.sync().then(() => {
            return this.campaign_master.findOne({
                where:{
                    Txn_ID:id
                }
            }).then((data) => {
                return data;
            }).catch((errLog) => {
                var err = {
                    data: 0,
                    errMsg: errMsg
                }
                log.addLog('model :: campaign_master.js 160' + errLog);
                return err;

            });
        }).catch((errLog) => {
            var err = {
                data: 0,
                errMsg: errMsg
            }
            log.addLog('model :: campaign_master.js 169 ' + errLog);
            return err;
        });
    }

};