var express = require('express');
var router = express.Router();
var FormAPI = require('../model/formAPI');
var bcrypt = require('bcryptjs');
var jwt = require('jsonwebtoken');
var myLogger = require('../middleware.js');


function buildResponse(res, statusCode, responseData) {
    res.status(statusCode).json(responseData);
}

module.exports = router;