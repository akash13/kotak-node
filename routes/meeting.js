var express = require('express');
var router = express.Router();
var bcrypt = require('bcryptjs');
var jwt = require('jsonwebtoken');
const Joi = require('joi');
var Meeting = require('../model/meeting');
var myLogger = require('../middleware.js');
var MyLogModel = require('../model/myLog');
var log = new MyLogModel();

const schema = Joi.object().keys({

    Meeting_Id: Joi.number().required().not(''),
    Meeting_Template_Id: Joi.number().required().not(''),

    Meeting_Date: Joi.string().required().not(''),

    Location: Joi.string().max(50).allow(''),
    Invitees: Joi.string().required().max(50).not(''),
    Description: Joi.string().max(255).allow(''),

    created_by: Joi.string().required().not(''),

});

const schema1 = Joi.object().keys({
    Meeting_Id: Joi.number().required().not(''),
    Meeting_Template_Id: Joi.number().required().not(''),
    For_Month_Or_Quoarter: Joi.string().required().max(20).not(''),
    Meeting_Date: Joi.string().required().not(''),
    Meeting_Start_Time: Joi.string().required().max(20).not(''),
    Meeting_End_Time: Joi.string().required().max(20).not(''),
    Location: Joi.string().max(50).allow(''),
    Invitees: Joi.string().required().max(50).not(''),
    Description: Joi.string().max(255).allow(''),

    updated_by: Joi.string().required().not(''),
});


router.post('/getNotSchuledMeeting', myLogger, function (req, res, next) {

    var request = new Meeting();
    var userData = req.body;

    var cnt = 0;
    var newData = [];
    
    var pageNumber = req.body.pageNumber;
    var NumberOfRows = 5;
    var offsetRow=0;
    var fetchRow=0;
     offsetRow = (pageNumber - 1) * 5;

     fetchRow = NumberOfRows * pageNumber;

    request.getNotSchuledMeetingForLog(userData,offsetRow,fetchRow)
        .then((logData) => {
            
            if (logData.length > 0) {
                logData.forEach(e => {

                    newData.push({
                        "meeting_id": e.T_INT_Meeting_Id,
                        "meeting_type": e.T_CHR_Meeting_Type,
                        "meeting_frequency": e.T_CHR_Frquency,
                        "meeting_short_name": e.T_CHR_Meeting_Short_Name,
                        "meeting_full_name": e.T_CHR_Meeting_Full_Name,
                        "convener_name": e.T_CHR_Convener_Name
                    });
                })
                
            }else{
                NumberOfRows=10;
                offsetRow = (pageNumber - 1) * 10;
                fetchRow = NumberOfRows * pageNumber;
            }
            
            
            request.getNotSchuledMeeting(userData,offsetRow,fetchRow)
                .then((data1) => {
                    
                    console.log("1111111111111111111yyyyyyyyyyyyyy1111111111111111111==",data1)

                    if(data1.length > 0){
                        data1.forEach(element1 => {
                            
                            request.getNotSchuledMeetingNew(element1,userData.month,userData.year)
                                .then((data2) => {
                                   
                                    if (data2.length > 0) {
                                        data2.forEach(ele => {
                                            console.log("eeeeeeeeeeeee===",ele)
                                            newData.push(ele);
                                        })
    
    
                                    }
    
                                    cnt = cnt + 1;
    
    
                                    if (cnt == data1.length) {
                                        buildResponse(res, 200, {
                                            error: false,
                                            message: "Stored Successfully",
                                            data: newData,
                                            numberOfRows:NumberOfRows*(pageNumber-1)
                                        });
                                    }
                                });
    
    
                        });
                    }else{
                        buildResponse(res, 200, {
                            error: false,
                            message: "Stored Successfully",
                            data: newData,
                            numberOfRows:NumberOfRows*(pageNumber-1)
                        });
                    }
                   



                }).catch((errLog) => {

                    log.addLog('route :: Meeting.js ' + errLog);
                    buildResponse(res, 500, {
                        error: true,
                        message: "server error"
                    });
                });

        }).catch((errLog) => {
            
            log.addLog('route :: Meeting.js ' + errLog);
            buildResponse(res, 500, {
                error: true,
                message: "server error"
            });
        });




});

router.post('/getScheduledMeeting', myLogger, function (req, res, next) {

    var request = new Meeting();
    var userData = req.body;
    var pageNumber = req.body.pageNumber;
    var NumberOfRows = 10

    var offsetRow = (pageNumber - 1) * 10;

    var fetchRow = NumberOfRows * pageNumber;

    request.getScheduledMeeting(userData,offsetRow,fetchRow)
        .then((data) => {
            buildResponse(res, 200, {
                error: false,
                message: "Stored Successfully",
                data: data,
                numberOfRows:NumberOfRows*(pageNumber-1)
            });

        }).catch((errLog) => {

            log.addLog('route :: Meeting.js ' + errLog);
            buildResponse(res, 500, {
                error: true,
                message: "server error"
            });
        });

});

router.post('/getScheduledMeetingExportToExcel', myLogger, function (req, res, next) {

    var request = new Meeting();
    var userData = req.body;
    var pageNumber = req.body.pageNumber;
    var NumberOfRows = 10

    var offsetRow = (pageNumber - 1) * 10;

    var fetchRow = NumberOfRows * pageNumber;

    request.getScheduledMeetingExportToExcel(userData,offsetRow,fetchRow)
        .then((data) => {
            buildResponse(res, 200, {
                error: false,
                message: "Stored Successfully",
                data: data
            });

        }).catch((errLog) => {

            log.addLog('route :: Meeting.js ' + errLog);
            buildResponse(res, 500, {
                error: true,
                message: "server error"
            });
        });

});

router.post('/getCompletedMeeting', myLogger, function (req, res, next) {

    var request = new Meeting();
    var userData = req.body;
    var pageNumber = req.body.pageNumber;
    var NumberOfRows = 10

    var offsetRow = (pageNumber - 1) * 10;

    var fetchRow = NumberOfRows * pageNumber;

    request.getCompletedMeeting(userData,offsetRow,fetchRow)
        .then((data) => {
            buildResponse(res, 200, {
                error: false,
                message: "Stored Successfully",
                data: data,
                numberOfRows:NumberOfRows*(pageNumber-1)
            });

        }).catch((errLog) => {

            log.addLog('route :: Meeting.js ' + errLog);
            buildResponse(res, 500, {
                error: true,
                message: "server error"
            });
        });

});


router.post('/getCompletedMeetingExportToExcel', myLogger, function (req, res, next) {

    var request = new Meeting();
    var userData = req.body;
    var pageNumber = req.body.pageNumber;
    var NumberOfRows = 10

    var offsetRow = (pageNumber - 1) * 10;

    var fetchRow = NumberOfRows * pageNumber;

    request.getCompletedMeetingExportToExcel(userData,offsetRow,fetchRow)
        .then((data) => {
            buildResponse(res, 200, {
                error: false,
                message: "Stored Successfully",
                data: data
            });

        }).catch((errLog) => {

            log.addLog('route :: Meeting.js ' + errLog);
            buildResponse(res, 500, {
                error: true,
                message: "server error"
            });
        });

});

router.post('/getOverdueMeeting', myLogger, function (req, res, next) {

    var request = new Meeting();
    var userData = req.body;

    var pageNumber = req.body.pageNumber;
    var NumberOfRows = 10

    var offsetRow = (pageNumber - 1) * 10;

    var fetchRow = NumberOfRows * pageNumber;
    request.getOverdueMeeting(userData,offsetRow,fetchRow)
        .then((data) => {
            buildResponse(res, 200, {
                error: false,
                message: "Stored Successfully",
                data: data,
                numberOfRows:NumberOfRows*(pageNumber-1)
            });

        }).catch((errLog) => {

            log.addLog('route :: Meeting.js ' + errLog);
            buildResponse(res, 500, {
                error: true,
                message: "server error"
            });
        });

});

router.post('/getOverdueMeetingExportToExcel', myLogger, function (req, res, next) {

    var request = new Meeting();
    var userData = req.body;

    var pageNumber = req.body.pageNumber;
    var NumberOfRows = 10

    var offsetRow = (pageNumber - 1) * 10;

    var fetchRow = NumberOfRows * pageNumber;
    request.getOverdueMeetingExportToExcel(userData,offsetRow,fetchRow)
        .then((data) => {
            buildResponse(res, 200, {
                error: false,
                message: "Stored Successfully",
                data: data
            });

        }).catch((errLog) => {

            log.addLog('route :: Meeting.js ' + errLog);
            buildResponse(res, 500, {
                error: true,
                message: "server error"
            });
        });

});

router.post('/getMeetingCount', myLogger, function (req, res, next) {

    var request = new Meeting();
    var userData = req.body;

    var cnt = 0;
    var newData = [];
    var notScheduleCount = 0;
    request.getNotSchuledMeetingForLogCount(userData)
        .then((logData) => {
            
            if (logData.length > 0) {

                notScheduleCount = notScheduleCount + logData.length;


            }
            request.getNotSchuledMeetingCount(userData)
                .then((data1) => {
                    
                    console.log(cnt, "==-======", data1.length)
                    if(data1.length > 0){
                        data1.forEach(element1 => {

                            request.getNotSchuledMeetingNew(element1,userData.month,userData.year)
                                .then((data2) => {
                                    
                                    if (data2.length > 0) {
    
                                        notScheduleCount = notScheduleCount + data2.length;
    
    
                                    }
    
                                    cnt = cnt + 1;
    
    
                                    if (cnt == data1.length) {
    
                                        request.getMeetingCount(userData)
                                            .then((data2) => {
                                                if(data2.length > 0){
                                                    buildResponse(res, 200, {
                                                        error: false,
                                                        message: "Stored Successfully",
                                                        notScheduledCount: notScheduleCount,
                                                        scheduledCount: data2[0]["scheduled_count"],
                                                        completedCount: data2[0]["completed_count"],
                                                        overdueCount: data2[0]["overdue_count"],
                                                    });
                                                }else{
                                                    buildResponse(res, 200, {
                                                        error: false,
                                                        message: "Stored Successfully",
                                                        notScheduledCount: notScheduleCount,
                                                        scheduledCount: 0,
                                                        completedCount: 0,
                                                        overdueCount: 0,
                                                    });
                                                }
                                                
                                            });
    
    
                                    }
                                });
    
    
                        });
                    }else{
                        request.getMeetingCount(userData)
                        .then((data2) => {
                            if(data2.length > 0){
                                buildResponse(res, 200, {
                                    error: false,
                                    message: "Stored Successfully",
                                    notScheduledCount: notScheduleCount,
                                    scheduledCount: data2[0]["scheduled_count"],
                                    completedCount: data2[0]["completed_count"],
                                    overdueCount: data2[0]["overdue_count"],
                                });
                            }else{
                                buildResponse(res, 200, {
                                    error: false,
                                    message: "Stored Successfully",
                                    notScheduledCount: notScheduleCount,
                                    scheduledCount: 0,
                                    completedCount: 0,
                                    overdueCount: 0,
                                });
                            }
                            
                        });
                    }
                   



                }).catch((errLog) => {

                    log.addLog('route :: Meeting.js ' + errLog);
                    buildResponse(res, 500, {
                        error: true,
                        message: "server error"
                    });
                });

        }).catch((errLog) => {

            log.addLog('route :: Meeting.js ' + errLog);
            buildResponse(res, 500, {
                error: true,
                message: "server error"
            });
        });




});

/* router.get('/getApplications', myLogger, function (req, res, next) {
    var request = new Meeting();

    request.getApplications().then((applicationData) => {
        buildResponse(res, 200, {
            error: false,
            message: "Successfully display!",
            ApplicationData: applicationData
        });
    });
}); */

/* router.get('/getApplication/:id', myLogger, function (req, res, next) {
    var request = new Meeting();
    var reqData = req.params.id;
    request.getApplicationById(reqData)
        .then((applicationData) => {

            buildResponse(res, 200, {
                error: false,
                message: 'Application Fetched successfully',
                ApplicationData: applicationData
            });
        }).catch((errLog) => {

            log.addLog('route :: application.js ' + errLog);
                    buildResponse(res, 500, {
                        error: true,
                        message: "server error"
                    });

           
        });
});
 */


router.post('/saveScheduleMeeting', myLogger, function (req, res, next) {

    var request = new Meeting();
    var d1 = new Date(req.body.start_time);
    //d1.setMinutes(d1.getMinutes() + 330);
    var d2 = new Date(req.body.end_time);
    //d2.setMinutes(d2.getMinutes() + 330);

    var Data = {
        Meeting_Id: req.body.meeting_id,
        Meeting_Template_Id: req.body.meeting_template,
        For_Month_Or_Quoarter: req.body.for_the_month,
        Meeting_Date: req.body.meeting_date,
        Meeting_Start_Time: d1,
        Meeting_End_Time: d2,
        Location: req.body.location,

        Invitees: req.body.invitees.toString(),
        Description: req.body.meeting_description,
        created_by: req.body.userId
    };
    Joi.validate({
        Meeting_Id: req.body.meeting_id,
        Meeting_Template_Id: req.body.meeting_template,

        Meeting_Date: req.body.meeting_date,

        Location: req.body.location,

        Invitees: req.body.invitees.toString(),
        Description: req.body.meeting_description,
        created_by: req.body.userId
    }, schema, function (err, applicationData) {
        if (err) {

            buildResponse(res, 200, {
                error: true,
                message: err["message"]
            });
        }
        else {
            
            request.saveScheduleMeeting(Data)
                .then((data) => {


                    if (data["data"] == 0) {
                        buildResponse(res, 200, {
                            error: true,
                            message: data["errMsg"]
                        });
                    } else {

                        buildResponse(res, 200, {
                            error: false,
                            message: "Stored Successfully"
                        });

                    }

                    request.deleteLogScheduleMeeting(req.body.meeting_id, data.dataValues['T_INT_Schedule_Meeting_Id']);

                }).catch((errLog) => {
                    log.addLog('route :: application.js ' + errLog);
                    buildResponse(res, 500, {
                        error: true,
                        message: "server error"
                    });
                });
        }
    })


});

router.post('/isPublish', myLogger, function (req, res, next) {

    var request = new Meeting();
  

    var Data = {
        scheduleId: req.body.scheduleId,
      
        created_by: req.body.userId
    };
            request.isPublish(Data)
                .then((data) => {


                    if (data["data"] == 0) {
                        buildResponse(res, 200, {
                            error: true,
                            message: data["errMsg"]
                        });
                    } else {

                        buildResponse(res, 200, {
                            error: false,
                            message: "Published Successfully"
                        });

                    }

                  

                }).catch((errLog) => {
                    log.addLog('route :: application.js ' + errLog);
                    buildResponse(res, 500, {
                        error: true,
                        message: "server error"
                    });
                });
        
    })






router.post('/getMeetingScheduleDetails', myLogger, function (req, res, next) {
    var request = new Meeting();

    request.getMeetingScheduleDetails(req.body).then((data) => {
        buildResponse(res, 200, {
            error: false,
            message: "Successfully display!",
            data: data
        });
    });
});

router.post('/getNotSchuledMeetingScheduler', function (req, res, next) {

    var request = new Meeting();
    var data = {
        userId: req.body.userId,
        role: req.body.role,
    };

    request.getNotSchuledMeetingScheduler(data)
        .then((data) => {

            buildResponse(res, 200, {
                error: false,
                message: "Stored Successfully",
                data: data
            });



        }).catch((errLog) => {
            log.addLog('route :: Meeting.js ' + errLog);
            buildResponse(res, 500, {
                error: true,
                message: "server error"
            });
        });



});


function buildResponse(res, statusCode, responseData) {
    res.status(statusCode).json(responseData);
}

module.exports = router;