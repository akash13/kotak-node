
import {HttpEvent, HttpHandler, HttpInterceptor, HttpRequest} from "@angular/common/http";
import {Observable} from "rxjs/Observable";
import {EmployeeService} from "./employee.service";
import {Injectable} from "@angular/core";

@Injectable()
export class EmployeeInterceptor implements HttpInterceptor{
    constructor(private employeeService: EmployeeService) {
    }
    intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        const copiedReq = req.clone({params: req.params.set('token', this.employeeService.getToken())});
        return next.handle(req);
    }
}



