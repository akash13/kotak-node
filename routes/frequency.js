var express = require('express');
var router = express.Router();
var bcrypt = require('bcryptjs');
var jwt = require('jsonwebtoken');
const Joi = require('joi');
var Frequency = require('../model/frequency');

var myLogger = require('../middleware.js');
var MyLogModel = require('../model/myLog');
var log = new MyLogModel();

const schema = Joi.object().keys({
    frequency_name: Joi.string().regex(/^[a-zA-Z ]*$/, '. Special symbols and numbers are not allow').max(20).required().not(''),
    frequency_status: Joi.string().max(10).required().not(''),
    created_by: Joi.string().allow(''),
    updated_by: Joi.string().allow(''),
    deleted_by: Joi.string().allow(''),

});

router.post('/saveFrequency', myLogger, function (req, res, next) {
    var frequency = new Frequency();
    var frequencyData = {
         frequency_name: req.body.frequency_name,
         frequency_status: req.body.frequency_status,
         created_by: req.body.created_by,
   };
   console.log("ddddddddddddddddd",frequencyData);
    Joi.validate({
        frequency_name: req.body.frequency_name,
        frequency_status: req.body.frequency_status,
        created_by: req.body.created_by,
     
     
    }, schema, function (err, frequencyData) {
        if (err) {

            buildResponse(res, 200, {
                error: true,
                message: err['details'][0]["message"]
            });
        }
        else {
            frequency.saveFrequency(frequencyData)
                .then((data) => {
                   console.log(data,"-----------")
                    if (data["data"] == 0) {
                        buildResponse(res, 200, {
                            error: true,
                            message: data["errMsg"]
                        });
                    } else {
                        buildResponse(res, 200, {
                            error: false,
                            message: "Stored Successfully"
                        });

                    }

                }).catch((errLog) => {
                   
                    log.addLog('route :: frequency.js ' + errLog);
                    buildResponse(res, 500, {
                        error: true,
                        message: "server error"
                    });
                });
        }
    });
});

router.post('/updateFrequency', myLogger, function (req, res, next) {
    console.log("ddddddddddddddddddddddddddddddd",req.body);
    var frequency = new Frequency();
    
    var frequencyData = {
        frequency_id:req.body.frequency_id,
        frequency_name: req.body.frequency_name,
        frequency_status: req.body.frequency_status,
        updated_by: req.body.updated_by,
   };
   console.log("------------------",frequencyData),
    Joi.validate({
        frequency_name: req.body.frequency_name,
        frequency_status: req.body.frequency_status,
        updated_by: req.body.updated_by,
     
     
    }, schema, function (err, schemaFrequencyData) {
        if (err) {

            buildResponse(res, 200, {
                error: true,
                message: err['details'][0]["message"]
            });
        }
        else {
            frequency.updateFrequency(frequencyData)
                .then((data) => {
                   console.log(data,"-----------")
                    if (data["data"] == 0) {
                        buildResponse(res, 200, {
                            error: true,
                            message: data["errMsg"]
                        });
                    } else {
                        buildResponse(res, 200, {
                            error: false,
                            message: "Updated Successfully"
                        });

                    }

                }).catch((errLog) => {
                   
                    log.addLog('route :: frequency.js ' + errLog);
                    buildResponse(res, 500, {
                        error: true,
                        message: "server error"
                    });
                });
        }
    });
});

router.get('/getFrequencys', myLogger, function (req, res, next) {
    var frequency = new Frequency();

    frequency.getFrequencys().then((data) => {
        
        if (data["data"] == '0') {
            buildResponse(res, 200, {
                error: false,
                message: data["errMsg"],

            });
        } else {
            buildResponse(res, 200, {
                error: false,
                message: "Successfully display!",
                data: data
            });
        }

    });

});

router.post('/deleteFrequency', myLogger, function (req, res, next) {
    var frequency = new Frequency();
    var frequencyData={
        frequency_id:req.body.frequency_id,
        deleted_by:req.body.deleted_by
    };
    console.log("111111111111111",frequencyData)
    //var reqData = req.body.frequency_id;
    frequency.deleteFrequency(frequencyData)
        .then((data) => {
            buildResponse(res, 200, {
                error: false,
                message: 'Frequency Deleted successfully',
                data: data
            });
        }).catch((errLog) => {
            log.addLog('route :: frequency.js ' + errLog);
            buildResponse(res, 404, {
                error: false,
                message: 'Please Try Again',
            });
        });
});

function buildResponse(res, statusCode, responseData) {
    res.status(statusCode).json(responseData);
}

module.exports = router;