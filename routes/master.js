var express = require('express');
var router = express.Router();
var Master = require('../model/master');

var imageData = [];


router.post('/saveAwarddata', function (req, res, next) {

    var request = new Master();
    var AWARD_ID = 0;
    var awardData = {
        awardName: req.body.award,

        awardDescription: req.body.description,
        createdby: req.body.createdBy,
        imagedata: req.body.filedata,
        today: req.body.date,
        status: req.body.status,
    };
  //  console.log("awardData1", awardData);

    request.saveData(awardData).then((PK_AWARD_return_ID) => {

       // console.log("PK_AWARD_return_ID", PK_AWARD_return_ID);
        FK_AWRD_ID = PK_AWARD_return_ID;

        //console.log("imagedata=",);
        req.body.filedata.forEach(element => {

         //   console.log("imagedata=", element);
            request.saveimagedata(element, FK_AWRD_ID);
        }
        )
        buildResponse(res, 200, {
            error: false,
            message: "Done"
        });
    }).catch((err) => {
        buildResponse(res, 200, {
            error: true,
            message: "Fail to Store"
        });
    });
});




router.post('/saveNews', function (req, res, next) {

    var request = new Master();
    var newsData = {
        news: req.body.news,


    };
    // console.log("awardData1",awardData);

    request.savnewData(newsData)
        .then((data) => {

            buildResponse(res, 200, {
                error: false,
                message: "Record Inserted Successfully"
            });

        }).catch((err) => {
            buildResponse(res, 200, {
                error: true,
                message: "Fail to Store"
            });
        });

})



router.get('/getAwardData', function (req, res, next) {
    var request = new Master();

    request.getAwardData().then((data) => {
        buildResponse(res, 200, {
            error: false,
            message: "Successfully display!",
            responseData: data
        });
         console.log("awardData=",responseData);
    });


});


router.get('/getAwardDataSignin', function (req, res, next) {
    var request = new Master();

    request.getAwardDataSignin().then((data) => {
        buildResponse(res, 200, {
            error: false,
            message: "Successfully display!",
            responseData: data
        });
        // console.log("awardData=",responseData);
    });


});
router.get('/getApplication/:id', function (req, res, next) {

    var request = new Master();
    var reqData = req.params.id;
    // console.log("reqdata",reqData);
    request.getApplicationById(reqData)
        .then((awardData) => {
            buildResponse(res, 200, {
                error: false,
                message: 'Application Fetched successfully',
                responseData: awardData


            });



        }).catch((err) => {
            buildResponse(res, 404, {
                error: false,
                message: 'Failed to Fetch',

            });
        });
    //  console.log("responce Data=",responseData);
});

router.get('/ShowAllAwards', function (req, res, next) {
    var request = new Master();
    // console.log("reqdata",reqData);
    request.getAllAwards()
        .then(awardData => {
           // console.log("awardddddd", imageData);
            buildResponse(res, 200, {
                error: false,
                message: 'All Awardsssssss Fetched successfully',
                responseData: awardData
            });
        }).catch((err) => {
            buildResponse(res, 404, {
                error: true,
                message: 'No data found',
            });
        });
    //  console.log("responce Data=",responseData);
});

router.get('/getImagesWithAward', function (req, res, next) {
    var request = new Master();
  //  console.log("reqdata", res);
    request.getAllImagesWithAwardId()
        .then(awardData => {
          //  console.log("awardddddddfdfd", imageData);
            buildResponse(res, 200, {
                error: false,
                message: 'All Awardsssssss imagessss Fetched successfully',
                responseData: awardData
            });
        }).catch((err) => {
            buildResponse(res, 404, {
                error: true,
                message: 'No data found',

            });
        });
    //  console.log("responce Data=",responseData);
});






router.get('/getNewsData/:id', function (req, res, next) {
    //console.log("inside route");
    var request = new Master();
    var reqData = req.params.id;
    // console.log("reqdata",reqData);
    request.getNewsDataById(reqData)
        .then((newsData) => {
            buildResponse(res, 200, {
                error: false,
                message: 'News Fetched successfully',
                responseData: newsData
            });
        }).catch((err) => {
            buildResponse(res, 404, {
                error: false,
                message: 'Failed to Fetch News',

            });
        });
});

router.get('/getBirthdayData', function (req, res, next) {
    console.log("inside route");
    var request = new Master();

    // console.log("reqdata",reqData);
    request.getbirthdayData()
        .then((birthdaydata) => {

            buildResponse(res, 200, {
                error: false,
                message: 'Application Fetched successfully',
                responseData: birthdaydata


            });
            // console.log("birthdaydata",birthdaydata)
        }).catch((err) => {
            buildResponse(res, 404, {
                error: false,
                message: 'Failed to Fetch',

            });
        });
});

router.post('/updateData/:id', function (req, res, next) {
    var Id = req.params.id;
    var request = new Master();

   // console.log("in router", Id);
    //console.log("in award", req.body.award);

    var awardData = {
        awardName: req.body.award,

        awardDescription: req.body.description,

        filedata: req.body.imagedata,
        Status: req.body.isActive

    };

    //console.log("imagedata=", req.body.imagedata);

    request.updateApplication(awardData, Id)
        .then((data) => {
            // request.checkfordata(Id).then((data)=>{
            //     console.log("data=",data);
            // })
            request.deleteImage(Id).then((data) => {

                req.body.imagedata.forEach(element => {

                   // console.log("imagedata=for",req.body.imagedata);
                    request.saveimagedata(element, Id);
                }
                )
            });


           
            buildResponse(res, 200, {
                error: false,
                message: "Done"
            });

            if (data == 0) {
                buildResponse(res, 200, {
                    error: true,
                    message: "Application Name should be unique"
                });
            }
            buildResponse(res, 200, {
                error: false,
                message: "Application Updated Successfully"
            });

        }).catch((err) => {
            buildResponse(res, 200, {
                error: true,
                message: "Fail to Update",
            });
        });


});


router.post('/InsertImage/:appId', function (req, res, next) {
    var Id = req.params.appId;
    var request = new Master();
   // console.log("body=",req.body);
    var imageData=req.body.imagedata
  
 

                       // console.log("imagedata=for",req.body.imagedata);
                        request.saveimagedata(imageData,Id).then((data)=>{

                        });
                        
                    buildResponse(res, 200, {
                        error: false,
                        message: "Done"
                    });

                    
                
             

});



router.get('/deleteApplication/:id', function (req, res, next) {
    var request = new Application();
    var reqData = req.params.id;
    request.deleteApplication(reqData)
        .then((applicationData) => {
            buildResponse(res, 200, {
                error: false,
                message: 'Application Deleted successfully',
                ApplicationData: applicationData
            });
        }).catch((err) => {
            buildResponse(res, 404, {
                error: false,
                message: 'Please Try Again',

            });
        });
});


router.get('/deleteAward/:id', function (req, res, next) {
    var request = new Master();
    var reqData = req.params.id;
    request.deleteAward(reqData)
        .then((awardData) => {
            buildResponse(res, 200, {
                error: false,
                message: 'Award Deleted successfully',
                awardData: awardData
            });
        }).catch((err) => {
            buildResponse(res, 404, {
                error: false,
                message: 'Please Try Again',

            });
        });
});


router.get('/deleteImage/:id', function (req, res, next) {
    var request = new Master();
    var reqData = req.params.id;
    request.deleteImage(reqData)
        .then((awardData) => {
            buildResponse(res, 200, {
                error: false,
                message: 'Award Image Deleted successfully',
                awardData: awardData
            });
        }).catch((err) => {
            buildResponse(res, 404, {
                error: false,
                message: 'Please Try Again',

            });
        });
});
function buildResponse(res, statusCode, responseData) {
    res.status(statusCode).json(responseData);
}

module.exports = router;

