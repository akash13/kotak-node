import {Component, ElementRef, ViewChild} from '@angular/core';
import {NgForm} from "@angular/forms";
import {FormService} from "../auth/form.service";
import {Router} from "@angular/router";

@Component({
    selector: 'app-formapi',
    templateUrl: './formapi.component.html'
})
export class FormAPIComponent {
   title ="formAPIComponent";
   @ViewChild('f') formFORM: NgForm;
   errorMsg: String = "";

   formAPIList = {};
    constructor(private formService: FormService, private router: Router) {}

    ngOnInit(){
        this.GetFormAPI();
    }

    GetFormAPI(){
        this.formService.displayFormAPI().subscribe(
            data => {
                if (data["error"]) {
                } else {
                   // console.log(data);
                }
                this.formAPIList= data;
            },
            (error) => console.log(error));
    }

}

/*

import { Component } from '@angular/core';
import { EmployeeService } from '../../../../services/employee.service';
import {Router} from "@angular/router";

@Component({
    selector: 'emp-display',
    templateUrl: './display.component.html'
 
})
export class DisplayComponent {

    $cnt=0;
   EmpDataList= {};
    constructor( private EmployeeService: EmployeeService, private router: Router){}
    
    ngOnInit(){
        this.GetEmployee();
    }
    

    GetEmployee(){
        this.EmployeeService.displayEmployee().subscribe(
            data => {
                if (data["error"]) {
                } else {
                    console.log(data);
                }
                this.EmpDataList= data;
               // console.log(this.EmpDataList);
            },
            (error) => console.log(error));
        
    }

    DeleteEmployee(employee){
        console.log("Emploee"+employee);
            this.EmployeeService.deleteEmployee(employee).subscribe(
               data => {
                 // refresh the list
                 this.GetEmployee();
                 return true;
               },
               error => {
                 console.error("Error deleting Employee!");
               }
            );
    }

}







*/ 