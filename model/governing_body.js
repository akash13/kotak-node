const Sequelize = require('sequelize');
var con = require('./connection');
var randomstring = require("randomstring");
var bcrypt = require('bcryptjs');
var MyLogModel = require('../model/myLog');
var log = new MyLogModel();
const Op = Sequelize.Op;
var errMsg = "Server issue please try after sometime";

Sequelize.DATE.prototype._stringify = function _stringify(date, options) {
    return this._applyTimezone(date, options).format('YYYY-MM-DD HH:mm:ss.SSS');
  };

module.exports = class Department {

    constructor() {

      
        this.GOVERNINGBODY = con.define('M_Governing_Body_Master', {
            T_INT_Governing_Body_Id: {
                type: Sequelize.INTEGER,
                primaryKey: true,
                autoIncrement: true
            },
           
            T_CHR_Governing_Body_Name: {
                type: Sequelize.STRING(20),
                allowNull: false,
                unique: true
            },
            T_CHR_Governing_Body_Status: {
                type: Sequelize.STRING(10),
                allowNull: false,
               
            },
            T_CHR_Created_By: {
                type: Sequelize.STRING(20),
                allowNull: false
            },
            T_DATE_Created_Date_Time: {
                type: Sequelize.DATE,
                allowNull: false
            },
            T_CHR_Updated_By: {
                type: Sequelize.STRING(20),
                allowNull: true
            },
            T_DATE_Updated_Date_Time: {
                type: Sequelize.DATE,
                allowNull: true
            },
            T_CHR_Isdelete: {
                type: Sequelize.STRING(2),
                allowNull: false,
                defaultValue:'0'
            },
            T_CHR_Deleted_By: {
                type: Sequelize.STRING(20),
                allowNull: true,
            },
            T_DATE_Deleted_Date_Time: {
                type: Sequelize.DATE,
                allowNull: true
            },
        },
        {
            timestamps:false,
           createdAt: false,
           UpdatedAt: false,
        });
    }

    saveGoverningBody(governingBodyData) {
        console.log("++++++++++++++++++++++++++++",governingBodyData);
        return sequelize.sync().then(() => {
            
            return this.GOVERNINGBODY.create({
              
                T_CHR_Governing_Body_Name: governingBodyData.governing_body_name,
                T_CHR_Governing_Body_Status: governingBodyData.governing_body_status,
                T_CHR_Created_By: governingBodyData.created_by,
                T_DATE_Created_Date_Time:new Date(),
            }).catch(function (errLog) {
                console.log("errMsg:", errLog)
                log.addLog('model :: governing_body.js ' + errLog);
                var err;
                if (errLog == "SequelizeUniqueConstraintError: Validation error") {
                    err = {
                        data: 0,
                        errMsg: "Governing Body name should be unique"
                    }
                } else if (errLog != "SequelizeUniqueConstraintError: Validation error") {
                    err = {
                        data: 0,
                        errMsg: errLog["errors"][0]["message"]
                    }

                }
                return err;
            });
        }).catch(function (errLog) {
            log.addLog('model :: governing_body.js ' + errLog);
        });
    }

    

    udpateGoverningBody(governingBodyData) {
        return sequelize.sync().then(() => {
            return this.GOVERNINGBODY.update({
              
                T_CHR_Governing_Body_Name: governingBodyData.governing_body_name,
                T_CHR_Governing_Body_Status: governingBodyData.governing_body_status,
                T_CHR_Updated_By: governingBodyData.updated_by,
                T_DATE_Updated_Date_Time:new Date()
                
              
            },{
                where:{
                    T_INT_Governing_Body_Id:governingBodyData.governing_body_id
                }
            }).catch(function (errLog) {
                console.log("errMsg:", errLog)
                log.addLog('model :: governing_body.js ' + errLog);
                var err;
                if (errLog == "SequelizeUniqueConstraintError: Validation error") {
                    err = {
                        data: 0,
                        errMsg: "Governing Body name should be unique"
                    }
                } else if (errLog != "SequelizeUniqueConstraintError: Validation error") {
                    err = {
                        data: 0,
                        errMsg: errLog["errors"][0]["message"]
                    }

                }
                return err;
            });
        }).catch(function (errLog) {
            log.addLog('model :: governing_body.js ' + errLog);
        });
    }

    getGoverningBody(){
        console.log("in governing body");
        return sequelize.sync().then(() => {
            return this.GOVERNINGBODY.all(
                {
                    where:{
                        T_CHR_Isdelete:'0'
                    }
                }
            ).then((data) => {
                return data
            }).catch((errLog) => {
                var err = {
                    data: 0,
                    errMsg: errMsg
                }
                log.addLog('model :: governing_body.js 154' + errLog);
                return err;
                
            });
        }).catch((errLog) => {
            var err = {
                data: 0,
                errMsg: errMsg
            }
            log.addLog('model :: governing_body.js 163 ' + errLog);
            return err;
        });
    }
   
    deleteGoverningBody(governingBodyData){
        return sequelize.sync().then(() => {
            return this.GOVERNINGBODY.update({
                T_CHR_Isdelete:'1',
                T_CHR_Deleted_By:governingBodyData.deleted_by,
                T_DATE_Deleted_Date_Time:new Date()
            },{
                where: {
                    T_INT_Governing_Body_Id: governingBodyData.governing_body_id
                }
            }).catch((errLog) => {
                var err = {
                    data: 0,
                    errMsg: errMsg
                }
                log.addLog('model :: governing_body.js 183 ' + errLog);
                return err;
            });
        });
    }

};