const Sequelize = require('sequelize');
var con = require('./connection');
var randomstring = require("randomstring");
var bcrypt = require('bcryptjs');
var MyLogModel = require('../model/myLog');
var log = new MyLogModel();
const Op = Sequelize.Op;
var errMsg = "Server issue please try after sometime";
module.exports = class Channel_Designation_UserLevel_Setup {

    constructor() {


        this.Channel_Designation_UserLevel_Setup = con.define('Channel_Designation_UserLevel_Setup', {
            Txn_ID: {
                type: Sequelize.INTEGER,
                primaryKey: true,
                autoIncrement: true
            },

            Channel_Code: {
                type: Sequelize.STRING(20),
                allowNull: true,
                primaryKey: true,
            },
            Sub_Channel_Code: {
                type: Sequelize.STRING(20),
                allowNull: true,
                primaryKey: true,
            },
            Designation_Code: {
                type: Sequelize.STRING(20),
                allowNull: true
            },
            User_Level: {
                type: Sequelize.STRING(3),
                allowNull: false
            },
            Business_Login: {
                type: Sequelize.STRING(2),
                allowNull: true
            },
            ETLI_Employee: {
                type: Sequelize.STRING(2),
                allowNull: true
            },
            DisplayDesignation: {
                type: Sequelize.STRING(50),
                allowNull: true
            },
            DisplayDesignationDescription: {
                type: Sequelize.STRING(50),
                allowNull: true
            },


        });
    }

    saveChannelDesignationUserLevelSetup(ChannelDesignationUserLevelSetupmasterData) {
        return sequelize.sync().then(() => {
            return this.Channel_Designation_UserLevel_Setup.create({

                Channel_Code: ChannelDesignationUserLevelSetupmasterData.Channel_Code,
                Sub_Channel_Code: ChannelDesignationUserLevelSetupmasterData.Sub_Channel_Code,
                Designation_Code: ChannelDesignationUserLevelSetupmasterData.Designation_Code,
                User_Level: ChannelDesignationUserLevelSetupmasterData.User_Level,
                Business_Login: ChannelDesignationUserLevelSetupmasterData.Business_Login,
                ETLI_Employee: ChannelDesignationUserLevelSetupmasterData.ETLI_Employee,
                DisplayDesignation: ChannelDesignationUserLevelSetupmasterData.DisplayDesignation,
                DisplayDesignationDescription: ChannelDesignationUserLevelSetupmasterData.DisplayDesignationDescription

            }).catch(function (errLog) {
                console.log("errMsg:", errLog)
                log.addLog('model :: Channel_Designation_UserLevel_Setup.js ' + errLog);
                var err;
                if (errLog == "SequelizeUniqueConstraintError: Validation error") {
                    err = {
                        data: 0,
                        errMsg: "menu name should be unique"
                    }
                } else if (errLog != "SequelizeUniqueConstraintError: Validation error") {
                    err = {
                        data: 0,
                        errMsg: errLog["errors"][0]["message"]
                    }

                }
                return err;
            });
        }).catch(function (errLog) {
            log.addLog('model :: Channel_Designation_UserLevel_Setup.js ' + errLog);
        });
    }

    updateChannelDesignationUserLevelSetup(ChannelDesignationUserLevelSetupmasterData) {
        return sequelize.sync().then(() => {
            return this.Channel_Designation_UserLevel_Setup.update({

                Channel_Code: ChannelDesignationUserLevelSetupmasterData.Channel_Code,
                Sub_Channel_Code: ChannelDesignationUserLevelSetupmasterData.Sub_Channel_Code,
                Designation_Code: ChannelDesignationUserLevelSetupmasterData.Designation_Code,
                User_Level: ChannelDesignationUserLevelSetupmasterData.User_Level,
                Business_Login: ChannelDesignationUserLevelSetupmasterData.Business_Login,
                ETLI_Employee: ChannelDesignationUserLevelSetupmasterData.ETLI_Employee,
                DisplayDesignation: ChannelDesignationUserLevelSetupmasterData.DisplayDesignation,
                DisplayDesignationDescription: ChannelDesignationUserLevelSetupmasterData.DisplayDesignationDescription


            }, {
                    where: {
                        Txn_ID: ChannelDesignationUserLevelSetupmasterData.Txn_ID
                    }
                }).catch(function (errLog) {
                    console.log("errMsg:", errLog)
                    log.addLog('model :: Channel_Designation_UserLevel_Setup.js ' + errLog);
                    var err;
                    if (errLog == "SequelizeUniqueConstraintError: Validation error") {
                        err = {
                            data: 0,
                            errMsg: "menu name should be unique"
                        }
                    } else if (errLog != "SequelizeUniqueConstraintError: Validation error") {
                        err = {
                            data: 0,
                            errMsg: errLog["errors"][0]["message"]
                        }

                    }
                    return err;
                });
        }).catch(function (errLog) {
            log.addLog('model :: Channel_Designation_UserLevel_Setup.js ' + errLog);
        });
    }


    getChannelDesignationUserLevelSetup() {
        return sequelize.sync().then(() => {
            return this.Channel_Designation_UserLevel_Setup.all().then((data) => {
                return data
            }).catch((errLog) => {
                var err = {
                    data: 0,
                    errMsg: errMsg
                }
                log.addLog('model :: Channel_Designation_UserLevel_Setup.js' + errLog);
                return err;

            });
        }).catch((errLog) => {
            var err = {
                data: 0,
                errMsg: errMsg
            }
            log.addLog('model :: Channel_Designation_UserLevel_Setup.js ' + errLog);
            return err;
        });
    }

    getChannelDesignationUserLevelSetupById(id) {
        return sequelize.sync().then(() => {
            return this.Channel_Designation_UserLevel_Setup.findOne({
                where: {
                    Txn_ID: id
                }
            }).then((data) => {
                return data;
            }).catch((errLog) => {
                var err = {
                    data: 0,
                    errMsg: errMsg
                }
                log.addLog('model :: Channel_Designation_UserLevel_Setup.js' + errLog);
                return err;

            });
        }).catch((errLog) => {
            var err = {
                data: 0,
                errMsg: errMsg
            }
            log.addLog('model :: Channel_Designation_UserLevel_Setup.js ' + errLog);
            return err;
        });
    }

};