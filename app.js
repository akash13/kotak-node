var express = require('express');
// const port = process.env.PORT;
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
const Sequelize = require('sequelize');
var con = require('./model/connection');
var appRoutes = require('./routes/app');

var formsRoutes = require('./routes/forms');
var applicationRoutes = require('./routes/application');

var userRoutes = require('./routes/user');
var deptRoutes=require('./routes/department');
var frequecnyRoutes=require('./routes/frequency');
var roleRoutes=require('./routes/role');
var statusRoutes=require('./routes/status');
var loginRoutes = require('./routes/login');


var app = express();
var dbConnection = require('./routes/dbConnection');
///////////////////////////////////////////
var campaignMasterRoutes=require('./routes/campaign_master');
var menuMasterRoutes=require('./routes/menu');
var channelDesignationUserLevelRoutes=require('./routes/channel_Designation_UserLevel_Setup');
var groupMenuMappingRoutes=require('./routes/group_Menu_Mapping');
var employeeBranchMappingRoutes=require('./routes/employee_Branch_Mapping');
var GOVERNINGBODY=require('./routes/governing_body');
var SCHEDULEMEETINGS=require('./routes/schedule_meetings');
var LOGPROGRESS=require('./routes/log_progress');
var MEETINGTASK=require('./routes/meeting_task');
var meetingRoutes=require('./routes/meeting');

////////////////////////
// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'hbs');

// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger('dev'));
var bodyParser = require('body-parser');
app.use(bodyParser.json({ limit: '100mb' }));
app.use(bodyParser.urlencoded({ limit: '100mb', extended: true }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));







app.use(function (req, res, next) {
  //  // res.setHeader('Access-Control-Allow-Origin', 'http://afapdev.kalyanicorp.com');
  //   res.setHeader('Access-Control-Allow-Origin', '*');
  //   //res.setHeader('Access-Control-Allow-Credentials', true);
  //   res.setHeader('Access-Control-Allow-Headers', 'someHeader,x-access-token,Origin, X-Requested-With, Content-Type, Accept, Content-Disposition');
  //   res.setHeader('Access-Control-Allow-Methods', 'POST, GET, PATCH, DELETE, OPTIONS');
  //   next();

  //res.setHeader('Access-Control-Allow-Origin', 'http://afapdev.kalyanicorp.com');
res.setHeader('Access-Control-Allow-Origin', '*');
  res.setHeader('Access-Control-Allow-Credentials', true);
  res.setHeader('Access-Control-Allow-Headers', 'x-access-token,Origin, X-Requested-With, Content-Type, Accept, Content-Disposition');
  //  res.setHeader('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept, Content-Disposition');
  res.setHeader('Access-Control-Allow-Methods', 'POST, GET, PATCH, DELETE, OPTIONS');
  next();
});

app.use(function (req, res, next) {
  res.header('Cache-Control', 'private, no-cache, no-store, must-revalidate');
  res.header('Expires', '-1');
  res.header('Pragma', 'no-cache');
  next();
});


app.use('/api/', appRoutes);
app.use('/api/uploads', express.static('uploads'));
app.use('/api/form', formsRoutes);
app.use('/api/application', applicationRoutes);

app.use('/api/user', userRoutes);
app.use('/api/department', deptRoutes);
app.use('/api/frequecny', frequecnyRoutes);
app.use('/api/role', roleRoutes);
app.use('/api/status', statusRoutes);

app.use('/api/login', loginRoutes);
app.use('/api/dbConnection', dbConnection)
////////////////////////////

app.use('/api/governigBody', GOVERNINGBODY);
app.use('/api/scheduleMeetings', SCHEDULEMEETINGS);
app.use('/api/logprogress', LOGPROGRESS);
app.use('/api/meetingtask', MEETINGTASK);
app.use('/api/meeting', meetingRoutes);

/////////////////////////

app.use('/api/campaignMaster', campaignMasterRoutes);
app.use('/api/menuMaster', menuMasterRoutes);
app.use('/api/channelDesignationUserLevelSetupMaster', channelDesignationUserLevelRoutes);
app.use('/api/groupMenuMappingMaster', groupMenuMappingRoutes);
app.use('/api/employeeBranchMappingMaster', employeeBranchMappingRoutes);


////////////////////
// // catch 404 and forward to error handler
// app.use(function (req, res, next) {
//     return res.render('index');
// });


//The 404 Route (ALWAYS Keep this as the last route)
app.use('*', function (req, res) {
  //  res.send('what???', 404);
  return res.render('index');
  // res.status(404).render('404_error_template', {title: "Sorry, page not found"});
});
// app.listen(port);
module.exports = app;
