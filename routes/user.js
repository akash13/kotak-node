var express = require('express');
var router = express.Router();
var bcrypt = require('bcryptjs');
var jwt = require('jsonwebtoken');
const Joi = require('joi');
var User = require('../model/user');

var myLogger = require('../middleware.js');
var MyLogModel = require('../model/myLog');
var log = new MyLogModel();

const schema = Joi.object().keys({
    empId: Joi.string().max(255).required().not(''),
    empName: Joi.string().max(255).required().not(''),
    empEmail: Joi.string().email({ minDomainAtoms: 2 }),
    empContact: Joi.string().min(10).max(12).required().not(''),
    empTelephone: Joi.string().max(20).allow(''),
    empDepartment: Joi.number().not(''),
    empRole: Joi.not(''),
    empPassword: Joi.required().not(''),
    empStatus: Joi.string().max(10).required().not(''),
    empDesignation: Joi.string().max(20).allow(''),
    createdBy: Joi.string().allow(''),
    emp_authentication_type: Joi.string().allow(''),

});

const schema1 = Joi.object().keys({
    empId: Joi.string().max(255).required().not(''),
    empName: Joi.string().max(255).required().not(''),
    empEmail: Joi.string().email({ minDomainAtoms: 2 }),
    empContact: Joi.string().min(10).max(12).required().not(''),
    empTelephone: Joi.string().max(20).allow(''),
    empDepartment: Joi.number().not(''),
    empRole: Joi.not(''),
    empStatus: Joi.string().max(10).required().not(''),
    empDesignation: Joi.string().max(20).allow(''),
    updatedBy: Joi.string().allow(''),
    emp_authentication_type: Joi.string().allow(''),

});


router.post('/saveUser', myLogger, function (req, res, next) {
    var user = new User();
    var userData = {
        empId: req.body.emp_id,
        empName: req.body.emp_name,
        empEmail: req.body.emp_email,
        empContact: req.body.emp_contact,
        empPassword: req.body.emp_password,
        empTelephone: req.body.emp_telephone,
        empStatus: req.body.emp_status,
        empRole: req.body.emp_role,
        empDesignation: req.body.emp_designation,
        empDepartment: req.body.emp_department,
        createdBy: req.body.created_by,
        emp_authentication_type: req.body.emp_authentication_type

    };
    console.log("data", userData)
    var bcryptPassword = bcrypt.hashSync(req.body.emp_password);


    Joi.validate({
        empId: req.body.emp_id,
        empName: req.body.emp_name,
        empEmail: req.body.emp_email,
        empContact: req.body.emp_contact,
        empPassword: bcryptPassword,
        empTelephone: req.body.emp_telephone,
        empStatus: req.body.emp_status,
        empRole: req.body.emp_role,
        empDesignation: req.body.emp_designation,
        empDepartment: req.body.emp_department,
        createdBy: req.body.created_by,
        emp_authentication_type: req.body.emp_authentication_type


    }, schema, function (err, userData) {
        if (err) {
            console.log("errr", err)
            buildResponse(res, 200, {
                error: true,
                message: err['details'][0]["message"]
            });
        }
        else {
            user.saveUser(userData)
                .then((data) => {

                    if (data["data"] == 0) {
                        buildResponse(res, 200, {
                            error: true,
                            message: data["errMsg"]
                        });
                    } else {
                        buildResponse(res, 200, {
                            error: false,
                            message: "Stored Successfully"
                        });

                    }

                }).catch((errLog) => {

                    log.addLog('route :: user.js ' + errLog);
                    buildResponse(res, 500, {
                        error: true,
                        message: "server error"
                    });
                });
        }
    });
});

router.get('/getUsers', myLogger, function (req, res, next) {
    var user = new User();

    user.getUsers().then((userData) => {

        if (userData["data"] == '0') {
            buildResponse(res, 200, {
                error: false,
                message: userData["errMsg"],

            });
        } else {
            buildResponse(res, 200, {
                error: false,
                message: "Successfully display!",
                UserData: userData
            });
        }

    });

});


router.get('/getInviteesUsers', myLogger, function (req, res, next) {
    var user = new User();

    user.getInviteesUsers().then((userData) => {

        if (userData["data"] == '0') {
            buildResponse(res, 200, {
                error: false,
                message: userData["errMsg"],

            });
        } else {
            buildResponse(res, 200, {
                error: false,
                message: "Successfully display!",
                UserData: userData
            });
        }

    });

});

router.post('/getFilterInviteesUsers', myLogger, function (req, res, next) {
    var user = new User();

    user.getFilterInviteesUsers(req.body).then((userData) => {

        if (userData["data"] == '0') {
            buildResponse(res, 200, {
                error: false,
                message: userData["errMsg"],

            });
        } else {
            buildResponse(res, 200, {
                error: false,
                message: "Successfully display!",
                UserData: userData
            });
        }

    });

});

router.post('/getPermanentUsers', myLogger, function (req, res, next) {
    var user = new User();
console.log("rrrrrrrrrrrrrr=",req.body);
    user.getPermanentUsers(req.body).then((userData) => {

        if (userData["data"] == '0') {
            buildResponse(res, 200, {
                error: false,
                message: userData["errMsg"],

            });
        } else {
            buildResponse(res, 200, {
                error: false,
                message: "Successfully display!",
                UserData: userData
            });
        }

    });

});

router.post('/getSelectedInviteesUsers', myLogger, function (req, res, next) {
    var user = new User();
console.log("rrrrrrrrrrrrrr=",req.body);
    user.getSelectedInviteesUsers(req.body).then((userData) => {

        if (userData["data"] == '0') {
            buildResponse(res, 200, {
                error: false,
                message: userData["errMsg"],

            });
        } else {
            buildResponse(res, 200, {
                error: false,
                message: "Successfully display!",
                UserData: userData
            });
        }

    });

});

router.post('/getSelectedChairUsers', myLogger, function (req, res, next) {
    var user = new User();
console.log("rrrrrrrrrrrrrr=",req.body);
    user.getSelectedChairUsers(req.body).then((userData) => {

        if (userData["data"] == '0') {
            buildResponse(res, 200, {
                error: false,
                message: userData["errMsg"],

            });
        } else {
            buildResponse(res, 200, {
                error: false,
                message: "Successfully display!",
                UserData: userData
            });
        }

    });

});

router.get('/getUser/:user_id', myLogger, function (req, res, next) {

    var user = new User();
    var reqData = req.params.user_id;
    user.getUserById(reqData)
        .then((data) => {

            if (data["data"] == '0') {
                buildResponse(res, 200, {
                    error: false,
                    message: data["errMsg"],

                });
            } else {
                buildResponse(res, 200, {
                    error: false,
                    message: "Successfully display!",
                    data: data
                });
            }
        }).catch((errLog) => {
            log.addLog('route :: user.js ' + errLog);
            buildResponse(res, 404, {
                error: false,
                message: 'server error',
            });
        });
});

router.get('/getUserByEmpId/:user_id', myLogger, function (req, res, next) {

    var user = new User();
    var reqData = req.params.user_id;
    user.getUserByEmpId(reqData)
        .then((userData) => {
            buildResponse(res, 200, {
                error: false,
                message: 'User Details Fetched successfully',
                UserData: userData
            });
        }).catch((err) => {
            buildResponse(res, 404, {
                error: false,
                message: 'server error',
            });
        });
});

router.post('/updateUser/:user_id', myLogger, function (req, res, next) {
    var userId = req.params.user_id;
    var user = new User();
    //console.log("emp_accountexpires:req.body.emp_accountexpires",req.body.emp_accountexpires);
    var userData = {
        empId: req.body.emp_id,
        empName: req.body.emp_name,
        empEmail: req.body.emp_email,
        empContact: req.body.emp_contact,
        empTelephone: req.body.emp_telephone,
        empStatus: req.body.emp_status,
        empRole: req.body.emp_role,
        empDesignation: req.body.emp_designation,
        empDepartment: req.body.emp_department,
        updatedBy: req.body.updated_by,
        emp_authentication_type: req.body.emp_authentication_type
    };
    var t = "";

    if (req.body.emp_telephone) {
        t = req.body.emp_telephone;
    }

    var d = "";

    if (req.body.emp_designation) {
        d = req.body.emp_designation;
    }

    var dept = "";
    if (req.body.emp_department) {
        dept = req.body.emp_department;
    }
    dept = req.body.emp_department;

    Joi.validate({
        empId: req.body.emp_id,
        empName: req.body.emp_name,
        empEmail: req.body.emp_email,
        empContact: req.body.emp_contact,
        empTelephone: t,
        empStatus: req.body.emp_status,
        empRole: req.body.emp_role,
        empDesignation: d,
        empDepartment: dept,
        updatedBy: req.body.updated_by,
        emp_authentication_type: req.body.emp_authentication_type

    }, schema1, function (err, userData) {
        if (err) {
            buildResponse(res, 200, {
                error: true,
                message: err['details'][0]["message"]
            });
        }
        else {
            user.updateUser(userData, userId)
                .then((data) => {
                    if (data["data"] == 0) {
                        buildResponse(res, 200, {
                            error: true,
                            message: data["errMsg"]
                        });
                    } else {
                        buildResponse(res, 200, {
                            error: false,
                            message: "Updated Successfully"
                        });

                    }
                }).catch((errLog) => {
                    log.addLog('route :: user.js ' + errLog);
                    buildResponse(res, 500, {
                        error: true,
                        message: "server error",
                    });
                });
        }
    });
});

router.get('/deleteUser/:id', myLogger, function (req, res, next) {
    // router.post('/deleteUser/:id', myLogger, function (req, res, next) {
    console.log("THIS FOR DELETE", req.params.id)
    var user = new User();
    var reqData = req.params.id;
    user.deleteUser(reqData)
        .then((userData) => {
            buildResponse(res, 200, {
                error: false,
                message: 'User Deleted successfully',
                UserData: userData
            });
        }).catch((errLog) => {
            log.addLog('route :: user.js ' + errLog);
            buildResponse(res, 404, {
                error: false,
                message: 'Please Try Again',
            });
        });
});


router.get('/getEmpRole', myLogger, function (req, res, next) {
    var user = new User();

    user.getEmpRole().then((userData) => {

        if (userData["data"] == '0') {
            buildResponse(res, 200, {
                error: false,
                message: userData["errMsg"],

            });
        } else {
            buildResponse(res, 200, {
                error: false,
                message: "Successfully display!",
                UserData: userData
            });
        }

    });

});
function buildResponse(res, statusCode, responseData) {
    res.status(statusCode).json(responseData);
}

module.exports = router;