var express = require('express');
var router = express.Router();
var bcrypt = require('bcryptjs');
var jwt = require('jsonwebtoken');
const Joi = require('joi');
var AdUsers = require('../../model/AdUsers')
var path = require('path')
var myLogger = require('../../middleware.js');
   
router.get('/getUserAndCompany',myLogger, function (req, res, next) {
    var request = new AdUsers();
  var totalcount=0;
    request.getUserAndCompany().then((userData) => {

        buildResponse(res, 200, {
            error: false,
            message: 'Forms Fetched successfully',
          responseData:userData
           
        });
    });

});
router.get('/getUsersEmail/:userId',myLogger, function (req, res, next) {
    var request = new AdUsers();
    var reqData = req.params.userId;
    request.getUsersEmail(reqData)
        .then((userData) => {
            buildResponse(res, 200, {
                error: false,
                message: 'User Email found successfully',
                UserData: userData
            });
        }).catch((err) => {
            buildResponse(res, 404, {
                error: false,
                message: 'Please Try Again',
            });
        });
});
router.post('/sendRequestorEmail',myLogger, function (req, res, next) {
    
    var request = new AdUsers();
     console.log("body=",req.body);
     request.sendRequestorEmail(req.body);
      buildResponse(res, 200, {
             error: false,
             message: 'Email Intimation Notification Send Successfully'
         });
 });
router.get('/getCompany/:userid',myLogger, function (req, res, next) {
    var request = new AdUsers();
    var userid=req.params.userid;
  var totalcount=0;
    request.getCompany(userid).then((companyData) => {

        buildResponse(res, 200, {
            error: false,
            message: 'Forms Fetched successfully',
          responseData:companyData
           
        });
    });

});
router.get('/getCompanyEmployee/:cname',myLogger, function (req, res, next) {
    var request = new AdUsers();
    var cname=req.params.cname;
  var totalcount=0;
    request.getCompanyEmployee(cname).then((EmployeeData) => {

        buildResponse(res, 200, {
            error: false,
            message: 'Forms Fetched successfully',
          responseData:EmployeeData
           
        });
    });

});

router.get('/getEmployeeDetailsForView/:id',myLogger, function (req, res, next) {
    var request = new AdUsers();
    var id=req.params.id;
  var totalcount=0;
    request.getEmployeeDetailsForView(id).then((EmployeeData) => {

        buildResponse(res, 200, {
            error: false,
            message: 'Forms Fetched successfully',
          responseData:EmployeeData
           
        });
    });

});


router.get('/getEmployeeDetails/:id',myLogger, function (req, res, next) {
    var request = new AdUsers();
    var id=req.params.id;
  var totalcount=0;
    request.getEmployeeDetails(id).then((EmployeeData) => {

        buildResponse(res, 200, {
            error: false,
            message: 'Forms Fetched successfully',
          responseData:EmployeeData
           
        });
    });

});



router.post('/saveUser',myLogger, function (req, res, next) {
    var request = new AdUsers();
    var userData = {
        user_id: req.body.user_id,
        company: req.body.company,
        created_By:req.body.created_by,
    };
    console.log("userdata",userData);
  var totalcount=0;
    request.saveUser(userData).then((userData) => {

        buildResponse(res, 200, {
            error: false,
            message: 'Stored successfully',
          responseData:userData
           
        });
    });

});

router.post('/updateUser',myLogger, function (req, res, next) {
    var request = new AdUsers();
    var userData = {
        id: req.body.id,
        user_id: req.body.user_id,
        company: req.body.company,
        updated_By:req.body.updated_by,
    };
    console.log("userdata",userData);
  var totalcount=0;
    request.updateUser(userData).then((userData) => {

        buildResponse(res, 200, {
            error: false,
            message: 'Updated successfully',
          responseData:userData
           
        });
    });


    

});



router.post('/updateUserDetails',myLogger, function (req, res, next) {
    var request = new AdUsers();
    var userData = {
        
        emp_id: req.body.user_id,
        remark: req.body.Remark,
        updated_by:req.body.created_by,
        sapid:req.body.SAPId,
        expireDate:req.body.expireDate,
        
    };
    console.log("userdata",userData);
  var totalcount=0;
    request.updateUserDetails(userData).then((userData) => {

        buildResponse(res, 200, {
            error: false,
            message: 'Updated successfully',
          responseData:userData
           
        });
    });


    

});




router.get('/deleteAdUsers/:id',myLogger, function (req, res, next) {
    var request = new AdUsers();
    var id=req.params.id
    request.deleteAdUsers(id).then(() => {

        buildResponse(res, 200, {
            error: false,
            message: 'Deleted successfully',
           
        });
    });

});




function buildResponse(res, statusCode, responseData) {
    res.status(statusCode).json(responseData);
}

module.exports = router;
