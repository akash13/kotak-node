import {HttpClient} from "@angular/common/http";
import {Injectable} from "@angular/core";
import {environment} from "../environment";

@Injectable()
export class EmployeeService {
    constructor(private httpClient: HttpClient) {
    }

    displayEmployee(){
        return this.httpClient.get(environment.apiUrl+ "getEmployees"); 
    }

    
    createEmployee(empData:{ firstname:string, lastname: string, email: string, dept:string, gender:string, roll:string, contact:string, password: string}){
         return this.httpClient.post(environment.apiUrl + "createEmployee", empData);
    }

    NewUpdateEmployee(empData:{ firstname:string, lastname: string, email: string, dept:string, gender:string, roll:string, contact:string, password: string}){
        return this.httpClient.post(environment.apiUrl + "NewUpdateEmployee", empData);
   }

    deleteEmployee(Id){
        return this.httpClient.get(environment.apiUrl + "deleteEmployee/"+Id);
    }
    
    UpdateEmployee(id){
        return this.httpClient.get(environment.apiUrl + "UpdateEmployee/"+ id);
        }
    
    ShowEmployee(id){
        return this.httpClient.get(environment.apiUrl + "ShowEmployee/" + id);
    }
        
/*
    updateEmployee(){

    }

    
    createEmployee(empData:{ firstname:string, lastname: string, email: string, dept:string, gender:string, roll:string, contact:string, password: string}){
        var str = JSON.stringify(empData);
        console.log( "before posting " + str);
         return this.httpClient.post(environment.apiUrl + "createEmployee", empData);
    }

    
    deleteEmployee(EmpId){
        return this.httpClient.post(environment.apiUrl + "deleteEmployee", EmpId);
    }


*/  
    getToken(){
        return localStorage.getItem("token") !== null?localStorage.getItem("token"):'';
    }
    
}